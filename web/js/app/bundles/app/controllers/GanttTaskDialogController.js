var GanttTaskDialogController = [
    '$scope',
    '$mdDialog',
    '$http',
    'data',
    'GanttUtilService',
    'UtilUIService',
    function(
        $scope,
        $mdDialog,
        $http,
        data,
        GanttUtilService,
        UtilUIService
    ) {
        $scope.uuis = UtilUIService;

        $scope.taskName = data.task.model.name;

        $scope.saveTask = function() {
            data.task.model.name = $scope.taskName;
            GanttUtilService.saveTask(data.task, data.ganttName, data.ganttId);
        };
        $scope.deleteTask = function() {
            GanttUtilService.deleteTask(data.task, data.ganttName, data.ganttId, data.parentScope.ganttData);
        };
}];