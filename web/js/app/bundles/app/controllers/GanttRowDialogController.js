var GanttRowDialogController = [
    '$scope',
    '$mdDialog',
    '$http',
    'data',
    'GanttUtilService',
    'UtilUIService',
    function(
        $scope,
        $mdDialog,
        $http,
        data,
        GanttUtilService,
        UtilUIService
    ) {
        $scope.uuis = UtilUIService;

        $scope.rowName = '';
        if (data.ganttRow) {
            $scope.rowName = data.ganttRow.model.name;
        }

        $scope.saveRow = function() {
            var ganttRowData = {
                id: data.ganttRow ? data.ganttRow.model.id : '',
                name: $scope.rowName,
            }
            GanttUtilService.saveRow(ganttRowData, data.ganttName, data.ganttId).then(function(response) {
                if (!data.ganttRow) { //new record.
                    data.parentScope.ganttData[data.parentScope.ganttData.length] = response.data.ganttRow;
                } else {
                    data.ganttRow.model.name = $scope.rowName;
                }
            });
        };
        $scope.deleteRow = function() {
            GanttUtilService.deleteRow(data.ganttRow, data.ganttName, data.ganttId, data.parentScope.ganttData);
        };
}];