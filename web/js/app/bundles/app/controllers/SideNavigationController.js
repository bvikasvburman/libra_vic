'use strict';

 angular.module('appBundle')
    .controller('SideNavigationController', [
        '$scope',
        '$state',
        '$http',
        'GlobalCfgService',
    function(
        $scope,
        $state,
        $http,
        GlobalCfgService
    ) {
        $scope.$state = $state;

        var topRooms = GlobalCfgService.getTopRooms();
        $scope.topRooms = topRooms;
    }
]);