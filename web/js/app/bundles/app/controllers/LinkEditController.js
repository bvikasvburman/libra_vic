'use strict';

var LinkEditController = [
    '$scope',
    'rowLnk',
    'data',
    '$injector',
    '$stateParams',
    '$rootScope',
    'UtilService',
    function(
        $scope,
        rowLnk,
        data,
        $injector,
        $stateParams,
        $rootScope,
        UtilService
    ) {
    $scope.rowLnk = rowLnk;

    //cancel / close dialog
    $scope.cancel = function() {
        data.$mdDialog.cancel();
    };
    $scope.save = function() {
        //ex: bundle = Company, link = Contact
        //To save Contact link we are using Contact repository (because it's linked with Contact entity).
        //But we use use Company's link api (api_company_contact_one) to save
        var reposLinkObj = $injector.get(data.linkObj.repository); //ex: new MediaHistoryRepository()
        var excludedFields = ['creationDate', 'modificationDate'];
        if (data.linkView == "edit") {
            var apiStateName = 'api_' + data.bundle + '_' + data.link + '_one';
            reposLinkObj.save($scope.rowLnk, {
                wrapper: data.link,
                apiStateName: apiStateName,
                idApiState: $stateParams.id,
                idlApiState: $stateParams.idl,
                excludedFields: excludedFields,
            })
            .then(function(linkRow) {
                //update the modified row model in the links data array
                var linkArrName = data.link + "Lnk";
                var linkArr = $rootScope.links[linkArrName];
                var arrInd = _.findIndex(linkArr, {id: parseInt(data.idl)});
                linkArr[arrInd] = linkRow;
                data.$mdDialog.hide();
            }, function() {
                alert('Error saving. Please try again!');
            });
        } else if (data.linkView == "new") {
            //ex: rowLnk: Contact & data.row = Company in a link Company > Contact;
            // rowLnk[company] = data.row;
            $scope.rowLnk[data.bundle] = data.row;
            var extraParams = {
                bundle: data.bundle,
                link: data.link,
                bundleEntityId: data.row.id,
            };

            //For new record we directly use link entity Contact's api (api_contact)
            reposLinkObj.save($scope.rowLnk, {
                wrapper: data.link,
                excludedFields: excludedFields,
                extraParams: extraParams,
            })
            .then(function(linkRow) {
                //insert the new link row in the links data array
                var linkArrName = data.link + "Lnk"; //ex: contactLnk
                var linkArr = $rootScope.links[linkArrName];
                linkArr[linkArr.length] = linkRow;
                data.$mdDialog.hide();
            }, function() {
                alert('Error saving. Please try again!');
            });
        }
    };
}];

