'use strict';

angular.module('appBundle').controller('DashboardController', [
    '$scope',
    '$rootScope',
    function(
        $scope,
        $rootScope
    ) {
        $scope.focusTypeArr = [
            'Prototype',
            'Enquiry',
            'Trading Order',
        ];
    } //end controller
]);


