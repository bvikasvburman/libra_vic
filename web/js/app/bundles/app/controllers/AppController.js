'use strict';

angular.module('appBundle').controller('AppController', [
    '$scope',
    '$rootScope',
    '$state',
    '$mdDialog',
    '$injector',
    'dragularService',
    'NavigationService',
    'ConfigService',
    'GlobalCfgService',
    'UtilService',
    'UtilUIService',
    'DbUtilService',
    'FormObjUtilService',
    'ListUtilService',
    'HotkeyService',
    'MediaService',
    function(
        $scope,
        $rootScope,
        $state,
        $mdDialog,
        $injector,
        dragularService,
        NavigationService,
        ConfigService,
        GlobalCfgService,
        UtilService,
        UtilUIService,
        DbUtilService,
        FormObjUtilService,
        ListUtilService,
        HotkeyService,
        MediaService
    ) {
        $scope.$state = $state;

        // dragularService('.md-dialog-container');
        $scope.dragularOptions = {};

        //shortcut keys
        HotkeyService.addHotkeys($scope);

        $scope.lus  = new ListUtilService($scope);
        $scope.ns   = NavigationService;
        $scope.utl  = UtilService;
        $scope.ms   = MediaService;
        $scope.gCfg = GlobalCfgService;
        $scope.dbu  = new DbUtilService($scope);
        $scope.fou  = new FormObjUtilService($scope);

        $rootScope.lus  = $scope.lus;
        $rootScope.ns   = $scope.ns;
        $rootScope.utl  = $scope.utl;
        $rootScope.gCfg = $scope.gCfg;
        $rootScope.dbu  = $scope.dbu;
        $rootScope.fou  = $scope.fou;
        $rootScope.ms   = $scope.ms;

        $scope.cfg        = null;
        $scope.listObj    = null;
        $scope.currView   = null;
        $scope.currBundle = null;
        $scope.currLink   = null;

        $scope.row = null;

        //for list sorting
        $scope.predicate = '';
        $scope.reverse = true;

        $scope.$watch('$state.current.name', function (newStateName, oldStateName) {
            if (newStateName == '') {
                $state.go('dashboard');
                return;
            }

            if (newStateName == 'dashboard') {
                $scope.currBundle = 'dashboard';
                $scope.currView   = 'list';
            } else {
                if (newStateName && newStateName !== oldStateName) {
                    $rootScope.cfg = $scope.cfg = ConfigService.getConfig();
                    $scope.listObj = $scope.cfg.listObj;
                    $scope.currBundle = UtilService.getCurrBundle();
                    $scope.currView   = UtilService.getCurrView();
                    $scope.currLink   = UtilService.getCurrLink();
                    $scope.currLinkObj = $scope.cfg.links[_.findIndex($scope.cfg.links, {name: $scope.currLink})];
                }
            }
        });

        $scope.callFn = function(name, ev){
            if (name == null) {
                return false;
            }

            //ex: ns.gotoEdit
            var fnTxt = "$scope" + "[name]";
            var nameArr = name.split('.');
            if (nameArr.length > 1) {
                var fnPref = nameArr[0];
                var fnSuff = nameArr[1];
                fnTxt = "$scope." + nameArr[0] + "[fnSuff]";
            }

            //$scope[gotoEdit](ev) or $scope.ns[gotoEdit](ev)
            eval(fnTxt + "(ev)");

            // if(angular.isFunction($scope.ns[name])) {
            // }
        }

        // $scope.$on('$stateChangeError', function(evt, to, toParams, from, fromParams, error) {
        //     console.log("WHOOAAAATTT!!! " + error);
        // })
    } //end controller
]);

