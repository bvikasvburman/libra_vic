'use strict';

var persistenceJar;
try {
   persistenceJar = angular.module("persistenceJar");
} catch(err) {
   persistenceJar = angular.module("persistenceJar", []);
}

persistenceJar.factory('BaseRepository', ['$http', function ($http) {
    function BaseRepository (objectType, data) {
        var self = this,
            objectType;

        /**
         * Api URLs
         */
        this.api = {
            baseURL:   '',      // GET    http://domain.tld/model
            createURL: '',      // POST   http://domain.tld/model
            deleteURL: '',      // DELETE http://domain.tld/model/{id}
            indexURL:  '',      // GET    http://domain.tld/model
            updateURL: '',      // PUT    http://domain.tld/model/{id}
            showURL:   '',      // GET    http://domain.tld/model/{id}
            searchURL: ''       // POST   http://domain.tld/model/search
        };

        /**
         * Set the class that is represented by this repository.
         * @param {mixed} objectType
         */
        function setObjectType(objectType) {
            if (!objectType) {
                // search on internet how to throw an error in javascript
            } else {
                self.objectType = objectType;
            }
        }

        setObjectType(objectType);
    }

    var standardDataTypes = ["string", "number", "boolean", "array", "object", "datetime"];

    BaseRepository.prototype.apply = function (obj) {
        obj.prototype = Object.create(BaseRepository.prototype);
        obj.prototype.constructor = obj;

        return obj;
    };

    /**
     * Find object by id.
     *
     * @param {string|int} id
     * @returns {object}
     */
    BaseRepository.prototype.find = function (id, options) {
        var options = options || {},
            self    = this;

        var request = {
            method: 'GET',
            url: this.api.showURL.replace('{id}', id).replace(':id', id)
        };

        if (options['method']) {
            request['method'] = options['method'];
        }

        if (options['url']) {
            request['url'] = options['url'].replace('{id}', id).replace(':id', id);
        }

        return $http(request).then(function (response) {
            if (response.data.length > 1) {
                var objects = [], newObject = null;
                for (var i = 0; i < response.data.length; i++) {
                    newObject = new self.objectType(response.data[i]);
                    objects.push(newObject);
                }
                return objects;
            } else {
                return new self.objectType(response.data);
            }
        });
    };

    /**
     * Find link records of the main entity.
     *
     * @param {string|int} id
     * @returns {object}
     */
    BaseRepository.prototype.findAllLink = function (id, apiStateName, options) {
        var options = options || {},
            self    = this;

        var request = {
            method: 'GET',
            url: Router.getRoutePath(apiStateName, false, {'id': id}),
        };

        if (options['method']) {
            request['method'] = options['method'];
        }

        if (options['url']) {
            request['url'] = options['url'].replace('{id}', id).replace('{link}', link);
        }

        return $http(request).then(function (response) {
            var objects = [], newObject = null;
            for (var i = 0; i < response.data.length; i++) {
                newObject = new self.objectType(response.data[i]);
                objects.push(newObject);
            }

            return objects;
        });
    };

    /**
     * Find link records of the main entity.
     *
     * @param {string|int} id
     * @returns {object}
     */
    BaseRepository.prototype.findOneLink = function (id, idl, apiStateName, options) {
        var options = options || {},
            self    = this;

        var request = {
            method: 'GET',
            url: Router.getRoutePath(apiStateName, false, {'id': id, 'idl': idl}),
        };

        if (options['method']) {
            request['method'] = options['method'];
        }

        if (options['url']) {
            request['url'] = options['url'].replace('{id}', id).replace('{link}', link);
        }

        return $http(request).then(function (response) {
            return new self.objectType(response.data);
        });
    };

    /**
     * Find by criteria
     *
     * @param {object} criteria (key value pair)
     * @param {object} retType return full set of data or just for valuelist (vl) or for any type
     * @param {object} sort (field: 1 / -1)
     * @param {object} limit
     * @returns {mixed}[]
     */
    BaseRepository.prototype.findBy = function (criteria, retType, opts, sort, limit, offset) {
        var self = this;
        // var params = {};

        // //ex: var criteria = {company_id: row.company.id};
        // for (criterion in criteria) {
        //     params[criterion] = criteria[criterion];
        // }

        var opts = angular.extend({}, {
            distinct: false,
        }, opts);
        var dbOpts = {
            distinct: opts.distinct,
        }

        var request = {
            method: 'GET',
            url: this.api.baseURL,
            params: {
                criteria: criteria ? criteria : {},
                sort: sort ? sort : {},
                limit: limit ? limit : '',
                offset: offset ? offset : '',
                retType: retType ? retType : '',
                dbOpts: dbOpts,
            },
            paramSerializer: '$httpParamSerializerJQLike'
        };


        return $http(request).then(function (response) {
            var objects = [], newObject = null;
            for (var i = 0; i < response.data.length; i++) {
                newObject = new self.objectType(response.data[i]);
                objects.push(newObject);
            }

            return objects;
        });

    };

    /**
     * Gets all instances.
     *
     * @param {object} options
     * @returns {mixed}[]
     */
    BaseRepository.prototype.all = function(options) {
        var options = options || {},
            self    = this;

        var request = {
            method: 'GET',
            url:    this.api.indexURL,
            params: {},
        };

        if (options['method']) {
            request['method'] = options['method'];
        }

        if (options['url']) {
            request['url'] = options['url'];
        }

        // eiterh return a full set of data or just for valuelist (vl) or for any type
        if (options['retType']) {
            request['params']['retType'] = options['retType'];
        }

        return $http(request).then(function (response) {
            var objects = [], newObject = null;
            for (var i = 0; i < response.data.length; i++) {
                newObject = new self.objectType(response.data[i]);
                objects.push(newObject);
            }

            return objects;
        });
    };

    /**
     * Delete instance.
     *
     * @param {string|id} id
     * @returns {mixed}
     */
    BaseRepository.prototype.delete = function (id, options) {
        var options = options || {};

        var request = {
            method: 'DELETE',
            url: this.api.deleteURL.replace('{id}', id).replace(':id', id)
        };

        if (options['method']) {
            request['method'] = options['method'];
        }

        if (options['url']) {
            request['url'] = options['url'].replace('{id}', id).replace(':id', id);
        }

        return $http(request);
    };

    BaseRepository.prototype.deleteLink = function (id, idl, options) {
        var options = angular.extend({}, {
            method: null,
            url: null,
            apiStateName: null,
        }, options);


        var request = {
            method: 'DELETE',
            url: this.api.deleteURL.replace('{id}', id).replace(':id', id)
        };

        if (options['method']) {
            request['method'] = options['method'];
        }
        if (options['url']) {
            request['url'] = options['url'].replace('{id}', id).replace('{idl}', id);
        }

        var apiUrl = '';
        if (options.apiStateName) {
            apiUrl = Router.getRoutePath(options.apiStateName, false, {
                'id': id,
                'idl': idl
            });

            request['url'] = apiUrl;
        }


        return $http(request);
    };

    /**
     * Saves an object.
     *
     * @param {mixed} object
     *
     * @param {object} options
     *      - {array}  excludedFields - array of fieldnames that will be excluded from request
     *      - {string} method  - use GET, DELETE, POST, PUT instead of PUT
     *      - {string} wrapper - wrap data within one object with the given name
     *      - {string} idField - specify id field if diferrent then id or _id
     *
     * @returns {}
     */
    BaseRepository.prototype.save = function (object, options) {
        var self = this;

        if (!object) {
            throw new Error('Cannot save an empty object.');
        }

        var options = angular.extend({}, {
            excludedFields: null,
            extraFields: null,
            extraParams: null,
            idField: null,
            method: null,
            url: null,
            wrapper: null,
            apiStateName: null,
            idApiState: null, //id for url of apiStateName passed
            idlApiState: null, //idl is link id for url of apiStateName passed
        }, options);

        var fieldsToSave   = {},
            excludedFields = [],
            apiStateName;

        if (options['excludedFields'] && Array.isArray(options['excludedFields'])) {
            excludedFields = options['excludedFields'];
        }

        fieldsToSave = object.toSimpleObject(true, true);

        var formatDate = function(fldValObj, fldType, fieldsToSave) {
            if (fldType == 'datetime') {
                fieldsToSave[key] = moment(fldValObj).format('YYYY-MM-DD[T]HH:mm:ssZZ');
            } else if (fldType == 'date') {
                fieldsToSave[key] = null;
                if (fldValObj) {
                    fieldsToSave[key] = moment(fldValObj).format('YYYY-MM-DD');
                    // fieldsToSave[key] = '2016-12-12';
                }
            }
        }

        //change the date format with moment.js to suite doctrine time format
        for (var key in fieldsToSave) {
            var fldValObj = fieldsToSave[key];
            formatDate(fldValObj, object.$$fields[key]['type'], fieldsToSave);
            // if (object.$$fields[key]['type'] == 'datetime') {
            //     fieldsToSave[key] = moment(fldValObj).format('YYYY-MM-DD[T]HH:mm:ssZZ');
            // } else if (object.$$fields[key]['type'] == 'date') {
            //     fieldsToSave[key] = null;
            //     if (fldValObj) {
            //         fieldsToSave[key] = moment(fldValObj).format('YYYY-MM-DD');
            //     }
            // }

            // if (fldValObj !== null && typeof fldValObj == "object") {
            //     for (var keySub in fldValObj) {
            //         var fldValObjSub = fldValObj[keySub];
            //         formatDate(fldValObjSub, object.$$fields[key]['type'], fieldsToSave);
            //     }
            // }
        }

        //remove the excluded fields from the main array to be submitted
        for (var i = 0; i < excludedFields.length; i++) {
            delete fieldsToSave[excludedFields[i]];
        }

        //remove the excluded fields from the sub arrays to be submitted
        for (var key in fieldsToSave) {
            var fldValObj = fieldsToSave[key];
            if (fldValObj !== null && typeof fldValObj == "object") {
                for (var i = 0; i < excludedFields.length; i++) {
                    delete fieldsToSave[key][excludedFields[i]];
                }
            }
        }

        if (options['extraFields']) {
            angular.extend(fieldsToSave, options['extraFields']);
        }

        var possibleIdFields = ['id', '_id'];
        var shouldPUT = false;
        var idField;

        if (options['idField']) {
            if (possibleIdFields[options['idField']]) {
                shouldPUT = true;
                idField   = options['idField'];
            }
        } else {
            for (var i = 0; i < possibleIdFields.length; i++) {
                if (fieldsToSave[possibleIdFields[i]]) {
                    shouldPUT = true;
                    idField   = possibleIdFields[i];
                    break;
                }
            }
        }

        //used especially for adding / saving links
        var apiUrl = '';
        if (options.apiStateName) {
            apiUrl = Router.getRoutePath(options.apiStateName, false, {
                'id': options.idApiState,
                'idl': options.idlApiState
            });
        }

        if (apiUrl == "") {
            apiUrl = shouldPUT ?
                     this.api.updateURL.replace('{id}', fieldsToSave[idField])
                     .replace(':id', fieldsToSave[idField]) :
                     this.api.createURL;
        }
        var request = {
            method: shouldPUT ? 'PUT' : 'POST',
            url:    apiUrl,
            data:   fieldsToSave
        };

        if (options['method']) {
            request['method'] = options['method'];
        }

        if (options['url']) {
            request['url'] = options['url'];
        }

        var dataObj = {};
        if (options['wrapper']) {
            dataObj[options['wrapper']] = fieldsToSave;
        }
        if (options['extraParams']) {
            dataObj['params'] = options['extraParams'];
        }
        if (!_.isEmpty(dataObj)) {
            request['data'] = dataObj;
        }

        return $http(request).then(function (response) {
            return new self.objectType(response.data);

            // var data = response.data;
            // if (data.id) {
            //     object.id = data.id;
            // } else if (data._id) {
            //     object._id = data._id;
            // }
            // return object;
        });
    };

    /**
     * Defines Api URL. You can redefine each of the URLs from the api object directly:
     *    this.api.createURL
     *
     * @param {string} url
     */
    BaseRepository.prototype.setApiURL = function (url) {
        var hasTrailingSlash = url[url.length - 1] == '/';

        var idStr     = (hasTrailingSlash ? '{id}' : '/{id}');
        var searchStr = (hasTrailingSlash ? 'search' : '/search');

        this.api.baseURL     = url;
        this.api.createURL   = this.api.baseURL;
        this.api.deleteURL   = this.api.baseURL + idStr;
        this.api.indexURL    = this.api.baseURL;
        this.api.updateURL   = this.api.baseURL + idStr;
        this.api.showURL     = this.api.baseURL + idStr;
        this.api.searchURL   = this.api.baseURL + searchStr;
    };

    return new BaseRepository();
}]);
