'use strict';

angular.module('app').filter('route', ['$state', function($state) {
    return function (stateName, params, absoluteUrl, includingHashbang) {
        var path = $state.href(stateName, params, {absolute: absoluteUrl});
        // if (includingHashbang) {
        //     path = '#' + path;
        // }
        return path;
    };
}]);