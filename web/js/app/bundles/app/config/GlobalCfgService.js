'use strict';
angular.module('appBundle')
    .factory('GlobalCfgService',
    ['TopRoomService', 'RoomService',
    function(TopRoomService, RoomService) {
        return {
            dateFormat: 'DD-MMM-YYYY',
            dateFormatTime: 'DD-MMM-YYYY HH:mm:ss',
            dateFormatEdit: 'YYYY-MM-DD',

            getTopRooms: function() {
                var roomsMaster = [
                    new RoomService('colorMaster'),
                    new RoomService('partsLocation'),
                    new RoomService('approvedMaterial', {title: 'App. Material'}),
                    new RoomService('approvedComponent', {title: 'App. Component'}),
                    new RoomService('approvedPackaging', {title: 'App. Packaging'}),
                    new RoomService('masterPerformance', {title: 'Master Perf.'}),
                    new RoomService('masterCompliance', {title: 'Master Compl.'}),
                    new RoomService('masterChecklist'),
                    new RoomService('productCoding'),
                ];
                var roomsERM = [
                    new RoomService('enquiry'),
                    new RoomService('quotation'),
                    new RoomService('tradingOrder'),
                    new RoomService('salesConfirmation', {title: 'Sales Conf.'}),
                    new RoomService('purchaseOrder'),
                    new RoomService('manufacturingOrder', {title: 'Mfg.Order'}),
                    new RoomService('invoice'),
                    new RoomService('vendorInvoice'),
                    new RoomService('accountsReceivable', {title: 'Acc. Receivable'}),
                    new RoomService('claim'),
                    new RoomService('accountsPayable', {title: 'Acc. Payable'}),
                    new RoomService('creditDebitNote', {title: 'Credit/Debit Note'}),
                    new RoomService('message'),
                ];
                var roomsPLM = [
                    new RoomService('protoType'),
                    new RoomService('research'),
                ];
                var roomsCRM = [
                    new RoomService('company'),
                    new RoomService('contact'),
                    new RoomService('interestGroup'),
                ];
                var roomsAdmin = [
                    new RoomService('user'),
                    new RoomService('role'),
                    new RoomService('translation'),
                    new RoomService('setting'),
                    new RoomService('valuelist'),
                ];
                var topRooms = [
                    new TopRoomService('master', {title: 'Masters', rooms: roomsMaster}),
                    new TopRoomService('erm', {title: 'ERM', rooms: roomsERM}),
                    new TopRoomService('plm', {title: 'PLM', rooms: roomsPLM}),
                    new TopRoomService('crm', {title: 'CRM', rooms: roomsCRM}),
                    new TopRoomService('admin', {rooms: roomsAdmin}),
                ];

                return topRooms;
            },

            vls: {
                valuelistNames: [
                    {key: 'approvedMaterialCategory', value: 'Approved Material Category'},
                    {key: 'approvedComponentCategory', value: 'Approved Component Category'},
                    {key: 'approvedPackagingCategory', value: 'Approved Packaging Category'},
                    {key: 'masterChecklistCategory', value: 'Master Checklist Category'},
                    {key: 'productFamily', value: 'Product Family'},
                ]

            },

        }
    }
]);
