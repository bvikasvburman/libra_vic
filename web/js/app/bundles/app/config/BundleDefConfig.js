'use strict';
angular.module('appBundle')
   .factory('BundleDefConfig', [
    'ActBtnService',
    'UtilService',
    'GlobalCfgService',
    '$state',
    function(
        ActBtnService,
        UtilService,
        GlobalCfgService,
        $state
    ) {
        function BundleDefConfig(bundleName, opts) {

            var stateEdit = bundleName + '.edit';
            var stateEditMainTab = bundleName + '.edit.main';
            var stateNew = bundleName + '.new';

            // if we have main tab for the module
            if ($state.get(stateEditMainTab)) {
                stateEdit = bundleName + '.edit.main';
                stateNew = bundleName + '.new.main';
            }

            var actBtns = [
                //list default actBtns
                new ActBtnService('new', 'list', stateNew),
                new ActBtnService('print', 'list', '', {callback: 'dbu.printList'}),

                //detail default actBtns
                // need to give a dot for stateName in order to not show the error in the console


                new ActBtnService('edit', 'detail', '', {callback: 'ns.gotoEditFromDetail'}),
                new ActBtnService('new', 'detail', stateNew),
                new ActBtnService('delete', 'detail', '', {callback: 'dbu.deleteRecord'}),

                //new default actBtns
                new ActBtnService('save', 'new', '', {callback: 'dbu.addRecord'}),
                new ActBtnService('cancel', 'new', '', {callback: 'dbu.cancelNew'}),

                //edit default actBtns
                new ActBtnService('save', 'edit', '', {callback: 'dbu.saveRecord'}),
                new ActBtnService('apply', 'edit', '', {title: 'Save & Continue', callback: 'dbu.applyRecord'}),
                new ActBtnService('cancel', 'edit', '', {callback: 'dbu.cancelEdit'}),
            ];

            this.name = bundleName;
            this.icon = bundleName + '.svg'; //button icon
            this.hasListPreview = false;
            this.listRowsLimit = 10;
            this.linkListRowsLimit = 10; //list rows limit per page for Links
            this.dateFormat = GlobalCfgService.dateFormat;
            this.actBtns = actBtns;
            this.listObj = null;
            this.links = null; //stores all the info related to link records. Array of LinkService obj
            this.tabs = null; //stores all the tabs in detail/edit page. Array of TabService obj

            this.title = s(bundleName).humanize().titleize().value();
            for (prop in opts) {
                this[prop] = opts[prop];
            }
        }

        BundleDefConfig.prototype = {
            constructor: BundleDefConfig,

            getLink: function(linkName) {
                return _.find(this.links, function(linkObj) {return linkObj.name == linkName;});
            }
        };

        return BundleDefConfig;
    }
]);

