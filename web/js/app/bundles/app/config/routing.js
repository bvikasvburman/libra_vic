Router.addRoutes([
    {
        name: 'root',
        url: '/',
        templateUrl: '/get-template/other/dashboard'
    },
    {
        name: 'dashboard',
        url: '/dashboard',
        templateUrl: '/get-template/other/dashboard'
    },
]);

Router.addRoutes([
    /* api */
    {name: 'api_ganttEnquiry', url: 'api/gantt/enquiry/{ganttId}'},
    {name: 'api_ganttTaskEnquiry', url: 'api/ganttTask/enquiry/{ganttId}'},
    {name: 'api_ganttRowEnquiry', url: 'api/ganttRow/enquiry/{ganttId}'},

    {name: 'api_ganttContact', url: 'api/gantt/contact/{ganttId}'},
    {name: 'api_ganttTaskContact', url: 'api/ganttTask/contact/{ganttId}'},
    {name: 'api_ganttRowContact', url: 'api/ganttRow/contact/{ganttId}'},

]);

