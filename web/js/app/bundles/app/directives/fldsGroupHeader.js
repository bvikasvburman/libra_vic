'use strict';
angular
    .module('appBundle')
    .directive('fldsGroupHeader', [function() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'js/app/bundles/app/directives/fldsGroupHeaderTpl.html',
            scope: {
              title: '@'
            },
            link: function(scope, elem, attributes) {
                $(".arrow md-icon", elem).bind('click', function() {
                    $(elem).next("md-content").slideToggle();
                    if ($(this).css("transform") == 'none') {
                        $(this).css("transform", "rotate(180deg)");
                    } else {
                        $(this).css("transform", "none");
                    }
                });
            }
        };
    }]);