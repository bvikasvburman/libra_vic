angular.module('appBundle').directive('attrHide', function(){
    return {
        restrict: 'A',
        scope: {
          attrHide: '='
        },
        link: function(scope, elm, attr){
            var targetAttr = attr.hiddenAttribute;
            var saveAttr = attr[targetAttr] || '';

            scope.$watch('attrHide', function(newVal){
                if (newVal) {
                    elm.removeAttr(targetAttr);
                } else {
                    elm.attr(targetAttr,saveAttr);
                }
            })
        }
    }
})