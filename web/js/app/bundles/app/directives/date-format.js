angular.module("app")
.directive('dateFormat', function (dateFilter) {
    return {
        require:'ngModel',
        link:function (scope, elm, attrs, ctrl) {

            var dateFormat = attrs['dateFormat'] || 'DD-MM-YYYY';

            ctrl.$formatters.unshift(function (modelValue) {
                return moment(new Date(modelValue)).format(dateFormat);
                // return dateFilter(modelValue, dateFormat);
            });
        }
    };
})
