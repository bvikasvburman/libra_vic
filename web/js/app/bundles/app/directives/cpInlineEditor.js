angular.module("app")
    .directive('onEnter', function() {
        var ENTER_KEY = 13;

        return {
            scope: {
                expressionFn: '&onEnter'
            },
            link: function(scope, element) {
                element.on('keypress', function(e) {
                    if (e.which === ENTER_KEY) {
                        scope.$apply(function() {
                            scope.expressionFn({
                                $event: e
                            });
                        });
                    }
                });
            }
        };
    })

.directive('cpInlineEditor', function() {
    return {
        restrict: 'E',
        scope: {
            changeFn: '&change',
            valueText: '@value'
        },
        controller: function($scope) {
            $scope.editing = false;

            $scope.switchToEditMode = function() {
                $scope.newValue = $scope.valueText;
                $scope.editing = true;
            };

            $scope.switchToNormalMode = function() {
                $scope.newValue = '';
                $scope.editing = false;
            };

            $scope.sendNewValue = function() {
                $scope.editing = false;
                $scope.changeFn({
                    $text: $scope.newValue
                });
            }
        },
        templateUrl: 'js/app/bundles/app/directives/cpInlineEditorTpl.html',
    }
})