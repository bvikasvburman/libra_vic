angular.module('app')
.directive('arrKeyToValue', function($rootScope) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
          dataArr:'=arrKeyToValue'
        },
        link: function(scope, elem, attrs, ngModel){
            var dataArr = scope.dataArr;
            scope.$watch(scope.dataArr, function(newValue, oldValue){
                if(newValue) {
                    ngModel.$formatters.push(function(val){
                        for (var i in dataArr) {
                            var row = dataArr[i];
                            if (row.key == val) {
                                return row.value;
                            }
                        }
                    });
                };
            });
            // ngModel.$parsers.push(function(val){
            //     return val.replace(/^\$/, '')
            // });
        }
    }
});