'use strict';

angular.module('appBundle').factory('NavigationService', [
    '$location',
    '$state',
    '$window',
    '$rootScope',
    'UtilService',
    function (
        $location,
        $state,
        $window,
        $rootScope,
        UtilService
    ) {
        return {
            gotoList: function() {
                var stateName = UtilService.getCurrBundle() + ".list";
                $state.go(stateName);
            },

            gotoDetail: function(event, id, bundle) {
                if (_.isUndefined(bundle)) {
                    bundle = UtilService.getCurrBundle();
                }
                var stateNameMainTab = bundle + ".detail.main";
                var stateName        = bundle + ".detail";

                if ($state.get(stateNameMainTab)) {
                    stateName = stateNameMainTab;
                }

                if (event.ctrlKey) {
                    $window.open($state.href(stateName, { id: id }, {absolute: true}), '_blank');
                } else {
                    $state.go(stateName, { id: id });
                }
            },

            gotoDetailFromEdit: function(id) {
                var stateName = $state.current.name; //ex: company.edit.contact

                stateName = stateName.replace('edit', 'detail'); //ex: company.detail.contact
                $state.go(stateName,{ id: id });
            },

            gotoEdit: function(id) {
                var stateNameMainTab = UtilService.getCurrBundle() + ".edit.main";
                var stateName = UtilService.getCurrBundle() + ".edit";
                var url = '';
                if ($state.get(stateNameMainTab)) {
                    $state.go(stateNameMainTab,{ id: id });
                } else {
                    $state.go(stateName,{ id: id });
                }
            },

            gotoEditFromDetail: function() {
                var id = $rootScope.row.id;

                var stateName = $state.current.name; //ex: company.detail.contact
                stateName = stateName.replace('detail', 'edit'); //ex: company.edit.contact
                $state.go(stateName,{ id: id });
            },

            getDetailUrl: function(id, bundle) {
                if (_.isUndefined(bundle)) {
                    bundle = UtilService.getCurrBundle();
                }
                var stateNameMainTab = bundle + ".detail.main";
                var stateName        = bundle + ".detail";

                var url = '';
                if ($state.get(stateNameMainTab)) {
                    url = $state.href(stateNameMainTab, { id: id });
                } else {
                    url = $state.href(stateName, { id: id });
                }
                return url;
            },

            getEditUrl: function(id) {
                var stateNameMainTab = UtilService.getCurrBundle() + ".edit.main";
                var stateName = UtilService.getCurrBundle() + ".detail";
                var url = '';
                if ($state.get(stateNameMainTab)) {
                    url = $state.href(stateNameMainTab, { id: id });
                } else {
                    url = $state.href(stateName, { id: id });
                }
                return url;
            },

            getDetailLinkUrl: function(id) {
                //if the we are in Company > Contact link tab
                // then the currLink will be contact. So we should get contact.detail.main
                var stateNameMainTab = UtilService.getCurrLink() + ".detail.main";
                var stateName = UtilService.getCurrLink() + ".detail";
                var url = '';
                if ($state.get(stateNameMainTab)) {
                    url = $state.href(stateNameMainTab, { id: id });
                } else {
                    url = $state.href(stateName, { id: id });
                }
                return url;
            },

            getEditLinkUrl: function(id, idl) {
                // return "/company/edit/:id/contact/edit:idl"
                var stateName = $state.current.name + ".edit";
                var url = $state.href(stateName, { id: id, idl: idl });

                return url;
            },

            getNewLinkUrl: function(id) {
                // return "/company/edit/:id/contact/new"
                var stateName = $state.current.name + ".new";
                var url = $state.href(stateName, { id: id });
                return url;
            },

            gotoNewRecord: function() {
                var currBundle = UtilService.getCurrBundle();
                var stateEditMainTab = currBundle + '.new.main';
                var stateNew = currBundle + '.new';

                // if we have main tab for the module
                if ($state.get(stateEditMainTab)) {
                    stateNew = currBundle + '.new.main';
                }
                $state.go(stateNew);
            },

            getListState: function() {
                var stateName = UtilService.getCurrBundle() + ".list";
                return stateName;
            },


        }
    }
]);