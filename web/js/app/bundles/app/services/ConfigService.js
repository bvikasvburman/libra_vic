'use strict';

angular.module('appBundle').factory('ConfigService', [
    '$state',
    '$injector',
    'UtilService',
    function (
        $state,
        $injector,
        UtilService
    ) {
        return {
            getConfig: function(configName) {
                if (typeof configName === "undefined") {
                    var configName = UtilService.getCurrBundle() + 'Config';
                }
                return $injector.get(configName);
            }
        }
    }
]);