'use strict';
angular.module('appBundle')
    .factory('TopRoomService', [function() {

        function TopRoomService(name, opts) {
            this.roomName = name;
            this.icon = this.roomName + '.svg'; //button icon
            this.title = s(name).humanize().titleize().value();
            this.rooms = [];

            for (prop in opts) {
                this[prop] = opts[prop];
            }
        }

        TopRoomService.prototype = {
            constructor: TopRoomService,
            // method1: function() {
            //     alert('save');
            // }
        };

        return TopRoomService;
    }
]);

