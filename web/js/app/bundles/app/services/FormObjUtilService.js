'use strict';

angular.module('appBundle').factory('FormObjUtilService', [
    '$state',
    '$q',
    '$rootScope',
    '$injector',
    '$state',
    'NavigationService',
    'UtilUIService',
    function (
        $state,
        $q,
        $rootScope,
        $injector,
        $state,
        NavigationService,
        UtilUIService
    ) {
        var thisScope;
        function FormObjUtilService($scope) {
            thisScope = $scope;
        }

        FormObjUtilService.prototype = {
            filterAutoComplete: function(rowsVL, searchText, searchCol) {
                var d = $q.defer();

                var filteredItems = [],
                searchStrLower = searchText.toLowerCase();

                filteredItems = rowsVL.filter(function(row) {
                    return (row[searchCol].toLowerCase()).indexOf(searchStrLower) > -1;
                });

                if (filteredItems.length === 0) {
                    return rowsVL;
                } else {
                    return filteredItems;
                }

                // $timeout(function() {
                //     // Emulate async loading
                //     var filteredItems = [],
                //     searchStrLower = searchText.toLowerCase();

                //     filteredItems = rowsVL.filter(function(item) {
                //         return (item.title.toLowerCase()).indexOf(searchStrLower) > -1;
                //     });

                //     if (filteredItems.length === 0) {
                //         d.resolve(rowsVL);
                //     } else {
                //         d.resolve(filteredItems);
                //     }
                // }, 500);

                return d.promise;
            },

            getSubVL: function(entityNameSubVL, rowMain, mainVLEntityName) {
                //ex: Company > Contact valuelists
                var repoName = s.capitalize(entityNameSubVL) + 'Repository'; //ex: ContactRepository
                var repo = $injector.get(repoName);

                if (rowMain[mainVLEntityName]) { //rowMain['company']
                    var mainVLIdFldName = mainVLEntityName + 'Id';
                    var criteria = {};
                    criteria[mainVLIdFldName] = rowMain[mainVLEntityName].id;
                    // var criteria = {mainVLFldName_id: rowMain.company.id};
                    //ex: SELECT * FROM contact WHERE company_id = {company_id}
                    repo.findBy(criteria, 'vl').then(function (vlModelRows) {
                        angular.copy(vlModelRows, $rootScope.vls[entityNameSubVL]);
                    });
                }
            },

            setSelectedCodeFldFromVL: function(row, valuelist, modelName, codeFldName, vlCodeFldName, valuelistFldInVL) {
                var searchCriteria = {};
                searchCriteria[valuelistFldInVL] = row[modelName];
                var vlItemObj = _.findWhere(valuelist, searchCriteria);
                row[codeFldName] = vlItemObj[vlCodeFldName]; //ex: row.code = vlItemObj.code

            }

        };

        return FormObjUtilService;
    }
]);