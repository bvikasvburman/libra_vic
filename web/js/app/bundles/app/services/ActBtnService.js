'use strict';

angular.module('appBundle').factory('ActBtnService', [
    'UtilService',
    '$state',
    function(
        UtilService,
        $state
    ) {

    function ActBtnService(name, view, stateName, opts) {
        this.name = name;
        this.view = view;
        this.stateName = stateName;
        this.title = s.titleize(name);
        this.callback = null;
        this.palette = 'accent';
        this.icon = name + '.svg'; //button icon

        for (prop in opts) {
            this[prop] = opts[prop];
        }

    }

    ActBtnService.prototype = {
        constructor: ActBtnService,

        getStateVar: function(stateName) {
            return $state.href(stateName, $state.params);
        },

        getHref: function(params) {
            var url = $state.href(this.stateName, params, {absolute: true});

            return url;
        }
    };

    return ActBtnService;
}]);