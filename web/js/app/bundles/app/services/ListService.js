'use strict';

angular.module('appBundle').factory('ListService', ['$injector', function($injector) {
    /**
     * This class is simply a collection of ListFldService objects.
     */
    function ListService(name, fldsArr) {
        this.name = name;

        // check any field is set with hasDetailLink
        var hasDetailLinkSet = _.find(fldsArr, function(fld){ return fld.hasDetailLink; });


        // if not set the first field with hasDetailLink
        if (!hasDetailLinkSet) {
            fldsArr[0].hasDetailLink = true;
        }
        this.fldsArr = fldsArr;
    }

    ListService.prototype = {
        constructor: ListService,
    };

    return ListService;
}]);