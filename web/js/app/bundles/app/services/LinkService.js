'use strict';

angular.module('appBundle').factory('LinkService', ['UtilService', function(UtilService) {

    var currBundle = UtilService.getCurrBundle();

    /**
     * This class is for creating link sections in the main bundle.
     * Ex: In the Company bundle add a link to Contacts.
     */
    function LinkService(name, opts) {
        this.name = name;
        this.bundle = currBundle;
        this.title = s(name).humanize().titleize().value();
        this.fields = [];
        this.hasEdit   = true;
        this.hasNew    = true;
        this.hasDelete = true;
        this.linkListRowsLimit = 10; //list rows limit per page for Links

        for (prop in opts) {
            this[prop] = opts[prop];
        }

        //set the linkName within each field for easy access
        for (var i in this.fields) {
            var field = this.fields[i];
            field.linkName = name;
        }

        // check any field is set with hasDetailLink
        var hasDetailLinkSet = _.find(this.fields, function(fld){ return fld.hasDetailLink; });

        // if not set the first field with hasDetailLink
        if (!hasDetailLinkSet && this.fields.length > 0) {
            this.fields[0].hasDetailLink = true;
        }
    }

    LinkService.prototype = {
        constructor: LinkService,
        method1: function() { }
    };

    return LinkService;
}]);