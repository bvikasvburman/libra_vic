'use strict';

angular.module('appBundle').factory('MediaService', [
    '$location',
    '$state',
    '$injector',
    '$filter',
    'UtilService',
    'FileUploader',
    'Lightbox',
    function (
        $location,
        $state,
        $injector,
        $filter,
        UtilService,
        FileUploader,
        Lightbox
    ) {
        return {
            getNewFileUploader: function($scope, linkNameMedia) {
                //ex: api_company_attachment
                var uploadApiState = 'api_' + UtilService.getCurrBundle() + '_' + linkNameMedia;

                var routeObj     = Router.getRouteObj($state.current.name);
                var relEntityObj = $injector.get(routeObj.linkObj.entity); //ex: CompanyMedia

                return new FileUploader({
                    alias: 'files',
                    method: 'POST',
                    removeAfterUpload: true,
                    url: Router.getRoutePath(uploadApiState, true, { id: $scope.row.id }),

                    onCompleteItem: function (item, data, status) {
                        var rowsReturned = data[linkNameMedia];
                        for (var i = 0; i < rowsReturned.length; i++) {
                            $scope.links[linkNameMedia + 'Lnk'].push(new relEntityObj(rowsReturned[i]));
                        }
                    }
                });
            },

            getMediaDownloadLink: function(rowLink, fldVal) {
                var fileName = rowLink.media ? rowLink.media.fileName : '';
                var url = "/media/normal/" + fileName;
                fldVal = "<a target='_new' href='" + url + "'>" + fldVal + "</a>";

                return fldVal;
            },

            getMediaThumbSrc: function(rowLink) {
                var fileName = rowLink.media ? rowLink.media.fileName : '';
                var url = "/media/normal/" + fileName;
                return url;
            },

            formatFileSize: function(mediaSize) {
                var mediaSize = mediaSize / 1024 / 1024;
                mediaSize = $filter('number')(mediaSize, 2);

                mediaSize = mediaSize + 'MB';
                return mediaSize;
            },

            openLightboxModal: function(index, imageArr) {
                Lightbox.openModal(imageArr, index);
            },
        }
    }
]);