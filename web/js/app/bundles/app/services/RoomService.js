'use strict';
angular.module('appBundle')
    .factory('RoomService', [function() {

        function RoomService(name, opts) {
            this.roomName = name;
            this.stateName = name + '.list';
            this.icon = this.roomName + '.svg'; //button icon
            this.title = s(name).humanize().titleize().value();

            for (prop in opts) {
                this[prop] = opts[prop];
            }
        }

        RoomService.prototype = {
            constructor: RoomService,
            // method1: function() {
            //     alert('bla bla');
            // }
        };

        return RoomService;
    }
]);

