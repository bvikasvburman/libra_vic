'use strict';

angular.module('appBundle').factory('DbUtilService', [
    '$state',
    '$q',
    '$rootScope',
    '$injector',
    '$state',
    'NavigationService',
    'UtilUIService',
    function (
        $state,
        $q,
        $rootScope,
        $injector,
        $state,
        NavigationService,
        UtilUIService
    ) {
        var thisScope;
        function DbUtilService($scope) {
            thisScope = $scope;
        }

        DbUtilService.prototype = {
            addRecordToDb: function() {
                var repoName = s.capitalize(thisScope.currBundle) + 'Repository';

                var repo = $injector.get(repoName);
                var excludedFields = [
                    'creationDate',
                    'modificationDate',
                    'userCreated',
                    'userModified',
                ];
                return repo.save($rootScope.row, {wrapper: thisScope.currBundle, excludedFields: excludedFields});
            },

            addRecord: function(gotoListAfterSave) {
                this.addRecordToDb()
                .then(function(row) {
                    if (gotoListAfterSave === true) {
                        NavigationService.gotoList();
                    } else {
                        var event = gotoListAfterSave; //the param is an event now
                        if (event && event.ctrlKey) { //if ctrl key pressed and save button clicked
                            NavigationService.gotoList();
                        } else {
                            NavigationService.gotoEdit(row.id);
                        }
                    }
                });
            },

            saveRecordToDb: function() {
                var repoName = s.capitalize(thisScope.currBundle) + 'Repository';
                var repo = $injector.get(repoName);
                var excludedFields = [
                    'creationDate',
                    'modificationDate',
                    'userCreated',
                    'userModified',
                ];

                return repo.save($rootScope.row, {
                    wrapper: thisScope.currBundle,
                    excludedFields: excludedFields
                });
            },

            /**
             * The first param can be a boolean (through hotkey) or event object (clicked the button save)
             *
             */
            saveRecord: function(gotoListAfterSave) {
                this.saveRecordToDb()
                .then(function() {
                    if (gotoListAfterSave === true) {
                        NavigationService.gotoList();
                    } else {
                        var event = gotoListAfterSave; //the param is an event now
                        if (event && event.ctrlKey) { //if ctrl key pressed and save button clicked
                            NavigationService.gotoList();
                        } else {
                            NavigationService.gotoDetailFromEdit($rootScope.row.id);
                        }
                    }
                });
            },

            applyRecord: function() {
                this.saveRecordToDb()
                .then(function() {
                    $state.go($state.current, {}, {reload: true});
                });
            },

            cancelNew: function(ev) {
                var opts = {
                    title: 'Cancel New',
                    message: 'Are you sure to cancel new record creation?',
                }
                UtilUIService.showConfirmDialog(ev, opts)
                .then(function() {
                    NavigationService.gotoList();
                });
            },

            cancelEdit: function(ev) {
                var opts = {
                    title: 'Cancel Edit',
                    message: 'Are you sure to cancel any changes that you have made?',
                }
                UtilUIService.showConfirmDialog(ev, opts)
                .then(function() {
                    NavigationService.gotoList();
                });
            },

            deleteRecord: function(ev) {
                var opts = {
                    title: 'Delete Record',
                    message: 'Are you sure to delete the current record?',
                }
                UtilUIService.showConfirmDialog(ev, opts)
                .then(function() {
                    var repoName = s.capitalize(thisScope.currBundle) + 'Repository';
                    var repo = $injector.get(repoName);
                    repo.delete($rootScope.row.id);
                    NavigationService.gotoList();
                });
            },


            deleteLinkRecord: function(ev, row, rowLink, linkName) {
                var currBundle = thisScope.currBundle;

                var opts = {
                    title: 'Delete!',
                    message: 'Are you sure to delete this record?',
                }
                UtilUIService.showConfirmDialog(ev, opts)
                .then(function() {
                    var routeObj     = Router.getRouteObj($state.current.name);
                    var relReposObj  = $injector.get(routeObj.linkObj.repository); //ex: CompanyMedia
                    var apiStateName = 'api_' + currBundle + '_' + linkName + '_one';

                    relReposObj.deleteLink(row.id, rowLink.id, {apiStateName: apiStateName})
                    .then(function() {
                        var linkArrName = linkName + "Lnk";
                        var linkArr = $rootScope.links[linkArrName];
                        var arrInd = _.findIndex(linkArr, {id: parseInt(rowLink.id)});
                        linkArr.splice(arrInd, 1);
                    });
                });
            },

            printList: function() {
                window.open('/print/contactList.pdf');
            }
        };

        return DbUtilService;
    }
]);