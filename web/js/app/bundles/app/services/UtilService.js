'use strict';

angular.module('appBundle').factory('UtilService', [
    '$state',
    '$q',
    '$rootScope',
    '$timeout',
    '$controller',
    '$http',
    'GlobalCfgService',
    function (
        $state,
        $q,
        $rootScope,
        $timeout,
        $controller,
        $http,
        GlobalCfgService
    ) {
        return {
            /**
             * if the current statename is tradingOrder.list
             * it will return tradingOrder
             */
            getCurrBundle: function() {
                var arr = $state.current.name.split(".");
                var currBundle = arr[0]; //returns tradingOrder
                return currBundle;
            },

            /**
             * if the current statename is tradingOrder.detail
             * it will return detail
             */
            getCurrView: function() {
                var arr = $state.current.name.split(".");
                var currView = arr[1]; //returns detail
                return currView;
            },

            /**
             * if the current statename is company.detail.contact
             * it will return detail
             */
            getCurrLink: function() {
                var arr = $state.current.name.split(".");
                var currLink = arr[2]; //returns contact
                return currLink;
            },

            controllerExists: function(controllerName) {
                try {
                    // inject '$scope' as a dummy local variable
                    // and flag the $controller with 'later' to delay instantiation
                    $controller(controllerName, { "$scope": {} }, true);
                    return true;
                }
                catch(ex) {
                    return false;
                }
            },

            setEmptyLinksResolveForParentController: function(controllerName) {
                try {
                    // inject '$scope' as a dummy local variable
                    // and flag the $controller with 'later' to delay instantiation
                    $controller(controllerName, { "$scope": {} }, true);
                    return true;
                }
                catch(ex) {
                    return false;
                }
            },

            initializeCommonControllerVars: function($scope, row) {
                $rootScope.row   = $scope.row = row;
                $rootScope.links = $scope.links = {}; //all links for current bundle
                $rootScope.vls   = $scope.vls = {}; //all valuelists for current bundle
                $scope.scope     = $scope; //used to access $scope from view
            },

            getBarcodeSrc: function(barcodeName, value) {
                var url = '/barcode/' + barcodeName + '/' + value;
                return url;
            },

            // getBarcodeSrc: function(bcImg, barcodeName, value) {
            //     $http.get(url).then(function(response) {
            //         // bcImg.imgUrl = 'data:image/jpeg;base64,' + response.data;
            //         bcImg.imgUrl = 'data:image/jpeg;base64,' + response.data;
            //     });
            //     return "/img/icons/others/ajax-loader.gif";
            // },

            /**
             * Sets the API URL for currentBundle data repository
             */
            // setApiURL: function(repository, bundleName) {
            //     var currBundle = this.getCurrBundle(bundleName);
            //     repository.setApiURL(Router.getRoutePath('api_' + currBundle, true));
            // }

            random4: function() {
                return Math.floor((1 + Math.random()) * 0x10000)
                    .toString(16)
                    .substring(1);
            },

            randomUuid: function() {
                return this.random4() + this.random4() + '-' + this.random4() + '-' + this.random4() + '-' +
                    this.random4() + '-' + this.random4() + this.random4() + this.random4();
            },

            formatDate: function(dateVal) {
                dateVal = moment(dateVal).format(GlobalCfgService.dateFormatTime);

                return dateVal;
            },

        }
    }
]);