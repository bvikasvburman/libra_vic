'use strict';

angular.module('appBundle').factory('ListUtilService', [
    '$injector',
    '$sce',
    '$state',
    '$compile',
    '$mdEditDialog',
    'UtilService',
    'NavigationService',
    'MediaService',
    'GlobalCfgService',
    function(
        $injector,
        $sce,
        $state,
        $compile,
        $mdEditDialog,
        UtilService,
        NavigationService,
        MediaService,
        GlobalCfgService
    ) {
        function ListUtilService($scope) {
            this.$scope = $scope;
        }

        ListUtilService.prototype = {
            sortList: function(predicate) {
                this.$scope.reverse = (this.$scope.predicate === predicate) ? !this.$scope.reverse : false;
                this.$scope.predicate = predicate;
            },

            /**
             * Flag/Unflag a row.
             *
             */
            flag: function(row) {
                row.flag = !row.flag;

                var repoName = s(this.$scope.currBundle).capitalize().value() + 'Repository';
                var repository = $injector.get(repoName);
                repository.save(row, {wrapper: this.$scope.currBundle});
            },

            getFieldVal: function(row, fld) {
                var fldName = fld.name; //firstName
                var fldNamePref = "";
                var fldVal = "";

                //spacer column
                if (!fldName) {
                    return '';
                }

                //ex: if field name is company.name
                if (fldName.split('.').length > 1) {
                    fldNamePref = fldName.split('.')[0]; //company
                    fldNameSuff = fldName.split('.')[1]; //name
                    fldPref = eval('row.$$data.' + fldNamePref);
                    fldVal = fldPref == null ? "" : fldPref[fldNameSuff];
                } else {
                    fldVal = row.$$data[fld.name];
                }

                if (fld.isDate) {
                    if (fldVal) {
                        fldVal = moment(fldVal).format(GlobalCfgService.dateFormat);
                    }
                }
                else if (fld.isDatetime) {
                    if (fldVal) {
                        fldVal = moment(fldVal).format(GlobalCfgService.dateFormatTime);
                    }
                }
                else if (fld.isSwitch) {
                    if (fldVal) {
                        fldVal = "<img src='/img/icons/others/tick.svg'>";
                    } else {
                        fldVal = "";
                    }
                }

                if (fld.keyValArr) {
                    for (var i in fld.keyValArr) {
                        var row = fld.keyValArr[i];
                        if (row.key == fldVal) {
                            fldVal = row.value;
                            break;
                        }
                    }
                }

                if (fld.hasDetailLink) {
                    var url = NavigationService.getDetailUrl(row.id);
                    fldVal = "<a href='" + url + "'>" + fldVal + "</a>";
                }

                if (typeof fldVal == "string") {
                    return $sce.trustAsHtml(fldVal);
                } else { //if date or number or other type we donot need to worry
                    return fldVal;
                }
            },

            getFieldValLink: function(rowLink, fld) {
                var fldName = fld.name; //firstName
                var fldNamePref = "";
                var fldVal = "";

                //ex: if field name is company.name
                if (fldName.split('.').length > 1) {
                    fldNamePref = fldName.split('.')[0]; //company
                    fldNameSuff = fldName.split('.')[1]; //name
                    fldPref = eval('rowLink.$$data.' + fldNamePref);
                    fldVal = fldPref == null ? "" : fldPref[fldNameSuff];
                } else {
                    fldVal = rowLink.$$data[fld.name];
                }

                if (fld.isDate) {
                    if (fldVal) {
                        fldVal = moment(fldVal).format(GlobalCfgService.dateFormat);
                    }
                } else if (fld.isDatetime) {
                    if (fldVal) {
                        fldVal = moment(fldVal).format(GlobalCfgService.dateFormatTime);
                    }
                } else if (fld.iSwitch) {
                    if (fldVal) {
                        fldVal = "<img src='/img/icons/others/tick.svg'>";
                    } else {
                        fldVal = "";
                    }
                } else if (fld.isSelect) {
                    fldVal = "\
                    <md-select ng-model=\"rowLink.type\" placeholder=\"Other\"> \
                        <md-option ng-value=\"type\" \
                            ng-repeat=\"type in ['aaa', 'bbb', 'ccc', 'ddd']\" \
                            ng-bind-html=\"type\"></md-option> \
                    </md-select>";
                }

                if (fld.truncateLength) {
                    fldVal = s(fldVal).prune(fld.truncateLength).value();
                }

                if (fld.hasMediaLink) {
                    fldVal = MediaService.getMediaDownloadLink(rowLink, fldVal);
                } else if (fld.hasDetailLink) {
                    var url = NavigationService.getDetailLinkUrl(rowLink.id);
                    fldVal = "<a href='" + url + "'>" + fldVal + "</a>";
                }

                return fldVal;
            },

            getWidthClass: function(fld) {
                var cssClass = '';
                if (fld.width) {
                    cssClass = 'w' + fld.width; //ex: w20 or w25pc
                }
                // var cssClass = 'w20pc';
                return cssClass;
            },

            editText: function(event, row, fldName) {
                var fldVal;
                //ex: if field name is company.name
                if (fldName.split('.').length > 1) {
                    fldNamePref = fldName.split('.')[0]; //company
                    fldNameSuff = fldName.split('.')[1]; //name
                    fldPref = eval('row.$$data.' + fldNamePref);
                    fldVal = fldPref == null ? "" : fldPref[fldNameSuff];
                } else {
                    fldVal = row.$$data[fldName];
                }

                var editDialog = {
                    modelValue: fldVal,
                    placeholder: 'Please enter text',
                    save: function(input) {
                        // if (input.$modelValue === 'Donald Trump') {
                        //     input.$invalid = true;
                        //     return $q.reject();
                        // }
                        // if (input.$modelValue === 'Bernie Sanders') {
                        //     return row[fldName] = 'FEEL THE BERN!'
                        // }
                        row.$$data[fldName] = input.$modelValue;
                    },
                    targetEvent: event,
                    title: 'Please enter text',
                    validators: {
                        'md-maxlength': 30
                    }
                };

                var promise;

                // if ($scope.options.largeEditDialog) {
                //     promise = $mdEditDialog.large(editDialog);
                // } else {
                //     promise = $mdEditDialog.small(editDialog);
                // }

                promise = $mdEditDialog.small(editDialog);

                promise.then(function(ctrl) {
                    var input = ctrl.getInput();

                    input.$viewChangeListeners.push(function() {
                        input.$setValidity('test', input.$modelValue !== 'test');
                    });
                });

            },
        };

        return ListUtilService;
    }
]);

