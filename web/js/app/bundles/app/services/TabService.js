'use strict';

angular.module('appBundle').factory('TabService', [
    'UtilService',
    function(
        UtilService
    ) {
    /**
     * This class is for showing the tabs within the detail and edit pages
     */
    function TabService(name, opts) {
        var currBundle = UtilService.getCurrBundle();

        this.name = name;
        this.bundle = currBundle;

        this.title = s(name).humanize().titleize().value();
        this.current = null; //shows the tab is currently selected
        this.icon = name + '.svg'; //button icon

        //ex: company.detail.main
        this.stateName = this.getDefaultStateName();

        for (prop in opts) {
            this[prop] = opts[prop];
        }
    }

    TabService.prototype = {
        constructor: TabService,

        addLinksToTab: function(tabs, links) {
            for (i in links) {
                var link = links[i];
                tabs[tabs.length] = new TabService(link.name);
            }
            return tabs;
        },

        getDefaultStateName: function() {
            var currView = UtilService.getCurrView();
            return this.bundle + '.' + currView + '.' + this.name;
        },

        getHoverMessage: function($state) {
            if (!$state.get(this.getDefaultStateName())) {
                return "Please save to enable this";
            }

            return "";
        }
    };

    return TabService;
}]);