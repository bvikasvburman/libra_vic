'use strict';

angular.module('appBundle').factory('ListFldService', function() {
    function ListFldService(name, opts) {
        this.name          = name;
        this.title         = s(name).humanize().titleize().value();
        this.type          = 'text'; //text/number/date/datetime/button/sortFld/switch
        this.hasDetailLink = false;
        this.orderBy       = name;
        this.width         = "auto";
        this.align         = "left";
        //if the model contains key then we display value using this array
        //ex: valuelistName
        this.keyValArr = null;

        for (prop in opts) {
            this[prop] = opts[prop];
        }
        this.class = "col-" + this.align;

        //set field Type properties
        var typesArr = ['text', 'number', 'date', 'datetime', 'button', 'sortFld', 'switch', 'select'];
        for (ind in typesArr) {
            var type = typesArr[ind];
            var propName = 'is' + s(type).capitalize().value(); //isText / isDate etc.
            this[propName] = this.type == type ? true : false;
        }
    }

    ListFldService.prototype = {
        constructor: ListFldService,
        method1: function() { }
    };

    return ListFldService;
});