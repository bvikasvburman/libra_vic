'use strict';

angular.module('appBundle').factory('HotkeyService', [
    '$rootScope',
    '$state',
    'hotkeys',
    'NavigationService',
    'UtilService',
    function (
        $rootScope,
        $state,
        hotkeys,
        NavigationService,
        UtilService
    ) {

        var preventDefaultEvent = function(e) {
            if (e.preventDefault) {
                e.preventDefault();
            } else {
                // internet explorer
                e.returnValue = false;
            }
        };

        return {
            addHotkeys: function($scope) {
                // New Record
                hotkeys.add({
                    combo: 'mod+alt+n',
                    description: 'New record. [list, detail]',
                    callback: function(e) {
                        preventDefaultEvent(e);
                        NavigationService.gotoNewRecord();
                    }
                });

                // Save Record
                hotkeys.add({
                    combo: 'mod+s',
                    description: 'Save record. [new, edit]',
                    allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
                    callback: function(e) {
                        preventDefaultEvent(e);
                        var currView = UtilService.getCurrView();
                        if (currView == 'new') {
                            $rootScope.dbu.addRecord();
                        } else if (currView == 'edit') {
                            $rootScope.dbu.saveRecord();
                        }
                    }
                });

                // Save Record & go back to list
                hotkeys.add({
                    combo: 'mod+shift+s',
                    description: 'Save record and go back to list. [new, edit]',
                    allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
                    callback: function(e) {
                        preventDefaultEvent(e);
                        var currView = UtilService.getCurrView();
                        if (currView == 'new') {
                            $rootScope.dbu.addRecord(true);
                        } else if (currView == 'edit') {
                            $rootScope.dbu.saveRecord(true);
                        }
                    }
                });

                // Save Record & create new
                hotkeys.add({
                    combo: 'mod+shift+alt+s',
                    description: 'Save record and create new. [new, edit]',
                    allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
                    callback: function(e) {
                        preventDefaultEvent(e);
                        var currView = UtilService.getCurrView();
                        if (currView == 'new') {
                            $rootScope.dbu.addRecordToDb();
                            $rootScope.ns.gotoNewRecord();
                            $state.go($state.current, {}, {reload: true});
                        } else if (currView == 'edit') {
                            $rootScope.dbu.saveRecordToDb();
                            $rootScope.ns.gotoNewRecord();
                        }
                    }
                });

                //applyRecord. Save & Continue
                hotkeys.add({
                    combo: 'mod+shift+a',
                    description: 'Save record & Continue Edit. [edit]',
                    allowIn: ['INPUT', 'SELECT', 'TEXTAREA'],
                    callback: function(e) {
                        preventDefaultEvent(e);
                        var currView = UtilService.getCurrView();
                        if (currView == 'edit') {
                            $rootScope.dbu.applyRecord();
                        }
                    }
                });

                // Edit Record
                hotkeys.add({
                    combo: 'mod+e',
                    description: 'Edit record. [detail]',
                    callback: function(e) {
                        preventDefaultEvent(e);
                        NavigationService.gotoEditFromDetail();
                    }
                });

                // Cancel New or Edit
                hotkeys.add({
                    combo: 'mod+shift+c',
                    description: 'Cancel New or Edit record. [new, edit]',
                    callback: function(e) {
                        preventDefaultEvent(e);
                        var currView = UtilService.getCurrView();
                        if (currView == 'new') {
                            $rootScope.dbu.cancelNew();
                        } else if (currView == 'edit') {
                            $rootScope.dbu.cancelEdit();
                        }
                    }
                });

                // Delete record
                hotkeys.add({
                    combo: 'alt+shift+del',
                    description: 'Delete a record. [detail]',
                    callback: function(e) {
                        preventDefaultEvent(e);
                        var currView = UtilService.getCurrView();
                        if (currView == 'detail') {
                            $rootScope.dbu.deleteRecord();
                        }
                    }
                });

                // Go back to list from detail
                hotkeys.add({
                    combo: 'u',
                    description: 'Go back to list. [detail]',
                    callback: function(e) {
                        // preventDefaultEvent(e);
                        NavigationService.gotoList();
                    }
                });
            },

        }
    }
]);