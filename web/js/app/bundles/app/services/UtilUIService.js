'use strict';

angular.module('appBundle').factory('UtilUIService', [
    '$mdDialog',
    function ($mdDialog) {
        return {
            showConfirmDialog: function(ev, opts) {
                var opts = angular.extend({}, {
                   title: 'Confirm!',
                   message: 'Are you sure to continue?',
                   okBtn: 'Yes',
                   cancelBtn: 'Cancel',
                }, opts);
                var confirm = $mdDialog.confirm()
                      .title(opts.title)
                      .textContent(opts.message)
                      .targetEvent(ev)
                      .ok(opts.okBtn)
                      .cancel(opts.cancelBtn);
                return $mdDialog.show(confirm);
            },

            showAlertDialog: function(ev, opts) {
                var opts = angular.extend({}, {
                   title: 'Alert!',
                   message: '',
                   okBtn: 'OK',
                }, opts);
                var alert = $mdDialog.alert()
                      .title(opts.title)
                      .textContent(opts.message)
                      .targetEvent(ev)
                      .ok(opts.okBtn)
                return $mdDialog.show(alert);
            },

            cancelDialog: function() {
                $mdDialog.cancel();
            },

        }
    }
]);