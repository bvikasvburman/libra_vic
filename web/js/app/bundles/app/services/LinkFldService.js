'use strict';

angular.module('appBundle').factory('LinkFldService', function() {
    /**
     * This class is simple
     */
    function LinkFldService(name, opts) {
        this.name           = name;
        // this.orderBy     = name;
        this.type           = 'text'; //text/number/date/datetime/button/sortFld/switch/select
        this.linkName       = null;
        this.hasDetailLink  = false;
        this.hasMediaLink   = false; //for media link records
        this.truncateLength = null; //truncate the display of this field after how many chars
        this.width          = null; //width of the table column. 5 (px) or 5pc (%)
        this.editable       = false; //editable field

        var title = name;
        var nameArr = name.split('.');
        if (nameArr.length > 1) {
            var title = nameArr[1];
        }
        this.title = s(title).humanize().titleize().value();

        for (prop in opts) {
            this[prop] = opts[prop];
        }

        //set field Type properties
        var typesArr = ['text', 'number', 'date', 'datetime', 'button', 'sortFld', 'switch', 'select'];
        for (ind in typesArr) {
            var type = typesArr[ind];
            var propName = 'is' + s(type).capitalize().value(); //isText / isDate etc.
            this[propName] = this.type == type ? true : false;
        }
    }

    LinkFldService.prototype = {
        constructor: LinkFldService,
        method1: function() { }
    };

    return LinkFldService;
});