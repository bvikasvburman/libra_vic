'use strict';

angular.module('appBundle').factory('GanttUtilService', [
    '$mdDialog',
    '$http',
    '$controller',
    'UtilService',
    'UtilUIService',
    function (
        $mdDialog,
        $http,
        $controller,
        UtilService,
        UtilUIService
    ) {
        return {
            getGanttOptions: function(scope, opts) {
                var self = this;
                var opts = angular.extend({}, {
                    fromDate: moment().format('YYYY-MM-DD'),
                    toDate: moment().add(7, 'days').format('YYYY-MM-DD'),
                    ganttName: '',
                }, opts);

                return {
                    daily: true,
                    sortMode: "custom",
                    viewScale: "day",
                    columnWidth: 35,
                    columnMagnet: "column",
                    timeFrameMagnet: true,
                    expandToFit: true,
                    shrinkToFit: false,
                    fromDate: opts.fromDate,
                    toDate: opts.toDate,
                    maxDate: "230",

                    api: function(api) {
                        api.core.on.ready(scope, function() {
                            api.tasks.on.moveBegin(scope, function(task) {
                                $(task.$element).addClass('taskMoving');
                            });
                            api.tasks.on.resizeBegin(scope, function(task) {
                                $(task.$element).addClass('taskMoving');
                            });
                            api.tasks.on.moveEnd(scope, function(task) {
                                self.saveTask(task, opts.ganttName, scope.row.id);
                            });
                            api.tasks.on.resizeEnd(scope, function(task) {
                                //new task created from here after the task draw end
                                self.saveTask(task, opts.ganttName, scope.row.id).then(function(response) {
                                    task.model.data.id = response.data.taskId;
                                });
                            });

                            api.directives.on.new(scope, function(directiveName, directiveScope, element) {
                                if (directiveName === 'ganttTask') {
                                    var task = directiveScope.task;
                                    element.bind('click', function(event) {
                                        event.stopPropagation();
                                        if (!$(element).hasClass('taskMoving')) {
                                            self.editTask(event, task, {scope: scope, ganttName: opts.ganttName});
                                        } else {
                                            $(element).removeClass('taskMoving');
                                        }
                                    });
                                }  else if (directiveName === 'ganttRowLabel') {
                                    var row = directiveScope.row;
                                    //just unbind events to workaround a bug in angular gantt that it lists
                                    //each element two times so that it binds click event
                                    element.unbind('click');
                                    element.bind('click', function(event) {
                                        event.stopPropagation();
                                        self.editRow(event, row, {scope: scope, ganttName: opts.ganttName});
                                    });
                                }
                            });
                        }); //api core on ready

                    }// api
                };

            },

            getGanttData: function(ganttName, ganttId) {
                //api_ganttEnquiry
                var url = Router.getRoutePath('api_gantt' + s.titleize(ganttName), true);
                var request = {
                    method: 'GET',
                    url: url.replace('{ganttId}', ganttId)
                };

                return $http(request).then(function (response) {
                    return response;
                });
            },

            drawTaskFactory: function() {
                return {
                    //prefix with TEMP-. we will get the actual id from db after task saved to the db.
                    // id: 'TEMP-' + UtilService.randomUuid(),
                    name: 'New task',
                    data: {id: ''}, // Name shown on top of each task.
                };
            },

            editTask: function(ev, task, opts) {
                $mdDialog.show({
                    templateUrl: '/get-template/other/gantt/task/edit',
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    controller: GanttTaskDialogController,
                    locals: {
                        data: {
                            task: task,
                            parentScope: opts.scope,
                            ganttId: opts.scope.row.id,
                            ganttName: opts.ganttName,
                        }
                    }
                })
            },

            saveTask: function(task, ganttName, ganttId) {
                var taskModel = {};
                angular.copy(task.model, taskModel);
                // var ganttId = ganttId;
                var taskId = taskModel.data ? taskModel.data.id : '';

                //angular gantt has the fields from & to but our db has fromDate & toDate
                taskModel['fromDate'] = moment(taskModel.from).format('YYYY-MM-DD');
                taskModel['toDate'] = moment(taskModel.to).format('YYYY-MM-DD');
                taskModel['id'] = taskId;
                taskModel['ganttRow'] = {id: task.row.model.id};

                var ganttTaskData = {ganttTask: taskModel};

                //save the task to db
                var url = Router.getRoutePath('api_ganttTask' + s.titleize(ganttName), true);

                var method = 'POST'; //new task
                if (taskId) { //update task
                    url += '/' + taskId;
                    method = 'PUT';
                }
                var request = {
                    method: method,
                    url: url.replace('{ganttId}', ganttId),
                    data: ganttTaskData
                };

                return $http(request).then(function (response) {
                    $mdDialog.hide();
                    return response;
                });
            },

            deleteTask: function(task, ganttName, ganttId, ganttData) {
                var self = this;
                var taskModel = task.model;
                var taskId = taskModel.data.id;

                var url = Router.getRoutePath('api_ganttTask' + s.titleize(ganttName), true)
                        + '/' + taskId;
                var request = {
                    method: 'DELETE',
                    url: url.replace('{ganttId}', ganttId),
                };

                return $http(request).then(function (response) {
                    var ind = self.findTaskIndInData(task, ganttData);
                    ganttData[ind.rowInd].tasks.splice(ind.taskInd, 1);

                    $mdDialog.hide();
                    return response;
                });
            },

            findTaskIndInData: function(task, ganttData) {
                var rowInd  = _.findIndex(ganttData, {id: task.row.model.id});
                var taskInd = _.findIndex(ganttData[rowInd].tasks, {id: task.model.id});
                return {
                    rowInd: rowInd,
                    taskInd: taskInd,
                };
            },

            newGanttRow: function(ev, opts) {
                $mdDialog.show({
                    templateUrl: '/get-template/other/gantt/row/new',
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    controller: GanttRowDialogController,
                    locals: {
                        data: {
                            parentScope: opts.scope,
                            ganttId: opts.scope.row.id,
                            ganttName: opts.ganttName,
                            ganttRow: null,
                        }
                    }
                })
            },

            editRow: function(ev, ganttRow, opts) {
                $mdDialog.show({
                    templateUrl: '/get-template/other/gantt/row/edit',
                    targetEvent: ev,
                    clickOutsideToClose: false,
                    controller: GanttRowDialogController,
                    locals: {
                        data: {
                            parentScope: opts.scope,
                            ganttId: opts.scope.row.id,
                            ganttName: opts.ganttName,
                            ganttRow: ganttRow,
                        }
                    }
                })
            },

            saveRow: function(ganttRowData, ganttName, ganttId) {
                //save the task to db
                var url = Router.getRoutePath('api_ganttRow' + s.titleize(ganttName), true);

                var rowId = ganttRowData.id;

                var method = 'POST'; //new row
                if (ganttRowData.id) { //update row
                    url += '/' + rowId;
                    method = 'PUT';
                }
                var request = {
                    method: method,
                    url: url.replace('{ganttId}', ganttId),
                    data: {ganttRow: ganttRowData}, //just to easily access the ganttRow in the PHP
                };

                return $http(request).then(function (response) {
                    $mdDialog.hide();
                    return response;
                });
            },

            deleteRow: function(ganttRow, ganttName, ganttId, ganttData) {
                var self = this;
                var rowId = ganttRow.model.id;

                var url = Router.getRoutePath('api_ganttRow' + s.titleize(ganttName), true)
                        + '/' + rowId;
                var request = {
                    method: 'DELETE',
                    url: url.replace('{ganttId}', ganttId),
                };

                return $http(request).then(function (response) {
                    var rowInd = _.findIndex(ganttData, {id: rowId});
                    ganttData.splice(rowInd, 1);

                    $mdDialog.hide();
                    return response;
                }, function(response) {
                    UtilUIService.showAlertDialog(null, {'message': response.data.message});
                });
            },
        }
    }
]);