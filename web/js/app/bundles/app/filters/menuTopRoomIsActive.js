angular.module('appBundle')
.filter('menuTopRoomIsActive', ['$state', function($state) {
    return function(topRoom) {
        for (var i in topRoom.rooms) {
            var room = topRoom.rooms[i];

            //if state is company.list then it will return company
            var parentState = room.stateName.split('.')[0];
            if ($state.includes(parentState)) {
                return true;
            }
        }
        return false;
    }
}]);