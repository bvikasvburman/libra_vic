angular.module('userBundle').factory('userConfig', [
    'BundleDefConfig',
    'ListService',
    'ListFldService',
    'UtilService',
    'LinkFldService',
    'TabService',
    function(
        BundleDefConfig,
        ListService,
        ListFldService,
        UtilService,
        LinkFldService,
        TabService
    ) {
        var lf = ListFldService;
        var lnkFld = LinkFldService;

        var listFlds = [
            new lf('firstName', {hasDetailLink: true}),
            new lf('lastName', {hasDetailLink: true}),
            new lf('username'),
            new lf('email'),
        ];

        var links = [
            new LinkService('attachment', {
                fields: [
                    new lnkFld('media.displayTitle', {hasMediaLink: true}),
                ]
            }),
            new LinkService('note', {
                fields: [
                    new lnkFld('note', {truncateLength: 100}),
                    new lnkFld('creationDate', {type: 'datetime', width: '20pc'}),
                ]
            }),
            new LinkService('message'),
        ];

        var tabs = [
            new TabService('main'),
        ];
        tabs = new TabService().addLinksToTab(tabs, links);

        return new BundleDefConfig('user', {
            listObj: new ListService(UtilService.getCurrBundle(), listFlds),
            links: links,
            tabs: tabs,
        })
   }
]);