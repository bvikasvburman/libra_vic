Router.addRoutes([
    /* api */
    {name: 'api_user', url: 'api/user'},
    {name: 'api_user_set_password', url: 'api/users/:id/set-password'},
    {name: 'api_user_photo', url: 'api/users/:id/media/photos'},
    {name: 'api_user_role', url: 'api/user-roles'},
    {name: 'modal_template', url: 'get-template/user/user/set-password'},

    Router.getRouteFromBundle('user'),
    Router.getRouteFromBundle('user.list'),
    Router.getRouteFromBundle('user.detail'),
    Router.getRouteFromBundle('user.new'),
    Router.getRouteFromBundle('user.edit'),

    Router.getRouteFromBundle('user.detail.main'),
    Router.getRouteFromBundle('user.new.main'),
    Router.getRouteFromBundle('user.edit.main'),
]);

