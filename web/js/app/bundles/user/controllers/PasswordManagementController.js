'use strict';

angular.module('userBundle').controller('PasswordManagementController',
['$scope', 'UserRepository', 'user',
    function PasswordManagementController($scope, UserRepository, user) {
        $scope.user = {};
        $scope.error = false;
        
        $scope.save = function () {
            if ($scope.user.password && $scope.user.confirm) {
                if ($scope.user.password === $scope.user.confirm) {
                    UserRepository.setPassword(user, $scope.user).then(function (response) {
                        if (response.data.success) {
                            $scope.close();
                        } else {
                            $scope.error = true;
                        }
                    });
                }
            }
        };
        
        $scope.close = function () {
            $scope.$close();
        };
    }
]);

