'use strict';

angular.module('userBundle').controller('AccessManagementController',
['$scope', 'userRoles', 'sections',
    function AccessManagementController($scope, userRoles, sections) {
        $scope.userRoles = userRoles;
        $scope.sections  = sections;
        
    }
]);

