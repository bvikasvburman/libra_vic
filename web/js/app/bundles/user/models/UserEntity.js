'use strict';

angular.module('userBundle').factory('User', [
    'BaseEntity',
    'Role',
    function (
        BaseEntity,
        Role
    ) {
        function User(data) {
            // inherit BaseEntity constructor
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('username', 'string');
            this.add('email', 'string');
            this.add('firstName', 'string');
            this.add('lastName', 'string');
            this.add('fullName', 'string');
            this.add('sex', 'string');
            this.add('addressStreet', 'string');
            this.add('addressTown', 'string');
            this.add('addressState', 'string');
            this.add('addressCountry', 'string');
            this.add('addressPoCode', 'string');
            this.add('phone', 'string');
            this.add('fax', 'string');
            this.add('mobile', 'string');
            this.add('notes', 'string');
            this.add('published', 'boolean');
            this.add('flag', 'boolean');
            this.add('status', 'string');
            this.add('staffRate', 'number');
            // this.add('role', Role);
        };

        BaseEntity.apply(User);

        return User;
    }
]);