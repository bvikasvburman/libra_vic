'use strict';

angular.module('userBundle').factory('UserRepository', ['BaseRepository', 'User',
    '$http',
    function (BaseRepository, User, $http) {
    
    function UserRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, User, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_user', true));
        
    };
    
    BaseRepository.apply(UserRepository);
    
    UserRepository.prototype.setPassword = function (user, userData) {
        return $http.post(Router.getRoutePath('api_user_set_password', true, {id: user.id}), {
            'userData': userData
        });
    };
    
    return new UserRepository();
}]);