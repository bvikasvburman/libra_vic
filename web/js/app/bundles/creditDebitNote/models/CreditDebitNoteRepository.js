angular.module('creditDebitNoteBundle').factory('CreditDebitNoteRepository', ['BaseRepository', 'CreditDebitNote',
    function (BaseRepository, CreditDebitNote) {

    function CreditDebitNoteRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, CreditDebitNote, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_creditDebitNote', true));
    };

    BaseRepository.apply(CreditDebitNoteRepository);

    return new CreditDebitNoteRepository();
}]);