'use strict';

angular.module('creditDebitNoteBundle').factory('CreditDebitNote', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function CreditDebitNote(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('tradingOrderId', 'number');
            this.add('purchaseOrderId', 'number');
            this.add('companyId', 'number');
            this.add('noteType', 'string');
            this.add('amount', 'number');
            this.add('notes', 'string');
            this.add('status', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(CreditDebitNote);

        return CreditDebitNote;
    }
]);

