'use strict';

angular.module('creditDebitNoteBundle').controller('CreditDebitNoteListController',
    ['$scope', 'rows',
    function CreditDebitNoteListController($scope, rows) {
        $scope.rows = rows;
    }
]);