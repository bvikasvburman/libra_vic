var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_creditDebitNote', url: 'api/creditDebitNote'},
    {name: 'api_creditDebitNote_attachment', url: 'api/creditDebitNote/:id/attachment'},
    {name: 'api_creditDebitNote_attachment_one', url: 'api/creditDebitNote/:id/attachment/:idl'},
    {name: 'api_creditDebitNote_note', url: 'api/creditDebitNote/:id/note'},
    {name: 'api_creditDebitNote_note_one', url: 'api/creditDebitNote/:id/note/:idl'},
    {name: 'api_creditDebitNote_message', url: 'api/creditDebitNote/:id/message'},
    {name: 'api_creditDebitNote_message_one', url: 'api/creditDebitNote/:id/message/:idl'},

    Router.getRouteFromBundle('creditDebitNote'),
    Router.getRouteFromBundle('creditDebitNote.list'),
    Router.getRouteFromBundle('creditDebitNote.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('creditDebitNote.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('creditDebitNote.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('creditDebitNote.detail.main'),
    Router.getRouteFromBundle('creditDebitNote.new.main'),
    Router.getRouteFromBundle('creditDebitNote.edit.main'),

    Router.getRouteFromBundle('creditDebitNote.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('creditDebitNote.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('creditDebitNote.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('creditDebitNote',linksForAutoRouteGen);

