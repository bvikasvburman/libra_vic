var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
];

Router.addRoutes([
    {name: 'api_productCoding', url: 'api/productCoding'},
    {name: 'api_productCoding_note', url: 'api/productCoding/:id/note'},
    {name: 'api_productCoding_note_one', url: 'api/productCoding/:id/note/:idl'},
    {name: 'api_productCoding_message', url: 'api/productCoding/:id/message'},
    {name: 'api_productCoding_message_one', url: 'api/productCoding/:id/message/:idl'},

    Router.getRouteFromBundle('productCoding'),
    Router.getRouteFromBundle('productCoding.list'),
    Router.getRouteFromBundle('productCoding.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('productCoding.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('productCoding.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('productCoding.detail.main'),
    Router.getRouteFromBundle('productCoding.new.main'),
    Router.getRouteFromBundle('productCoding.edit.main'),

    Router.getRouteFromBundle('productCoding.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('productCoding.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('productCoding',linksForAutoRouteGen);

