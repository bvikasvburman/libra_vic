angular.module('productCodingBundle').factory('productCodingConfig', [
    'BundleDefConfig',
    'ListService',
    'ListFldService',
    'UtilService',
    'LinkService',
    'LinkFldService',
    'TabService',
    function(
        BundleDefConfig,
        ListService,
        ListFldService,
        UtilService,
        LinkService,
        LinkFldService,
        TabService
    ) {
        var lf = ListFldService;
        var lnkFld = LinkFldService;

        var listFlds = [
            new lf('productFamily'),
            new lf('productFamilyCode'),
            new lf('productType'),
            new lf('productTypeCode'),
        ];

        var links = [
            new LinkService('note', {
                fields: [
                    new lnkFld('note', {truncateLength: 100}),
                    new lnkFld('creationDate', {type: 'datetime', width: '20pc'}),
                ]
            }),
            new LinkService('message'),
        ];

        var tabs = [
            new TabService('main'),
        ];
        tabs = new TabService().addLinksToTab(tabs, links);

        return new BundleDefConfig('productCoding', {
           listObj: new ListService(UtilService.getCurrBundle(), listFlds),
           links: links,
           tabs: tabs,
        })
   }]
);