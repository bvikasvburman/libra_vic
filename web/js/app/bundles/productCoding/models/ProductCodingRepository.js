angular.module('productCodingBundle').factory('ProductCodingRepository', ['BaseRepository', 'ProductCoding',
    function (BaseRepository, ProductCoding) {

    function ProductCodingRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, ProductCoding, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_productCoding', true));
    };

    BaseRepository.apply(ProductCodingRepository);

    return new ProductCodingRepository();
}]);