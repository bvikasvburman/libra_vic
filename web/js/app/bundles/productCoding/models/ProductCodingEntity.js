'use strict';

angular.module('productCodingBundle').factory('ProductCoding', ['BaseEntity',
    function (BaseEntity) {
    function ProductCoding(data) {
        // inherit BaseEntity constructor
        BaseEntity.constructor.call(this, data);

        this.add('id', 'number');
        this.add('productFamily', 'string');
        this.add('productFamilyCode', 'string');
        this.add('productType', 'string');
        this.add('productTypeCode', 'string');
        this.add('flag', 'number');
        this.add('userIdCreated', 'number');
        this.add('userIdModified', 'number');
    };

    BaseEntity.apply(ProductCoding);

    return ProductCoding;
}]);

