'use strict';

angular.module('productCodingBundle').controller('ProductCodingEditController', [
    '$scope',
    '$rootScope',
    'row',
    'noteLnk',
    'messageLnk',
    'ProductCodingRepository',
    'productCodingConfig',
    'UtilService',
    'ValuelistRepository',
    function ProductCodingEditController(
        $scope,
        $rootScope,
        row,
        noteLnk,
        messageLnk,
        ProductCodingRepository,
        productCodingConfig,
        UtilService,
        ValuelistRepository
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.vls.productFamily = [];

        $scope.links.noteLnk    = noteLnk;
        $scope.links.messageLnk = messageLnk;

        ValuelistRepository.findBy({valuelistName: 'productFamily'}, 'vl')
        .then(function (records) {
            angular.copy(records, $scope.vls.productFamily);
        });
    }
]);