'use strict';

angular.module('productCodingBundle').controller('ProductCodingListController',
    ['$scope', 'rows',
    function ProductCodingListController($scope, rows) {
        $scope.rows = rows;
    }
]);