angular.module('messageBundle').factory('MessageRepository', ['BaseRepository', 'Message',
    function (BaseRepository, Message) {

    function MessageRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Message, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_message', true));
    };

    BaseRepository.apply(MessageRepository);

    return new MessageRepository();
}]);