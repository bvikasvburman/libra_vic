'use strict';

angular.module('messageBundle').factory('Message', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function Message(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('subject', 'string');
            this.add('message', 'string');
            this.add('module', 'string');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Message);

        return Message;
    }
]);
