var links = [
    Router.getLinkObjForRoute('attachment'),
];

Router.addRoutes([
    /* api */
    {name: 'api_message', url: 'api/message'},

    Router.getRouteFromBundle('message'),
    Router.getRouteFromBundle('message.list'),
    Router.getRouteFromBundle('message.detail', {links: links}),
    Router.getRouteFromBundle('message.new', {links: links}),
    Router.getRouteFromBundle('message.edit', {links: links}),

    Router.getRouteFromBundle('message.detail.main'),
    Router.getRouteFromBundle('message.new.main'),
    Router.getRouteFromBundle('message.edit.main'),
]);

Router.createRoutesForLinks('message', links);