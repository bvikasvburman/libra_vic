'use strict';

angular.module('messageBundle').controller('MessageListController',
    ['$scope', 'rows',
    function MessageListController($scope, rows) {
        $scope.rows = rows;
    }
]);