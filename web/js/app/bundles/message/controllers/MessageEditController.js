'use strict';

angular.module('messageBundle').controller('MessageEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'MessageRepository',
    'messageConfig',
    'UtilService',
    function MessageEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        MessageRepository,
        messageConfig,
        UtilService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.attachmentLnk = attachmentLnk;
    }
]);