'use strict';

angular.module('accountsReceivableBundle').controller('AccountsReceivableListController',
    ['$scope', 'rows',
    function AccountsReceivableListController($scope, rows) {
        $scope.rows = rows;
    }
]);