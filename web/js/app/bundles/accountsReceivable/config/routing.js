var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_accountsReceivable', url: 'api/accountsReceivable'},
    {name: 'api_accountsReceivable_attachment', url: 'api/accountsReceivable/:id/attachment'},
    {name: 'api_accountsReceivable_attachment_one', url: 'api/accountsReceivable/:id/attachment/:idl'},
    {name: 'api_accountsReceivable_note', url: 'api/accountsReceivable/:id/note'},
    {name: 'api_accountsReceivable_note_one', url: 'api/accountsReceivable/:id/note/:idl'},
    {name: 'api_accountsReceivable_message', url: 'api/accountsReceivable/:id/message'},
    {name: 'api_accountsReceivable_message_one', url: 'api/accountsReceivable/:id/message/:idl'},

    Router.getRouteFromBundle('accountsReceivable'),
    Router.getRouteFromBundle('accountsReceivable.list'),
    Router.getRouteFromBundle('accountsReceivable.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('accountsReceivable.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('accountsReceivable.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('accountsReceivable.detail.main'),
    Router.getRouteFromBundle('accountsReceivable.new.main'),
    Router.getRouteFromBundle('accountsReceivable.edit.main'),

    Router.getRouteFromBundle('accountsReceivable.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('accountsReceivable.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('accountsReceivable.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('accountsReceivable',linksForAutoRouteGen);

