angular.module('accountsReceivableBundle').factory('AccountsReceivableRepository', ['BaseRepository', 'AccountsReceivable',
    function (BaseRepository, AccountsReceivable) {

    function AccountsReceivableRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, AccountsReceivable, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_accountsReceivable', true));
    };

    BaseRepository.apply(AccountsReceivableRepository);

    return new AccountsReceivableRepository();
}]);