'use strict';

angular.module('companyBundle').controller('CompanyEditController', [
    '$scope',
    '$rootScope',
    '$injector',
    'row',
    'contactLnk',
    'branchLnk',
    'attachmentLnk',
    'enquiryLnk',
    'quotationLnk',
    'tradingOrderLnk',
    'salesConfirmationLnk',
    'purchaseOrderLnk',
    'invoiceLnk',
    'claimLnk',
    'creditDebitNoteLnk',
    'noteLnk',
    'messageLnk',
    'CompanyRepository',
    'ContactRepository',
    'FileUploader',
    'companyConfig',
    'UtilService',
    'MediaService',
    '$mdDialog',
    function CompanyEditController(
        $scope,
        $rootScope,
        $injector,
        row,
        contactLnk,
        branchLnk,
        attachmentLnk,
        enquiryLnk,
        quotationLnk,
        tradingOrderLnk,
        salesConfirmationLnk,
        purchaseOrderLnk,
        invoiceLnk,
        claimLnk,
        creditDebitNoteLnk,
        noteLnk,
        messageLnk,
        CompanyRepository,
        ContactRepository,
        FileUploader,
        companyConfig,
        UtilService,
        MediaService,
        $mdDialog
    ) {
        $scope.currentUser = cpSym.user;

        var bundle = 'company';
        var link = 'note';
        var link = 'message';
        var entityName = s.capitalize(link);

        $scope.rowLnk = new ($injector.get(entityName));

        $scope.newMessage = function () {
            var reposLinkObj = $injector.get(entityName+'Repository'); //ex: new MediaHistoryRepository()
            var excludedFields = ['creationDate', 'modificationDate'];

            $scope.rowLnk[bundle] = row;
            $scope.rowLnk.userModified = {
                user: {id: 1}
            };

            var extraParams = {
                bundle: bundle,
                link: link,
                bundleEntityId: row.id,
            };
            reposLinkObj.save($scope.rowLnk, {
                wrapper: link,
                excludedFields: excludedFields,
                extraParams: extraParams,
            }).then(function(linkRow) {
                var linkArrName = link + "Lnk";
                var linkArr = $rootScope.links[linkArrName];
                linkArr[linkArr.length] = linkRow;
                $scope.rowLnk = new ($injector.get(entityName));
            }, function() {
                alert('Error saving. Please try again!');
            });
        };



        var alert;
        $scope.alerta = 'No changes';

        $scope.recipients = [
            {firstName: 'John'},
            {firstName: 'Hameed'}
        ];

        $scope.addRecipients = function($event) {
            var parentEl = angular.element(document.body);
            $mdDialog.show({
                parent: parentEl,
                targetEvent: $event,
                template:
                '<md-dialog aria-label="List dialog">' +
                '  <md-dialog-content>'+
                '    <md-list>'+
                '      <md-list-item ng-repeat="item in recipients">'+
                '       <p>Number {{item}}</p>' +
                '    </md-list-item></md-list>'+
                '  </md-dialog-content>' +
                '  <md-dialog-actions>' +
                '    <md-button ng-click="closeDialog()" class="md-primary">' +
                '      Close Dialog' +
                '    </md-button>' +
                '  </md-dialog-actions>' +
                '</md-dialog>',
                locals: {
                    recipients: $scope.recipients
                },
                controller: DialogController
            })
            .then(function() {
                $scope.alerta = 'You said the information was';
            }, function() {
                $scope.alerta = 'You cancelled the dialog.';
            });

            function DialogController($scope, $mdDialog, recipients) {
                $scope.recipients = recipients;
                $scope.closeDialog = function() {
                    $scope.recipients.push({firstName: 'Vyacheslav'});
                    $mdDialog.hide();
                }
            }
        }




        UtilService.initializeCommonControllerVars($scope, row);
        $scope.vls.categories = ['Client', 'Supplier'];

        //set the links here
        $scope.links.contactLnk           = contactLnk;
        $scope.links.branchLnk            = branchLnk;
        $scope.links.attachmentLnk        = attachmentLnk;
        $scope.links.enquiryLnk           = enquiryLnk;
        $scope.links.quotationLnk         = quotationLnk;
        $scope.links.tradingOrderLnk      = tradingOrderLnk;
        $scope.links.salesConfirmationLnk = salesConfirmationLnk;
        $scope.links.purchaseOrderLnk     = purchaseOrderLnk;
        $scope.links.invoiceLnk           = invoiceLnk;
        $scope.links.claimLnk             = claimLnk;
        $scope.links.creditDebitNoteLnk   = creditDebitNoteLnk;
        $scope.links.noteLnk              = noteLnk;
        $scope.links.messageLnk           = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);