'use strict';

angular.module('companyBundle').controller('CompanyListController',
    ['$scope', 'rows',
    function CompanyListController($scope, rows) {
        $scope.rows = rows;
    }
]);