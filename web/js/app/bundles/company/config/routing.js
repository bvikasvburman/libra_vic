//we are setting this links for just auto generation of routes.
//FYI, the routes are generated during config phase (start up stage) of the app.
//we still need set the links in config/config.js file for the actual link object and properties

var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('contact'),
    getLOFR('branch'),
    getLOFR('enquiry'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
    getLOFR('quotation'),
    getLOFR('tradingOrder'),
    getLOFR('salesConfirmation'),
    getLOFR('purchaseOrder'),
    getLOFR('manufacturingOrder'),
    getLOFR('invoice'),
    getLOFR('claim'),
    getLOFR('creditDebitNote'),
    getLOFR('note'),
    getLOFR('message'),
];

Router.addRoutes([
    {name: 'api_company', url: 'api/company'},
    {name: 'api_company_contact', url: 'api/company/:id/contact'},
    {name: 'api_company_contact_one', url: 'api/company/:id/contact/:idl'},
    {name: 'api_company_attachment', url: 'api/company/:id/attachment'},
    {name: 'api_company_attachment_one', url: 'api/company/:id/attachment/:idl'},
    {name: 'api_company_branch', url: 'api/company/:id/branch'},
    {name: 'api_company_branch_one', url: 'api/company/:id/branch/:idl'},
    {name: 'api_company_enquiry', url: 'api/company/:id/enquiry'},
    {name: 'api_company_enquiry_one', url: 'api/company/:id/enquiry/:idl'},
    {name: 'api_company_quotation', url: 'api/company/:id/quotation'},
    {name: 'api_company_quotation_one', url: 'api/company/:id/quotation/:idl'},
    {name: 'api_company_tradingOrder', url: 'api/company/:id/tradingOrder'},
    {name: 'api_company_tradingOrder_one', url: 'api/company/:id/tradingOrder/:idl'},
    {name: 'api_company_salesConfirmation', url: 'api/company/:id/salesConfirmation'},
    {name: 'api_company_salesConfirmation_one', url: 'api/company/:id/salesConfirmation/:idl'},
    {name: 'api_company_purchaseOrder', url: 'api/company/:id/purchaseOrder'},
    {name: 'api_company_purchaseOrder_one', url: 'api/company/:id/purchaseOrder/:idl'},
    {name: 'api_company_manufacturingOrder', url: 'api/company/:id/manufacturingOrder'},
    {name: 'api_company_manufacturingOrder_one', url: 'api/company/:id/manufacturingOrder/:idl'},
    {name: 'api_company_invoice', url: 'api/company/:id/invoice'},
    {name: 'api_company_invoice_one', url: 'api/company/:id/invoice/:idl'},
    {name: 'api_company_claim', url: 'api/company/:id/claim'},
    {name: 'api_company_claim_one', url: 'api/company/:id/claim/:idl'},
    {name: 'api_company_creditDebitNote', url: 'api/company/:id/creditDebitNote'},
    {name: 'api_company_creditDebitNote_one', url: 'api/company/:id/creditDebitNote/:idl'},
    {name: 'api_company_note', url: 'api/company/:id/note'},
    {name: 'api_company_note_one', url: 'api/company/:id/note/:idl'},
    {name: 'api_company_message', url: 'api/company/:id/message'},
    {name: 'api_company_message_one', url: 'api/company/:id/message/:idl'},

    Router.getRouteFromBundle('company'),
    Router.getRouteFromBundle('company.list'),
    Router.getRouteFromBundle('company.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('company.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('company.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('company.detail.main'),
    Router.getRouteFromBundle('company.new.main'),
    Router.getRouteFromBundle('company.edit.main'),

    Router.getRouteFromBundle('company.edit.contact.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'contact')}),
    Router.getRouteFromBundle('company.edit.contact.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'contact')}),

    Router.getRouteFromBundle('company.edit.branch.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'branch')}),
    Router.getRouteFromBundle('company.edit.branch.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'branch')}),

    Router.getRouteFromBundle('company.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('company.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('company.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('company', linksForAutoRouteGen);

