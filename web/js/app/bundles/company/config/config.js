angular.module('companyBundle').factory('companyConfig', [
    'BundleDefConfig',
    'ListService',
    'ListFldService',
    'UtilService',
    'LinkService',
    'LinkFldService',
    'TabService',
    function(
        BundleDefConfig,
        ListService,
        ListFldService,
        UtilService,
        LinkService,
        LinkFldService,
        TabService
    ) {
        var lf = ListFldService;
        var lnkFld = LinkFldService;

        var listFlds = [
            new lf('code', {hasDetailLink: true}),
            new lf('name', {title: 'Company Name', hasDetailLink: true}),
            new lf('category'),
            new lf('phone'),
            new lf('email'),
            new lf('website'),
        ];

        var links = [
            new LinkService('contact', {
                fields: [
                    new lnkFld('firstName', {hasDetailLink: true}),
                    new lnkFld('lastName', {hasDetailLink: true}),
                    new lnkFld('email'),
                    new lnkFld('phone'),
                ]
            }),
            new LinkService('branch', {
                fields: [
                    new lnkFld('name'),
                    new lnkFld('address'),
                    new lnkFld('phone'),
                    new lnkFld('email'),
                ]
            }),
            new LinkService('enquiry', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code'),
                    new lnkFld('description'),
                    new lnkFld('enquiryDate', {type: 'date'}),
                    new lnkFld('status'),
                ]
            }),
            new LinkService('attachment', {
                fields: [
                    new lnkFld('media.displayTitle', {hasMediaLink: true}),
                ]
            }),
            new LinkService('quotation', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'Quotation Code'}),
                    new lnkFld('enquiry.code', {title: 'Enquiry Code'}),
                    new lnkFld('enquiry.description', {title: 'Enquiry Description'}),
                    new lnkFld('quotationDate', {type: 'date'}),
                    new lnkFld('status'),
                ]
            }),
            new LinkService('tradingOrder', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'Order Code'}),
                    new lnkFld('quotation.code', {title: 'Quotation Code'}),
                    new lnkFld('clientOrderRef'),
                    new lnkFld('orderDetails'),
                    new lnkFld('etd', {type: 'date'}),
                    new lnkFld('orderDate', {type: 'date'}),
                    new lnkFld('status'),
                ]
            }),
            new LinkService('salesConfirmation', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'S/C Code'}),
                    new lnkFld('tradingOrder.code', {title: 'T/O Code'}),
                    new lnkFld('status'),
                    new lnkFld('expectedDateDeparture', {type: 'date', title: 'ETD'}),
                    new lnkFld('amount', {type: 'number'}),
                    new lnkFld('creationDate', {type: 'date'}),
                ]
            }),
            new LinkService('purchaseOrder', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'P/O Code'}),
                    new lnkFld('tradingOrder.code', {title: 'T/O Code'}),
                    new lnkFld('status'),
                    new lnkFld('requiredDeliveryDate', {type: 'date'}),
                    new lnkFld('amount', {type: 'number'}),
                    new lnkFld('creationDate', {type: 'date'}),
                ]
            }),
            new LinkService('manufacturingOrder', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'M/O Code'}),
                    new lnkFld('tradingOrder.code', {title: 'T/O Code'}),
                    new lnkFld('status'),
                    new lnkFld('requiredDeliveryDate', {type: 'date'}),
                    new lnkFld('amount', {type: 'number'}),
                    new lnkFld('creationDate', {type: 'date'}),
                ]
            }),
            new LinkService('invoice', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'Invoice Code'}),
                    new lnkFld('tradingOrder.code', {title: 'T/O Code'}),
                    new lnkFld('status'),
                    new lnkFld('sailingDate', {type: 'date'}),
                    new lnkFld('amount', {type: 'number'}),
                    new lnkFld('creationDate', {type: 'date'}),
                ]
            }),
            new LinkService('claim', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'Claim Code'}),
                    new lnkFld('title'),
                    new lnkFld('description'),
                    new lnkFld('tradingOrder.orderDate'),
                    new lnkFld('entertainClaim'),
                    new lnkFld('creationDate', {type: 'date'}),
                ]
            }),
            new LinkService('creditDebitNote', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('tradingOrder.code', {title: 'T/O Code'}),
                    new lnkFld('currency'),
                    new lnkFld('amount', {type: 'number'}),
                    new lnkFld('noteType'),
                    new lnkFld('tradingOrder.orderDate'),
                    new lnkFld('status'),
                    new lnkFld('creationDate', {type: 'date'}),
                ]
            }),
            new LinkService('note', {
                fields: [
                    new lnkFld('note', {truncateLength: 100}),
                    new lnkFld('creationDate', {type: 'datetime', width: '20pc'}),
                ]
            }),
            new LinkService('message'),
        ];

        var tabs = [
            new TabService('main'),
        ];
        tabs = new TabService().addLinksToTab(tabs, links);

        return new BundleDefConfig('company', {
           listObj: new ListService(UtilService.getCurrBundle(), listFlds),
           links: links,
           tabs: tabs,
           status: {
               status1: 'Status 1',
               status2: 'Status 2',
               status3: 'Status 3',
               status4: 'Status 4',
               status5: 'Status 5',
               status6: 'Status 6'
           },
        })
   }
]);