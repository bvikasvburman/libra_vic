angular.module('companyBundle').factory('CompanyRepository', [
    'BaseRepository',
    'Company',
    function (
        BaseRepository,
        Company
    ) {

    function CompanyRepository(data) {
        BaseRepository.constructor.call(this, Company, data);
        this.setApiURL(Router.getRoutePath('api_company', true));
    };

    BaseRepository.apply(CompanyRepository);

    return new CompanyRepository();
}]);