'use strict';

angular.module('companyBundle').factory('Company', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function Company(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('name', 'string');
            this.add('category', 'string');
            this.add('phone', 'string');
            this.add('email', 'string');
            this.add('address', 'string');
            this.add('city', 'string');
            this.add('state', 'string');
            this.add('zip', 'string');
            this.add('country', 'string');
            this.add('fax', 'string');
            this.add('website', 'string');
            this.add('city', 'string');
            this.add('addressDelivery', 'string');
            this.add('paymentTerms', 'string');
            this.add('deliveryTerms', 'string');
            this.add('portOfDelivery', 'string');
            this.add('flag', 'string');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Company);

        return Company;
    }
]);