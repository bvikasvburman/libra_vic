angular.module('companyBundle').factory('BranchRepository', [
    'BaseRepository',
    'Branch',
    function (
        BaseRepository,
        Branch
    ) {

    function BranchRepository(data) {
        BaseRepository.constructor.call(this, Branch, data);
    };

    BaseRepository.apply(BranchRepository);

    return new BranchRepository();
}]);