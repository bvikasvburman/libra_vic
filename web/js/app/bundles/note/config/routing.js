var links = [

];

Router.addRoutes([
    /* api */
    {name: 'api_note', url: 'api/note'},

    Router.getRouteFromBundle('note'),
    Router.getRouteFromBundle('note.list'),
    Router.getRouteFromBundle('note.detail', {links: links}),
    Router.getRouteFromBundle('note.new', {links: links}),
    Router.getRouteFromBundle('note.edit', {links: links}),

    Router.getRouteFromBundle('note.detail.main'),
    Router.getRouteFromBundle('note.new.main'),
    Router.getRouteFromBundle('note.edit.main'),
]);

Router.createRoutesForLinks('note', links);