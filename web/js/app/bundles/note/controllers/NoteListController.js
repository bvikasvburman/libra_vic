'use strict';

angular.module('noteBundle').controller('NoteListController',
    ['$scope', 'rows',
    function NoteListController($scope, rows) {
        $scope.rows = rows;
    }
]);