'use strict';

angular.module('noteBundle').controller('NoteEditController', [
    '$scope',
    '$rootScope',
    'row',

    'NoteRepository',
    'noteConfig',
    'UtilService',
    function NoteEditController(
        $scope,
        $rootScope,
        row,

        NoteRepository,
        noteConfig,
        UtilService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        //set the links array here

    }
]);