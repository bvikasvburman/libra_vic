angular.module('noteBundle').factory('NoteRepository', ['BaseRepository', 'Note',
    function (BaseRepository, Note) {

    function NoteRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Note, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_note', true));
    };

    BaseRepository.apply(NoteRepository);

    return new NoteRepository();
}]);