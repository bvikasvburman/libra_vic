'use strict';

angular.module('noteBundle').factory('Note', [
    'BaseEntity',
    function (
        BaseEntity
    ) {
        function Note(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('note', 'string');
            this.add('module', 'string');
            this.add('relRecordId', 'string');
            this.add('userIdCreated', 'number');
            this.add('userIdModified', 'number');
        };
        BaseEntity.apply(Note);
        return Note;
    }
]);

