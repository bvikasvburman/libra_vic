var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_colorMaster', url: 'api/colorMaster'},
    {name: 'api_colorMaster_attachment', url: 'api/colorMaster/:id/attachment'},
    {name: 'api_colorMaster_attachment_one', url: 'api/colorMaster/:id/attachment/:idl'},
    {name: 'api_colorMaster_note', url: 'api/colorMaster/:id/note'},
    {name: 'api_colorMaster_note_one', url: 'api/colorMaster/:id/note/:idl'},
    {name: 'api_colorMaster_message', url: 'api/colorMaster/:id/message'},
    {name: 'api_colorMaster_message_one', url: 'api/colorMaster/:id/message/:idl'},

    Router.getRouteFromBundle('colorMaster'),
    Router.getRouteFromBundle('colorMaster.list'),
    Router.getRouteFromBundle('colorMaster.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('colorMaster.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('colorMaster.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('colorMaster.detail.main'),
    Router.getRouteFromBundle('colorMaster.new.main'),
    Router.getRouteFromBundle('colorMaster.edit.main'),

    Router.getRouteFromBundle('colorMaster.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('colorMaster.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('colorMaster.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('colorMaster',linksForAutoRouteGen);