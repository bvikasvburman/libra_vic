'use strict';

angular.module('colorMasterBundle').controller('ColorMasterListController',
    ['$scope', 'rows',
    function ColorMasterListController($scope, rows) {
        $scope.rows = rows;
    }
]);