'use strict';

angular.module('colorMasterBundle').controller('ColorMasterEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'ColorMasterRepository',
    'colorMasterConfig',
    'UtilService',
    'MediaService',
    function ColorMasterEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        ColorMasterRepository,
        colorMasterConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.attachmentLnk = attachmentLnk;
        $scope.links.noteLnk       = noteLnk;
        $scope.links.messageLnk    = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);