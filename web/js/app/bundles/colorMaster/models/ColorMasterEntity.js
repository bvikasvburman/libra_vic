'use strict';

angular.module('colorMasterBundle').factory('ColorMaster', [
    'BaseEntity',
    'User',
    'ProtoTypeColorMaster',
    function (
        BaseEntity,
        User,
        ProtoTypeColorMaster
    ) {
        function ColorMaster(data) {
            // inherit BaseEntity constructor
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('title', 'string');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('protoTypeColorMaster', ProtoTypeColorMaster);
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(ColorMaster);

        return ColorMaster;
    }
]);

