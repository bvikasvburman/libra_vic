angular.module('colorMasterBundle').factory('ColorMasterRepository', [
    'BaseRepository',
    'ColorMaster',
    function (
        BaseRepository,
        ColorMaster
    ) {
        function ColorMasterRepository(data) {
            // inherit BaseRepository constructor
            BaseRepository.constructor.call(this, ColorMaster, data);
            // set api url
            this.setApiURL(Router.getRoutePath('api_colorMaster', true));
        };

        BaseRepository.apply(ColorMasterRepository);

        return new ColorMasterRepository();
    }
]);