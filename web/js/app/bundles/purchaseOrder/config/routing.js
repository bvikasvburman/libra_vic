var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_purchaseOrder', url: 'api/purchaseOrder'},
    {name: 'api_purchaseOrder_attachment', url: 'api/purchaseOrder/:id/attachment'},
    {name: 'api_purchaseOrder_attachment_one', url: 'api/purchaseOrder/:id/attachment/:idl'},
    {name: 'api_purchaseOrder_note', url: 'api/purchaseOrder/:id/note'},
    {name: 'api_purchaseOrder_note_one', url: 'api/purchaseOrder/:id/note/:idl'},
    {name: 'api_purchaseOrder_message', url: 'api/purchaseOrder/:id/message'},
    {name: 'api_purchaseOrder_message_one', url: 'api/purchaseOrder/:id/message/:idl'},

    Router.getRouteFromBundle('purchaseOrder'),
    Router.getRouteFromBundle('purchaseOrder.list'),
    Router.getRouteFromBundle('purchaseOrder.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('purchaseOrder.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('purchaseOrder.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('purchaseOrder.detail.main'),
    Router.getRouteFromBundle('purchaseOrder.new.main'),
    Router.getRouteFromBundle('purchaseOrder.edit.main'),

    Router.getRouteFromBundle('purchaseOrder.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('purchaseOrder.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('purchaseOrder.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('purchaseOrder',linksForAutoRouteGen);

