'use strict';

angular.module('purchaseOrderBundle').controller('PurchaseOrderListController',
    ['$scope', 'rows',
    function PurchaseOrderListController($scope, rows) {
        $scope.rows = rows;
    }
]);