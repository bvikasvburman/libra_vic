angular.module('purchaseOrderBundle').factory('PurchaseOrderRepository', ['BaseRepository', 'PurchaseOrder',
    function (BaseRepository, PurchaseOrder) {

    function PurchaseOrderRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, PurchaseOrder, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_purchaseOrder', true));
    };

    BaseRepository.apply(PurchaseOrderRepository);

    return new PurchaseOrderRepository();
}]);