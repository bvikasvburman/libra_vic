angular.module('quotationBundle').factory('quotationConfig', [
    'BundleDefConfig',
    'ListService',
    'ListFldService',
    'UtilService',
    'LinkService',
    'LinkFldService',
    'TabService',
    function(
        BundleDefConfig,
        ListService,
        ListFldService,
        UtilService,
        LinkService,
        LinkFldService,
        TabService
    ) {
        var lf = ListFldService;
        var lnkFld = LinkFldService;

        var listFlds = [
            new lf('code'),
            new lf('enquiry.code', {title: 'Enquiry Code'}),
            new lf('company.code', {title: 'Company Code'}),
            new lf('company.name', {title: 'Company Name'}),
            new lf('enquiry.description', {title: 'Enquiry Details'}),
            new lf('quoteDate'),
            new lf('status'),
        ];

        var links = [
            new LinkService('attachment', {
                fields: [
                    new lnkFld('media.displayTitle', {hasMediaLink: true}),
                ]
            }),
            new LinkService('note', {
                fields: [
                    new lnkFld('note', {truncateLength: 100}),
                    new lnkFld('creationDate', {type: 'datetime', width: '20pc'}),
                ]
            }),
            new LinkService('message'),
        ];

        var tabs = [
            new TabService('main'),
        ];
        tabs = new TabService().addLinksToTab(tabs, links);

        return new BundleDefConfig('quotation', {
           listObj: new ListService(UtilService.getCurrBundle(), listFlds),
           links: links,
           tabs: tabs,
        })
   }
]);