var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_quotation', url: 'api/quotation'},
    {name: 'api_quotation_attachment', url: 'api/quotation/:id/attachment'},
    {name: 'api_quotation_attachment_one', url: 'api/quotation/:id/attachment/:idl'},
    {name: 'api_quotation_note', url: 'api/quotation/:id/note'},
    {name: 'api_quotation_note_one', url: 'api/quotation/:id/note/:idl'},
    {name: 'api_quotation_message', url: 'api/quotation/:id/message'},
    {name: 'api_quotation_message_one', url: 'api/quotation/:id/message/:idl'},

    Router.getRouteFromBundle('quotation'),
    Router.getRouteFromBundle('quotation.list'),
    Router.getRouteFromBundle('quotation.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('quotation.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('quotation.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('quotation.detail.main'),
    Router.getRouteFromBundle('quotation.new.main'),
    Router.getRouteFromBundle('quotation.edit.main'),

    Router.getRouteFromBundle('quotation.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('quotation.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('quotation.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('quotation',linksForAutoRouteGen);
