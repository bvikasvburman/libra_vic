'use strict';

angular.module('quotationBundle').factory('Quotation', [
    'BaseEntity',
    'User',
    'Enquiry',
    'Company',
    function (
        BaseEntity,
        User,
        Enquiry,
        Company
    ) {
        function Quotation(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('companyId', 'number');
            this.add('contactId', 'string');
            this.add('salesDeliveryTerms', 'string');
            this.add('portOfDelivery', 'string');
            this.add('shippingMedium', 'string');
            this.add('offerValidity', 'number');
            this.add('salesPaymentTerms', 'string');
            this.add('leadTime', 'number');
            this.add('quotationDate', 'date');
            this.add('status', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('enquiry', Enquiry);
            this.add('company', Company);
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Quotation);

        return Quotation;
    }
]);

