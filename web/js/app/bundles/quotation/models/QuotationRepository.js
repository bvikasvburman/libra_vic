angular.module('quotationBundle').factory('QuotationRepository', ['BaseRepository', 'Quotation',
    function (BaseRepository, Quotation) {

    function QuotationRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Quotation, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_quotation', true));
    };

    BaseRepository.apply(QuotationRepository);

    return new QuotationRepository();
}]);