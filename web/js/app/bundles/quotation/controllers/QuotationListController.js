'use strict';

angular.module('quotationBundle').controller('QuotationListController',
    ['$scope', 'rows',
    function QuotationListController($scope, rows) {
        $scope.rows = rows;
    }
]);