var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_masterPerformance', url: 'api/masterPerformance'},
    {name: 'api_masterPerformance_attachment', url: 'api/masterPerformance/:id/attachment'},
    {name: 'api_masterPerformance_attachment_one', url: 'api/masterPerformance/:id/attachment/:idl'},
    {name: 'api_masterPerformance_note', url: 'api/masterPerformance/:id/note'},
    {name: 'api_masterPerformance_note_one', url: 'api/masterPerformance/:id/note/:idl'},
    {name: 'api_masterPerformance_message', url: 'api/masterPerformance/:id/message'},
    {name: 'api_masterPerformance_message_one', url: 'api/masterPerformance/:id/message/:idl'},

    Router.getRouteFromBundle('masterPerformance'),
    Router.getRouteFromBundle('masterPerformance.list'),
    Router.getRouteFromBundle('masterPerformance.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('masterPerformance.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('masterPerformance.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('masterPerformance.detail.main'),
    Router.getRouteFromBundle('masterPerformance.new.main'),
    Router.getRouteFromBundle('masterPerformance.edit.main'),

    Router.getRouteFromBundle('masterPerformance.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('masterPerformance.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('masterPerformance.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),

]);

Router.createRoutesForLinks('masterPerformance',linksForAutoRouteGen);
