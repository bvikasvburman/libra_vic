angular.module('masterPerformanceBundle').factory('MasterPerformanceRepository', ['BaseRepository', 'MasterPerformance',
    function (BaseRepository, MasterPerformance) {

    function MasterPerformanceRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, MasterPerformance, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_masterPerformance', true));
    };

    BaseRepository.apply(MasterPerformanceRepository);

    return new MasterPerformanceRepository();
}]);