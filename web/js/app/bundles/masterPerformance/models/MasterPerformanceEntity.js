'use strict';

angular.module('masterPerformanceBundle').factory('MasterPerformance', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function MasterPerformance(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('part', 'string');
            this.add('performance', 'string');
            this.add('standard', 'string');
            this.add('profGuarValue', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(MasterPerformance);

        return MasterPerformance;
    }
]);

