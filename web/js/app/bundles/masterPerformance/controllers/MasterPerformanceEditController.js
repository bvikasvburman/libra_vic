'use strict';

angular.module('masterPerformanceBundle').controller('MasterPerformanceEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'MasterPerformanceRepository',
    'masterPerformanceConfig',
    'UtilService',
    'MediaService',
    function MasterPerformanceEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        MasterPerformanceRepository,
        masterPerformanceConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.attachmentLnk   = attachmentLnk;
        $scope.links.noteLnk    = noteLnk;
        $scope.links.messageLnk = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);