'use strict';

angular.module('masterPerformanceBundle').controller('MasterPerformanceListController',
    ['$scope', 'rows',
    function MasterPerformanceListController($scope, rows) {
        $scope.rows = rows;
    }
]);