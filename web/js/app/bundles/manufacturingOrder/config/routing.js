var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_manufacturingOrder', url: 'api/manufacturingOrder'},
    {name: 'api_manufacturingOrder_attachment', url: 'api/manufacturingOrder/:id/attachment'},
    {name: 'api_manufacturingOrder_attachment_one', url: 'api/manufacturingOrder/:id/attachment/:idl'},
    {name: 'api_manufacturingOrder_note', url: 'api/manufacturingOrder/:id/note'},
    {name: 'api_manufacturingOrder_note_one', url: 'api/manufacturingOrder/:id/note/:idl'},
    {name: 'api_manufacturingOrder_message', url: 'api/manufacturingOrder/:id/message'},
    {name: 'api_manufacturingOrder_message_one', url: 'api/manufacturingOrder/:id/message/:idl'},

    Router.getRouteFromBundle('manufacturingOrder'),
    Router.getRouteFromBundle('manufacturingOrder.list'),
    Router.getRouteFromBundle('manufacturingOrder.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('manufacturingOrder.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('manufacturingOrder.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('manufacturingOrder.detail.main'),
    Router.getRouteFromBundle('manufacturingOrder.new.main'),
    Router.getRouteFromBundle('manufacturingOrder.edit.main'),

    Router.getRouteFromBundle('manufacturingOrder.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('manufacturingOrder.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('manufacturingOrder.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('manufacturingOrder',linksForAutoRouteGen);

