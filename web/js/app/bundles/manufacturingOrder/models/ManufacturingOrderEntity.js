'use strict';

angular.module('manufacturingOrderBundle').factory('ManufacturingOrder', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function ManufacturingOrder(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('tradingOrderId', 'number');
            this.add('companyIdSupplier', 'number');
            this.add('deliveryTerms', 'string');
            this.add('paymentTerms', 'string');
            this.add('shippingMedium', 'string');
            this.add('portOfDelivery', 'string');
            this.add('finalDestination', 'string');
            this.add('requiredDeliveryDate', 'date');
            this.add('warrantyAndClaims', 'string');
            this.add('notes', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(ManufacturingOrder);

        return ManufacturingOrder;
    }
]);

