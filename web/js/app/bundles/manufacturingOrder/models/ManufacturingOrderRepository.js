angular.module('manufacturingOrderBundle').factory('ManufacturingOrderRepository', ['BaseRepository', 'ManufacturingOrder',
    function (BaseRepository, ManufacturingOrder) {

    function ManufacturingOrderRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, ManufacturingOrder, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_manufacturingOrder', true));
    };

    BaseRepository.apply(ManufacturingOrderRepository);

    return new ManufacturingOrderRepository();
}]);