'use strict';

angular.module('manufacturingOrderBundle').controller('ManufacturingOrderListController',
    ['$scope', 'rows',
    function ManufacturingOrderListController($scope, rows) {
        $scope.rows = rows;
    }
]);