'use strict';

angular.module('manufacturingOrderBundle').controller('ManufacturingOrderEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'ManufacturingOrderRepository',
    'manufacturingOrderConfig',
    'UtilService',
    'MediaService',
    function ManufacturingOrderEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        ManufacturingOrderRepository,
        manufacturingOrderConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.attachmentLnk = attachmentLnk;
        $scope.links.noteLnk       = noteLnk;
        $scope.links.messageLnk    = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);
