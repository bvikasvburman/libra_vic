angular.module('accountsPayableBundle').factory('AccountsPayableRepository', ['BaseRepository', 'AccountsPayable',
    function (BaseRepository, AccountsPayable) {

    function AccountsPayableRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, AccountsPayable, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_accountsPayable', true));
    };

    BaseRepository.apply(AccountsPayableRepository);

    return new AccountsPayableRepository();
}]);