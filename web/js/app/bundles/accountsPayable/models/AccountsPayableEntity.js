'use strict';

angular.module('accountsPayableBundle').factory('AccountsPayable', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function AccountsPayable(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('tradingOrderId', 'number');
            this.add('companyId', 'number');
            this.add('invoiceId', 'number');
            this.add('notes', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(AccountsPayable);

        return AccountsPayable;
    }
]);

