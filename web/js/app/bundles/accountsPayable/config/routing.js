var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_accountsPayable', url: 'api/accountsPayable'},
    {name: 'api_accountsPayable_attachment', url: 'api/accountsPayable/:id/attachment'},
    {name: 'api_accountsPayable_attachment_one', url: 'api/accountsPayable/:id/attachment/:idl'},
    {name: 'api_accountsPayable_note', url: 'api/accountsPayable/:id/note'},
    {name: 'api_accountsPayable_note_one', url: 'api/accountsPayable/:id/note/:idl'},
    {name: 'api_accountsPayable_message', url: 'api/accountsPayable/:id/message'},
    {name: 'api_accountsPayable_message_one', url: 'api/accountsPayable/:id/message/:idl'},

    Router.getRouteFromBundle('accountsPayable'),
    Router.getRouteFromBundle('accountsPayable.list'),
    Router.getRouteFromBundle('accountsPayable.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('accountsPayable.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('accountsPayable.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('accountsPayable.detail.main'),
    Router.getRouteFromBundle('accountsPayable.new.main'),
    Router.getRouteFromBundle('accountsPayable.edit.main'),

    Router.getRouteFromBundle('accountsPayable.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('accountsPayable.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('accountsPayable.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('accountsPayable',linksForAutoRouteGen);
