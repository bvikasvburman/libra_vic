'use strict';

angular.module('accountsPayableBundle').controller('AccountsPayableListController',
    ['$scope', 'rows',
    function AccountsPayableListController($scope, rows) {
        $scope.rows = rows;
    }
]);