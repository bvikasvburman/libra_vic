angular.module('masterChecklistBundle').factory('MasterChecklistRepository', ['BaseRepository', 'MasterChecklist',
    function (BaseRepository, MasterChecklist) {

    function MasterChecklistRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, MasterChecklist, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_masterChecklist', true));
    };

    BaseRepository.apply(MasterChecklistRepository);

    return new MasterChecklistRepository();
}]);