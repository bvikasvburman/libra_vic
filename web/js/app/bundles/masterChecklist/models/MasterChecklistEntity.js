'use strict';

angular.module('masterChecklistBundle').factory('MasterChecklist', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function MasterChecklist(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('title', 'string');
            this.add('category', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(MasterChecklist);

        return MasterChecklist;
    }
]);

