var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_masterChecklist', url: 'api/masterChecklist'},
    {name: 'api_masterChecklist_attachment', url: 'api/masterChecklist/:id/attachment'},
    {name: 'api_masterChecklist_attachment_one', url: 'api/masterChecklist/:id/attachment/:idl'},
    {name: 'api_masterChecklist_note', url: 'api/masterChecklist/:id/note'},
    {name: 'api_masterChecklist_note_one', url: 'api/masterChecklist/:id/note/:idl'},
    {name: 'api_masterChecklist_message', url: 'api/masterChecklist/:id/message'},
    {name: 'api_masterChecklist_message_one', url: 'api/masterChecklist/:id/message/:idl'},

    Router.getRouteFromBundle('masterChecklist'),
    Router.getRouteFromBundle('masterChecklist.list'),
    Router.getRouteFromBundle('masterChecklist.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('masterChecklist.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('masterChecklist.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('masterChecklist.detail.main'),
    Router.getRouteFromBundle('masterChecklist.new.main'),
    Router.getRouteFromBundle('masterChecklist.edit.main'),

    Router.getRouteFromBundle('masterChecklist.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('masterChecklist.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('masterChecklist.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),

]);

Router.createRoutesForLinks('masterChecklist',linksForAutoRouteGen);

