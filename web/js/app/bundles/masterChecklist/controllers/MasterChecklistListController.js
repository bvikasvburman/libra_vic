'use strict';

angular.module('masterChecklistBundle').controller('MasterChecklistListController',
    ['$scope', 'rows',
    function MasterChecklistListController($scope, rows) {
        $scope.rows = rows;
    }
]);