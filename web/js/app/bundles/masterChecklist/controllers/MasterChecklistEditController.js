'use strict';

angular.module('masterChecklistBundle').controller('MasterChecklistEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'MasterChecklistRepository',
    'ValuelistRepository',
    'masterChecklistConfig',
    'UtilService',
    'MediaService',
    function MasterChecklistEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        MasterChecklistRepository,
        ValuelistRepository,
        masterChecklistConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);
        $scope.vls.category = [];

        ValuelistRepository.findBy({valuelistName: 'masterChecklistCategory'}, 'vl')
        .then(function (categories) {
            angular.copy(categories, $scope.vls.category);
        });

        $scope.links.attachmentLnk = attachmentLnk;
        $scope.links.noteLnk       = noteLnk;
        $scope.links.messageLnk    = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);