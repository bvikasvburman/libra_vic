angular.module('tradingOrderBundle').factory('TradingOrderRepository', ['BaseRepository', 'TradingOrder',
    function (BaseRepository, TradingOrder) {

    function TradingOrderRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, TradingOrder, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_tradingOrder', true));
    };

    BaseRepository.apply(TradingOrderRepository);

    return new TradingOrderRepository();
}]);