'use strict';

angular.module('tradingOrderBundle').factory('TradingOrder', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function TradingOrder(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('companyId', 'number');
            this.add('contactId', 'number');
            this.add('orderDetails', 'string');
            this.add('enquiryId', 'number');
            this.add('clientOrderRef', 'string');
            this.add('orderDate', 'date');
            this.add('status', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(TradingOrder);

        return TradingOrder;
    }
]);

