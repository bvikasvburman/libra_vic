'use strict';

angular.module('tradingOrderBundle').controller('TradingOrderListController',
    ['$scope', 'rows',
    function TradingOrderListController($scope, rows) {
        $scope.rows = rows;
    }
]);