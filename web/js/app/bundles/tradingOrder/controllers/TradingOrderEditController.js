'use strict';

angular.module('tradingOrderBundle').controller('TradingOrderEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'TradingOrderRepository',
    'tradingOrderConfig',
    'UtilService',
    'MediaService',
    function TradingOrderEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        TradingOrderRepository,
        tradingOrderConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.attachmentLnk = attachmentLnk;
        $scope.links.noteLnk       = noteLnk;
        $scope.links.messageLnk    = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);


'use strict';

angular.module('tradingOrderBundle').controller('TradingOrderEditController', [
    '$scope',
    '$rootScope',
    'row',
    // 'attachmentLnk',
    // 'noteLnk',
    // 'messageLnk',
    // 'taskLnk',
    'TradingOrderRepository',
    'tradingOrderConfig',
    'UtilService',
    function TradingOrderEditController(
        $scope,
        $rootScope,
        row,
        // attachmentLnk,
        // noteLnk,
        // messageLnk,
        // taskLnk,
        TradingOrderRepository,
        tradingOrderConfig,
        UtilService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        //set the links array here
        // $scope.links.attachmentLnk = attachmentLnk;
        // $scope.links.noteLnk       = noteLnk;
        // $scope.links.messageLnk  = messageLnk;
        // $scope.links.taskLnk       = taskLnk;
    }
]);