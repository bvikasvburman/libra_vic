var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_tradingOrder', url: 'api/tradingOrder'},
    {name: 'api_tradingOrder_attachment', url: 'api/tradingOrder/:id/attachment'},
    {name: 'api_tradingOrder_attachment_one', url: 'api/tradingOrder/:id/attachment/:idl'},
    {name: 'api_tradingOrder_note', url: 'api/tradingOrder/:id/note'},
    {name: 'api_tradingOrder_note_one', url: 'api/tradingOrder/:id/note/:idl'},
    {name: 'api_tradingOrder_message', url: 'api/tradingOrder/:id/message'},
    {name: 'api_tradingOrder_message_one', url: 'api/tradingOrder/:id/message/:idl'},

    Router.getRouteFromBundle('tradingOrder'),
    Router.getRouteFromBundle('tradingOrder.list'),
    Router.getRouteFromBundle('tradingOrder.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('tradingOrder.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('tradingOrder.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('tradingOrder.detail.main'),
    Router.getRouteFromBundle('tradingOrder.new.main'),
    Router.getRouteFromBundle('tradingOrder.edit.main'),

    Router.getRouteFromBundle('tradingOrder.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('tradingOrder.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('tradingOrder.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('tradingOrder',linksForAutoRouteGen);
