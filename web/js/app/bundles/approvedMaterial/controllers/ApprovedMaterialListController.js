'use strict';

angular.module('approvedMaterialBundle').controller('ApprovedMaterialListController',
    ['$scope', 'rows', 'Company',
    function ApprovedMaterialListController($scope, rows, Company) {
        $scope.rows = rows;
    }
]);