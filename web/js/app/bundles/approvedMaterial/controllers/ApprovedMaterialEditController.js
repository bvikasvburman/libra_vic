'use strict';

angular.module('approvedMaterialBundle').controller('ApprovedMaterialEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'ApprovedMaterialRepository',
    'CompanyRepository',
    'ColorMasterRepository',
    'PartsLocationRepository',
    'ValuelistRepository',
    'approvedMaterialConfig',
    'UtilService',
    'MediaService',
    function ApprovedMaterialEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        ApprovedMaterialRepository,
        CompanyRepository,
        ColorMasterRepository,
        PartsLocationRepository,
        ValuelistRepository,
        approvedMaterialConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);
        $scope.vls.colorMaster   = [];
        $scope.vls.company       = [];
        $scope.vls.partsLocation = [];
        $scope.vls.category      = [];

        //set the links array here
        $scope.links.attachmentLnk = attachmentLnk;
        $scope.links.noteLnk       = noteLnk;
        $scope.links.messageLnk    = messageLnk;

        ColorMasterRepository.all().then(function (colors) {
            angular.copy(colors, $scope.vls.colorMaster);
        });
        CompanyRepository.all().then(function (companies) {
            angular.copy(companies, $scope.vls.company);
        });
        PartsLocationRepository.all().then(function (partsLocations) {
            angular.copy(partsLocations, $scope.vls.partsLocation);
        });
        ValuelistRepository.findBy({valuelistName: 'approvedMaterialCategory'}, 'vl')
        .then(function (categories) {
            angular.copy(categories, $scope.vls.category);
        });

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);