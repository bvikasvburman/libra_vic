var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_approvedMaterial', url: 'api/approvedMaterial'},
    {name: 'api_approvedMaterial_attachment', url: 'api/approvedMaterial/:id/attachment'},
    {name: 'api_approvedMaterial_attachment_one', url: 'api/approvedMaterial/:id/attachment/:idl'},
    {name: 'api_approvedMaterial_note', url: 'api/approvedMaterial/:id/note'},
    {name: 'api_approvedMaterial_note_one', url: 'api/approvedMaterial/:id/note/:idl'},
    {name: 'api_approvedMaterial_message', url: 'api/approvedMaterial/:id/message'},
    {name: 'api_approvedMaterial_message_one', url: 'api/approvedMaterial/:id/message/:idl'},

    Router.getRouteFromBundle('approvedMaterial'),
    Router.getRouteFromBundle('approvedMaterial.list'),
    Router.getRouteFromBundle('approvedMaterial.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('approvedMaterial.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('approvedMaterial.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('approvedMaterial.detail.main'),
    Router.getRouteFromBundle('approvedMaterial.new.main'),
    Router.getRouteFromBundle('approvedMaterial.edit.main'),

    Router.getRouteFromBundle('approvedMaterial.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('approvedMaterial.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('approvedMaterial.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),

]);

Router.createRoutesForLinks('approvedMaterial',linksForAutoRouteGen);