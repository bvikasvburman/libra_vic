angular.module('approvedMaterialBundle').factory('approvedMaterialConfig', [
    'BundleDefConfig',
    'ListService',
    'ListFldService',
    'UtilService',
    'LinkService',
    'LinkFldService',
    'TabService',
    function(
        BundleDefConfig,
        ListService,
        ListFldService,
        UtilService,
        LinkService,
        LinkFldService,
        TabService
    ) {
        var lf = ListFldService;
        var lnkFld = LinkFldService;

        var listFlds = [
            new lf('part'),
            new lf('material'),
            new lf('colorCode'),
            new lf('measurement'),
            new lf('company.name', {title: 'Supplier'}),
            new lf('category'),
            new lf('approved', {align: 'center', type: 'switch'}),
        ];

        var links = [
            new LinkService('attachment', {
                fields: [
                    new lnkFld('media.displayTitle', {hasMediaLink: true}),
                ]
            }),
            new LinkService('note', {
                fields: [
                    new lnkFld('note', {truncateLength: 100}),
                    new lnkFld('creationDate', {type: 'datetime', width: '20pc'}),
                ]
            }),
            new LinkService('message'),
        ];

        var tabs = [
            new TabService('main'),
        ];
        tabs = new TabService().addLinksToTab(tabs, links);

        return new BundleDefConfig('approvedMaterial', {
           listObj: new ListService(UtilService.getCurrBundle(), listFlds),
           links: links,
           tabs: tabs,
        })
   }]
);