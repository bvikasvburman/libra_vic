'use strict';

angular.module('approvedMaterialBundle').factory('ApprovedMaterial', [
    'BaseEntity',
    'Company',
    'PartsLocation',
    'User',
    function (
        BaseEntity,
        Company,
        PartsLocation,
        User
        ) {
        function ApprovedMaterial(data) {
            // inherit BaseEntity constructor
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('part', 'string');
            this.add('material', 'string');
            this.add('category', 'string');
            this.add('colorCode', 'string');
            this.add('measurement', 'string');
            this.add('companyId', 'number');
            this.add('approved', 'number');
            this.add('partsLocationId', 'number');
            this.add('barcode', 'string');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
            this.add('company', Company);
            this.add('partsLocation', PartsLocation);
        };

        BaseEntity.apply(ApprovedMaterial);

        return ApprovedMaterial;
    }
]);

