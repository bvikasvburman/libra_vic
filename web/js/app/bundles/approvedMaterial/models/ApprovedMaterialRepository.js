angular.module('approvedMaterialBundle').factory('ApprovedMaterialRepository', ['BaseRepository', 'ApprovedMaterial',
    function (BaseRepository, ApprovedMaterial) {

    function ApprovedMaterialRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, ApprovedMaterial, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_approvedMaterial', true));
    };

    BaseRepository.apply(ApprovedMaterialRepository);

    return new ApprovedMaterialRepository();
}]);