'use strict';

angular.module('translationBundle').factory('Translation', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function Translation(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('keyText', 'string');
            this.add('value', 'string');
            this.add('description', 'string');
            this.add('chiValue', 'string');
            this.add('groupName', 'string');
            this.add('flag', 'boolean');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Translation);

        return Translation;
    }
]);

