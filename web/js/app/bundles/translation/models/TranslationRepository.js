'use strict';

angular.module('translationBundle').factory('TranslationRepository', ['BaseRepository', 'Translation',
    function (BaseRepository, Translation) {
    
    function TranslationRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Translation, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_translation', true));
        
    };
    
    BaseRepository.apply(TranslationRepository);
    
    return new TranslationRepository();
}]);