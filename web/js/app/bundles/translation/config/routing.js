var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_translation', url: 'api/translation'},
    {name: 'api_translation_attachment', url: 'api/translation/:id/attachment'},
    {name: 'api_translation_attachment_one', url: 'api/translation/:id/attachment/:idl'},
    {name: 'api_translation_note', url: 'api/translation/:id/note'},
    {name: 'api_translation_note_one', url: 'api/translation/:id/note/:idl'},
    {name: 'api_translation_message', url: 'api/translation/:id/message'},
    {name: 'api_translation_message_one', url: 'api/translation/:id/message/:idl'},

    Router.getRouteFromBundle('translation'),
    Router.getRouteFromBundle('translation.list'),
    Router.getRouteFromBundle('translation.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('translation.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('translation.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('translation.detail.main'),
    Router.getRouteFromBundle('translation.new.main'),
    Router.getRouteFromBundle('translation.edit.main'),

    Router.getRouteFromBundle('translation.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('translation.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('translation.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('translation',linksForAutoRouteGen);

