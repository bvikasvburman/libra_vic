'use strict';

angular.module('translationBundle').controller('TranslationListController',
    ['$scope', 'rows',
    function TranslationListController($scope, rows) {
        $scope.rows = rows;
    }
]);