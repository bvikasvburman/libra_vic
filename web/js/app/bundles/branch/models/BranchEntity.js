'use strict';

angular.module('branchBundle').factory('Branch', [
    'BaseEntity',
    'Company',
    'User',
    function (
        BaseEntity,
        Company,
        User
    ) {
        function Branch(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('name', 'string');
            this.add('phone', 'string');
            this.add('email', 'string');
            this.add('address', 'string');
            this.add('city', 'string');
            this.add('state', 'string');
            this.add('zip', 'string');
            this.add('country', 'string');
            this.add('fax', 'string');
            this.add('website', 'string');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('company', Company);
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Branch);

        return Branch;
    }
]);