angular.module('branchBundle').factory('BranchRepository', [
    'BaseRepository',
    'Branch',
    function (
        BaseRepository,
        Branch
    ) {

    function BranchRepository(data) {
        BaseRepository.constructor.call(this, Branch, data);
        this.setApiURL(Router.getRoutePath('api_branch', true));
    };

    BaseRepository.apply(BranchRepository);

    return new BranchRepository();
}]);