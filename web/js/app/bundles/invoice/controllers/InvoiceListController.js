'use strict';

angular.module('invoiceBundle').controller('InvoiceListController',
    ['$scope', 'rows',
    function InvoiceListController($scope, rows) {
        $scope.rows = rows;
    }
]);