var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_invoice', url: 'api/invoice'},
    {name: 'api_invoice_attachment', url: 'api/invoice/:id/attachment'},
    {name: 'api_invoice_attachment_one', url: 'api/invoice/:id/attachment/:idl'},
    {name: 'api_invoice_note', url: 'api/invoice/:id/note'},
    {name: 'api_invoice_note_one', url: 'api/invoice/:id/note/:idl'},
    {name: 'api_invoice_message', url: 'api/invoice/:id/message'},
    {name: 'api_invoice_message_one', url: 'api/invoice/:id/message/:idl'},

    Router.getRouteFromBundle('invoice'),
    Router.getRouteFromBundle('invoice.list'),
    Router.getRouteFromBundle('invoice.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('invoice.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('invoice.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('invoice.detail.main'),
    Router.getRouteFromBundle('invoice.new.main'),
    Router.getRouteFromBundle('invoice.edit.main'),

    Router.getRouteFromBundle('invoice.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('invoice.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('invoice.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('invoice',linksForAutoRouteGen);

