angular.module('invoiceBundle').factory('InvoiceRepository', ['BaseRepository', 'Invoice',
    function (BaseRepository, Invoice) {

    function InvoiceRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Invoice, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_invoice', true));
    };

    BaseRepository.apply(InvoiceRepository);

    return new InvoiceRepository();
}]);