'use strict';

angular.module('invoiceBundle').factory('Invoice', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function Invoice(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('tradingOrderId', 'number');
            this.add('companyId', 'number');
            this.add('enquiryId', 'number');
            this.add('salesConfirmationId', 'number');
            this.add('shippingMedium', 'string');
            this.add('vesselName', 'string');
            this.add('voyageNo', 'string');
            this.add('sailingOnDate', 'date');
            this.add('invoiceDate', 'date');
            this.add('portOfLoading', 'string');
            this.add('portOfDelivery', 'string');
            this.add('finalDestination', 'string');
            this.add('containerNo', 'string');
            this.add('lcNo', 'string');
            this.add('blNo', 'string');
            this.add('blIssuedBy', 'string');
            this.add('insurancePolicyNo', 'string');
            this.add('policyNo', 'string');
            this.add('paymentTerms', 'string');
            this.add('deliveryTerms', 'string');
            this.add('clientAddress', 'string');
            this.add('ourBankAccount', 'string');
            this.add('ourBankDetails', 'string');
            this.add('notes', 'string');
            this.add('status', 'string');
            this.add('paidDate', 'date');
            this.add('goodsDescription', 'string');
            this.add('noOfCtns', 'number');
            this.add('grossWeight', 'number');
            this.add('measurement', 'number');
            this.add('shippingMarkFront', 'string');
            this.add('shippingMarkSide', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Invoice);

        return Invoice;
    }
]);

