var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_approvedPackaging', url: 'api/approvedPackaging'},
    {name: 'api_approvedPackaging_attachment', url: 'api/approvedPackaging/:id/attachment'},
    {name: 'api_approvedPackaging_attachment_one', url: 'api/approvedPackaging/:id/attachment/:idl'},
    {name: 'api_approvedPackaging_note', url: 'api/approvedPackaging/:id/note'},
    {name: 'api_approvedPackaging_note_one', url: 'api/approvedPackaging/:id/note/:idl'},
    {name: 'api_approvedPackaging_message', url: 'api/approvedPackaging/:id/message'},
    {name: 'api_approvedPackaging_message_one', url: 'api/approvedPackaging/:id/message/:idl'},

    Router.getRouteFromBundle('approvedPackaging'),
    Router.getRouteFromBundle('approvedPackaging.list'),
    Router.getRouteFromBundle('approvedPackaging.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('approvedPackaging.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('approvedPackaging.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('approvedPackaging.detail.main'),
    Router.getRouteFromBundle('approvedPackaging.new.main'),
    Router.getRouteFromBundle('approvedPackaging.edit.main'),

    Router.getRouteFromBundle('approvedPackaging.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('approvedPackaging.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('approvedPackaging.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),

]);

Router.createRoutesForLinks('approvedPackaging',linksForAutoRouteGen);
