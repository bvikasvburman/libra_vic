angular.module('approvedPackagingBundle').factory('ApprovedPackagingRepository', ['BaseRepository', 'ApprovedPackaging',
    function (BaseRepository, ApprovedPackaging) {

    function ApprovedPackagingRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, ApprovedPackaging, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_approvedPackaging', true));
    };

    BaseRepository.apply(ApprovedPackagingRepository);

    return new ApprovedPackagingRepository();
}]);