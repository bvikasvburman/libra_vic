'use strict';

angular.module('approvedPackagingBundle').factory('ApprovedPackaging', [
    'BaseEntity',
    'Company',
    'PartsLocation',
    'User',
    function (
        BaseEntity,
        Company,
        PartsLocation,
        User
    ) {
        function ApprovedPackaging(data) {
            // inherit BaseEntity constructor
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('packagingMaterial', 'string');
            this.add('category', 'string');
            this.add('companyId', 'number');
            this.add('approved', 'number');
            this.add('partsLocationId', 'number');
            this.add('barcode', 'string');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
            this.add('company', Company);
            this.add('partsLocation', PartsLocation);
        };

    BaseEntity.apply(ApprovedPackaging);

    return ApprovedPackaging;
}]);

