'use strict';

angular.module('approvedPackagingBundle').controller('ApprovedPackagingListController',
    ['$scope', 'rows',
    function ApprovedPackagingListController($scope, rows) {
        $scope.rows = rows;
    }
]);