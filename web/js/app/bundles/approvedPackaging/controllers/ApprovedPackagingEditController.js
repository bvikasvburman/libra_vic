'use strict';

angular.module('approvedPackagingBundle').controller('ApprovedPackagingEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'ApprovedPackagingRepository',
    'CompanyRepository',
    'PartsLocationRepository',
    'ValuelistRepository',
    'approvedPackagingConfig',
    'UtilService',
    'MediaService',
    function ApprovedPackagingEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        ApprovedPackagingRepository,
        CompanyRepository,
        PartsLocationRepository,
        ValuelistRepository,
        approvedPackagingConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);
        $scope.vls.company       = [];
        $scope.vls.partsLocation = [];
        $scope.vls.category      = [];

        CompanyRepository.all().then(function (companies) {
            angular.copy(companies, $scope.vls.company);
        });
        PartsLocationRepository.all().then(function (partsLocations) {
            angular.copy(partsLocations, $scope.vls.partsLocation);
        });
        ValuelistRepository.findBy({valuelistName: 'approvedPackagingCategory'}, 'vl')
        .then(function (categories) {
            angular.copy(categories, $scope.vls.category);
        });

        //set the links array here
        $scope.links.attachmentLnk   = attachmentLnk;
        $scope.links.noteLnk    = noteLnk;
        $scope.links.messageLnk = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);