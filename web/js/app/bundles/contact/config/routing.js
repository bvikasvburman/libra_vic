var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;

var linksForAutoRouteGen = [
    getLOFR('attachment', {repository: 'ContactMedia', linkNameActual: 'media'}),
    getLOFR('note'),
    getLOFR('message'),
];

Router.addRoutes([
    /* api */
    {name: 'api_contact', url: 'api/contact'},
    {name: 'api_contact_attachment', url: 'api/contact/:id/attachment'},
    {name: 'api_contact_attachment_one', url: 'api/contact/:id/attachment/:idl'},
    {name: 'api_contact_note', url: 'api/contact/:id/note'},
    {name: 'api_contact_note_one', url: 'api/contact/:id/note/:idl'},
    {name: 'api_contact_message', url: 'api/contact/:id/message'},
    {name: 'api_contact_message_one', url: 'api/contact/:id/message/:idl'},

    Router.getRouteFromBundle('contact'),
    Router.getRouteFromBundle('contact.list'),
    Router.getRouteFromBundle('contact.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('contact.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('contact.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('contact.detail.main'),
    Router.getRouteFromBundle('contact.new.main'),
    Router.getRouteFromBundle('contact.edit.main'),

    Router.getRouteFromBundle('contact.detail.mainGantt'),
    Router.getRouteFromBundle('contact.new.mainGantt'),
    Router.getRouteFromBundle('contact.edit.mainGantt'),

    Router.getRouteFromBundle('contact.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('contact.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('contact.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),

]);

Router.createRoutesForLinks('contact', linksForAutoRouteGen);

