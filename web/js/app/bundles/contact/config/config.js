angular.module('contactBundle').factory('contactConfig', [
    'BundleDefConfig',
    'ListService',
    'ListFldService',
    'UtilService',
    'LinkService',
    'LinkFldService',
    'TabService',
    function(
        BundleDefConfig,
        ListService,
        ListFldService,
        UtilService,
        LinkService,
        LinkFldService,
        TabService
    ) {
        var lf = ListFldService;
        var lnkFld = LinkFldService;

        var listFlds = [
            new lf('firstName'),
            new lf('lastName'),
            new lf('email'),
            new lf('company.name', 'Company'),
            new lf('phone'),
        ];

        var links = [
            new LinkService('attachment', {
                fields: [
                    new lnkFld('media.displayTitle', {hasMediaLink: true}),
                ]
            }),
            new LinkService('note', {
                fields: [
                    new lnkFld('note', {truncateLength: 100}),
                    new lnkFld('creationDate', {type: 'datetime', width: '20pc'}),
                ]
            }),
            new LinkService('message'),
        ];

        var tabs = [
            new TabService('main'),
            new TabService('mainGantt', {title: 'Contact Marketing Gantt'}),
        ];
        tabs = new TabService().addLinksToTab(tabs, links);

        return new BundleDefConfig('contact', {
           listObj: new ListService(UtilService.getCurrBundle(), listFlds),
           links: links,
           tabs: tabs,
        })
   }]
);