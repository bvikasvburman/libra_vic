angular.module('contactBundle').factory('ContactRepository', [
    'BaseRepository',
    'Contact',
    function (
        BaseRepository,
        Contact
    ) {

    function ContactRepository(data) {
        BaseRepository.constructor.call(this, Contact, data);
        this.setApiURL(Router.getRoutePath('api_contact', true));
    };

    BaseRepository.apply(ContactRepository);

    return new ContactRepository();
}]);