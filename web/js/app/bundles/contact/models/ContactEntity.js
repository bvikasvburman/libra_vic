'use strict';

angular.module('contactBundle').factory('Contact', [
    'BaseEntity',
    'Company',
    'User',
    function (
        BaseEntity,
        Company,
        User
    ) {
        function Contact(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('firstName', 'string');
            this.add('lastName', 'string');
            this.add('fullName', 'string');
            this.add('phone', 'string');
            this.add('fax', 'string');
            this.add('email', 'string');
            this.add('company_id', 'number');
            this.add('position', 'string');
            this.add('address', 'string');
            this.add('city', 'string');
            this.add('state', 'string');
            this.add('zip', 'string');
            this.add('country', 'string');
            this.add('flag', 'number');
            this.add('ganttStartDate', 'date');
            this.add('ganttEndDate', 'date');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('company', Company);
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Contact);

        return Contact;
    }
]);