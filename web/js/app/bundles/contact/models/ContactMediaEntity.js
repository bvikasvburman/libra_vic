'use strict';

angular.module('contactBundle').factory('ContactMedia', [
    'BaseEntity',
    'Contact',
    'Media',
    function (
        BaseEntity,
        Contact,
        Media
    ) {
    function ContactMedia(data) {
        BaseEntity.constructor.call(this, data);

        this.add('id', 'number');
        this.add('contactId', 'number');
        this.add('mediaId', 'number');
        this.add('mediaType', 'string');
        this.add('creationDate', 'string');
        // this.add('contact', Contact);
        this.add('media', Media);
    };

    BaseEntity.apply(ContactMedia);

    return ContactMedia;
}]);
