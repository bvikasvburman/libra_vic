angular.module('contactBundle').factory('ContactMediaRepository', [
    'BaseRepository',
    'ContactMedia',
    function (
        BaseRepository,
        ContactMedia
    ) {
    function ContactMediaRepository(data) {
        BaseRepository.constructor.call(this, ContactMedia, data);
        this.setApiURL(Router.getRoutePath('api_contact', true));
    };

    BaseRepository.apply(ContactMediaRepository);

    return new ContactMediaRepository();
}]);