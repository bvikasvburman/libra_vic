'use strict';

angular.module('contactBundle').controller('ContactEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'ContactRepository',
    'CompanyRepository',
    'contactConfig',
    'UtilService',
    'GanttUtilService',
    'MediaService',
    function ContactEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        ContactRepository,
        CompanyRepository,
        contactConfig,
        UtilService,
        GanttUtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);
        $scope.vls.company = [];
        $scope.vls.contact = [];

        $scope.gus = GanttUtilService;

        CompanyRepository.all().then(function (companies) {
            angular.copy(companies, $scope.vls.company);
        });

        //gantt chart
        var opts = {
            fromDate: row.ganttStartDate,
            toDate: row.ganttEndDate,
            ganttName: 'contact',
        }
        $scope.ganttOptions = GanttUtilService.getGanttOptions($scope, opts);

        $scope.ganttData = [];
        GanttUtilService.getGanttData('contact', row.id).then(function(response) {
            $scope.ganttData = response.data;
        });

        //set the links here
        $scope.links.attachmentLnk = attachmentLnk;
        $scope.links.noteLnk       = noteLnk;
        $scope.links.messageLnk    = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }

    }
]);