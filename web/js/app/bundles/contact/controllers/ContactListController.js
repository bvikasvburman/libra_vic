'use strict';

angular.module('contactBundle').controller('ContactListController',
    ['$scope', 'rows', 'Company',
    function ContactListController($scope, rows, Company) {
        $scope.rows = rows;
    }
]);