'use strict';

angular.module('salesConfirmationBundle').controller('SalesConfirmationListController',
    ['$scope', 'rows',
    function SalesConfirmationListController($scope, rows) {
        $scope.rows = rows;
    }
]);