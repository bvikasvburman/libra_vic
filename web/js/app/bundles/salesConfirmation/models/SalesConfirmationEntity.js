'use strict';

angular.module('salesConfirmationBundle').factory('SalesConfirmation', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function SalesConfirmation(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('companyId', 'number');
            this.add('contactId', 'number');
            this.add('enquiryId', 'number');
            this.add('tradingOrderId', 'number');
            this.add('specification', 'string');
            this.add('clientOrderRef', 'string');
            this.add('deliveryTerms', 'string');
            this.add('paymentTerms', 'string');
            this.add('expectedDateDeparture', 'date');
            this.add('expectedDateArrival', 'date');
            this.add('shippingMedium', 'string');
            this.add('portOfDelivery', 'string');
            this.add('finalDestination', 'string');
            this.add('warrantyAndClaims', 'string');
            this.add('notes', 'string');
            this.add('status', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };
        BaseEntity.apply(SalesConfirmation);

        return SalesConfirmation;
    }
]);

