angular.module('salesConfirmationBundle').factory('SalesConfirmationRepository', ['BaseRepository', 'SalesConfirmation',
    function (BaseRepository, SalesConfirmation) {

    function SalesConfirmationRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, SalesConfirmation, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_salesConfirmation', true));
    };

    BaseRepository.apply(SalesConfirmationRepository);

    return new SalesConfirmationRepository();
}]);