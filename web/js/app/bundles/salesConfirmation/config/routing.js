var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_salesConfirmation', url: 'api/salesConfirmation'},
    {name: 'api_salesConfirmation_attachment', url: 'api/salesConfirmation/:id/attachment'},
    {name: 'api_salesConfirmation_attachment_one', url: 'api/salesConfirmation/:id/attachment/:idl'},
    {name: 'api_salesConfirmation_note', url: 'api/salesConfirmation/:id/note'},
    {name: 'api_salesConfirmation_note_one', url: 'api/salesConfirmation/:id/note/:idl'},
    {name: 'api_salesConfirmation_message', url: 'api/salesConfirmation/:id/message'},
    {name: 'api_salesConfirmation_message_one', url: 'api/salesConfirmation/:id/message/:idl'},

    Router.getRouteFromBundle('salesConfirmation'),
    Router.getRouteFromBundle('salesConfirmation.list'),
    Router.getRouteFromBundle('salesConfirmation.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('salesConfirmation.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('salesConfirmation.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('salesConfirmation.detail.main'),
    Router.getRouteFromBundle('salesConfirmation.new.main'),
    Router.getRouteFromBundle('salesConfirmation.edit.main'),

    Router.getRouteFromBundle('salesConfirmation.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('salesConfirmation.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('salesConfirmation.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('salesConfirmation',linksForAutoRouteGen);

