Router.addRoutes([
    /* api */
    {name: 'api_setting', url: 'api/setting'},
    {name: 'api_setting_media', url: 'api/setting/:id/media/:type'},

    Router.getRouteFromBundle('setting'),
    Router.getRouteFromBundle('setting.list'),
    Router.getRouteFromBundle('setting.detail'),
    Router.getRouteFromBundle('setting.new'),
    Router.getRouteFromBundle('setting.edit'),
]);
