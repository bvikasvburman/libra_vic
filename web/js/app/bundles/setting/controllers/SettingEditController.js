'use strict';

angular.module('settingBundle').controller('SettingEditController', [
    '$scope',
    '$rootScope',
    'row',
    'SettingRepository',
    'UtilService',
    'settingConfig',
    function SettingEditController(
        $scope,
        $rootScope,
        row,
        SettingRepository,
        UtilService,
        settingConfig
    ) {
        UtilService.initializeCommonControllerVars($scope, row);
    }
]);

