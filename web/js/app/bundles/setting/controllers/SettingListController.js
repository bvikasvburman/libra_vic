'use strict';

angular.module('settingBundle').controller('SettingListController',
['$scope', 'settings', '$state', 'SettingRepository', '$mdSidenav', 'settingConfig',
    function SettingListController($scope, settings, $state, SettingRepository, $mdSidenav, settingConfig) {

        /**
         * Display record preview
         *
         * @param {Setting} setting
         */
        $scope.toggleQuickDetails = function (setting) {
            if (!settingConfig.preview) {
                return $state.go('setting_detail', { id: setting.id });
            }

            $scope.selected = setting;
            $mdSidenav('right').toggle(); // promise
        };

        /**
         * Flag/Unflag a setting.
         *
         * @param {Setting} item
         */
        $scope.flag = function (item) {
            item.flag = !item.flag;
            SettingRepository.save(item, {wrapper: 'setting'});
        };

        $scope.settings       = settings;
    }
]);

