'use strict';

angular.module('settingBundle').factory('SettingRepository', ['BaseRepository', 'Setting',
    function (BaseRepository, Setting) {
    
    function SettingRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Setting, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_setting', true));
        
    };
    
    BaseRepository.apply(SettingRepository);
    
    return new SettingRepository();
}]);