'use strict';

angular.module('settingBundle').factory('Setting', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function Setting(data) {
            this.add('id', 'number');
            this.add('description', 'string');
            this.add('keyText', 'string');
            this.add('value', 'string');
            this.add('groupName', 'string');
            this.add('valueType', 'string');
            this.add('flag', 'boolean');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Setting);
        return Setting;
    }
]);