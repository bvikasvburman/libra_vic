'use strict';

angular.module('researchBundle').controller('ResearchListController',
    ['$scope', 'rows',
    function ResearchListController($scope, rows) {
        $scope.rows = rows;
    }
]);