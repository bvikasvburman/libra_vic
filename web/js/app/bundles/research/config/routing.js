var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_research', url: 'api/research'},
    {name: 'api_research_attachment', url: 'api/research/:id/attachment'},
    {name: 'api_research_attachment_one', url: 'api/research/:id/attachment/:idl'},
    {name: 'api_research_note', url: 'api/research/:id/note'},
    {name: 'api_research_note_one', url: 'api/research/:id/note/:idl'},
    {name: 'api_research_message', url: 'api/research/:id/message'},
    {name: 'api_research_message_one', url: 'api/research/:id/message/:idl'},

    Router.getRouteFromBundle('research'),
    Router.getRouteFromBundle('research.list'),
    Router.getRouteFromBundle('research.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('research.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('research.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('research.detail.main'),
    Router.getRouteFromBundle('research.new.main'),
    Router.getRouteFromBundle('research.edit.main'),

    Router.getRouteFromBundle('research.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('research.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('research.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),

]);

Router.createRoutesForLinks('research', linksForAutoRouteGen);