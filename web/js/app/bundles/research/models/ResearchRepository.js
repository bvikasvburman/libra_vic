angular.module('researchBundle').factory('ResearchRepository', ['BaseRepository', 'Research',
    function (BaseRepository, Research) {

    function ResearchRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Research, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_research', true));
    };

    BaseRepository.apply(ResearchRepository);

    return new ResearchRepository();
}]);