'use strict';

angular.module('researchBundle').factory('Research', [
    'BaseEntity',
    'User',
    'ProtoType',
    'Company',
    'Enquiry',
    function (
        BaseEntity,
        User,
        ProtoType,
        Company,
        Enquiry
    ) {
        function Research(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('protoTypeId', 'number');
            this.add('companyId', 'number');
            this.add('researchArea', 'string');
            this.add('enquiryId', 'number');
            this.add('description', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('protoType', ProtoType, {vlFlds: ['id', 'code']});
            this.add('company', Company);
            this.add('enquiry', Enquiry);
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Research);

        return Research;
    }
]);

