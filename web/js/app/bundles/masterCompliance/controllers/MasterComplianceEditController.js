'use strict';

angular.module('masterComplianceBundle').controller('MasterComplianceEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'MasterComplianceRepository',
    'masterComplianceConfig',
    'UtilService',
    'MediaService',
    function MasterComplianceEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        MasterComplianceRepository,
        masterComplianceConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.attachmentLnk = attachmentLnk;
        $scope.links.noteLnk       = noteLnk;
        $scope.links.messageLnk    = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);