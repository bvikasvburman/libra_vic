'use strict';

angular.module('masterComplianceBundle').controller('MasterComplianceListController',
    ['$scope', 'rows',
    function MasterComplianceListController($scope, rows) {
        $scope.rows = rows;
    }
]);