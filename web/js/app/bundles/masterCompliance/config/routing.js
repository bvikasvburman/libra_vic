var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_masterCompliance', url: 'api/masterCompliance'},
    {name: 'api_masterCompliance_attachment', url: 'api/masterCompliance/:id/attachment'},
    {name: 'api_masterCompliance_attachment_one', url: 'api/masterCompliance/:id/attachment/:idl'},
    {name: 'api_masterCompliance_note', url: 'api/masterCompliance/:id/note'},
    {name: 'api_masterCompliance_note_one', url: 'api/masterCompliance/:id/note/:idl'},
    {name: 'api_masterCompliance_message', url: 'api/masterCompliance/:id/message'},
    {name: 'api_masterCompliance_message_one', url: 'api/masterCompliance/:id/message/:idl'},

    Router.getRouteFromBundle('masterCompliance'),
    Router.getRouteFromBundle('masterCompliance.list'),
    Router.getRouteFromBundle('masterCompliance.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('masterCompliance.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('masterCompliance.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('masterCompliance.detail.main'),
    Router.getRouteFromBundle('masterCompliance.new.main'),
    Router.getRouteFromBundle('masterCompliance.edit.main'),

    Router.getRouteFromBundle('masterCompliance.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('masterCompliance.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('masterCompliance.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),

]);

Router.createRoutesForLinks('masterCompliance',linksForAutoRouteGen);

