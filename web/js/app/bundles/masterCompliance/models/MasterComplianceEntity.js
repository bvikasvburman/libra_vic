'use strict';

angular.module('masterComplianceBundle').factory('MasterCompliance', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function MasterCompliance(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('part', 'string');
            this.add('compliance', 'string');
            this.add('requiredValue', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(MasterCompliance);

        return MasterCompliance;
    }
]);

