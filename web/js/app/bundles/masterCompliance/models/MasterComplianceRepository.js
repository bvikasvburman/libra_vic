angular.module('masterComplianceBundle').factory('MasterComplianceRepository', ['BaseRepository', 'MasterCompliance',
    function (BaseRepository, MasterCompliance) {

    function MasterComplianceRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, MasterCompliance, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_masterCompliance', true));
    };

    BaseRepository.apply(MasterComplianceRepository);

    return new MasterComplianceRepository();
}]);