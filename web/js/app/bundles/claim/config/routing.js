var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_claim', url: 'api/claim'},
    {name: 'api_claim_attachment', url: 'api/claim/:id/attachment'},
    {name: 'api_claim_attachment_one', url: 'api/claim/:id/attachment/:idl'},
    {name: 'api_claim_note', url: 'api/claim/:id/note'},
    {name: 'api_claim_note_one', url: 'api/claim/:id/note/:idl'},
    {name: 'api_claim_message', url: 'api/claim/:id/message'},
    {name: 'api_claim_message_one', url: 'api/claim/:id/message/:idl'},

    Router.getRouteFromBundle('claim'),
    Router.getRouteFromBundle('claim.list'),
    Router.getRouteFromBundle('claim.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('claim.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('claim.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('claim.detail.main'),
    Router.getRouteFromBundle('claim.new.main'),
    Router.getRouteFromBundle('claim.edit.main'),

    Router.getRouteFromBundle('claim.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('claim.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('claim.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('claim',linksForAutoRouteGen);
