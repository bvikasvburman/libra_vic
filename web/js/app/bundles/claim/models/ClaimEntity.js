'use strict';

angular.module('claimBundle').factory('Claim', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function Claim(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('tradingOrderId', 'number');
            this.add('companyId', 'number');
            this.add('title', 'string');
            this.add('description', 'string');
            this.add('status', 'string');
            this.add('entertainClaim', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Claim);

        return Claim;
    }
]);