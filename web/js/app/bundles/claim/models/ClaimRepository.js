angular.module('claimBundle').factory('ClaimRepository', ['BaseRepository', 'Claim',
    function (BaseRepository, Claim) {

    function ClaimRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Claim, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_claim', true));
    };

    BaseRepository.apply(ClaimRepository);

    return new ClaimRepository();
}]);