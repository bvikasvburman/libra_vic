'use strict';

angular.module('claimBundle').controller('ClaimEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'ClaimRepository',
    'claimConfig',
    'UtilService',
    'MediaService',
    function ClaimEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        ClaimRepository,
        claimConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.attachmentLnk = attachmentLnk;
        $scope.links.noteLnk       = noteLnk;
        $scope.links.messageLnk    = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);
