'use strict';

angular.module('claimBundle').controller('ClaimListController',
    ['$scope', 'rows',
    function ClaimListController($scope, rows) {
        $scope.rows = rows;
    }
]);