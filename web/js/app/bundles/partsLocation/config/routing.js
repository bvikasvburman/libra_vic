var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_partsLocation', url: 'api/partsLocation'},
    {name: 'api_partsLocation_attachment', url: 'api/partsLocation/:id/attachment'},
    {name: 'api_partsLocation_attachment_one', url: 'api/partsLocation/:id/attachment/:idl'},
    {name: 'api_partsLocation_note', url: 'api/partsLocation/:id/note'},
    {name: 'api_partsLocation_note_one', url: 'api/partsLocation/:id/note/:idl'},
    {name: 'api_partsLocation_message', url: 'api/partsLocation/:id/message'},
    {name: 'api_partsLocation_message_one', url: 'api/partsLocation/:id/message/:idl'},

    Router.getRouteFromBundle('partsLocation'),
    Router.getRouteFromBundle('partsLocation.list'),
    Router.getRouteFromBundle('partsLocation.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('partsLocation.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('partsLocation.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('partsLocation.detail.main'),
    Router.getRouteFromBundle('partsLocation.new.main'),
    Router.getRouteFromBundle('partsLocation.edit.main'),

    Router.getRouteFromBundle('partsLocation.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('partsLocation.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('partsLocation.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),

]);

Router.createRoutesForLinks('partsLocation',linksForAutoRouteGen);