'use strict';

angular.module('partsLocationBundle').factory('PartsLocation', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function PartsLocation(data) {
            // inherit BaseEntity constructor
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('location', 'string');
            this.add('floor', 'string');
            this.add('rack', 'string');
            this.add('rackSection', 'string');
            this.add('barcode', 'string');
            this.add('detail', 'string');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };
        BaseEntity.apply(PartsLocation);
        return PartsLocation;
    }
]);

