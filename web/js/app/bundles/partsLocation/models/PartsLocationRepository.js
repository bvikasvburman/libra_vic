angular.module('partsLocationBundle').factory('PartsLocationRepository', ['BaseRepository', 'PartsLocation',
    function (BaseRepository, PartsLocation) {

    function PartsLocationRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, PartsLocation, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_partsLocation', true));
    };

    BaseRepository.apply(PartsLocationRepository);

    return new PartsLocationRepository();
}]);