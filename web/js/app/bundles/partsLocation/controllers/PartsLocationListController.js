'use strict';

angular.module('partsLocationBundle').controller('PartsLocationListController',
    ['$scope', 'rows',
    function PartsLocationListController($scope, rows) {
        $scope.rows = rows;
    }
]);