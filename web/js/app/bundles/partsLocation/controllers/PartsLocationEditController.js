'use strict';

angular.module('partsLocationBundle').controller('PartsLocationEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'PartsLocationRepository',
    'partsLocationConfig',
    'UtilService',
    'MediaService',
    function PartsLocationEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        PartsLocationRepository,
        partsLocationConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.attachmentLnk   = attachmentLnk;
        $scope.links.noteLnk    = noteLnk;
        $scope.links.messageLnk = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);