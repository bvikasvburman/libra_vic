var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_activity', url: 'api/activity'},
    {name: 'api_activity_attachment', url: 'api/activity/:id/attachment'},
    {name: 'api_activity_attachment_one', url: 'api/activity/:id/attachment/:idl'},
    {name: 'api_activity_note', url: 'api/activity/:id/note'},
    {name: 'api_activity_note_one', url: 'api/activity/:id/note/:idl'},
    {name: 'api_activity_message', url: 'api/activity/:id/message'},
    {name: 'api_activity_message_one', url: 'api/activity/:id/message/:idl'},

    Router.getRouteFromBundle('activity'),
    Router.getRouteFromBundle('activity.list'),
    Router.getRouteFromBundle('activity.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('activity.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('activity.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('activity.detail.main'),
    Router.getRouteFromBundle('activity.new.main'),
    Router.getRouteFromBundle('activity.edit.main'),

    Router.getRouteFromBundle('activity.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('activity.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('activity.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('activity',linksForAutoRouteGen);
