'use strict';

angular.module('activityBundle').controller('ActivityEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'ActivityRepository',
    'activityConfig',
    'UtilService',
    function ActivityEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        ActivityRepository,
        activityConfig,
        UtilService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.attachmentLnk = attachmentLnk;
        $scope.links.noteLnk       = noteLnk;
        $scope.links.messageLnk    = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);