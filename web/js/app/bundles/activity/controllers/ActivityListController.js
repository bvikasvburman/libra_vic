'use strict';

angular.module('activityBundle').controller('ActivityListController',
    ['$scope', 'rows',
    function ActivityListController($scope, rows) {
        $scope.rows = rows;
    }
]);