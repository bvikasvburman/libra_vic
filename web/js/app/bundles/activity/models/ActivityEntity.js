'use strict';

angular.module('activityBundle').factory('Activity', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function Activity(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('title', 'string');
            this.add('messageBody', 'text');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Activity);

        return Activity;
    }
]);
