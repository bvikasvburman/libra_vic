angular.module('activityBundle').factory('ActivityRepository', ['BaseRepository', 'Activity',
    function (BaseRepository, Activity) {

    function ActivityRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Activity, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_activity', true));
    };

    BaseRepository.apply(ActivityRepository);

    return new ActivityRepository();
}]);