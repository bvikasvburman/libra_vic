angular.module('vendorInvoiceBundle').factory('VendorInvoiceRepository', [
    'BaseRepository',
    'VendorInvoice',
    function (
        BaseRepository,
        VendorInvoice
    ) {

        function VendorInvoiceRepository(data) {
            // inherit BaseRepository constructor
            BaseRepository.constructor.call(this, VendorInvoice, data);
            // set api url
            this.setApiURL(Router.getRoutePath('api_vendorInvoice', true));
        };

        BaseRepository.apply(VendorInvoiceRepository);

        return new VendorInvoiceRepository();
    }
]);