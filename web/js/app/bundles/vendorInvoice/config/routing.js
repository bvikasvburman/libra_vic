var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_vendorInvoice', url: 'api/vendorInvoice'},
    {name: 'api_vendorInvoice_attachment', url: 'api/vendorInvoice/:id/attachment'},
    {name: 'api_vendorInvoice_attachment_one', url: 'api/vendorInvoice/:id/attachment/:idl'},
    {name: 'api_vendorInvoice_note', url: 'api/vendorInvoice/:id/note'},
    {name: 'api_vendorInvoice_note_one', url: 'api/vendorInvoice/:id/note/:idl'},
    {name: 'api_vendorInvoice_message', url: 'api/vendorInvoice/:id/message'},
    {name: 'api_vendorInvoice_message_one', url: 'api/vendorInvoice/:id/message/:idl'},

    Router.getRouteFromBundle('vendorInvoice'),
    Router.getRouteFromBundle('vendorInvoice.list'),
    Router.getRouteFromBundle('vendorInvoice.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('vendorInvoice.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('vendorInvoice.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('vendorInvoice.detail.main'),
    Router.getRouteFromBundle('vendorInvoice.new.main'),
    Router.getRouteFromBundle('vendorInvoice.edit.main'),

    Router.getRouteFromBundle('vendorInvoice.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('vendorInvoice.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('vendorInvoice.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('vendorInvoice',linksForAutoRouteGen);

