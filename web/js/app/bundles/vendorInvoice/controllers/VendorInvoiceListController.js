'use strict';

angular.module('vendorInvoiceBundle').controller('VendorInvoiceListController',
    ['$scope', 'rows',
    function VendorInvoiceListController($scope, rows) {
        $scope.rows = rows;
    }
]);