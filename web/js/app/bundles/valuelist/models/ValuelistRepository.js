angular.module('valuelistBundle').factory('ValuelistRepository', [
    'BaseRepository',
    'Valuelist',
    function (
        BaseRepository,
        Valuelist
    ) {
        function ValuelistRepository(data) {
            // inherit BaseRepository constructor
            BaseRepository.constructor.call(this, Valuelist, data);
            // set api url
            this.setApiURL(Router.getRoutePath('api_valuelist', true));
        };

        BaseRepository.apply(ValuelistRepository);

        return new ValuelistRepository();
    }
]);