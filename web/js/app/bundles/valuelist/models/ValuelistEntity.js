'use strict';

angular.module('valuelistBundle').factory('Valuelist', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function Valuelist(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('valuelistName', 'string');
            this.add('value', 'string');
            this.add('code', 'string');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Valuelist);

        return Valuelist;
    }
]);