var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('message'),
];

Router.addRoutes([
    {name: 'api_valuelist', url: 'api/valuelist'},
    {name: 'api_valuelist_message', url: 'api/valuelist/:id/message'},
    {name: 'api_valuelist_message_one', url: 'api/valuelist/:id/message/:idl'},

    Router.getRouteFromBundle('valuelist'),
    Router.getRouteFromBundle('valuelist.list'),
    Router.getRouteFromBundle('valuelist.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('valuelist.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('valuelist.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('valuelist.detail.main'),
    Router.getRouteFromBundle('valuelist.new.main'),
    Router.getRouteFromBundle('valuelist.edit.main'),
]);

Router.createRoutesForLinks('valuelist',linksForAutoRouteGen);


