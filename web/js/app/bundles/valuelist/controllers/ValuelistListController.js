'use strict';

angular.module('valuelistBundle').controller('ValuelistListController',
    ['$scope', 'rows',
    function ValuelistListController($scope, rows) {
        $scope.rows = rows;
    }
]);