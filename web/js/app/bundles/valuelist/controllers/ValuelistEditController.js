'use strict';

angular.module('valuelistBundle').controller('ValuelistEditController', [
    '$scope',
    '$rootScope',
    'row',
    'messageLnk',
    'ValuelistRepository',
    'valuelistConfig',
    'UtilService',
    function ValuelistEditController(
        $scope,
        $rootScope,
        row,
        messageLnk,
        ValuelistRepository,
        valuelistConfig,
        UtilService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.messageLnk  = messageLnk;
    }
]);