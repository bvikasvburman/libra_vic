var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_interestGroup', url: 'api/interestGroup'},
    {name: 'api_interestGroup_attachment', url: 'api/interestGroup/:id/attachment'},
    {name: 'api_interestGroup_attachment_one', url: 'api/interestGroup/:id/attachment/:idl'},
    {name: 'api_interestGroup_note', url: 'api/interestGroup/:id/note'},
    {name: 'api_interestGroup_note_one', url: 'api/interestGroup/:id/note/:idl'},
    {name: 'api_interestGroup_message', url: 'api/interestGroup/:id/message'},
    {name: 'api_interestGroup_message_one', url: 'api/interestGroup/:id/message/:idl'},

    Router.getRouteFromBundle('interestGroup'),
    Router.getRouteFromBundle('interestGroup.list'),
    Router.getRouteFromBundle('interestGroup.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('interestGroup.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('interestGroup.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('interestGroup.detail.main'),
    Router.getRouteFromBundle('interestGroup.new.main'),
    Router.getRouteFromBundle('interestGroup.edit.main'),

    Router.getRouteFromBundle('interestGroup.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('interestGroup.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('interestGroup.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('interestGroup',linksForAutoRouteGen);

