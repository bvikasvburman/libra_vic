'use strict';

angular.module('interestGroupBundle').controller('InterestGroupListController',
    ['$scope', 'rows',
    function InterestGroupListController($scope, rows) {
        $scope.rows = rows;
    }
]);