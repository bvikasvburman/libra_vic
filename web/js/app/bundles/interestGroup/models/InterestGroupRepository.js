angular.module('interestGroupBundle').factory('InterestGroupRepository', [
    'BaseRepository',
    'InterestGroup',
    function (
        BaseRepository,
        InterestGroup
    ) {
        function InterestGroupRepository(data) {
            // inherit BaseRepository constructor
            BaseRepository.constructor.call(this, InterestGroup, data);
            // set api url
            this.setApiURL(Router.getRoutePath('api_interestGroup', true));
        };

        BaseRepository.apply(InterestGroupRepository);

        return new InterestGroupRepository();
    }
]);