'use strict';

angular.module('interestGroupBundle').factory('InterestGroup', [
    'BaseEntity',
    'User',
    function (
        BaseEntity,
        User
    ) {
        function InterestGroup(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('title', 'string');
            this.add('type', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(InterestGroup);

        return InterestGroup;
    }
]);

