'use strict';

angular.module('approvedComponentBundle').factory('ApprovedComponent', [
    'BaseEntity',
    'Company',
    'PartsLocation',
    'User',
    function (
        BaseEntity,
        Company,
        PartsLocation,
        User
    ) {
        function ApprovedComponent(data) {
            // inherit BaseEntity constructor
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('part', 'string');
            this.add('code', 'string');
            this.add('description', 'string');
            this.add('measurement', 'string');
            this.add('companyId', 'number');
            this.add('approved', 'number');
            this.add('partsLocationId', 'number');
            this.add('barcode', 'string');
            this.add('category', 'string');
            this.add('company', Company);
            this.add('partsLocation', PartsLocation);
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

    BaseEntity.apply(ApprovedComponent);

    return ApprovedComponent;
}]);

