angular.module('approvedComponentBundle').factory('ApprovedComponentRepository', ['BaseRepository', 'ApprovedComponent',
    function (BaseRepository, ApprovedComponent) {

    function ApprovedComponentRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, ApprovedComponent, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_approvedComponent', true));
    };

    BaseRepository.apply(ApprovedComponentRepository);

    return new ApprovedComponentRepository();
}]);