var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
];

Router.addRoutes([
    {name: 'api_approvedComponent', url: 'api/approvedComponent'},
    {name: 'api_approvedComponent_attachment', url: 'api/approvedComponent/:id/attachment'},
    {name: 'api_approvedComponent_attachment_one', url: 'api/approvedComponent/:id/attachment/:idl'},
    {name: 'api_approvedComponent_note', url: 'api/approvedComponent/:id/note'},
    {name: 'api_approvedComponent_note_one', url: 'api/approvedComponent/:id/note/:idl'},
    {name: 'api_approvedComponent_message', url: 'api/approvedComponent/:id/message'},
    {name: 'api_approvedComponent_message_one', url: 'api/approvedComponent/:id/message/:idl'},

    Router.getRouteFromBundle('approvedComponent'),
    Router.getRouteFromBundle('approvedComponent.list'),
    Router.getRouteFromBundle('approvedComponent.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('approvedComponent.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('approvedComponent.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('approvedComponent.detail.main'),
    Router.getRouteFromBundle('approvedComponent.new.main'),
    Router.getRouteFromBundle('approvedComponent.edit.main'),

    Router.getRouteFromBundle('approvedComponent.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('approvedComponent.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('approvedComponent.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),

]);

Router.createRoutesForLinks('approvedComponent',linksForAutoRouteGen);
