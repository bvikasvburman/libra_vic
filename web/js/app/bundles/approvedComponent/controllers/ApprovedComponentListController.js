'use strict';

angular.module('approvedComponentBundle').controller('ApprovedComponentListController',
    ['$scope', 'rows',
    function ApprovedComponentListController($scope, rows) {
        $scope.rows = rows;
    }
]);