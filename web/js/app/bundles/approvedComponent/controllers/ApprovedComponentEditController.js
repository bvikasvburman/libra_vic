'use strict';

angular.module('approvedComponentBundle').controller('ApprovedComponentEditController', [
    '$scope',
    '$rootScope',
    'row',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'ApprovedComponentRepository',
    'CompanyRepository',
    'PartsLocationRepository',
    'ValuelistRepository',
    'approvedComponentConfig',
    'UtilService',
    'MediaService',
    function ApprovedComponentEditController(
        $scope,
        $rootScope,
        row,
        attachmentLnk,
        noteLnk,
        messageLnk,
        ApprovedComponentRepository,
        CompanyRepository,
        PartsLocationRepository,
        ValuelistRepository,
        approvedComponentConfig,
        UtilService,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);
        $scope.vls.company       = [];
        $scope.vls.partsLocation = [];
        $scope.vls.category      = [];

        CompanyRepository.all().then(function (companies) {
            angular.copy(companies, $scope.vls.company);
        });
        PartsLocationRepository.all().then(function (partsLocations) {
            angular.copy(partsLocations, $scope.vls.partsLocation);
        });
        ValuelistRepository.findBy({valuelistName: 'approvedComponentCategory'}, 'vl')
        .then(function (categories) {
            angular.copy(categories, $scope.vls.category);
        });

        $scope.links.attachmentLnk   = attachmentLnk;
        $scope.links.noteLnk    = noteLnk;
        $scope.links.messageLnk = messageLnk;

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
    }
]);