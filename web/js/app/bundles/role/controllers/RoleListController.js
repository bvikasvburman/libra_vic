'use strict';

angular.module('roleBundle').controller('RoleListController',
    ['$scope', 'rows',
    function RoleListController($scope, rows) {
        $scope.rows = rows;
    }
]);