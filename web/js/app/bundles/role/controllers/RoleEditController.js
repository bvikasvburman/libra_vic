'use strict';

angular.module('roleBundle').controller('RoleEditController',
    ['$scope', '$rootScope', 'row',
    'UtilService',
    'RoleRepository',
    'roleConfig',
    function RoleEditController($scope, $rootScope, row,
        UtilService,
        RoleRepository,
        roleConfig
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        //set the links array here

    }
]);