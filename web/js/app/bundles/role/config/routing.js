var links = [

];

Router.addRoutes([
    /* api */
    {name: 'api_role', url: 'api/role'},

    Router.getRouteFromBundle('role'),
    Router.getRouteFromBundle('role.list'),
    Router.getRouteFromBundle('role.detail', {links: links}),
    Router.getRouteFromBundle('role.new', {links: links}),
    Router.getRouteFromBundle('role.edit', {links: links}),

    Router.getRouteFromBundle('role.detail.main'),
    Router.getRouteFromBundle('role.new.main'),
    Router.getRouteFromBundle('role.edit.main'),
]);

Router.createRoutesForLinks('role', links);