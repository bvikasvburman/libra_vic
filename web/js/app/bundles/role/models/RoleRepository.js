angular.module('roleBundle').factory('RoleRepository', ['BaseRepository', 'Role',
    function (BaseRepository, Role) {

    function RoleRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Role, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_role', true));
    };

    BaseRepository.apply(RoleRepository);

    return new RoleRepository();
}]);