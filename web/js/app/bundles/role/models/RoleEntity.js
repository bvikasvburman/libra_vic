'use strict';

angular.module('roleBundle').factory('Role', [
    'BaseEntity',
    // 'User',
    function (
        BaseEntity
        // User
    ) {
        function Role(data) {
            BaseEntity.constructor.call(this, data);
			if(data === undefined) {
			    data = {};
				data.roomId = [1,2,3,4,5,6,7];
			    data.columns = ["list","detail","new","edit","delete","duplicate","search",	"print","publish","unpublish","import","export"];
			}
            this.add('id', 'number');
            this.add('title', 'string');
            this.add('roleType', 'string');
            this.add('description', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
			if(data !== undefined && data.columns !== undefined && data.roomId !== undefined) {
				this.add('room', 'number');
				this.setFieldValue('room', data.roomId);
				this.add('columns', 'number');
				this.setFieldValue('columns', data.columns);
				for (var r=0; r<Object.keys(data.roomId).length; r++) {
					if(data.roomRoleId !== undefined && data.roomRoleId[data.roomId[r]] !== undefined) {
						this.add('roomRoleId_'+data.roomId[r], 'number');
						this.setFieldValue('roomRoleId_'+data.roomId[r], data.roomRoleId[data.roomId[r]]);
					}
					for (var i=0; i<Object.keys(data.columns).length; i++) {
						this.add(data.columns[i]+'_'+data.roomId[r], 'number');
					}
				}
			}
            // this.add('userCreated', User);
            // this.add('userModified', User);
        };

        BaseEntity.apply(Role);

        return Role;
    }
]);