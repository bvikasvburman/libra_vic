angular.module('enquiryBundle').factory('EnquiryRepository', ['BaseRepository', 'Enquiry',
    function (BaseRepository, Enquiry) {

    function EnquiryRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Enquiry, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_enquiry', true));
    };

    BaseRepository.apply(EnquiryRepository);

    return new EnquiryRepository();
}]);