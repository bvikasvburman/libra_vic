'use strict';

angular.module('enquiryBundle').factory('Enquiry', [
    'BaseEntity',
    'Company',
    'Contact',
    'User',
    function (
        BaseEntity,
        Company,
        Contact,
        User
    ) {
        function Enquiry(data) {
            // inherit BaseEntity constructor
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('code', 'string');
            this.add('description', 'string');
            this.add('enquiryDate', 'date');
            this.add('status', 'string');
            this.add('flag', 'number');
            this.add('company', Company, {vlFlds: ['id', 'name']});
            this.add('contact', Contact, {vlFlds: ['id']});
            this.add('projectStartDate', 'date');
            this.add('projectEndDate', 'date');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('userCreated', User);
            this.add('userModified', User);
        };

        BaseEntity.apply(Enquiry);

        return Enquiry;
    }
]);

