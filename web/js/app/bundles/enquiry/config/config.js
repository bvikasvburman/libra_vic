angular.module('enquiryBundle').factory('enquiryConfig', [
    'BundleDefConfig',
    'ListService',
    'ListFldService',
    'UtilService',
    'LinkService',
    'LinkFldService',
    'TabService',
    function(
        BundleDefConfig,
        ListService,
        ListFldService,
        UtilService,
        LinkService,
        LinkFldService,
        TabService
    ) {
        var lf = ListFldService;
        var lnkFld = LinkFldService;

        var listFlds = [
            new lf('code', {'title': 'Enquiry Code'}),
            new lf('company.name', {'title': 'Company Name'}),
            new lf('description'),
            new lf('enquiryDate', {type: 'date'}),
            new lf('status'),
        ];

        var links = [
            new LinkService('protoType', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('productName', {hasDetailLink: true}),
                    new lnkFld('productFamily'),
                    new lnkFld('productType'),
                    new lnkFld('artNo'),
                    new lnkFld('creationSequence'),
                    new lnkFld('code', {title: 'Product Code'}),
                    new lnkFld('creationDate', {type: 'datetime', width: '20pc'}),
                ]
            }),
            new LinkService('costing', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'Quotation Code'}),
                    new lnkFld('enquiry.code', {title: 'Enquiry Code'}),
                    new lnkFld('enquiry.description', {title: 'Enquiry Description'}),
                    new lnkFld('quotationDate', {type: 'date'}),
                    new lnkFld('status'),
                ]
            }),
            new LinkService('quotation', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'Quotation Code'}),
                    new lnkFld('enquiry.code', {title: 'Enquiry Code'}),
                    new lnkFld('enquiry.description', {title: 'Enquiry Description'}),
                    new lnkFld('quotationDate', {type: 'date'}),
                    new lnkFld('status'),
                ]
            }),
            new LinkService('tradingOrder', {
                hasNew: false,
                hasDelete: false,
                hasEdit: false,
                fields: [
                    new lnkFld('code', {title: 'Order Code'}),
                    new lnkFld('quotation.code', {title: 'Quotation Code'}),
                    new lnkFld('clientOrderRef'),
                    new lnkFld('orderDetails'),
                    new lnkFld('etd', {type: 'date'}),
                    new lnkFld('orderDate', {type: 'date'}),
                    new lnkFld('status'),
                ]
            }),
            new LinkService('attachment', {
                fields: [
                    new lnkFld('media.displayTitle', {hasMediaLink: true}),
                ]
            }),
            new LinkService('note', {
                fields: [
                    new lnkFld('note', {truncateLength: 100}),
                    new lnkFld('creationDate', {type: 'datetime', width: '20pc'}),
                ]
            }),
            new LinkService('message'),
        ];


        var tabs = [
            new TabService('main'),
            new TabService('mainGantt', {title: 'Enquiry Dates & Gates'}),
        ];
        tabs = new TabService().addLinksToTab(tabs, links);

        return new BundleDefConfig('enquiry', {
           listObj: new ListService(UtilService.getCurrBundle(), listFlds),
           links: links,
           tabs: tabs,
        })
   }
]);