var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('protoType'),
    getLOFR('costing'),
    getLOFR('quotation'),
    getLOFR('tradingOrder'),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
    getLOFR('note'),
    getLOFR('message'),
    getLOFR('task'),
];

Router.addRoutes([
    {name: 'api_enquiry', url: 'api/enquiry'},
    {name: 'api_enquiry_protoType', url: 'api/enquiry/:id/protoType'},
    {name: 'api_enquiry_protoType_one', url: 'api/enquiry/:id/protoType/:idl'},
    {name: 'api_enquiry_costing', url: 'api/enquiry/:id/costing'},
    {name: 'api_enquiry_costing_one', url: 'api/enquiry/:id/costing/:idl'},
    {name: 'api_enquiry_quotation', url: 'api/enquiry/:id/quotation'},
    {name: 'api_enquiry_quotation_one', url: 'api/enquiry/:id/quotation/:idl'},
    {name: 'api_enquiry_tradingOrder', url: 'api/enquiry/:id/tradingOrder'},
    {name: 'api_enquiry_tradingOrder_one', url: 'api/enquiry/:id/tradingOrder/:idl'},
    {name: 'api_enquiry_attachment', url: 'api/enquiry/:id/attachment'},
    {name: 'api_enquiry_attachment_one', url: 'api/enquiry/:id/attachment/:idl'},
    {name: 'api_enquiry_note', url: 'api/enquiry/:id/note'},
    {name: 'api_enquiry_note_one', url: 'api/enquiry/:id/note/:idl'},
    {name: 'api_enquiry_message', url: 'api/enquiry/:id/message'},
    {name: 'api_enquiry_message_one', url: 'api/enquiry/:id/message/:idl'},


    Router.getRouteFromBundle('enquiry'),
    Router.getRouteFromBundle('enquiry.list'),
    Router.getRouteFromBundle('enquiry.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('enquiry.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('enquiry.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('enquiry.detail.main'),
    Router.getRouteFromBundle('enquiry.new.main'),
    Router.getRouteFromBundle('enquiry.edit.main'),

    Router.getRouteFromBundle('enquiry.detail.mainGantt'),
    Router.getRouteFromBundle('enquiry.new.mainGantt'),
    Router.getRouteFromBundle('enquiry.edit.mainGantt'),

    Router.getRouteFromBundle('enquiry.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),
    Router.getRouteFromBundle('enquiry.edit.note.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
    Router.getRouteFromBundle('enquiry.edit.note.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'note')}),
]);

Router.createRoutesForLinks('enquiry', linksForAutoRouteGen);