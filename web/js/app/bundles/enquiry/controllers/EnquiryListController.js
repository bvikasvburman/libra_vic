'use strict';

angular.module('enquiryBundle').controller('EnquiryListController',
    ['$scope', 'rows',
    function EnquiryListController($scope, rows) {
        $scope.rows = rows;
    }
]);