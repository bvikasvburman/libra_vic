'use strict';

angular.module('enquiryBundle').controller('EnquiryEditController', [
    '$scope',
    '$rootScope',
    '$log',
    'row',
    'protoTypeLnk',
    'costingLnk',
    'quotationLnk',
    'tradingOrderLnk',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'protoTypeLnk',
    'EnquiryRepository',
    'CompanyRepository',
    'ContactRepository',
    'UtilService',
    'GanttUtilService',
    'enquiryConfig',
    'MediaService',
    '$mdDialog',
    '$state',
    function EnquiryEditController(
        $scope,
        $rootScope,
        $log,
        row,
        protoTypeLnk,
        costingLnk,
        quotationLnk,
        tradingOrderLnk,
        attachmentLnk,
        noteLnk,
        messageLnk,
        protoTypeLnk,
        EnquiryRepository,
        CompanyRepository,
        ContactRepository,
        UtilService,
        GanttUtilService,
        enquiryConfig,
        MediaService,
        $mdDialog,
        $state
    ) {

        UtilService.initializeCommonControllerVars($scope, row);

        $scope.links.costingLnk      = costingLnk;
        $scope.links.quotationLnk    = quotationLnk;
        $scope.links.tradingOrderLnk = tradingOrderLnk;
        $scope.links.attachmentLnk   = attachmentLnk;
        $scope.links.noteLnk         = noteLnk;
        $scope.links.protoTypeLnk    = protoTypeLnk;

        $scope.vls.status = ['Confirmed', 'Cancelled', 'Pending'];
        $scope.vls.company = [];
        $scope.vls.contact = [];

        $scope.gus = GanttUtilService;

        CompanyRepository.all({retType: 'vl'}).then(function (companies) {
            angular.copy(companies, $scope.vls.company);
            $scope.fou.getSubVL('contact', row, 'company');
        });

        // $scope.getContacts = function() {
        //     var criteria = {companyId: row.company.id};
        //     // row.contact = null; //make secondary valuelist field empty on change
        //     ContactRepository.findBy(criteria).then(function (contacts) {
        //         angular.copy(contacts, $scope.contacts);
        //     });
        // }

        //gantt chart
        var opts = {
            fromDate: row.projectStartDate,
            toDate: row.projectEndDate,
            ganttName: 'enquiry',
        }
        $scope.ganttOptions = GanttUtilService.getGanttOptions($scope, opts);

        $scope.ganttData = [];
        GanttUtilService.getGanttData('enquiry', row.id).then(function(response) {
            $scope.ganttData = response.data;
        });

        $scope.attachmentUploader = '';
        if (UtilService.getCurrLink() == "attachment") {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }

        // Proto Type functions
        $scope.createNewProtoType = function(enquiryId) {
            var stateName = "protoType.new.main";
            $state.go(stateName, {'enquiryId': enquiryId});
        }
    }
]);