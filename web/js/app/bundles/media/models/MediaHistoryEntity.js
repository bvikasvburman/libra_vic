'use strict';

angular.module('mediaBundle').factory('MediaHistory', [
    'BaseEntity',
    'Company',
    'Media',
    function (
        BaseEntity,
        Company,
        Media
    ) {
    function MediaHistory(data) {
        BaseEntity.constructor.call(this, data);

        this.add('id', 'number');
        this.add('companyId', 'number');
        this.add('mediaId', 'number');
        this.add('mediaType', 'string');
        this.add('creationDate', 'string');
        this.add('media', Media);
    };

    BaseEntity.apply(MediaHistory);

    return MediaHistory;
}]);
