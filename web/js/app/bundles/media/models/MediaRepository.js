angular.module('mediaBundle').factory('MediaRepository', ['BaseRepository', 'Media',
    function (BaseRepository, Media) {

    function MediaRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, Media, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_media', true));
    };

    BaseRepository.apply(MediaRepository);

    return new MediaRepository();
}]);