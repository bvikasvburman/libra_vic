angular.module('mediaBundle').factory('MediaHistoryRepository', [
    '$http',
    'BaseRepository',
    'MediaHistory',
    function (
        $http,
        BaseRepository,
        MediaHistory
    ) {

    function MediaHistoryRepository(data) {
        BaseRepository.constructor.call(this, MediaHistory, data);
    };

    BaseRepository.apply(MediaHistoryRepository);

    return new MediaHistoryRepository();
}]);