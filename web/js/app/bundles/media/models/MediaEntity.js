'use strict';

angular.module('mediaBundle').factory('Media', ['BaseEntity',
    function (BaseEntity) {
    function Media(data) {
        // inherit BaseEntity constructor
        BaseEntity.constructor.call(this, data);

        this.add('id', 'number');
        this.add('actualFileName', 'string');
        this.add('fileName', 'string');
        this.add('displayTitle', 'string');
        this.add('contentType', 'string');
        this.add('mediaSize', 'number');
        this.add('creationDate', 'string');
        this.add('modificationDate', 'string');
    };

    BaseEntity.apply(Media);

    return Media;
}]);

