'use strict';

angular.module('mediaBundle').controller('MediaListController',
    ['$scope', 'rows',
    function MediaListController($scope, rows) {
        $scope.rows = rows;
    }
]);