'use strict';

angular.module('mediaBundle').controller('MediaEditController',
    ['$scope', '$rootScope', 'row',
    'MediaRepository',
    'mediaConfig',
    'UtilService',
    function MediaEditController($scope, $rootScope, row,
        MediaRepository,
        mediaConfig,
        UtilService
    ) {
        $scope.date = new Date();
        UtilService.initializeCommonControllerVars($scope, row);
    }
]);