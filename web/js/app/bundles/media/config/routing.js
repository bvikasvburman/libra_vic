var linksForAutoRouteGen = [
    Router.getLinkObjForRoute('image'),
    Router.getLinkObjForRoute('note'),
    Router.getLinkObjForRoute('message'),
];

Router.addRoutes([
    /* api */
    {name: 'api_media', url: 'api/media'},

    Router.getRouteFromBundle('media'),
    Router.getRouteFromBundle('media.list'),
    Router.getRouteFromBundle('media.detail'),
    Router.getRouteFromBundle('media.new'),
    Router.getRouteFromBundle('media.edit'),

    Router.getRouteFromBundle('media.detail.main'),
    Router.getRouteFromBundle('media.new.main'),
    Router.getRouteFromBundle('media.edit.main'),
]);

Router.createRoutesForLinks('media', linksForAutoRouteGen);