angular.module('protoTypeBundle').factory('protoTypeConfig', [
    'BundleDefConfig',
    'ListService',
    'ListFldService',
    'UtilService',
    'LinkService',
    'LinkFldService',
    'TabService',
    function(
        BundleDefConfig,
        ListService,
        ListFldService,
        UtilService,
        LinkService,
        LinkFldService,
        TabService
    ) {
        var lf = ListFldService;
        var lnkFld = LinkFldService;

        var listFlds = [
            new lf('productName'),
            new lf('productFamily'),
            new lf('productType'),
            new lf('artNo'),
            new lf('creationSequence'),
            new lf('code', {title: 'Product Code'}),
            new lf('creationDate', {type: 'datetime'}),
        ];

        var links = [
            new LinkService('research', {
                fields: [
                    new lnkFld('code'),
                    new lnkFld('researchArea'),
                    new lnkFld('enquiry.code', {title: 'Enquiry Code'}),
                    new lnkFld('company.name', {title: 'Company Name'}),
                    new lnkFld('creationDate', {type: 'date'}),
                ]
            }),
            new LinkService('image', {
                fields: [
                    new lnkFld('media.displayTitle', {hasMediaLink: true}),
                ]
            }),
            new LinkService('colorMaster', {
                fields: [
                    new lnkFld('colorMaster.title'),
                    new lnkFld('colorMaster.code', {editable: true}),
                    new lnkFld('colorType', {type: 'select', editable: true}),
                    new lnkFld('creationDate', {type: 'date'}),
                ]
            }),
            new LinkService('approvedPackaging'),
            new LinkService('approvedMaterial'),
            new LinkService('approvedComponent'),
            new LinkService('masterCompliance'),
            new LinkService('protoAssembly'),

            new LinkService('attachment', {
                fields: [
                    new lnkFld('media.displayTitle', {hasMediaLink: true}),
                ]
            }),
            new LinkService('note', {
                fields: [
                    new lnkFld('note', {truncateLength: 100}),
                    new lnkFld('creationDate', {type: 'datetime', width: '20pc'}),
                ]
            }),
            new LinkService('message'),
        ];

        var tabs = [
            new TabService('main'),
        ];
        tabs = new TabService().addLinksToTab(tabs, links);

        return new BundleDefConfig('protoType', {
           listObj: new ListService(UtilService.getCurrBundle(), listFlds),
           links: links,
           tabs: tabs,
        })
   }]
);