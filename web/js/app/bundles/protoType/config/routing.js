var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
    getLOFR('research'),
    getLOFR('branch'),
    getLOFR('enquiry'),
    getLOFR('image', {repository: 'MediaHistory', linkNameActual: 'media'}),
    getLOFR('attachment', {repository: 'MediaHistory', linkNameActual: 'media'}),
    getLOFR('colorMaster'),
    getLOFR('approvedPackaging'),
    getLOFR('approvedMaterial'),
    getLOFR('approvedComponent'),
    getLOFR('masterCompliance'),
    getLOFR('protoAssembly'),
    getLOFR('note'),
    getLOFR('message'),
];

Router.addRoutes([
    /* api */
    {name: 'api_protoType', url: 'api/protoType'},
    {name: 'api_protoType_research', url: 'api/protoType/:id/research'},
    {name: 'api_protoType_research_one', url: 'api/protoType/:id/research/:idl'},
    {name: 'api_protoType_image', url: 'api/protoType/:id/image'},
    {name: 'api_protoType_image_one', url: 'api/protoType/:id/image/:idl'},
    {name: 'api_protoType_attachment', url: 'api/protoType/:id/attachment'},
    {name: 'api_protoType_attachment_one', url: 'api/protoType/:id/attachment/:idl'},
    {name: 'api_protoType_colorMaster', url: 'api/protoType/:id/colorMaster'},
    {name: 'api_protoType_colorMaster_one', url: 'api/protoType/:id/colorMaster/:idl'},
    {name: 'api_protoType_approvedPackaging', url: 'api/protoType/:id/approvedPackaging'},
    {name: 'api_protoType_approvedPackaging_one', url: 'api/protoType/:id/approvedPackaging/:idl'},
    {name: 'api_protoType_approvedMaterial', url: 'api/protoType/:id/approvedMaterial'},
    {name: 'api_protoType_approvedMaterial_one', url: 'api/protoType/:id/approvedMaterial/:idl'},
    {name: 'api_protoType_approvedComponent', url: 'api/protoType/:id/approvedComponent'},
    {name: 'api_protoType_approvedComponent_one', url: 'api/protoType/:id/approvedComponent/:idl'},
    {name: 'api_protoType_masterCompliance', url: 'api/protoType/:id/masterCompliance'},
    {name: 'api_protoType_masterCompliance_one', url: 'api/protoType/:id/masterCompliance/:idl'},
    {name: 'api_protoType_protoAssembly', url: 'api/protoType/:id/protoAssembly'},
    {name: 'api_protoType_protoAssembly_one', url: 'api/protoType/:id/protoAssembly/:idl'},
    {name: 'api_protoType_note', url: 'api/protoType/:id/note'},
    {name: 'api_protoType_note_one', url: 'api/protoType/:id/note/:idl'},
    {name: 'api_protoType_message', url: 'api/protoType/:id/message'},
    {name: 'api_protoType_message_one', url: 'api/protoType/:id/message/:idl'},


    Router.getRouteFromBundle('protoType'),
    Router.getRouteFromBundle('protoType.list'),
    Router.getRouteFromBundle('protoType.detail', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('protoType.new', {links: linksForAutoRouteGen}),
    Router.getRouteFromBundle('protoType.edit', {links: linksForAutoRouteGen}),

    Router.getRouteFromBundle('protoType.detail.main'),
    Router.getRouteFromBundle('protoType.new.main', { queryPars: {enquiryId:''} }),
    Router.getRouteFromBundle('protoType.edit.main'),

    Router.getRouteFromBundle('protoType.edit.image.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'image')} ),
    Router.getRouteFromBundle('protoType.edit.attachment.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'attachment')} ),

    Router.getRouteFromBundle('protoType.edit.research.edit', {linkObj: searchLOFR(linksForAutoRouteGen, 'research')}),
    Router.getRouteFromBundle('protoType.edit.research.new', {linkObj: searchLOFR(linksForAutoRouteGen, 'research')}),
]);

Router.createRoutesForLinks('protoType', linksForAutoRouteGen);