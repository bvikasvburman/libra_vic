'use strict';

angular.module('protoTypeBundle').factory('ProtoType', [
    'BaseEntity',
    'User',
    'Enquiry',
    function (
        BaseEntity,
        User,
        Enquiry
    ) {
        function ProtoType(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('enquiryId', 'number');
            this.add('code', 'string');
            this.add('artNo', 'string');
            this.add('productFamilyCode', 'string');
            this.add('productTypeCode', 'string');
            this.add('productName', 'string');
            this.add('creationSequence', 'string');
            this.add('sampleReadyDate', 'date');
            this.add('requestedQty', 'number');
            this.add('weight', 'number');
            this.add('capacity', 'number');
            this.add('dimLength', 'number');
            this.add('dimWidth', 'number');
            this.add('dimHeight', 'number');
            this.add('companyIdSupplier', 'number');
            this.add('companyIdClient', 'number');
            this.add('description', 'string');
            this.add('flag', 'number');
            this.add('creationDate', 'datetime');
            this.add('modificationDate', 'datetime');
            this.add('enquiry', Enquiry, {vlFlds: ['id', 'code']});
            this.add('userCreated', User);
            this.add('userModified', User);
            this.add('productFamily', 'string');
            this.add('productType', 'string');
        };

        BaseEntity.apply(ProtoType);

        return ProtoType;
    }
]);
