angular.module('protoTypeBundle').factory('ProtoTypeRepository', ['BaseRepository', 'ProtoType',
    function (BaseRepository, ProtoType) {

    function ProtoTypeRepository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, ProtoType, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_protoType', true));
    };

    BaseRepository.apply(ProtoTypeRepository);

    return new ProtoTypeRepository();
}]);