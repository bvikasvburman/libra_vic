'use strict';

angular.module('protoTypeBundle').factory('ProtoTypeColorMaster', [
    'BaseEntity',
    function (
        BaseEntity
    ) {
        function ProtoTypeColorMaster(data) {
            BaseEntity.constructor.call(this, data);

            this.add('id', 'number');
            this.add('colorType', 'string');
            this.add('creationDate', 'datetime');
        };

        BaseEntity.apply(ProtoTypeColorMaster);

        return ProtoTypeColorMaster;
    }
]);
