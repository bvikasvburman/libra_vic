'use strict';

angular.module('protoTypeBundle').controller('ProtoTypeListController',
    ['$scope', 'rows',
    function ProtoTypeListController($scope, rows) {
        $scope.rows = rows;
    }
]);