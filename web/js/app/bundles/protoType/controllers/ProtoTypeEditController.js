'use strict';

angular.module('protoTypeBundle').controller('ProtoTypeEditController', [
    '$scope',
    '$rootScope',
    '$location',
    'row',
    'researchLnk',
    'imageLnk',
    'colorMasterLnk',
    'approvedPackagingLnk',
    'approvedMaterialLnk',
    'approvedComponentLnk',
    'masterComplianceLnk',
    'protoAssemblyLnk',
    'attachmentLnk',
    'noteLnk',
    'messageLnk',
    'ProtoTypeRepository',
    'protoTypeConfig',
    'UtilService',
    'ProductCodingRepository',
    'EnquiryRepository',
    'MediaService',
    function ProtoTypeEditController(
        $scope,
        $rootScope,
        $location,
        row,
        researchLnk,
        imageLnk,
        colorMasterLnk,
        approvedPackagingLnk,
        approvedMaterialLnk,
        approvedComponentLnk,
        masterComplianceLnk,
        protoAssemblyLnk,
        attachmentLnk,
        noteLnk,
        messageLnk,
        ProtoTypeRepository,
        protoTypeConfig,
        UtilService,
        ProductCodingRepository,
        EnquiryRepository,
        MediaService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);
        $scope.vls.productFamily = [];
        $scope.vls.productType   = [];
        $scope.vls.creationSequence = [
            {code: 'W', displayVal: 'Initial Wish'},
            {code: 'G', displayVal: 'Graphic'},
            {code: 'P', displayVal: 'Proto'},
            {code: 'F', displayVal: 'Proto Final'},
            {code: 'V', displayVal: 'Validated'},
        ];

        //value list: productFamily & productType
        ProductCodingRepository.findBy({}, 'prodFamily')
        .then(function (records) {
            angular.copy(records, $scope.vls.productFamily);
            $scope.getProductTypes();
        });
        $scope.getProductTypes = function() {
            var criteria = {productFamilyCode: row.productFamilyCode};
            ProductCodingRepository.findBy(criteria, 'prodType')
            .then(function (vlModelRows) {
                angular.copy(vlModelRows, $scope.vls.productType);
            });
        }

        $scope.setProductCode = function() {
            row.code = row.productFamilyCode + '-'
                     + row.productTypeCode + '-'
                     + row.artNo + '-'
                     + row.creationSequence;
        };

        $scope.links.researchLnk          = researchLnk;
        $scope.links.imageLnk             = imageLnk;
        $scope.links.colorMasterLnk       = colorMasterLnk;
        $scope.links.approvedPackagingLnk = approvedPackagingLnk;
        $scope.links.approvedMaterialLnk  = approvedMaterialLnk;
        $scope.links.approvedComponentLnk = approvedComponentLnk;
        $scope.links.masterComplianceLnk  = masterComplianceLnk;
        $scope.links.protoAssemblyLnk     = protoAssemblyLnk;
        $scope.links.attachmentLnk        = attachmentLnk;
        $scope.links.noteLnk              = noteLnk;
        $scope.links.messageLnk           = messageLnk;

        $scope.attachmentUploader = '';
        $scope.imageUploader = '';
        if (UtilService.getCurrLink() == 'attachment') {
            $scope.attachmentUploader = MediaService.getNewFileUploader($scope, 'attachment');
        }
        if (UtilService.getCurrLink() == 'image') {
            $scope.imageUploader = MediaService.getNewFileUploader($scope, 'image');
        }

        //Enquiry related stuff
        $scope.enquiryId = '';
        $scope.enquiryRow = {};
        if ($location.search().enquiryId) {
            $scope.enquiryId = $location.search().enquiryId;
            row.enquiryId = $scope.enquiryId;
            EnquiryRepository.find($scope.enquiryId).then(function (rowModel) {
                angular.copy(rowModel, $scope.enquiryRow);
            });
        }

        $scope.selected = [];
    }
]);