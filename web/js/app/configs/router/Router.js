var Router = Router || { Routes: {} };

Router = (function ($routeProvider, language, $injector) {

    /**
     * url: '/company',
     * templateUrl: 'templates/company/company/detail',
     * controller: 'CompanyEditController'
     */
    var routes = [];

    return {
        addRoutes: function (newRoutes) {
            for (var routeName in newRoutes) {
                routes[routes.length] = newRoutes[routeName];
            }
        },

        getBaseUrl: function () {
            var base = angular.element(document.getElementsByTagName('base')).attr('href');
            return base ? base : '';
        },

        getAllRoutes: function () {
            return routes;
        },

        /**
         * Gets the url property of a route.
         *
         * @param {string} stateName
         * @returns {object}
         */
        getRouteFromBundle: function(stateName, opts) {
            var opts = angular.extend({}, {
                // getting this from routing.js for creating default
                // resolve vars links tab injected to controller
                links: {},
                linkObj: {}, //if we received linkObj then we use it for accessing link's repository
                queryPars: {},
            }, opts);

            //stateName = company.list
            var arr = s(stateName).words('.');
            var bundle = arr[0]; //company
            var view = (typeof arr[1] !== 'undefined') ? arr[1] : ''; //detail
            var link = (typeof arr[2] !== 'undefined') ? arr[2] : ''; //contact - link tab or a main tab

            //ex: edit view inside a link. ex: company/contact/{edit}
            var linkView = (typeof arr[3] !== 'undefined') ? arr[3] : '';

            var bundleName = s.capitalize(bundle);
            var viewName   = s.capitalize(view);
            var linkName   = s.capitalize(link);

            //ex: get-template/company/company/list
            //first company is bunldle but second company is the controller in symfony
            var templateUrl = 'get-template/main/' + bundle + '/' + bundle + '/' + view;
            var reposName  = bundleName + 'Repository'; //ex: CompanyRepository
            var entityName = s.capitalize(bundle); //ex: Company
            var reposNameLink = linkName + 'Repository'; //ex: ContactRepository

            //set the linkObj in the route for later usage
            //not especially used for route related operation but used to access repository at some point.
            var route = {linkObj: opts.linkObj};
            route.name = stateName;

            //this is a parent abstract state. ex: company ie. /company
            if (arr.length == 1) {
                route.url = '/' + bundle; //ex: /company
                route.abstract = true;
                route.template = '<ui-view/>';
            }
            //this a normal list/detail/new/edit page
            else if (arr.length == 2) { //
                //ex: get-template/company/company/list
                //first company is bunldle but second company is the controller in symfony
                route.templateUrl =  templateUrl;

                if (view == 'list') {
                    route.url = '/list'; //ex: /list
                    route.resolve = {
                        rows: function ($injector) {
                            var reposObj = $injector.get(reposName);
                            return reposObj.all();
                        }
                    };
                    route.controller = bundleName + viewName + 'Controller'; //ex: CompanyListController

                } else if (view == 'detail') {
                    //ex: /company/detail/:id
                    route.url = '/detail/:id';
                    var resolve = {
                        row: function ($injector, $stateParams) {
                            var reposObj = $injector.get(reposName); //ex: new CompanyRepository()
                            return reposObj.find($stateParams.id);
                        }
                    };
                    resolve = Router.getDefaultLinkResolves(resolve, opts.links);
                    route.resolve = resolve;
                    route.controller = bundleName + 'EditController';


                } else if (view == 'new') {
                    route.url = '/new';
                    var resolve = {
                        row: function ($injector, Company) {
                            var entityObj = $injector.get(entityName);
                            return new entityObj(); //ex: new Company()
                        }
                    };
                    resolve = Router.getDefaultLinkResolves(resolve, opts.links);
                    route.resolve = resolve;
                    route.controller = bundleName + 'EditController';

                } else if (view == 'edit') {
                    //ex: /company/edit/:id
                    route.url = '/edit/:id';
                    var resolve = {
                        row: function ($injector, $stateParams) {
                            var reposObj = $injector.get(reposName); //ex: new CompanyRepository()
                            return reposObj.find($stateParams.id);
                        }
                    };

                    resolve = Router.getDefaultLinkResolves(resolve, opts.links);
                    route.resolve = resolve;
                    route.controller = bundleName + 'EditController';
                }
            }

            //this a is a link or a main tab
            else if (arr.length == 3) {
                //ex: /company/edit/:id/contact
                route.url = '/' + link;
                route.templateUrl =  templateUrl + '/' + link;
                if (!s(link).startsWith('main')) { //ex: main, mainGantt, mainXyz etc
                    //ex: api_company_contact;
                    var apiStateName = 'api_' + bundle + '_' + link;
                    var resolve = {};
                    resolve[link + 'Lnk'] = [
                        '$injector', '$stateParams',
                        function ($injector, $stateParams) {
                            //We use the link repository with master bundle's link api url
                            //ex: api_company_contact which is api/company/:id/contact
                            //Once the records are returned it will populate the link Entity ex: Contact
                            var reposLinkObj = $injector.get(opts.linkObj.repository);
                            return reposLinkObj.findAllLink($stateParams.id, apiStateName);
                    }];

                    route.resolve = resolve;

                    route.controllerProvider = function($controller, UtilService) {
                        // if the link tab has got an individual controller the we can use it
                        // other wise we use the EditController
                        //ex: CompanyLinkContactController
                        var controllerEdit = bundleName + 'EditController';
                        var controllerLink = bundleName + 'Link' + linkName + 'Controller';

                        if (UtilService.controllerExists(controllerLink)) {
                            return controllerLink;
                        } else {
                            return controllerEdit;
                        }
                    }
                }
            }

            //this is editing /viewing a link record
            else if (arr.length == 4) {
                var linkNameActual = "linkNameActual" in opts.linkObj ? opts.linkObj.linkNameActual : '';

                var templateUrl = 'get-template/link/' + link + '/' + link + '/edit';
                if (linkNameActual != '') {
                    templateUrl = 'get-template/link/' + linkNameActual + '/' + linkNameActual + '/edit';
                    reposNameLink = s.titleize(linkNameActual) + 'Repository';
                }
                var apiStateName = 'api_' + bundle + '_' + link + '_one';

                //first link variable is bundle but second link variable is the controller
                route.templateUrl = templateUrl;
                var resolve = {};
                if (linkView == 'edit') {
                    //ex: /company/edit/:id/contact/edit/:idl
                    route.url = '/edit/:idl';
                    resolve.rowLnk = function($injector, $stateParams) {
                        var reposLinkObj = $injector.get(opts.linkObj.repository);
                        return reposLinkObj.findOneLink($stateParams.id, $stateParams.idl, apiStateName);
                    };
                } else if (linkView == 'new') {
                    route.url = '/new';
                    resolve.rowLnk = function($injector, $stateParams) {
                        var entityLinkObj = $injector.get(linkName);
                        return new entityLinkObj(); //ex: new Contact()
                    };
                }

                route.onEnter = ['$stateParams', '$state', '$mdDialog', '$mdMedia', 'row', 'UtilUIService',
                function($stateParams, $state, $mdDialog, $mdMedia, row, UtilUIService) {
                    var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && $scope.customFullscreen;
                    // UtilUIService.showAlertDialog(null, {'message': 'test'});
                    //     return;

                    $mdDialog.show({
                        locals: {
                            data: {
                                idl: $stateParams.idl,
                                row: row,
                                bundle: bundle,
                                link: link,
                                linkView: linkView,
                                links: link,
                                linkObj: opts.linkObj,
                                reposNameLink: reposNameLink,
                                $mdDialog: $mdDialog,
                            }
                        },
                        controller: LinkEditController,
                        resolve: resolve,
                        templateUrl: templateUrl,
                        parent: angular.element(document.body),
                        clickOutsideToClose:false,
                        fullscreen: useFullScreen
                    })
                    .then(function(answer) {
                        $state.go('^');
                    }, function() {
                        $state.go('^');
                    });
                }];
            }

            if (_.size(opts.queryPars) > 0) {
                route.url += '?' + _.keys(opts.queryPars).join('&');
            }

            return route;
        },

        getRoute: function (name, absoluteUrl, params) {
            // var route = angular.copy(routes[name]);
            var route = angular.copy(_.find(routes, function(rt) {return rt.name == name;}));

            if (absoluteUrl) {
                route['url'] = Router.getBaseUrl() + route['url'];
            }

            if (typeof params == 'object' && Object.keys(params).length > 0) {
                var paramKeys, key, value;
                paramKeys = Object.keys(params);

                for (var i = 0; i < paramKeys.length; i++) {
                    key = paramKeys[i];
                    value = params[key];

                    route['url'] = route['url'].replace('{' + key + '}', value);
                    route['url'] = route['url'].replace(':' + key, value);
                }
            }

            return route;
        },

        /**
         * Gets the url property of a route.
         *
         * @param {string} name
         * @param {boolean} absoluteUrl
         * @param {object} params
         * @returns {string}
         */
        getRoutePath: function (name, absoluteUrl, params) {
            var route = this.getRoute(name, absoluteUrl, params);
            return route ? route['url'] : false;
        },

        /**
         * Inject new routes into the base Router.
         *
         * @param {object} newRoutes
         * @returns {undefined}
         */
        inject: function (newRoutes) {
            angular.extend(routes, newRoutes);
        },

        checkControllerExists: function (controllerName) {
            if(typeof window[controllerName] == 'function') {
                return true;
            }
            return false;
        },

        createRoutesForLinks: function(bundle, linksArr) {
            var routesArr = [];
            var viewsArr = ['detail', 'edit'];
            for (var i in linksArr) {
                var linkObj = linksArr[i];
                for (var j in viewsArr) {
                    var view = viewsArr[j];
                    var routeName = bundle + '.' + view + '.' + linkObj.name;
                    // console.log(routeName)
                    routesArr[routesArr.length] = Router.getRouteFromBundle(routeName, {linkObj: linkObj});
                }
            }
            Router.addRoutes(routesArr);
        },

        /**
         * generate empty link resolve (arrays) for Edit/Detail controllers
         *
         * @param {array} links
         * @returns {resolve}
         */
        getDefaultLinkResolves: function (resolve, links) {
            for (var i in links) {
                var linkObj = links[i];
                var linkResolveName = linkObj.name + "Lnk"; //contactLnk
                resolve[linkResolveName] = function() {
                    return {};
                }
            }
            return resolve;
        },

        createRoutesForLinksNewEdit: function(bundle, linksArr) {
            var routesArr = [];
            var viewsArr = ['detail', 'edit'];
            for (var i in linksArr) {
                var linkObj = linksArr[i];
                for (var j in viewsArr) {
                    var view = viewsArr[j];
                    var routeName = bundle + '.' + view + '.' + linkObj.name;
                    routesArr[routesArr.length] = Router.getRouteFromBundle(routeName, {linkObj: linkObj});
                }
            }
            Router.addRoutes(routesArr);
        },

        /**
         * generate initial link for object for auto creation of routings
         *
         */
        getLinkObjForRoute: function (linkName, opts) {
            var link = angular.extend({}, {
                name: linkName,
                repository: '',
                entity: '',
                linkNameActual: '',
            }, opts);

            var linkName = s(linkName).capitalize().value(); //ex: Contact or CompanyMedia
            var repository = link.repository != '' ? link.repository + 'Repository' : linkName + 'Repository';
            var entity     = link.repository != '' ? link.repository : linkName;

            link.repository = repository;
            link.entity     = entity;

            return link;
        },

        searchLinkObjForRoute: function (linksForAutoRouteGen, linkName) {
            return _.find(linksForAutoRouteGen, function(linkObj) {return linkObj.name == linkName;});
        },

        getRouteObj: function (stateName) {
            return _.find(Router.getAllRoutes(), function(routeObj) {
                return routeObj.name == stateName;
            });
        },

    };
})();
