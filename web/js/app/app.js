'use strict';

angular.module('app', [
    'ui.router',
    'ngMaterial',
    'ngAnimate',
    'ngAria',
    'ngSanitize',
    'angularUtils.directives.dirPagination',
    'dragularModule',
    'app.directives',
    'angularFileUpload',
    'angularMoment',
    'ui.bootstrap',
    'cfp.hotkeys',
    'persistenceJar',
    'angular-loading-bar',
    'angular-bind-html-compile',
    'gantt',
    'gantt.sortable',
    'gantt.movable',
    'gantt.drawtask',
    'gantt.tooltips',
    'gantt.bounds',
    'gantt.progress',
    'gantt.table',
    'gantt.tree',
    'gantt.groups',
    'gantt.dependencies',
    'gantt.overlap',
    // 'gantt.resizeSensor',

    'bootstrapLightbox',
    'md.data.table',

    //---- app related bundles ----//
    'appBundle',

    //---- Masters ----//
    'colorMasterBundle',
    'partsLocationBundle',
    'approvedMaterialBundle',
    'approvedComponentBundle',
    'approvedPackagingBundle',
    'masterPerformanceBundle',
    'masterComplianceBundle',
    'masterChecklistBundle',
    'productCodingBundle',

    //----- CRM -----//
    'companyBundle',
    'branchBundle',
    'contactBundle',
    'interestGroupBundle',

    //---- ERM -----//
    'enquiryBundle',
    'quotationBundle',
    'tradingOrderBundle',
    'salesConfirmationBundle',
    'purchaseOrderBundle',
    'manufacturingOrderBundle',
    'invoiceBundle',
    'vendorInvoiceBundle',
    'accountsReceivableBundle',
    'accountsPayableBundle',
    'creditDebitNoteBundle',
    'claimBundle',
    'noteBundle',
    'messageBundle',

    //----- PLM -----//
    'protoTypeBundle',
    'researchBundle',

    //---- Admin ----//
    'settingBundle',
    'translationBundle',
    'valuelistBundle',
    'userBundle',
    'roleBundle',

    //---- Others ----//
    'mediaBundle'
])
.config([
    '$stateProvider',
    '$mdThemingProvider',
    '$mdDateLocaleProvider',
    '$provide',
    '$locationProvider',
    'hotkeysProvider',
    'cfpLoadingBarProvider',
    'LightboxProvider',
    function (
        $stateProvider,
        $mdThemingProvider,
        $mdDateLocaleProvider,
        $provide,
        $locationProvider,
        hotkeysProvider,
        cfpLoadingBarProvider,
        LightboxProvider
    ) {
        //Loading Bar
        cfpLoadingBarProvider.includeSpinner = false;
        cfpLoadingBarProvider.latencyThreshold = 100;

        //Theming
        $mdThemingProvider.theme('default')
        .primaryPalette('blue',{
            'default': '700'
        })
        .accentPalette('pink',{
            'default': '800'
        })
        .warnPalette('red',{
            'default': '800'
        });

        //Routes
        var routes = Router.getAllRoutes();

        for (var i in routes) {
            var route = routes[i];
            var stateName = route.name;

            var state = {};

            if (route.hasOwnProperty('templateUrl') ||
                route.hasOwnProperty('template') ||
                route.hasOwnProperty('onEnter')
                ) {
                state.url = route.url;
                if (!_.isUndefined(route.controller)) {
                    state.controller = route.controller;
                }
                if (!_.isUndefined(route.controllerProvider)) {
                    state.controllerProvider = route.controllerProvider;
                }
                if (!_.isUndefined(route.templateUrl)) {
                    state.templateUrl = route.templateUrl;
                }
                if (!_.isUndefined(route.template)) {
                    state.template = route.template;
                }
                if (!_.isUndefined(route.resolve)) {
                    state.resolve = route.resolve;
                }
                if (!_.isUndefined(route.onEnter)) {
                    state.onEnter = route.onEnter;
                }
                $stateProvider.state(stateName, state);
            }
        }

        //Date Locale. set the date format for md-datepicker
        $mdDateLocaleProvider.formatDate = function(date) {
           return moment(date).format('YYYY-MM-DD');
        };

        //angular-file-upload hover out fix
        $provide.decorator('nvFileOverDirective', function ($delegate) {
            var directive = $delegate[0],
                link = directive.link;

            directive.compile = function () {
                return function (scope, element, attrs) {
                    var overClass = attrs.overClass || 'nv-file-over';
                    link.apply(this, arguments);
                    element.on('dragleave', function () {
                        element.removeClass(overClass);
                    });
                };
            };

            return $delegate;
        });

        //Hotkey: keyboard shorcuts template to show the hotkeys help.
        hotkeysProvider.templateHeader = '<div class="my-own-header"></div>';
        hotkeysProvider.templateFooter = '<div class="my-own-footer"></div>';

        //Lightbox: tell the plugin about the correct values
        LightboxProvider.getImageUrl = function(image) {
            return '/media/normal/' + image.media.fileName;
        };

        LightboxProvider.getImageCaption = function(image) {
            return image.media.displayTitle;
        };

    }
]).run([
    '$rootScope', '$state', '$stateParams', '$controller', 'Util',
    function($rootScope, $state, $stateParams, $controller, Util) {
        $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
            throw error;
        });

        $rootScope.$state = $state;
        return $rootScope.$stateParams = $stateParams;

    }
])

angular.element(document).ready(function () {
    angular.bootstrap(document, ['app']);
});