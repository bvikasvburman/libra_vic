describe("CompanyEntity", function() {
    beforeEach(module('companyBundle'));
    beforeEach(module('persistenceJar'));
        
    describe("when a new company is created", function () { 
        var company;
        
        beforeEach(inject(function (Company, BaseEntity) {
            company = new Company({
                id: 1,
                name: 'Google',
                email: 'google@gmail.com',
                phone: '123456789',
                fax: '123456789',
                creationDate: '2015-06-17',
                modificationDate: '2015-06-17',
                flag: false,
                status: 'active',
                website: 'http://google.com',
                category: 'software',
                industry: 'it&c',
                companyAddress: 'Earth'
            });
        }));
        
        it("should contain id field", function() {
            expect(company.id).toBeDefined();
        });
        
        it("should have the right id value", function() {
            expect(company.id).toEqual(1);
        });
    });
});


