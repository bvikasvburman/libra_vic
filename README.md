testpilotweb
============

How to install:

1. Clone this repository
2. Import the database

3. Run the following command to install the php dependencies: php composer.phar update
4. Go to web/js and run the following command to install the js dependecies: bower update
5. Run gulp.

6. Add to your database the data found in db/data/*.sql

Gulp
If you do not have gulp installed, you need to install it from this page: http://gulpjs.com/
You also need to install from https://www.npmjs.com the following modules that are currently 
used to minify the JS files:

1. gulp-concat
2. gulp-uglify
3. gulp-sourcemaps
4. gulp-ng-annotate
5. gulp-bower
=======
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

