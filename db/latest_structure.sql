-- MySQL dump 10.13  Distrib 5.6.24, for osx10.8 (x86_64)
--
-- Host: 127.0.0.1    Database: pilotcms
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `sort_order` int(11) DEFAULT NULL,
  `published` int(1) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `chi_title` varchar(250) DEFAULT NULL,
  `chi_description` text,
  `category_type` varchar(50) DEFAULT NULL,
  `external_link` varchar(250) DEFAULT NULL,
  `meta_title` text,
  `meta_keyword` text,
  `meta_description` text,
  `internal_link` varchar(250) DEFAULT NULL,
  `show_in_nav` tinyint(4) DEFAULT '1',
  `flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,3,'Logistics','This is first category',1,1,'2015-06-28 22:51:16','2015-06-28 22:51:19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1),(17,1,'Film',NULL,NULL,1,NULL,NULL,NULL,NULL,'film',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `phone` varchar(250) DEFAULT NULL,
  `fax` varchar(250) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `flag` tinyint(4) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `website` varchar(250) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `industry` varchar(250) DEFAULT NULL,
  `company_address_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'DHL','dhs@dhs.com','123456789','123456789','2015-07-24 11:50:00','2015-07-24 11:10:00',2,'current','http://dhs.com',1,'logistic',NULL),(2,'Google','google@gmail.com','123456789','123456789','2015-07-24 11:10:00','2015-07-24 11:10:00',1,'current','http://google.com',NULL,'it',NULL),(3,'Pearsons-Specter-Litt',NULL,'123456789','123456789',NULL,NULL,NULL,'active',NULL,NULL,'legal',NULL),(4,'Microsoft',NULL,'123456789','1234567889',NULL,NULL,NULL,'active','http://microsoft.com/',NULL,'it',NULL),(5,'MGM',NULL,'123456789','12347569',NULL,NULL,NULL,'active','http://mgm.com',17,'film',NULL),(6,'HBO',NULL,'123456789','12345689',NULL,NULL,NULL,'active','http://hbo.com',17,'film',NULL);
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_media`
--

DROP TABLE IF EXISTS `company_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `actual_file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `type` smallint(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_media`
--

LOCK TABLES `company_media` WRITE;
/*!40000 ALTER TABLE `company_media` DISABLE KEYS */;
/*!40000 ALTER TABLE `company_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `position` varchar(250) DEFAULT NULL,
  `fax` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `phone` varchar(250) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `address_flat_bldg` varchar(200) DEFAULT NULL,
  `address_street` varchar(200) DEFAULT NULL,
  `address_city` varchar(200) DEFAULT NULL,
  `address_area` varchar(100) DEFAULT NULL,
  `address_state` varchar(200) DEFAULT NULL,
  `address_country_code` char(2) DEFAULT NULL,
  `address_po_code` varchar(30) DEFAULT NULL,
  `flag` tinyint(4) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `member_type` varchar(50) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `staff_id_created` int(11) DEFAULT NULL,
  `staff_id_modified` int(11) DEFAULT NULL,
  `category` varchar(240) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'Piter22','Parker',NULL,'123456789','piterparker@gmail.com','123456789','123456789','18','Wall Street','New Yoik','New York','USA','12','123895',1,'1','1','2015-05-05 17:41:29','2015-06-21 17:41:40',1,NULL,'client',1),(5,'Clark','Kent',NULL,NULL,'clarkkent@gmail.com','123456789','123456789',NULL,NULL,'Louisville','Louisville','USA',NULL,NULL,1,'1','1','2015-08-05 21:43:00',NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `content`
--

DROP TABLE IF EXISTS `content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `show_title` int(1) DEFAULT NULL,
  `description_short` text,
  `description` text,
  `sort_order` int(11) DEFAULT NULL,
  `published` int(1) DEFAULT NULL,
  `latest` int(1) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `content_date` date DEFAULT NULL,
  `chi_title` varchar(250) DEFAULT NULL,
  `chi_description` text,
  `content_type` varchar(50) DEFAULT NULL,
  `external_link` varchar(250) DEFAULT NULL,
  `meta_title` text,
  `meta_keyword` text,
  `meta_description` text,
  `chi_description_short` text,
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  `internal_link` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `content`
--

LOCK TABLES `content` WRITE;
/*!40000 ALTER TABLE `content` DISABLE KEYS */;
INSERT INTO `content` VALUES (1,1,NULL,NULL,'Hello',1,'This is a short content','Yes, this is all about it',NULL,NULL,NULL,'2015-06-02 22:06:11','2015-06-24 22:06:16','2015-06-27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),(2,NULL,NULL,NULL,'Hello',1,'This is a short content','Yes, this is all about it',NULL,NULL,NULL,'2015-06-02 22:06:11','2015-06-24 22:06:16','2015-06-27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL);
/*!40000 ALTER TABLE `content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deal`
--

DROP TABLE IF EXISTS `deal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `deal_stage` varchar(50) DEFAULT NULL,
  `chance` varchar(10) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `difficulty` varchar(11) DEFAULT NULL,
  `staff_id_project_manager` int(11) DEFAULT NULL,
  `inquiry_date` date DEFAULT NULL,
  `follow_up_date` date DEFAULT NULL,
  `estimated_start_date` date DEFAULT NULL,
  `deal_amount` int(11) DEFAULT NULL,
  `flag` tinyint(4) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `staff_id_created` int(11) DEFAULT NULL,
  `staff_id_modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deal`
--

LOCK TABLES `deal` WRITE;
/*!40000 ALTER TABLE `deal` DISABLE KEYS */;
INSERT INTO `deal` VALUES (1,'abc','abc',NULL,1,'Appointment Scheduled',NULL,NULL,NULL,NULL,NULL,NULL,NULL,2000,NULL,NULL,NULL,NULL,NULL),(2,'def','def',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,500,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `deal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deal_staff`
--

DROP TABLE IF EXISTS `deal_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deal_staff` (
  `deal_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `deal_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`deal_staff_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deal_staff`
--

LOCK TABLES `deal_staff` WRITE;
/*!40000 ALTER TABLE `deal_staff` DISABLE KEYS */;
/*!40000 ALTER TABLE `deal_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ma_other_action`
--

DROP TABLE IF EXISTS `ma_other_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ma_other_action` (
  `other_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `has_list` tinyint(4) DEFAULT NULL,
  `has_detail` tinyint(4) DEFAULT NULL,
  `has_new` tinyint(4) DEFAULT NULL,
  `has_edit` tinyint(4) DEFAULT NULL,
  `has_find` tinyint(4) DEFAULT NULL,
  `has_modify` tinyint(4) DEFAULT NULL,
  `is_global` tinyint(4) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`other_action_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ma_other_action`
--

LOCK TABLES `ma_other_action` WRITE;
/*!40000 ALTER TABLE `ma_other_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `ma_other_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ma_role`
--

DROP TABLE IF EXISTS `ma_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ma_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `role_type` varchar(50) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `site_id` int(11) DEFAULT NULL,
  `flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ma_role`
--

LOCK TABLES `ma_role` WRITE;
/*!40000 ALTER TABLE `ma_role` DISABLE KEYS */;
INSERT INTO `ma_role` VALUES (1,'User','ROLE_USER',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ma_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ma_role_other_action`
--

DROP TABLE IF EXISTS `ma_role_other_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ma_role_other_action` (
  `role_other_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `other_action_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `has_access` tinyint(4) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`role_other_action_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ma_role_other_action`
--

LOCK TABLES `ma_role_other_action` WRITE;
/*!40000 ALTER TABLE `ma_role_other_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `ma_role_other_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ma_room`
--

DROP TABLE IF EXISTS `ma_room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ma_room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `list` tinyint(4) DEFAULT NULL,
  `detail` tinyint(4) DEFAULT NULL,
  `new` tinyint(4) DEFAULT NULL,
  `edit` tinyint(4) DEFAULT NULL,
  `delete` tinyint(4) DEFAULT NULL,
  `duplicate` tinyint(4) DEFAULT NULL,
  `search` tinyint(4) DEFAULT NULL,
  `print` tinyint(4) DEFAULT NULL,
  `publish` tinyint(4) DEFAULT NULL,
  `unpublish` tinyint(4) DEFAULT NULL,
  `import` tinyint(4) DEFAULT NULL,
  `export` tinyint(4) DEFAULT NULL,
  `room_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ma_room`
--

LOCK TABLES `ma_room` WRITE;
/*!40000 ALTER TABLE `ma_room` DISABLE KEYS */;
/*!40000 ALTER TABLE `ma_room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ma_room_role`
--

DROP TABLE IF EXISTS `ma_room_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ma_room_role` (
  `room_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `list` tinyint(4) DEFAULT NULL,
  `detail` tinyint(4) DEFAULT NULL,
  `new` tinyint(4) DEFAULT NULL,
  `edit` tinyint(4) DEFAULT NULL,
  `delete` tinyint(4) DEFAULT NULL,
  `duplicate` tinyint(4) DEFAULT NULL,
  `print` tinyint(4) DEFAULT NULL,
  `publish` tinyint(4) DEFAULT NULL,
  `unpublish` tinyint(4) DEFAULT NULL,
  `import` tinyint(4) DEFAULT NULL,
  `export` tinyint(4) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `room_title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`room_role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ma_room_role`
--

LOCK TABLES `ma_room_role` WRITE;
/*!40000 ALTER TABLE `ma_room_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `ma_room_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ma_staff`
--

DROP TABLE IF EXISTS `ma_staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ma_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_rate` int(10) DEFAULT NULL,
  `role_id` int(3) DEFAULT NULL,
  `position` varchar(250) DEFAULT NULL,
  `email` varchar(250) DEFAULT NULL,
  `address_street` text,
  `address_town` varchar(250) DEFAULT NULL,
  `address_state` varchar(250) DEFAULT NULL,
  `address_country` varchar(250) DEFAULT NULL,
  `address_po_code` varchar(250) DEFAULT NULL,
  `phone` varchar(250) DEFAULT NULL,
  `fax` varchar(250) DEFAULT NULL,
  `notes` text,
  `published` int(1) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `user_name` varchar(25) DEFAULT NULL,
  `pass_word` varchar(60) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `mobile` varchar(100) DEFAULT NULL,
  `flag` int(1) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ma_staff`
--

LOCK TABLES `ma_staff` WRITE;
/*!40000 ALTER TABLE `ma_staff` DISABLE KEYS */;
INSERT INTO `ma_staff` VALUES (1,NULL,1,NULL,'nicu@localhost.dev',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'nicolae.tusinean','$2y$15$im2ygBUM4v0ccMJecPKmwe8Cl55cheJmJ1QXWO9X3sZ/T3GjGVGte','Nicolae','Tusinean',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ma_staff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` datetime DEFAULT NULL,
  `media_type` varchar(100) DEFAULT NULL,
  `actual_file_name` varchar(250) DEFAULT NULL,
  `display_title` varchar(250) DEFAULT NULL,
  `file_name` varchar(250) DEFAULT NULL,
  `content_type` varchar(250) DEFAULT NULL,
  `media_size` int(11) DEFAULT NULL,
  `room_name` varchar(50) DEFAULT NULL,
  `record_type` varchar(50) DEFAULT NULL,
  `lang` varchar(10) DEFAULT NULL,
  `alt_tag_data` varchar(250) DEFAULT NULL,
  `external_link` varchar(250) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `caption` varchar(250) DEFAULT NULL,
  `chi_caption` text,
  `sort_order` int(11) DEFAULT NULL,
  `record_id` int(11) DEFAULT NULL,
  `description` text,
  `internal_link` varchar(250) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media`
--

LOCK TABLES `media` WRITE;
/*!40000 ALTER TABLE `media` DISABLE KEYS */;
/*!40000 ALTER TABLE `media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_category`
--

DROP TABLE IF EXISTS `media_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_category` (
  `category_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`category_id`,`media_id`),
  UNIQUE KEY `UNIQ_92D3773EA9FDD75` (`media_id`),
  KEY `IDX_92D377312469DE2` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_category`
--

LOCK TABLES `media_category` WRITE;
/*!40000 ALTER TABLE `media_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_contact`
--

DROP TABLE IF EXISTS `media_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_contact` (
  `contact_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`contact_id`,`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_contact`
--

LOCK TABLES `media_contact` WRITE;
/*!40000 ALTER TABLE `media_contact` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_content`
--

DROP TABLE IF EXISTS `media_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_content` (
  `content_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`content_id`,`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_content`
--

LOCK TABLES `media_content` WRITE;
/*!40000 ALTER TABLE `media_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_deal`
--

DROP TABLE IF EXISTS `media_deal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_deal` (
  `deal_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`deal_id`,`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_deal`
--

LOCK TABLES `media_deal` WRITE;
/*!40000 ALTER TABLE `media_deal` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_deal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_section`
--

DROP TABLE IF EXISTS `media_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `media_section` (
  `section_id` varchar(45) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`section_id`,`media_id`),
  UNIQUE KEY `media_id_UNIQUE` (`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_section`
--

LOCK TABLES `media_section` WRITE;
/*!40000 ALTER TABLE `media_section` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20150615224956'),('20150615232137');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `section`
--

DROP TABLE IF EXISTS `section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `sort_order` int(11) DEFAULT NULL,
  `published` int(1) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `external_link` varchar(500) DEFAULT NULL,
  `chi_title` varchar(250) DEFAULT NULL,
  `chi_description` text,
  `button_position` varchar(50) DEFAULT NULL,
  `section_type` varchar(50) DEFAULT NULL,
  `meta_title` text,
  `meta_keyword` text,
  `meta_description` text,
  `seo_title` varchar(250) DEFAULT NULL,
  `internal_link` varchar(250) DEFAULT NULL,
  `show_in_nav` tinyint(4) NOT NULL DEFAULT '1',
  `flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 PACK_KEYS=0;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `section`
--

LOCK TABLES `section` WRITE;
/*!40000 ALTER TABLE `section` DISABLE KEYS */;
INSERT INTO `section` VALUES (1,'Logistics','Hello there',1,1,'2015-06-28 21:51:05','2015-06-28 21:51:08',NULL,'chi title','chi description','ok','type1',NULL,NULL,NULL,NULL,NULL,1,NULL),(2,'hello section','this is first section',1,0,'2015-06-28 21:54:49','2015-06-28 21:54:53',NULL,NULL,NULL,'1','type1',NULL,NULL,NULL,NULL,NULL,1,NULL),(3,'hello section','this is first section',1,0,'2015-06-28 21:54:49','2015-06-28 21:54:53','http://www.domain.tld/blublu',NULL,NULL,NULL,'type1',NULL,NULL,NULL,NULL,'/blublu',1,NULL);
/*!40000 ALTER TABLE `section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting`
--

DROP TABLE IF EXISTS `setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `key_text` varchar(200) DEFAULT NULL,
  `value` text,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `group_name` varchar(50) DEFAULT NULL,
  `value_type` varchar(25) DEFAULT NULL,
  `flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_text` (`key_text`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting`
--

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;
INSERT INTO `setting` VALUES (1,'homepage setting','home','homepage',NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_category`
--

DROP TABLE IF EXISTS `sub_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_category` (
  `sub_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `chi_title` varchar(250) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `published` int(1) DEFAULT NULL,
  `external_link` varchar(250) DEFAULT NULL,
  `sub_category_type` varchar(50) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keyword` varchar(1000) DEFAULT NULL,
  `meta_description` varchar(1000) DEFAULT NULL,
  `internal_link` varchar(250) DEFAULT NULL,
  `show_in_nav` tinyint(4) NOT NULL DEFAULT '1',
  `flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`sub_category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_category`
--

LOCK TABLES `sub_category` WRITE;
/*!40000 ALTER TABLE `sub_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `sub_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translation`
--

DROP TABLE IF EXISTS `translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_text` varchar(200) DEFAULT NULL,
  `value` text,
  `chi_value` text,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `group_name` varchar(50) DEFAULT NULL,
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `key_text` (`key_text`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translation`
--

LOCK TABLES `translation` WRITE;
/*!40000 ALTER TABLE `translation` DISABLE KEYS */;
INSERT INTO `translation` VALUES (1,'cp.companyName','Pilot Simple Software',NULL,NULL,NULL,NULL,0);
/*!40000 ALTER TABLE `translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `valuelist`
--

DROP TABLE IF EXISTS `valuelist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `valuelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_text` varchar(250) DEFAULT NULL,
  `value` text,
  `chi_value` text,
  `creation_date` date DEFAULT NULL,
  `modification_date` date DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_group_heading` int(1) DEFAULT NULL,
  `code` varchar(250) DEFAULT NULL,
  `flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`valuelist_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `valuelist`
--

LOCK TABLES `valuelist` WRITE;
/*!40000 ALTER TABLE `valuelist` DISABLE KEYS */;
/*!40000 ALTER TABLE `valuelist` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-09-20 18:22:58
