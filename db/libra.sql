-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 06, 2016 at 10:14 AM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `libra`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts_payable`
--

CREATE TABLE IF NOT EXISTS `accounts_payable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trading_order_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `notes` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_12741D3CCBC7D824` (`user_id_created`),
  KEY `IDX_12741D3CA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `accounts_payable`
--

INSERT INTO `accounts_payable` (`id`, `trading_order_id`, `company_id`, `invoice_id`, `notes`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, NULL, NULL, NULL, 'asdasd', NULL, 1, 1, '2016-03-06 18:48:13', '2016-03-06 18:48:13');

-- --------------------------------------------------------

--
-- Table structure for table `accounts_receivable`
--

CREATE TABLE IF NOT EXISTS `accounts_receivable` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trading_order_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `notes` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4A2FD289CBC7D824` (`user_id_created`),
  KEY `IDX_4A2FD289A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `accounts_receivable`
--

INSERT INTO `accounts_receivable` (`id`, `trading_order_id`, `company_id`, `invoice_id`, `notes`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, 1, 1, '2016-03-06 18:48:02', '2016-03-06 18:48:02');

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE IF NOT EXISTS `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message_body` longtext COLLATE utf8_unicode_ci,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AC74095ACBC7D824` (`user_id_created`),
  KEY `IDX_AC74095AA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `approved_component`
--

CREATE TABLE IF NOT EXISTS `approved_component` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `part` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `measurement` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parts_location_id` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `approved` int(11) DEFAULT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_DC6A9B3979B1AD6` (`company_id`),
  KEY `IDX_DC6A9B3437031EE` (`parts_location_id`),
  KEY `IDX_DC6A9B3CBC7D824` (`user_id_created`),
  KEY `IDX_DC6A9B3A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `approved_component`
--

INSERT INTO `approved_component` (`id`, `company_id`, `part`, `code`, `description`, `measurement`, `parts_location_id`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`, `approved`, `category`) VALUES
(1, 19, 'asdadasd', 'asdasd', 'adada', '12 asdfasd', 5, NULL, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL),
(2, NULL, 'dddddd', '44r', 'sdfsdf', NULL, NULL, 1, 1, '0000-00-00 00:00:00', '2016-03-09 21:07:22', NULL, 'Category 3'),
(3, 7, 'aaa', 'bbb', 'aaaaaaa', NULL, 7, 1, 1, '2016-03-06 16:26:56', '2016-04-19 09:35:19', 1, 'Category 3');

-- --------------------------------------------------------

--
-- Table structure for table `approved_material`
--

CREATE TABLE IF NOT EXISTS `approved_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `part` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `material` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color_code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `measurement` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `parts_location_id` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `approved` int(11) DEFAULT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_409BE2F9979B1AD6` (`company_id`),
  KEY `IDX_409BE2F9437031EE` (`parts_location_id`),
  KEY `IDX_409BE2F9CBC7D824` (`user_id_created`),
  KEY `IDX_409BE2F9A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `approved_material`
--

INSERT INTO `approved_material` (`id`, `part`, `material`, `color_code`, `measurement`, `company_id`, `parts_location_id`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`, `approved`, `category`) VALUES
(1, 'asdsad', 'asdada', 'YL', NULL, 19, 8, NULL, 1, '0000-00-00 00:00:00', '2016-03-09 19:06:25', 0, 'Category 5'),
(2, 'Part 1', 'Material 1', 'RD', NULL, 14, 22, 1, 1, '2016-03-06 16:21:18', '2016-03-09 20:24:08', 1, 'Category 4'),
(3, 'asdad', 'asdasd', NULL, NULL, NULL, NULL, 1, 1, '2016-04-15 14:56:59', '2016-04-19 01:12:27', NULL, 'Approved Material Category 3');

-- --------------------------------------------------------

--
-- Table structure for table `approved_packaging`
--

CREATE TABLE IF NOT EXISTS `approved_packaging` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `packaging_material` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `parts_location_id` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `approved` int(11) DEFAULT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FBB30561979B1AD6` (`company_id`),
  KEY `IDX_FBB30561437031EE` (`parts_location_id`),
  KEY `IDX_FBB30561CBC7D824` (`user_id_created`),
  KEY `IDX_FBB30561A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `approved_packaging`
--

INSERT INTO `approved_packaging` (`id`, `packaging_material`, `company_id`, `parts_location_id`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`, `approved`, `category`) VALUES
(1, 'asdadas', 23, 5, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL),
(2, NULL, NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(3, 'asdasdas', 18, NULL, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(4, 'sadad', 18, NULL, 1, 1, '2016-03-06 16:30:37', '2016-03-09 21:13:39', 1, 'Category 2');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fax` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BB861B1F979B1AD6` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `company_id`, `name`, `phone`, `email`, `address`, `city`, `state`, `zip`, `country`, `fax`, `website`, `flag`, `creation_date`, `modification_date`, `user_id_created`, `user_id_modified`) VALUES
(6, 13, 'test 2', '324234', 'dfsdfsdf@qerqeq', 'qaqdas', 'dasdasd', NULL, NULL, NULL, NULL, NULL, NULL, '2016-04-14 00:00:00', '2016-04-14 00:00:00', NULL, NULL),
(7, 66, 'test branch', '1234234234', 'sdfsdfsdf@sfsdfsd.com', 'dsafasdfa af adf', '2wfsdf', 'sadfsdf', '34234', 'India', '24234', NULL, NULL, '2016-04-17 16:26:15', '2016-04-17 16:26:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `sort_order` int(11) DEFAULT NULL,
  `published` int(1) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `chi_title` varchar(250) DEFAULT NULL,
  `chi_description` text,
  `category_type` varchar(50) DEFAULT NULL,
  `external_link` varchar(250) DEFAULT NULL,
  `meta_title` text,
  `meta_keyword` text,
  `meta_description` text,
  `internal_link` varchar(250) DEFAULT NULL,
  `show_in_nav` tinyint(4) DEFAULT '1',
  `flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `section_id`, `title`, `description`, `sort_order`, `published`, `creation_date`, `modification_date`, `chi_title`, `chi_description`, `category_type`, `external_link`, `meta_title`, `meta_keyword`, `meta_description`, `internal_link`, `show_in_nav`, `flag`) VALUES
(1, 3, 'Logistics', 'This is first category', 1, 1, '2015-06-28 22:51:16', '2015-06-28 22:51:19', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1),
(17, 1, 'Film', NULL, NULL, 1, NULL, NULL, NULL, NULL, 'film', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `claim`
--

CREATE TABLE IF NOT EXISTS `claim` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entertain_claim` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A769DE27CBC7D824` (`user_id_created`),
  KEY `IDX_A769DE27A6AB49AC` (`user_id_modified`),
  KEY `IDX_A769DE27579AC939` (`trading_order_id`),
  KEY `IDX_A769DE27979B1AD6` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `claim`
--

INSERT INTO `claim` (`id`, `code`, `trading_order_id`, `company_id`, `title`, `description`, `status`, `entertain_claim`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, 'CL10001', 1, 66, 'Broken strap in the bag', 'Broken strap in the bag. Broken strap in the bag. Broken strap in the bag. Broken strap in the bag. Broken strap in the bag. Broken strap in the bag. ', NULL, 'Y', NULL, NULL, NULL, '2016-04-17 00:00:00', '2016-04-17 19:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `color_master`
--

CREATE TABLE IF NOT EXISTS `color_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_817BD24CBC7D824` (`user_id_created`),
  KEY `IDX_817BD24A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `color_master`
--

INSERT INTO `color_master` (`id`, `code`, `title`, `creation_date`, `modification_date`, `user_id_created`, `user_id_modified`) VALUES
(1, 'BL', 'Blue Color', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(7, 'YL', 'Yellow', '0000-00-00 00:00:00', '2016-03-03 16:34:12', NULL, NULL),
(9, 'PNK', 'Pink Color', '2016-03-03 14:48:55', '2016-03-03 14:48:56', NULL, NULL),
(10, 'RD', 'Red123123', '2016-03-03 14:48:55', '2016-04-15 16:19:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) DEFAULT NULL,
  `title` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526CE7A1254A` (`contact_id`),
  KEY `IDX_9474526CCBC7D824` (`user_id_created`),
  KEY `IDX_9474526CA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `contact_id`, `title`, `description`, `creation_date`, `modification_date`, `user_id_created`, `user_id_modified`) VALUES
(1, 12, 'Test comment 1', 'Test comment 1Test comment 1Test comment 1Test comment 1Test comment 1Test comment 1Test comment 1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(2, NULL, 'Test comment 2', 'Test comment 2Test comment 2Test comment 2Test comment 2Test comment 2Test comment 2Test comment 2Test comment 2Test comment 2', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `state` varchar(150) DEFAULT NULL,
  `zip` varchar(30) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `website` varchar(150) DEFAULT NULL,
  `address_delivery` varchar(300) DEFAULT NULL,
  `city` varchar(150) DEFAULT NULL,
  `payment_terms` varchar(150) DEFAULT NULL,
  `delivery_terms` varchar(150) DEFAULT NULL,
  `port_of_delivery` varchar(150) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_4FBF094F77153098` (`code`),
  KEY `IDX_4FBF094FCBC7D824` (`user_id_created`),
  KEY `IDX_4FBF094FA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=78 ;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`id`, `code`, `name`, `category`, `phone`, `fax`, `email`, `address`, `state`, `zip`, `country`, `website`, `address_delivery`, `city`, `payment_terms`, `delivery_terms`, `port_of_delivery`, `flag`, `creation_date`, `modification_date`, `user_id_created`, `user_id_modified`) VALUES
(7, 'C100015', 'Facebook Inc.', 'Client', '424234', '5346346456', 'sdfsfd@sdfsf.com', 'aaaaaaaaaaa', 'sssssssssssss', 'zzzzzzzzzzz', 'cccccccccccc', 'wwwwwwwwwwwww', 'adddasdsad', 'vvvvvvv', 'sdfsf', 'sfsdfsdf', 'sdfsdf', 0, '2016-01-01 00:00:00', '2016-03-10 14:36:52', 1, 1),
(13, 'C100014', 'Microsoftaaaaaaaa', 'Client', '777778787', '123456789', 'dhs@dhs.com', '', '', '', '', 'http://dhs.com', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(14, 'C100013', 'New Test Company', '', '123456789', '123456789', 'dhs@dhs.com', '', '', '', '', 'http://dhs.com', '', '', '', '', '', 0, '2015-07-24 11:10:00', '2015-07-24 11:10:00', NULL, NULL),
(18, 'C100012', 'Great Company', 'Supplier', '123311321', '', 'info@pilot.com.hk', '', '', '', '', '', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(19, 'C100011', 'A Fabulous Company', 'aaaaaaaa', 'aaaaaaaa', 'aaaaaaaa', 'aaaaaaaaaaa', 'aaaaaaaaaaa', 'aaaaaaaaaaa', 'aaaaa', 'aaaaaaaaa', 'aaaaaaaaaaa', 'aaaaaaaaaaa', 'aaaaaaaa', 'aaaaaaaaaaaaaaaa', 'aaaaaaaaaaaa', 'aaaaaaaaaaaaaaa', NULL, '2016-03-02 00:00:00', '2016-03-11 14:29:34', 1, 1),
(20, 'C100010', 'HBO', 'Client', '123456789', '123456789', 'dhs@dhs.com', '', '', '', '', 'http://dhs.com', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(21, 'C100009', 'MGM', 'Supplier', '0866543454', '123456789', 'dhs@dhs.com', '', '', '', '', 'http://dhs.com', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(22, 'C100008', 'Pearsons-Specter-Litt', 'Client', '123456789', '123456789', 'dhs@dhs.com', '', '', '', '', 'http://dhs.com', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(23, 'C100007', 'Microsoft', 'Supplier', '777778787', '123456789', 'dhs@dhs.com', '', '', '', '', 'http://dhs.com', '', '', '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(24, 'C100006', 'Best Company 234234', 'Supplier', '123456789', '123456789', 'dhs@dhs.com', '', '', '', '', 'http://dhs.com', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(25, 'C100005', 'New Company', 'Supplier', '123456789', '123456789', 'dhs@dhs.com', 'sasdasdsadasd', 'adasd', '', '', 'http://dhs.com.hk', '', 'asdasdas', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(26, 'C100004', 'XYZ Company', 'Supplier', '22423424', 'sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf s', 'abc@abc.com', 'This is a beautiful', 'sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf', 'sdfsdf sdfsdf sdfsdf sdfsdf sd', 'sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sdfsdf sd', 'http://www.abc.com.hk', 'sdfsdf', 'TST TST TST TST TST TST TST TST TST TST TST TST TST TST TST TST', 'sdfsdfdsf', 'FOB Shanghai', '', 1, '0000-00-00 00:00:00', '2016-02-01 11:33:22', NULL, NULL),
(29, 'C100003', 'Abc Suppliers', 'Supplier', '2134234', NULL, 'info@abc.com.hk', '123 Queens Road', 'Hong Kong', '0000', 'Hong Kong', NULL, NULL, 'Central', NULL, NULL, NULL, NULL, '0000-00-00 00:00:00', '2016-02-01 10:09:13', NULL, NULL),
(65, 'C100002', 'Google Incorporation dfsfs', NULL, '123448679', '3424234', 'info@google.com', 'sadasd', 'asasdad', '1123123', 'hofsdfsf', NULL, NULL, 'asasdad', NULL, NULL, NULL, NULL, '2016-02-01 13:16:06', '2016-02-01 14:23:17', NULL, NULL),
(66, 'C100001', 'ABC Company Ltd', 'Client', '12346578', NULL, 'info@abccompany.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-06 18:49:39', '2016-05-17 12:22:08', NULL, NULL),
(76, 'C000076', 'sdfsfd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-04-27 14:37:14', '2016-04-27 14:37:14', 1, 1),
(77, 'C000077', 'sdfsfd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-04-27 14:37:19', '2016-04-27 14:37:19', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `position` varchar(150) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `code` varchar(30) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `gantt_start_date` date DEFAULT NULL,
  `gantt_end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_4C62E638979B1AD6` (`company_id`),
  KEY `IDX_4C62E638CBC7D824` (`user_id_created`),
  KEY `IDX_4C62E638A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=48 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `first_name`, `last_name`, `position`, `fax`, `email`, `phone`, `flag`, `creation_date`, `modification_date`, `company_id`, `code`, `address`, `city`, `state`, `zip`, `country`, `user_id_created`, `user_id_modified`, `gantt_start_date`, `gantt_end_date`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-10 11:21:29', '2016-03-11 23:51:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Shahul', 'Hameed', 'xzczxcz', NULL, 'hameed@pilot.com.hk', '28101110', NULL, '2016-03-10 11:21:29', '2016-02-01 20:31:01', 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Faheem', 'Hameed', 'aasdas', NULL, 'faheemhameed313@gmail.com', '333333333333', NULL, '2016-03-10 11:21:29', '2016-02-01 20:50:30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'Abdul', 'Qayyoom', NULL, NULL, NULL, NULL, NULL, '2016-03-10 11:21:29', '2016-03-06 16:48:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'Kamil', 'Faheem', NULL, NULL, NULL, NULL, NULL, '2016-03-10 11:21:29', '2016-03-10 11:21:29', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'sadada', NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-10 11:21:29', '2016-04-18 08:52:59', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(28, 'Abdul', 'Qadir', 'Programmer', '23423', 'abdul.qadir@pilot.com.hk', '123232323', NULL, '2016-03-10 11:21:29', '2016-03-12 19:32:50', 19, NULL, 'asdads', '2222222', '22222222', '34234', '234234', NULL, 1, NULL, NULL),
(30, 'Shahul', 'Hameed', '33333333', NULL, 'abc@abc.com', '123232311', NULL, '2016-02-01 13:14:06', '2016-04-12 22:52:48', 66, NULL, 'asdas', 'asdasd', 'asdasd', '324234', 'dsafsdfsd', NULL, 1, '2016-04-12', '2016-05-12'),
(31, 'Faheem', 'Salih', NULL, NULL, 'sadad@qweqwe', '1212', NULL, '2016-04-13 06:59:38', '2016-04-13 07:19:28', 66, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL),
(32, 'Muhammad', 'Adil', NULL, NULL, '234324@dssf.com', '23423423', NULL, '2016-04-13 08:52:28', '2016-04-13 08:52:28', 66, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, NULL),
(47, 'Faheem', 'Qadir', 'asdasd', NULL, NULL, NULL, NULL, '2016-04-13 13:26:07', '2016-05-02 13:08:53', 66, NULL, 'asd', 'asdasd', 'asdasd', 'asdasd', NULL, 1, 1, '2016-04-01', '2016-05-16');

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `show_title` int(1) DEFAULT NULL,
  `description_short` text,
  `description` text,
  `sort_order` int(11) DEFAULT NULL,
  `published` int(1) DEFAULT NULL,
  `latest` int(1) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `content_date` date DEFAULT NULL,
  `chi_title` varchar(250) DEFAULT NULL,
  `chi_description` text,
  `content_type` varchar(50) DEFAULT NULL,
  `external_link` varchar(250) DEFAULT NULL,
  `meta_title` text,
  `meta_keyword` text,
  `meta_description` text,
  `chi_description_short` text,
  `flag` tinyint(4) NOT NULL DEFAULT '0',
  `internal_link` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `section_id`, `category_id`, `sub_category_id`, `title`, `show_title`, `description_short`, `description`, `sort_order`, `published`, `latest`, `creation_date`, `modification_date`, `content_date`, `chi_title`, `chi_description`, `content_type`, `external_link`, `meta_title`, `meta_keyword`, `meta_description`, `chi_description_short`, `flag`, `internal_link`) VALUES
(1, 1, NULL, NULL, 'Hello', 1, 'This is a short content', 'Yes, this is all about it', NULL, NULL, NULL, '2015-06-02 22:06:11', '2015-06-24 22:06:16', '2015-06-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(2, NULL, NULL, NULL, 'Hello', 1, 'This is a short content', 'Yes, this is all about it', NULL, NULL, NULL, '2015-06-02 22:06:11', '2015-06-24 22:06:16', '2015-06-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `credit_debit_note`
--

CREATE TABLE IF NOT EXISTS `credit_debit_note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `note_type` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `notes` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_96E32F1CCBC7D824` (`user_id_created`),
  KEY `IDX_96E32F1CA6AB49AC` (`user_id_modified`),
  KEY `IDX_96E32F1C579AC939` (`trading_order_id`),
  KEY `IDX_96E32F1C979B1AD6` (`company_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `credit_debit_note`
--

INSERT INTO `credit_debit_note` (`id`, `code`, `trading_order_id`, `purchase_order_id`, `company_id`, `note_type`, `amount`, `notes`, `status`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, NULL, 1, NULL, 66, 'credit', 10500, NULL, NULL, NULL, 1, 1, '2016-03-06 18:48:21', '2016-03-06 18:48:21');

-- --------------------------------------------------------

--
-- Table structure for table `deal`
--

CREATE TABLE IF NOT EXISTS `deal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `deal_stage` varchar(50) DEFAULT NULL,
  `chance` varchar(10) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL,
  `difficulty` varchar(11) DEFAULT NULL,
  `staff_id_project_manager` int(11) DEFAULT NULL,
  `inquiry_date` date DEFAULT NULL,
  `follow_up_date` date DEFAULT NULL,
  `estimated_start_date` date DEFAULT NULL,
  `deal_amount` int(11) DEFAULT NULL,
  `flag` tinyint(4) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `staff_id_created` int(11) DEFAULT NULL,
  `staff_id_modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `company_id` (`company_id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `deal`
--

INSERT INTO `deal` (`id`, `title`, `description`, `company_id`, `contact_id`, `deal_stage`, `chance`, `category`, `difficulty`, `staff_id_project_manager`, `inquiry_date`, `follow_up_date`, `estimated_start_date`, `deal_amount`, `flag`, `creation_date`, `modification_date`, `staff_id_created`, `staff_id_modified`) VALUES
(1, 'abc', 'abc', NULL, 1, 'Appointment Scheduled', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2000, 1, NULL, NULL, NULL, NULL),
(2, 'def', 'def', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 500, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `deal_staff`
--

CREATE TABLE IF NOT EXISTS `deal_staff` (
  `deal_staff_id` int(11) NOT NULL AUTO_INCREMENT,
  `deal_id` int(11) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`deal_staff_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `enquiry`
--

CREATE TABLE IF NOT EXISTS `enquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `enquiry_date` date DEFAULT NULL,
  `status` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `project_start_date` date DEFAULT NULL,
  `project_end_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9D996984979B1AD6` (`company_id`),
  KEY `IDX_9D996984E7A1254A` (`contact_id`),
  KEY `IDX_9D996984A6AB49AC` (`user_id_modified`),
  KEY `IDX_9D996984CBC7D824` (`user_id_created`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=87 ;

--
-- Dumping data for table `enquiry`
--

INSERT INTO `enquiry` (`id`, `code`, `description`, `flag`, `creation_date`, `modification_date`, `company_id`, `contact_id`, `enquiry_date`, `status`, `user_id_modified`, `user_id_created`, `project_start_date`, `project_end_date`) VALUES
(81, 'E000081', NULL, NULL, '2016-03-10 21:30:25', '2016-05-17 12:22:08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 'E000082', 'adasdasd', NULL, '2016-04-13 16:12:44', '2016-04-13 16:13:45', 19, 28, '2016-04-13', NULL, 1, 1, NULL, NULL),
(84, 'E000084', NULL, NULL, '2015-11-01 00:00:00', '2016-05-04 08:37:19', 66, 30, '2016-05-04', 'Confirmed', 1, NULL, NULL, NULL),
(85, 'E000085', 'asdasdasd', NULL, '2016-04-27 14:38:59', '2016-04-27 14:38:59', NULL, NULL, NULL, NULL, 1, 1, NULL, NULL),
(86, 'E000086', 'asdasdasd', NULL, '2016-04-27 14:41:09', '2016-05-02 15:43:54', 66, 30, NULL, 'Confirmed', 1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gantt_row`
--

CREATE TABLE IF NOT EXISTS `gantt_row` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `gantt_id` int(11) DEFAULT NULL,
  `gantt_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6D3D56BBCBC7D824` (`user_id_created`),
  KEY `IDX_6D3D56BBA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Dumping data for table `gantt_row`
--

INSERT INTO `gantt_row` (`id`, `user_id_created`, `user_id_modified`, `name`, `color`, `sort_order`, `creation_date`, `modification_date`, `gantt_id`, `gantt_name`) VALUES
(8, 1, 1, 'Enquiry', NULL, NULL, '2016-04-11 13:33:12', '2016-04-11 14:46:04', 81, 'enquiry'),
(9, 1, 1, 'Trading Order', NULL, NULL, '2016-04-11 14:49:24', '2016-04-11 14:49:24', 81, 'enquiry'),
(11, 1, 1, 'test row1111111', NULL, NULL, '2016-04-12 16:27:57', '2016-04-12 16:28:13', 81, 'enquiry'),
(12, 1, 1, '4546456', NULL, NULL, '2016-04-12 17:49:58', '2016-04-12 17:50:20', 81, 'enquiry'),
(13, 1, 1, 'yyyyyyyyyyy', NULL, NULL, '2016-04-12 17:55:19', '2016-04-12 17:55:19', 81, 'enquiry'),
(14, 1, 1, 'New row', NULL, NULL, '2016-04-12 18:36:30', '2016-04-12 20:30:08', 30, 'contact'),
(18, 1, 1, 'Enquiry', NULL, NULL, '2016-05-02 13:10:45', '2016-05-02 13:10:45', 47, 'contact');

-- --------------------------------------------------------

--
-- Table structure for table `gantt_task`
--

CREATE TABLE IF NOT EXISTS `gantt_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gantt_row_id` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_date` date DEFAULT NULL,
  `to_date` date DEFAULT NULL,
  `color` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1F25B7DD1BC3626F` (`gantt_row_id`),
  KEY `IDX_1F25B7DDCBC7D824` (`user_id_created`),
  KEY `IDX_1F25B7DDA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=187 ;

--
-- Dumping data for table `gantt_task`
--

INSERT INTO `gantt_task` (`id`, `gantt_row_id`, `user_id_created`, `user_id_modified`, `name`, `from_date`, `to_date`, `color`, `creation_date`, `modification_date`) VALUES
(183, 8, 1, 1, 'New task1212', '2016-03-03', '2016-03-06', NULL, '2016-04-11 13:33:20', '2016-04-12 17:56:21'),
(184, 8, 1, 1, 'asda', '2016-03-12', '2016-03-15', NULL, '2016-04-11 15:51:04', '2016-04-11 15:51:08'),
(185, 11, 1, 1, 'New task', '2016-03-07', '2016-03-08', NULL, '2016-04-12 18:02:45', '2016-04-12 18:02:45'),
(186, 14, 1, 1, 'New task', '2016-04-14', '2016-04-17', NULL, '2016-04-12 18:42:20', '2016-04-12 20:28:48');

-- --------------------------------------------------------

--
-- Table structure for table `interest_group`
--

CREATE TABLE IF NOT EXISTS `interest_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8960A54ECBC7D824` (`user_id_created`),
  KEY `IDX_8960A54EA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `interest_group`
--

INSERT INTO `interest_group` (`id`, `title`, `type`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, 'Group 1', NULL, NULL, 1, 1, '2016-05-02 13:35:32', '2016-05-02 13:35:32');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `sales_confirmation_id` int(11) DEFAULT NULL,
  `shipping_medium` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vessel_name` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `voyage_no` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sailing_on_date` date DEFAULT NULL,
  `invoice_date` date DEFAULT NULL,
  `port_of_loading` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port_of_delivery` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final_destination` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `container_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lc_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bl_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bl_issued_by` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insurance_policy_no` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `policy_no` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_address` varchar(350) COLLATE utf8_unicode_ci DEFAULT NULL,
  `our_bank_account` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `our_bank_details` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid_date` date DEFAULT NULL,
  `goods_description` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_of_ctns` int(11) DEFAULT NULL,
  `gross_weight` decimal(10,0) DEFAULT NULL,
  `measurement` decimal(10,0) DEFAULT NULL,
  `shipping_mark_front` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_mark_side` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_90651744CBC7D824` (`user_id_created`),
  KEY `IDX_90651744A6AB49AC` (`user_id_modified`),
  KEY `IDX_90651744579AC939` (`trading_order_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `code`, `trading_order_id`, `company_id`, `enquiry_id`, `sales_confirmation_id`, `shipping_medium`, `vessel_name`, `voyage_no`, `sailing_on_date`, `invoice_date`, `port_of_loading`, `port_of_delivery`, `final_destination`, `container_no`, `lc_no`, `bl_no`, `bl_issued_by`, `insurance_policy_no`, `policy_no`, `payment_terms`, `delivery_terms`, `client_address`, `our_bank_account`, `our_bank_details`, `notes`, `status`, `paid_date`, `goods_description`, `no_of_ctns`, `gross_weight`, `measurement`, `shipping_mark_front`, `shipping_mark_side`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, 'dsfsdfsf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2016-04-15 15:58:04', '2016-04-15 15:58:04');

-- --------------------------------------------------------

--
-- Table structure for table `manufacturing_order`
--

CREATE TABLE IF NOT EXISTS `manufacturing_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `company_id_supplier` int(11) DEFAULT NULL,
  `delivery_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_medium` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port_of_delivery` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final_destination` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required_delivery_date` date DEFAULT NULL,
  `warranty_and_claims` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_34010DB1CBC7D824` (`user_id_created`),
  KEY `IDX_34010DB1A6AB49AC` (`user_id_modified`),
  KEY `IDX_34010DB1579AC939` (`trading_order_id`),
  KEY `IDX_34010DB1D2203873` (`company_id_supplier`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `manufacturing_order`
--

INSERT INTO `manufacturing_order` (`id`, `code`, `trading_order_id`, `company_id_supplier`, `delivery_terms`, `payment_terms`, `shipping_medium`, `port_of_delivery`, `final_destination`, `required_delivery_date`, `warranty_and_claims`, `notes`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, 'MO10001', 1, 66, 'asddasd', NULL, NULL, NULL, NULL, '2016-01-05', NULL, NULL, NULL, 1, 1, '2016-03-06 17:46:31', '2016-03-06 17:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `master_checklist`
--

CREATE TABLE IF NOT EXISTS `master_checklist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_2FF6CE62CBC7D824` (`user_id_created`),
  KEY `IDX_2FF6CE62A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `master_checklist`
--

INSERT INTO `master_checklist` (`id`, `title`, `category`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, 'Check list', 'asdasd', NULL, NULL, 1, '0000-00-00 00:00:00', '2016-05-02 15:14:50'),
(2, 'asdasd', 'Pre-Production', NULL, 1, 1, '2016-03-06 16:42:19', '2016-04-19 19:18:23');

-- --------------------------------------------------------

--
-- Table structure for table `master_compliance`
--

CREATE TABLE IF NOT EXISTS `master_compliance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `part` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `compliance` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required_value` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_ACD75F5DCBC7D824` (`user_id_created`),
  KEY `IDX_ACD75F5DA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `master_compliance`
--

INSERT INTO `master_compliance` (`id`, `part`, `compliance`, `required_value`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, 'Plastic parts', 'Cadmium', '7', NULL, NULL, 1, '0000-00-00 00:00:00', '2016-03-09 21:18:16'),
(2, 'Fabrics', 'AZO dyes', '10', NULL, 1, 1, '2016-03-06 16:39:33', '2016-03-09 21:17:39');

-- --------------------------------------------------------

--
-- Table structure for table `master_performance`
--

CREATE TABLE IF NOT EXISTS `master_performance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `part` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `performance` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `standard` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prof_guar_value` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_88DF2604CBC7D824` (`user_id_created`),
  KEY `IDX_88DF2604A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `master_performance`
--

INSERT INTO `master_performance` (`id`, `part`, `performance`, `standard`, `prof_guar_value`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, 'Shoulder straps and back fabric', 'Color fastness to rubbing', 'ISO 105 X 12', '2-3', NULL, NULL, 1, '0000-00-00 00:00:00', '2016-03-09 21:16:35'),
(2, 'Main fabric', 'Color fastness to water', 'ISO 105 E01', '3', NULL, 1, 1, '2016-03-06 16:35:55', '2016-03-09 21:16:00');

-- --------------------------------------------------------

--
-- Table structure for table `ma_other_action`
--

CREATE TABLE IF NOT EXISTS `ma_other_action` (
  `other_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `has_list` tinyint(4) DEFAULT NULL,
  `has_detail` tinyint(4) DEFAULT NULL,
  `has_new` tinyint(4) DEFAULT NULL,
  `has_edit` tinyint(4) DEFAULT NULL,
  `has_find` tinyint(4) DEFAULT NULL,
  `has_modify` tinyint(4) DEFAULT NULL,
  `is_global` tinyint(4) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`other_action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ma_role`
--

CREATE TABLE IF NOT EXISTS `ma_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `role_type` varchar(100) NOT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `description` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_78CB23C8CBC7D824` (`user_id_created`),
  KEY `IDX_78CB23C8A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ma_role`
--

INSERT INTO `ma_role` (`id`, `title`, `role_type`, `creation_date`, `modification_date`, `flag`, `user_id_created`, `user_id_modified`, `description`) VALUES
(1, 'User', 'ROLE_USER', '0000-00-00 00:00:00', '2016-04-20 11:42:08', 0, NULL, 1, 'aasdad'),
(2, 'Editors', 'ROLE_EDITOR', '2016-04-20 11:49:06', '2016-04-20 11:49:06', NULL, 1, 1, NULL),
(3, 'Admin', 'ROLE_ADMIN', '2016-04-20 12:29:46', '2016-04-20 12:29:46', NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ma_role_other_action`
--

CREATE TABLE IF NOT EXISTS `ma_role_other_action` (
  `role_other_action_id` int(11) NOT NULL AUTO_INCREMENT,
  `other_action_id` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `has_access` tinyint(4) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`role_other_action_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ma_room`
--

CREATE TABLE IF NOT EXISTS `ma_room` (
  `room_id` int(11) NOT NULL AUTO_INCREMENT,
  `list` tinyint(4) DEFAULT NULL,
  `detail` tinyint(4) DEFAULT NULL,
  `new` tinyint(4) DEFAULT NULL,
  `edit` tinyint(4) DEFAULT NULL,
  `delete` tinyint(4) DEFAULT NULL,
  `duplicate` tinyint(4) DEFAULT NULL,
  `search` tinyint(4) DEFAULT NULL,
  `print` tinyint(4) DEFAULT NULL,
  `publish` tinyint(4) DEFAULT NULL,
  `unpublish` tinyint(4) DEFAULT NULL,
  `import` tinyint(4) DEFAULT NULL,
  `export` tinyint(4) DEFAULT NULL,
  `room_name` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ma_room`
--

INSERT INTO `ma_room` (`room_id`, `list`, `detail`, `new`, `edit`, `delete`, `duplicate`, `search`, `print`, `publish`, `unpublish`, `import`, `export`, `room_name`, `title`, `creation_date`, `modification_date`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Category', 'Category', '2016-03-11 00:00:00', '2016-03-11 00:00:00'),
(2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 'Contact', 'Contact', '2016-03-17 00:00:00', '2016-03-17 00:00:00'),
(3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, 'Content', 'Content', '2016-03-17 00:00:00', '2016-03-17 00:00:00'),
(4, 1, 1, 1, NULL, 1, 1, 1, 1, 1, 1, NULL, 1, 'Enquiry', 'Enquiry', '2016-03-17 00:00:00', '2016-03-17 00:00:00'),
(5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 'Section', 'Section', '2016-03-17 00:00:00', '2016-03-17 00:00:00'),
(6, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 'Service', 'Service', '2016-03-17 00:00:00', '2016-03-17 00:00:00'),
(7, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 'Setting', 'Setting', '2016-03-17 00:00:00', '2016-03-17 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `ma_room_role`
--

CREATE TABLE IF NOT EXISTS `ma_room_role` (
  `room_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `room_title` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `ac_list` tinyint(1) DEFAULT NULL,
  `ac_detail` tinyint(1) DEFAULT NULL,
  `ac_new` tinyint(1) DEFAULT NULL,
  `ac_edit` tinyint(1) DEFAULT NULL,
  `ac_delete` tinyint(1) DEFAULT NULL,
  `ac_duplicate` tinyint(1) DEFAULT NULL,
  `ac_search` tinyint(1) DEFAULT NULL,
  `ac_print` tinyint(1) DEFAULT NULL,
  `ac_publish` tinyint(1) DEFAULT NULL,
  `ac_unpublish` tinyint(1) DEFAULT NULL,
  `ac_import` tinyint(1) DEFAULT NULL,
  `ac_export` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`room_role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Dumping data for table `ma_room_role`
--

INSERT INTO `ma_room_role` (`room_role_id`, `room_id`, `creation_date`, `role_id`, `room_title`, `modification_date`, `ac_list`, `ac_detail`, `ac_new`, `ac_edit`, `ac_delete`, `ac_duplicate`, `ac_search`, `ac_print`, `ac_publish`, `ac_unpublish`, `ac_import`, `ac_export`) VALUES
(1, 1, '2016-04-07 17:53:54', 1, 'Category', '2016-04-20 12:30:44', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(2, 2, '2016-04-07 17:53:54', 1, 'Contact', '2016-04-20 12:30:44', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(3, 3, '2016-04-07 17:53:55', 1, 'Content', '2016-04-20 12:30:44', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(4, 4, '2016-04-07 17:53:55', 1, 'Enquiry', '2016-04-20 12:30:44', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(5, 5, '2016-04-07 17:53:55', 1, 'Section', '2016-04-20 12:30:44', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(6, 6, '2016-04-07 17:53:55', 1, 'Service', '2016-04-20 12:30:44', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(7, 7, '2016-04-07 17:53:55', 1, 'Setting', '2016-04-20 12:30:44', 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1),
(8, 1, '2016-04-20 11:49:06', 2, 'Category', '2016-04-20 11:49:16', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(9, 2, '2016-04-20 11:49:06', 2, 'Contact', '2016-04-20 11:49:16', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(10, 3, '2016-04-20 11:49:06', 2, 'Content', '2016-04-20 11:49:16', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(11, 4, '2016-04-20 11:49:06', 2, 'Enquiry', '2016-04-20 11:49:16', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(12, 5, '2016-04-20 11:49:06', 2, 'Section', '2016-04-20 11:49:16', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(13, 6, '2016-04-20 11:49:06', 2, 'Service', '2016-04-20 11:49:16', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(14, 7, '2016-04-20 11:49:06', 2, 'Setting', '2016-04-20 11:49:16', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 1, '2016-04-20 12:29:46', 3, 'Category', '2016-04-20 12:29:46', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(16, 2, '2016-04-20 12:29:46', 3, 'Contact', '2016-04-20 12:29:46', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(17, 3, '2016-04-20 12:29:46', 3, 'Content', '2016-04-20 12:29:46', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0),
(18, 4, '2016-04-20 12:29:46', 3, 'Enquiry', '2016-04-20 12:29:46', 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1),
(19, 5, '2016-04-20 12:29:46', 3, 'Section', '2016-04-20 12:29:46', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0),
(20, 6, '2016-04-20 12:29:46', 3, 'Service', '2016-04-20 12:29:46', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0),
(21, 7, '2016-04-20 12:29:46', 3, 'Setting', '2016-04-20 12:29:46', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user`
--

CREATE TABLE IF NOT EXISTS `ma_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staff_rate` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `address_street` varchar(255) DEFAULT NULL,
  `address_town` varchar(255) DEFAULT NULL,
  `address_state` varchar(255) DEFAULT NULL,
  `address_country` varchar(255) DEFAULT NULL,
  `address_po_code` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `published` tinyint(1) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `pass_word` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `flag` tinyint(1) DEFAULT NULL,
  `sex` varchar(255) DEFAULT NULL,
  `date_of_birth` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ma_user`
--

INSERT INTO `ma_user` (`id`, `staff_rate`, `email`, `address_street`, `address_town`, `address_state`, `address_country`, `address_po_code`, `phone`, `fax`, `notes`, `published`, `creation_date`, `modification_date`, `user_name`, `pass_word`, `first_name`, `last_name`, `mobile`, `flag`, `sex`, `date_of_birth`, `status`) VALUES
(1, 0, 'nicu@localhost.dev', 'address_street 111111', 'address_town', 'address_state', 'address_country', 'address_po_code', '111111111111', '22222222222', 'dasfsadfsdf', 1, '2016-04-26 00:00:00', '2016-05-17 14:34:09', 'admin', '$2y$15$hFS.Dyqyxk0NKgeEkGTVfeUrPJPJRhJl08rxtNWgP/he5H1T583SO', 'Pilot', 'Admin', '12121212', 0, 'M', '2016-04-26 00:00:00', 'active'),
(2, NULL, 'paolo.sepulcri@libra.com.hk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-03-06 15:35:01', '2016-03-06 15:35:01', 'paolo.sepulcri', NULL, 'Paolo', 'Sepulcri', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ma_user_role`
--

CREATE TABLE IF NOT EXISTS `ma_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  UNIQUE KEY `UNIQ_48CF61B4D60322AC` (`role_id`),
  KEY `IDX_DF8CEDB9A76ED395` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ma_user_role`
--

INSERT INTO `ma_user_role` (`user_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `actual_file_name` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `file_name` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `media_size` int(11) DEFAULT NULL,
  `display_title` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modification_date` datetime NOT NULL,
  `description` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=210 ;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `actual_file_name`, `file_name`, `creation_date`, `media_size`, `display_title`, `content_type`, `modification_date`, `description`) VALUES
(140, 'falls5.jpg', '140_falls5.jpg', '2016-04-18 00:00:20', NULL, 'falls5.jpg', NULL, '2016-04-18 00:00:20', NULL),
(141, 'falls5.jpg', '141_falls5.jpg', '2016-04-18 00:22:43', NULL, 'falls5.jpg', NULL, '2016-04-18 00:22:44', NULL),
(142, 'falls5.jpg', '142_falls5.jpg', '2016-04-18 00:23:12', NULL, 'falls5.jpg', NULL, '2016-04-18 00:23:12', NULL),
(143, 'falls3.jpg', '143_falls3.jpg', '2016-04-18 10:37:25', NULL, 'falls3.jpg', NULL, '2016-04-18 10:37:25', NULL),
(144, 'falls5.jpg', '144_falls5.jpg', '2016-04-18 11:13:49', NULL, 'falls5.jpg', NULL, '2016-04-18 11:13:49', NULL),
(146, 'images-2.jpg', '146_images-2.jpg', '2016-04-18 11:28:51', NULL, 'images-2.jpg', NULL, '2016-04-18 11:28:51', NULL),
(147, 'images.jpg', '147_images.jpg', '2016-04-18 23:53:15', NULL, 'images.jpg', NULL, '2016-04-18 23:53:15', NULL),
(148, 'falls5.jpg', '148_falls5.jpg', '2016-04-19 01:31:09', NULL, 'falls5.jpg', NULL, '2016-04-19 01:31:09', NULL),
(149, 'falls1.jpg', '149_falls1.jpg', '2016-04-19 09:42:09', NULL, 'falls1.jpg', NULL, '2016-04-19 09:42:09', NULL),
(150, 'falls2.jpg', '150_falls2.jpg', '2016-04-19 09:42:10', NULL, 'falls2.jpg', NULL, '2016-04-19 09:42:10', NULL),
(151, 'falls3.jpg', '151_falls3.jpg', '2016-04-19 09:42:10', NULL, 'falls3.jpg', NULL, '2016-04-19 09:42:10', NULL),
(152, 'falls4.jpg', '152_falls4.jpg', '2016-04-19 09:42:11', NULL, 'falls4.jpg', NULL, '2016-04-19 09:42:11', NULL),
(153, 'falls6.JPG', '153_falls6.JPG', '2016-04-19 17:53:04', NULL, 'falls6.JPG', NULL, '2016-04-19 17:53:04', NULL),
(154, 'falls6.JPG', '154_falls6.JPG', '2016-04-19 17:58:16', NULL, 'falls6.JPG', NULL, '2016-04-19 17:58:16', NULL),
(155, 'falls4.jpg', '155_falls4.jpg', '2016-04-19 19:12:29', NULL, 'falls4.jpg', NULL, '2016-04-19 19:12:29', NULL),
(156, 'falls5.jpg', '156_falls5.jpg', '2016-04-19 19:12:30', NULL, 'falls5.jpg', NULL, '2016-04-19 19:12:30', NULL),
(157, 'falls4.jpg', '157_falls4.jpg', '2016-04-19 19:16:21', NULL, 'falls4.jpg', NULL, '2016-04-19 19:16:21', NULL),
(158, 'falls4.jpg', '158_falls4.jpg', '2016-04-19 19:18:31', NULL, 'falls4.jpg', NULL, '2016-04-19 19:18:31', NULL),
(159, 'falls5.jpg', '159_falls5.jpg', '2016-04-19 19:18:32', NULL, 'falls5.jpg', NULL, '2016-04-19 19:18:32', NULL),
(160, 'falls6.JPG', '160_falls6.JPG', '2016-04-19 19:18:33', NULL, 'falls6.JPG', NULL, '2016-04-19 19:18:33', NULL),
(161, 'falls3.jpg', '161_falls3.jpg', '2016-04-29 14:06:04', NULL, 'falls3.jpg', NULL, '2016-04-29 14:06:04', NULL),
(162, 'falls5.jpg', '162_falls5.jpg', '2016-05-02 13:49:01', NULL, 'falls5.jpg', NULL, '2016-05-02 13:49:01', NULL),
(170, 'falls5.jpg', '170_falls5.jpg', '2016-05-02 14:16:37', NULL, 'falls5.jpg', NULL, '2016-05-02 14:16:37', NULL),
(171, 'falls4.jpg', '171_falls4.jpg', '2016-05-02 14:20:57', NULL, 'falls4.jpg', NULL, '2016-05-02 14:20:58', NULL),
(179, 'falls2.jpg', '179_falls2.jpg', '2016-05-02 15:03:53', NULL, 'falls2.jpg', NULL, '2016-05-02 15:03:53', NULL),
(180, 'falls3.jpg', '180_falls3.jpg', '2016-05-02 15:03:54', NULL, 'falls3.jpg', NULL, '2016-05-02 15:03:54', NULL),
(181, 'falls4.jpg', '181_falls4.jpg', '2016-05-02 15:03:55', NULL, 'falls4.jpg', NULL, '2016-05-02 15:03:55', NULL),
(182, 'falls5.jpg', '182_falls5.jpg', '2016-05-02 15:14:05', NULL, 'falls5.jpg', NULL, '2016-05-02 15:14:05', NULL),
(183, 'falls4.jpg', '183_falls4.jpg', '2016-05-02 16:24:21', NULL, 'falls4.jpg', NULL, '2016-05-02 16:24:21', NULL),
(184, 'falls4.jpg', '184_falls4.jpg', '2016-05-02 18:28:02', NULL, 'falls4.jpg', NULL, '2016-05-02 18:28:02', NULL),
(185, 'falls5.jpg', '185_falls5.jpg', '2016-05-02 18:32:17', NULL, 'falls5.jpg', NULL, '2016-05-02 18:32:17', NULL),
(186, 'falls4.jpg', '186_falls4.jpg', '2016-05-02 18:42:06', NULL, 'falls4.jpg', NULL, '2016-05-02 18:42:06', NULL),
(187, 'falls5.jpg', '187_falls5.jpg', '2016-05-02 18:43:42', NULL, 'falls5.jpg', NULL, '2016-05-02 18:43:43', NULL),
(190, 'bssh3.jpg', '190_bssh3.jpg', '2016-05-04 14:01:15', NULL, 'bssh3.jpg', NULL, '2016-05-04 14:01:15', NULL),
(191, 'bssh4.jpg', '191_bssh4.jpg', '2016-05-04 14:02:52', NULL, 'bssh4.jpg', NULL, '2016-05-04 14:02:52', NULL),
(202, 'scenery-5.jpg', '202_scenery-5.jpg', '2016-05-05 13:21:24', 1157361, 'scenery-5.jpg', NULL, '2016-05-05 13:21:24', NULL),
(203, 'scenery-4.jpg', '203_scenery-4.jpg', '2016-05-05 13:21:25', 1618214, 'scenery-4.jpg', NULL, '2016-05-05 13:21:25', NULL),
(204, 'scenery-3.jpg', '204_scenery-3.jpg', '2016-05-05 13:21:26', 489599, 'scenery-3.jpg', NULL, '2016-05-05 13:21:26', NULL),
(205, 'scenery-2.jpg', '205_scenery-2.jpg', '2016-05-05 13:21:27', 1150848, 'scenery-2.jpg', NULL, '2016-05-05 13:21:27', NULL),
(206, 'scenery-1.jpg', '206_scenery-1.jpg', '2016-05-05 13:21:27', 687171, 'scenery-1.jpg', NULL, '2016-05-05 13:21:28', NULL),
(207, 'scenery-2.jpg', '207_scenery-2.jpg', '2016-05-17 21:02:35', 1150848, 'Beautiful Scenery', NULL, '2016-05-17 21:03:22', NULL),
(208, 'scenery-3.jpg', '208_scenery-3.jpg', '2016-05-17 21:02:36', 489599, 'scenery-3.jpg', NULL, '2016-05-17 21:02:36', NULL),
(209, 'scenery-4.jpg', '209_scenery-4.jpg', '2016-05-17 21:02:36', 1618214, 'scenery-4.jpg', NULL, '2016-05-17 21:02:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_history`
--

CREATE TABLE IF NOT EXISTS `media_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `media_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `media_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `color_master_id` int(11) DEFAULT NULL,
  `parts_location_id` int(11) DEFAULT NULL,
  `approved_material_id` int(11) DEFAULT NULL,
  `approved_component_id` int(11) DEFAULT NULL,
  `approved_packaging_id` int(11) DEFAULT NULL,
  `master_performance_id` int(11) DEFAULT NULL,
  `master_compliance_id` int(11) DEFAULT NULL,
  `master_checklist_id` int(11) DEFAULT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `sales_confirmation_id` int(11) DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `manufacturing_order_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `vendor_invoice_id` int(11) DEFAULT NULL,
  `accounts_receivable_id` int(11) DEFAULT NULL,
  `accounts_payable_id` int(11) DEFAULT NULL,
  `credit_debit_note_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `research_id` int(11) DEFAULT NULL,
  `proto_type_id` int(11) DEFAULT NULL,
  `interest_group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_466DF602EA9FDD75` (`media_id`),
  KEY `IDX_466DF602979B1AD6` (`company_id`),
  KEY `IDX_466DF602E7A1254A` (`contact_id`),
  KEY `IDX_466DF6028BBD7E36` (`color_master_id`),
  KEY `IDX_466DF602437031EE` (`parts_location_id`),
  KEY `IDX_466DF602D7C1568D` (`approved_material_id`),
  KEY `IDX_466DF602AC9BE551` (`approved_component_id`),
  KEY `IDX_466DF6024B72AF` (`approved_packaging_id`),
  KEY `IDX_466DF602B4FA2105` (`master_performance_id`),
  KEY `IDX_466DF60227B17E93` (`master_compliance_id`),
  KEY `IDX_466DF6022CB577B8` (`master_checklist_id`),
  KEY `IDX_466DF602BD918DFF` (`enquiry_id`),
  KEY `IDX_466DF602B4EA4E60` (`quotation_id`),
  KEY `IDX_466DF602579AC939` (`trading_order_id`),
  KEY `IDX_466DF6021D22091A` (`sales_confirmation_id`),
  KEY `IDX_466DF602A45D7E6A` (`purchase_order_id`),
  KEY `IDX_466DF60260741FF8` (`manufacturing_order_id`),
  KEY `IDX_466DF6022989F1FD` (`invoice_id`),
  KEY `IDX_466DF602B3FC259A` (`vendor_invoice_id`),
  KEY `IDX_466DF602907A7FB1` (`accounts_receivable_id`),
  KEY `IDX_466DF602E33400EF` (`accounts_payable_id`),
  KEY `IDX_466DF6025862141D` (`credit_debit_note_id`),
  KEY `IDX_466DF602A76ED395` (`user_id`),
  KEY `IDX_466DF6027909E1ED` (`research_id`),
  KEY `IDX_466DF60216839E83` (`proto_type_id`),
  KEY `IDX_466DF60282874C87` (`interest_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=76 ;

--
-- Dumping data for table `media_history`
--

INSERT INTO `media_history` (`id`, `media_id`, `company_id`, `media_type`, `module`, `creation_date`, `contact_id`, `color_master_id`, `parts_location_id`, `approved_material_id`, `approved_component_id`, `approved_packaging_id`, `master_performance_id`, `master_compliance_id`, `master_checklist_id`, `enquiry_id`, `quotation_id`, `trading_order_id`, `sales_confirmation_id`, `purchase_order_id`, `manufacturing_order_id`, `invoice_id`, `vendor_invoice_id`, `accounts_receivable_id`, `accounts_payable_id`, `credit_debit_note_id`, `user_id`, `research_id`, `proto_type_id`, `interest_group_id`) VALUES
(7, 141, NULL, 'attachment', 'contact', '2016-04-18 00:22:44', 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 142, NULL, 'attachment', 'contact', '2016-04-18 00:23:12', 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 143, 66, 'attachment', 'company', '2016-04-18 10:37:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 144, 66, 'attachment', 'company', '2016-04-18 11:13:49', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 146, 66, 'attachment', 'company', '2016-04-18 11:28:51', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 147, NULL, 'attachment', 'partsLocation', '2016-04-18 23:53:15', NULL, NULL, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 148, NULL, 'attachment', 'approvedMaterial', '2016-04-19 01:31:09', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 149, NULL, 'image', 'approvedComponent', '2016-04-19 09:42:09', NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 150, NULL, 'attachment', 'approvedComponent', '2016-04-19 09:42:10', NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 151, NULL, 'attachment', 'approvedComponent', '2016-04-19 09:42:10', NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 152, NULL, 'attachment', 'approvedComponent', '2016-04-19 09:42:11', NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 153, NULL, 'attachment', 'approvedPackaging', '2016-04-19 17:53:04', NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 154, NULL, 'attachment', 'masterPerformance', '2016-04-19 17:58:16', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 155, NULL, 'attachment', 'colorMaster', '2016-04-19 19:12:29', NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 156, NULL, 'attachment', 'colorMaster', '2016-04-19 19:12:30', NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 157, NULL, 'attachment', 'masterCompliance', '2016-04-19 19:16:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 158, NULL, 'attachment', 'masterChecklist', '2016-04-19 19:18:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 159, NULL, 'attachment', 'masterChecklist', '2016-04-19 19:18:32', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 160, NULL, 'attachment', 'masterChecklist', '2016-04-19 19:18:33', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 161, NULL, 'attachment', 'research', '2016-04-29 14:06:04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL),
(28, 162, NULL, 'attachment', 'colorMaster', '2016-05-02 13:49:01', NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 170, NULL, 'attachment', 'colorMaster', '2016-05-02 14:16:37', NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 171, NULL, 'attachment', 'colorMaster', '2016-05-02 14:20:58', NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 179, NULL, 'attachment', 'interestGroup', '2016-05-02 15:03:53', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(46, 180, NULL, 'attachment', 'interestGroup', '2016-05-02 15:03:54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(47, 181, NULL, 'attachment', 'interestGroup', '2016-05-02 15:03:55', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(48, 182, NULL, 'attachment', 'masterCompliance', '2016-05-02 15:14:05', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 183, NULL, 'attachment', 'enquiry', '2016-05-02 16:24:21', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 184, NULL, 'attachment', 'accountsReceivable', '2016-05-02 18:28:02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 185, NULL, 'attachment', 'accountsPayable', '2016-05-02 18:32:17', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(52, 186, NULL, 'attachment', 'protoType', '2016-05-02 18:42:06', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL),
(53, 187, NULL, 'attachment', 'protoType', '2016-05-02 18:43:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL),
(56, 190, NULL, 'attachment', 'protoType', '2016-05-04 14:01:15', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(57, 191, NULL, 'attachment', 'enquiry', '2016-05-04 14:02:52', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 202, NULL, 'image', 'protoType', '2016-05-05 13:21:24', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(69, 203, NULL, 'image', 'protoType', '2016-05-05 13:21:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(70, 204, NULL, 'image', 'protoType', '2016-05-05 13:21:26', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(71, 205, NULL, 'image', 'protoType', '2016-05-05 13:21:27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(72, 206, NULL, 'image', 'protoType', '2016-05-05 13:21:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(73, 207, NULL, 'image', 'protoType', '2016-05-17 21:02:35', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(74, 208, NULL, 'image', 'protoType', '2016-05-17 21:02:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL),
(75, 209, NULL, 'image', 'protoType', '2016-05-17 21:02:36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media_user`
--

CREATE TABLE IF NOT EXISTS `media_user` (
  `user_id` int(11) NOT NULL,
  `media_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`media_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `subject` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` varchar(4000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `color_master_id` int(11) DEFAULT NULL,
  `parts_location_id` int(11) DEFAULT NULL,
  `approved_material_id` int(11) DEFAULT NULL,
  `approved_component_id` int(11) DEFAULT NULL,
  `approved_packaging_id` int(11) DEFAULT NULL,
  `master_performance_id` int(11) DEFAULT NULL,
  `master_compliance_id` int(11) DEFAULT NULL,
  `master_checklist_id` int(11) DEFAULT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `sales_confirmation_id` int(11) DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `manufacturing_order_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `vendor_invoice_id` int(11) DEFAULT NULL,
  `accounts_receivable_id` int(11) DEFAULT NULL,
  `accounts_payable_id` int(11) DEFAULT NULL,
  `credit_debit_note_id` int(11) DEFAULT NULL,
  `interest_group_id` int(11) DEFAULT NULL,
  `research_id` int(11) DEFAULT NULL,
  `proto_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B6BD307FCBC7D824` (`user_id_created`),
  KEY `IDX_B6BD307FA6AB49AC` (`user_id_modified`),
  KEY `IDX_B6BD307F979B1AD6` (`company_id`),
  KEY `IDX_B6BD307FE7A1254A` (`contact_id`),
  KEY `IDX_B6BD307F8BBD7E36` (`color_master_id`),
  KEY `IDX_B6BD307F437031EE` (`parts_location_id`),
  KEY `IDX_B6BD307FD7C1568D` (`approved_material_id`),
  KEY `IDX_B6BD307FAC9BE551` (`approved_component_id`),
  KEY `IDX_B6BD307F4B72AF` (`approved_packaging_id`),
  KEY `IDX_B6BD307FB4FA2105` (`master_performance_id`),
  KEY `IDX_B6BD307F27B17E93` (`master_compliance_id`),
  KEY `IDX_B6BD307F2CB577B8` (`master_checklist_id`),
  KEY `IDX_B6BD307FBD918DFF` (`enquiry_id`),
  KEY `IDX_B6BD307FB4EA4E60` (`quotation_id`),
  KEY `IDX_B6BD307F579AC939` (`trading_order_id`),
  KEY `IDX_B6BD307F1D22091A` (`sales_confirmation_id`),
  KEY `IDX_B6BD307FA45D7E6A` (`purchase_order_id`),
  KEY `IDX_B6BD307F60741FF8` (`manufacturing_order_id`),
  KEY `IDX_B6BD307F2989F1FD` (`invoice_id`),
  KEY `IDX_B6BD307FB3FC259A` (`vendor_invoice_id`),
  KEY `IDX_B6BD307F907A7FB1` (`accounts_receivable_id`),
  KEY `IDX_B6BD307FE33400EF` (`accounts_payable_id`),
  KEY `IDX_B6BD307F5862141D` (`credit_debit_note_id`),
  KEY `IDX_B6BD307F82874C87` (`interest_group_id`),
  KEY `IDX_B6BD307F7909E1ED` (`research_id`),
  KEY `IDX_B6BD307F16839E83` (`proto_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `creation_date`, `modification_date`, `user_id_created`, `user_id_modified`, `subject`, `message`, `module`, `company_id`, `contact_id`, `color_master_id`, `parts_location_id`, `approved_material_id`, `approved_component_id`, `approved_packaging_id`, `master_performance_id`, `master_compliance_id`, `master_checklist_id`, `enquiry_id`, `quotation_id`, `trading_order_id`, `sales_confirmation_id`, `purchase_order_id`, `manufacturing_order_id`, `invoice_id`, `vendor_invoice_id`, `accounts_receivable_id`, `accounts_payable_id`, `credit_debit_note_id`, `interest_group_id`, `research_id`, `proto_type_id`) VALUES
(1, '2016-04-19 00:00:00', '2016-04-19 00:00:00', NULL, NULL, 'test subject', NULL, NULL, 66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `message_reply`
--

CREATE TABLE IF NOT EXISTS `message_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message_id` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `subject` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reply` longtext COLLATE utf8_unicode_ci,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_20E390B537A1329` (`message_id`),
  KEY `IDX_20E390BCBC7D824` (`user_id_created`),
  KEY `IDX_20E390BA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migration_versions`
--

CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20150615224956'),
('20150615232137');

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE IF NOT EXISTS `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `note` varchar(5000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `module` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parts_location_id` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `color_master_id` int(11) DEFAULT NULL,
  `approved_material_id` int(11) DEFAULT NULL,
  `approved_component_id` int(11) DEFAULT NULL,
  `approved_packaging_id` int(11) DEFAULT NULL,
  `master_performance_id` int(11) DEFAULT NULL,
  `master_compliance_id` int(11) DEFAULT NULL,
  `master_checklist_id` int(11) DEFAULT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `quotation_id` int(11) DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `sales_confirmation_id` int(11) DEFAULT NULL,
  `purchase_order_id` int(11) DEFAULT NULL,
  `manufacturing_order_id` int(11) DEFAULT NULL,
  `invoice_id` int(11) DEFAULT NULL,
  `vendor_invoice_id` int(11) DEFAULT NULL,
  `accounts_receivable_id` int(11) DEFAULT NULL,
  `accounts_payable_id` int(11) DEFAULT NULL,
  `credit_debit_note_id` int(11) DEFAULT NULL,
  `proto_type_id` int(11) DEFAULT NULL,
  `research_id` int(11) DEFAULT NULL,
  `interest_group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CFBDFA14CBC7D824` (`user_id_created`),
  KEY `IDX_CFBDFA14A6AB49AC` (`user_id_modified`),
  KEY `IDX_CFBDFA14979B1AD6` (`company_id`),
  KEY `IDX_CFBDFA14E7A1254A` (`contact_id`),
  KEY `IDX_CFBDFA148BBD7E36` (`color_master_id`),
  KEY `IDX_CFBDFA14437031EE` (`parts_location_id`),
  KEY `IDX_CFBDFA14D7C1568D` (`approved_material_id`),
  KEY `IDX_CFBDFA14AC9BE551` (`approved_component_id`),
  KEY `IDX_CFBDFA144B72AF` (`approved_packaging_id`),
  KEY `IDX_CFBDFA14B4FA2105` (`master_performance_id`),
  KEY `IDX_CFBDFA1427B17E93` (`master_compliance_id`),
  KEY `IDX_CFBDFA142CB577B8` (`master_checklist_id`),
  KEY `IDX_CFBDFA14BD918DFF` (`enquiry_id`),
  KEY `IDX_CFBDFA14B4EA4E60` (`quotation_id`),
  KEY `IDX_CFBDFA14579AC939` (`trading_order_id`),
  KEY `IDX_CFBDFA141D22091A` (`sales_confirmation_id`),
  KEY `IDX_CFBDFA14A45D7E6A` (`purchase_order_id`),
  KEY `IDX_CFBDFA1460741FF8` (`manufacturing_order_id`),
  KEY `IDX_CFBDFA142989F1FD` (`invoice_id`),
  KEY `IDX_CFBDFA14B3FC259A` (`vendor_invoice_id`),
  KEY `IDX_CFBDFA14907A7FB1` (`accounts_receivable_id`),
  KEY `IDX_CFBDFA14E33400EF` (`accounts_payable_id`),
  KEY `IDX_CFBDFA145862141D` (`credit_debit_note_id`),
  KEY `IDX_CFBDFA147909E1ED` (`research_id`),
  KEY `IDX_CFBDFA1416839E83` (`proto_type_id`),
  KEY `IDX_CFBDFA1482874C87` (`interest_group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=73 ;

--
-- Dumping data for table `note`
--

INSERT INTO `note` (`id`, `note`, `module`, `parts_location_id`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`, `company_id`, `contact_id`, `color_master_id`, `approved_material_id`, `approved_component_id`, `approved_packaging_id`, `master_performance_id`, `master_compliance_id`, `master_checklist_id`, `enquiry_id`, `quotation_id`, `trading_order_id`, `sales_confirmation_id`, `purchase_order_id`, `manufacturing_order_id`, `invoice_id`, `vendor_invoice_id`, `accounts_receivable_id`, `accounts_payable_id`, `credit_debit_note_id`, `proto_type_id`, `research_id`, `interest_group_id`) VALUES
(53, 'asd asd asd12313', 'company', NULL, 1, 1, '2016-04-17 23:36:45', '2016-04-17 23:55:43', 66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'asda SADF 32423', 'company', NULL, 1, 1, '2016-04-17 23:56:34', '2016-04-17 23:56:34', 66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 'a new comment', 'contact', NULL, 1, 1, '2016-04-18 00:23:41', '2016-04-18 00:27:03', NULL, 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'This is a test note adasdasdas', 'company', NULL, 1, 1, '2016-04-18 12:24:51', '2016-04-18 12:24:59', 66, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test.small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test.small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test. small notes test.', 'colorMaster', NULL, 1, 1, '2016-04-18 21:00:20', '2016-05-02 15:09:46', NULL, NULL, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 'sdsadasdasd', 'approvedMaterial', NULL, 1, 1, '2016-04-19 01:31:02', '2016-04-19 01:31:02', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'test note', 'approvedComponent', NULL, 1, 1, '2016-04-19 09:36:01', '2016-04-19 09:36:01', NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'asdasdasd', 'masterPerformance', NULL, 1, 1, '2016-04-19 17:58:25', '2016-04-19 17:58:25', NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 'asdasd', 'masterCompliance', NULL, 1, 1, '2016-04-19 19:16:28', '2016-04-19 19:16:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'adad', 'masterChecklist', NULL, 1, 1, '2016-04-19 19:18:40', '2016-04-19 19:18:40', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 'asdasda', 'research', NULL, 1, 1, '2016-04-29 15:23:46', '2016-04-29 15:23:46', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, '12312 v13 1231 This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasdThis is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. This is a sample note. asdasd', 'research', NULL, 1, 1, '2016-04-29 17:23:55', '2016-04-30 15:57:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4, NULL),
(65, 'asdasd', 'company', NULL, 1, 1, '2016-05-02 13:04:40', '2016-05-02 13:04:40', 77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 'asdadasd', 'interestGroup', NULL, 1, 1, '2016-05-02 15:04:43', '2016-05-02 15:04:43', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(67, 'This is a new note', 'masterCompliance', NULL, 1, 1, '2016-05-02 15:14:22', '2016-05-02 15:14:37', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 'asd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asdasd asd asd asd', 'enquiry', NULL, 1, 1, '2016-05-02 16:29:34', '2016-05-02 16:29:48', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 'dfsdfs', 'accountsReceivable', NULL, 1, 1, '2016-05-02 18:28:09', '2016-05-02 18:28:09', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL),
(70, 'asdasdasd', 'accountsPayable', NULL, 1, 1, '2016-05-02 18:32:25', '2016-05-02 18:32:25', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(71, 'this is a test note', 'enquiry', NULL, 1, 1, '2016-05-03 06:39:28', '2016-05-03 06:39:28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 'asdasd', 'quotation', NULL, 1, 1, '2016-05-03 07:15:03', '2016-05-03 07:15:03', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `parts_location`
--

CREATE TABLE IF NOT EXISTS `parts_location` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location` varchar(175) COLLATE utf8_unicode_ci DEFAULT NULL,
  `floor` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rack` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rack_section` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E51211DDCBC7D824` (`user_id_created`),
  KEY `IDX_E51211DDA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Dumping data for table `parts_location`
--

INSERT INTO `parts_location` (`id`, `location`, `floor`, `rack`, `rack_section`, `creation_date`, `modification_date`, `user_id_created`, `user_id_modified`) VALUES
(5, 'New Loc2', 'fl1', 'rack1', 'rack section 1', '0000-00-00 00:00:00', '2016-03-02 18:26:23', NULL, NULL),
(6, 'dfsdf', '34', '44', '5454', '0000-00-00 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(7, 'New Location', '12', 'R1', 'sdfs', '0000-00-00 00:00:00', '2016-03-02 18:25:39', NULL, NULL),
(8, 'XYZ Bldg1', '11', '15', '2', '2016-03-02 18:27:00', '2016-03-02 18:27:19', NULL, NULL),
(19, 'aaaaaaaaaa', NULL, NULL, NULL, '2016-03-02 19:08:44', '2016-03-02 19:08:44', NULL, NULL),
(22, 'Test the barcode asdasd', '11', '2', 'B', '2016-03-02 19:13:10', '2016-03-06 15:47:19', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_coding`
--

CREATE TABLE IF NOT EXISTS `product_coding` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_family` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_family_code` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_type` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_type_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `product_coding`
--

INSERT INTO `product_coding` (`id`, `product_family`, `product_family_code`, `product_type`, `product_type_code`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, 'Bags', '01', 'Sport bag', 'SPOR', NULL, NULL, NULL, NULL, NULL),
(2, 'Bags', '01', 'Weel bag', 'WHEE', NULL, NULL, NULL, NULL, NULL),
(3, 'Bags', '01', 'Sport bag', 'SPOR', NULL, NULL, NULL, NULL, NULL),
(4, 'Bags', '01', 'Sport bag', 'SPOR', NULL, NULL, NULL, NULL, NULL),
(5, 'Garments', '04', 'Sweater', 'SWEA', NULL, NULL, NULL, NULL, NULL),
(6, 'Backpacks', '02', 'Daypack', 'DAYP', NULL, NULL, NULL, NULL, NULL),
(8, 'Backpacks', '02', 'Daypack cooler', 'DAYC', NULL, NULL, NULL, NULL, NULL),
(9, 'Backpacks', '02', 'Rucksack - 1 compartment', 'RUCO', NULL, NULL, NULL, NULL, NULL),
(10, 'Backpacks', '02', 'Rucksack - 2 compartments', 'RUCT', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `proto_type`
--

CREATE TABLE IF NOT EXISTS `proto_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `art_no` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_name` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_sequence` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sample_ready_date` date DEFAULT NULL,
  `requested_qty` int(11) DEFAULT NULL,
  `weight` decimal(10,0) DEFAULT NULL,
  `capacity` decimal(10,0) DEFAULT NULL,
  `dim_length` decimal(10,0) DEFAULT NULL,
  `dim_width` decimal(10,0) DEFAULT NULL,
  `dim_height` decimal(10,0) DEFAULT NULL,
  `company_id_supplier` int(11) DEFAULT NULL,
  `company_id_client` int(11) DEFAULT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `product_family_code` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `product_type_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `color_code` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_34012D32CBC7D824` (`user_id_created`),
  KEY `IDX_34012D32A6AB49AC` (`user_id_modified`),
  KEY `IDX_34012D32BD918DFF` (`enquiry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `proto_type`
--

INSERT INTO `proto_type` (`id`, `art_no`, `product_name`, `creation_sequence`, `sample_ready_date`, `requested_qty`, `weight`, `capacity`, `dim_length`, `dim_width`, `dim_height`, `company_id_supplier`, `company_id_client`, `description`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`, `enquiry_id`, `product_family_code`, `product_type_code`, `color_code`, `code`) VALUES
(1, '12313', 'adadad', 'G', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-04-26 00:00:00', '2016-05-17 16:32:16', NULL, '01', 'SPOR', NULL, '01-SPOR-11111-W'),
(2, '100001', 'Rucksack 1', 'W', '2016-05-25', 1000, 144, 32, 11, 12, 12, NULL, NULL, NULL, NULL, 1, 1, '2016-03-06 18:48:43', '2016-05-03 13:28:00', 86, '01', 'SPOR', NULL, '01-SPOR-100001-W'),
(3, '213123', 'adasd', 'W', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, '2016-05-03 13:36:41', '2016-05-03 13:36:41', NULL, '04', 'SWEA', NULL, '04-SWEA-213123-W'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2016-05-03 13:39:18', '2016-05-17 12:49:26', NULL, NULL, NULL, NULL, '01-SPOR-234234-P');

-- --------------------------------------------------------

--
-- Table structure for table `proto_type_color_master`
--

CREATE TABLE IF NOT EXISTS `proto_type_color_master` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `color_master_id` int(11) DEFAULT NULL,
  `proto_type_id` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `color_type` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1361A07D8BBD7E36` (`color_master_id`),
  KEY `IDX_1361A07D16839E83` (`proto_type_id`),
  KEY `IDX_1361A07DCBC7D824` (`user_id_created`),
  KEY `IDX_1361A07DA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `proto_type_color_master`
--

INSERT INTO `proto_type_color_master` (`id`, `color_master_id`, `proto_type_id`, `user_id_created`, `user_id_modified`, `color_type`, `creation_date`, `modification_date`) VALUES
(1, 1, 1, NULL, NULL, 'Side', '2016-05-01 00:00:00', '0000-00-00 00:00:00'),
(2, 7, 1, NULL, NULL, 'Main', '2016-05-01 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE IF NOT EXISTS `purchase_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `company_id_supplier` int(11) DEFAULT NULL,
  `delivery_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_medium` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port_of_delivery` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final_destination` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required_delivery_date` date DEFAULT NULL,
  `warranty_and_claims` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `status` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_21E210B2CBC7D824` (`user_id_created`),
  KEY `IDX_21E210B2A6AB49AC` (`user_id_modified`),
  KEY `IDX_21E210B2579AC939` (`trading_order_id`),
  KEY `IDX_21E210B2D2203873` (`company_id_supplier`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `code`, `trading_order_id`, `company_id_supplier`, `delivery_terms`, `payment_terms`, `shipping_medium`, `port_of_delivery`, `final_destination`, `required_delivery_date`, `warranty_and_claims`, `notes`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`, `status`) VALUES
(1, 'PO10001', 1, 66, 'sdfsf', NULL, NULL, NULL, NULL, '2016-05-12', NULL, NULL, NULL, 1, 1, '2016-03-06 17:41:54', '2016-03-06 17:42:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE IF NOT EXISTS `quotation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `sales_delivery_terms` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port_of_delivery` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_medium` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `offer_validity` int(11) DEFAULT NULL,
  `sales_payment_terms` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lead_time` int(11) DEFAULT NULL,
  `quotation_date` date DEFAULT NULL,
  `status` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_474A8DB9CBC7D824` (`user_id_created`),
  KEY `IDX_474A8DB9A6AB49AC` (`user_id_modified`),
  KEY `IDX_474A8DB9979B1AD6` (`company_id`),
  KEY `IDX_474A8DB9BD918DFF` (`enquiry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `code`, `company_id`, `contact_id`, `sales_delivery_terms`, `port_of_delivery`, `shipping_medium`, `offer_validity`, `sales_payment_terms`, `lead_time`, `quotation_date`, `status`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`, `enquiry_id`) VALUES
(1, 'Q123456', 66, NULL, 'adadasdad', NULL, NULL, NULL, NULL, 12, '2016-02-02', 'confirmed', NULL, NULL, 1, '2016-04-14 00:00:00', '2016-04-18 09:44:54', 81);

-- --------------------------------------------------------

--
-- Table structure for table `research`
--

CREATE TABLE IF NOT EXISTS `research` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proto_type_id` int(11) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `research_area` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `description` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_57EB50C2CBC7D824` (`user_id_created`),
  KEY `IDX_57EB50C2A6AB49AC` (`user_id_modified`),
  KEY `IDX_57EB50C2979B1AD6` (`company_id`),
  KEY `IDX_57EB50C216839E83` (`proto_type_id`),
  KEY `IDX_57EB50C2BD918DFF` (`enquiry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `research`
--

INSERT INTO `research` (`id`, `code`, `proto_type_id`, `company_id`, `research_area`, `enquiry_id`, `description`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(4, 'R000004', NULL, 66, 'Research for Rucksack', 81, 'test description. test description. test description. test description. test description. test description. test description. test description. test description. test description. test description. test description.', NULL, NULL, NULL, '2016-04-26 00:00:00', '2016-05-17 12:25:07'),
(5, 'R000005', 4, NULL, 'test reserac area', NULL, 'asdadasd', NULL, NULL, NULL, '2016-04-26 00:00:00', '2016-05-17 12:51:09'),
(6, 'R000006', 1, NULL, 'A new research dasdasdsadad222222222222', NULL, 'this is a test description. this is a test description. this is a test description. this is a test description. this is a test description. this is a test description. this is a test description. this is a test description. \nthis is a test description. this is a test description. this is a test description. this is a test description. this is a test description. this is a test description. this is a test description. this is a test description.', NULL, NULL, NULL, '2016-05-17 14:34:09', '2016-05-18 11:14:20');

-- --------------------------------------------------------

--
-- Table structure for table `sales_confirmation`
--

CREATE TABLE IF NOT EXISTS `sales_confirmation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `specification` varchar(600) COLLATE utf8_unicode_ci DEFAULT NULL,
  `client_order_ref` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `expected_date_departure` date DEFAULT NULL,
  `expected_date_arrival` date DEFAULT NULL,
  `shipping_medium` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port_of_delivery` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final_destination` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `warranty_and_claims` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D4701F84CBC7D824` (`user_id_created`),
  KEY `IDX_D4701F84A6AB49AC` (`user_id_modified`),
  KEY `IDX_D4701F84579AC939` (`trading_order_id`),
  KEY `IDX_D4701F84979B1AD6` (`company_id`),
  KEY `IDX_D4701F84E7A1254A` (`contact_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sales_confirmation`
--

INSERT INTO `sales_confirmation` (`id`, `code`, `company_id`, `contact_id`, `enquiry_id`, `trading_order_id`, `specification`, `client_order_ref`, `delivery_terms`, `payment_terms`, `expected_date_departure`, `expected_date_arrival`, `shipping_medium`, `port_of_delivery`, `final_destination`, `warranty_and_claims`, `notes`, `status`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, 'SC10001', 66, NULL, NULL, 1, 'qweqwe', NULL, NULL, NULL, '2015-05-05', '2015-05-05', NULL, NULL, NULL, NULL, NULL, 'Confirmed', NULL, NULL, 1, '2016-03-06 17:30:53', '2016-03-06 17:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `description` text,
  `sort_order` int(11) DEFAULT NULL,
  `published` int(1) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `external_link` varchar(500) DEFAULT NULL,
  `chi_title` varchar(250) DEFAULT NULL,
  `chi_description` text,
  `button_position` varchar(50) DEFAULT NULL,
  `section_type` varchar(50) DEFAULT NULL,
  `meta_title` text,
  `meta_keyword` text,
  `meta_description` text,
  `seo_title` varchar(250) DEFAULT NULL,
  `internal_link` varchar(250) DEFAULT NULL,
  `show_in_nav` tinyint(4) NOT NULL DEFAULT '1',
  `flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 PACK_KEYS=0 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `title`, `description`, `sort_order`, `published`, `creation_date`, `modification_date`, `external_link`, `chi_title`, `chi_description`, `button_position`, `section_type`, `meta_title`, `meta_keyword`, `meta_description`, `seo_title`, `internal_link`, `show_in_nav`, `flag`) VALUES
(1, 'Logistics', 'Hello there', 1, 1, '2015-06-28 21:51:05', '2015-06-28 21:51:08', NULL, 'chi title', 'chi description', 'ok', 'type1', NULL, NULL, NULL, NULL, NULL, 1, 0),
(2, 'hello section', 'this is first section', 1, 0, '2015-06-28 21:54:49', '2015-06-28 21:54:53', NULL, NULL, NULL, '1', 'type1', NULL, NULL, NULL, NULL, NULL, 1, NULL),
(3, 'hello section', 'this is first section', 1, 0, '2015-06-28 21:54:49', '2015-06-28 21:54:53', 'http://www.domain.tld/blublu', NULL, NULL, NULL, 'type1', NULL, NULL, NULL, NULL, '/blublu', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE IF NOT EXISTS `setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `key_text` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `value_type` varchar(255) NOT NULL,
  `flag` int(11) NOT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9F74B898CBC7D824` (`user_id_created`),
  KEY `IDX_9F74B898A6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id`, `description`, `key_text`, `value`, `creation_date`, `modification_date`, `group_name`, `value_type`, `flag`, `user_id_created`, `user_id_modified`) VALUES
(1, 'homepage setting', 'home', 'homepage', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', 0, NULL, NULL),
(2, 'enable (1) or disable (0) record preview', 'company.preview', '1', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE IF NOT EXISTS `sub_category` (
  `sub_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `chi_title` varchar(250) DEFAULT NULL,
  `sort_order` int(11) DEFAULT '0',
  `published` int(1) DEFAULT NULL,
  `external_link` varchar(250) DEFAULT NULL,
  `sub_category_type` varchar(50) DEFAULT NULL,
  `creation_date` datetime DEFAULT NULL,
  `modification_date` datetime DEFAULT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keyword` varchar(1000) DEFAULT NULL,
  `meta_description` varchar(1000) DEFAULT NULL,
  `internal_link` varchar(250) DEFAULT NULL,
  `show_in_nav` tinyint(4) NOT NULL DEFAULT '1',
  `flag` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`sub_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `trading_order`
--

CREATE TABLE IF NOT EXISTS `trading_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `contact_id` int(11) DEFAULT NULL,
  `order_details` varchar(600) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enquiry_id` int(11) DEFAULT NULL,
  `client_order_ref` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `status` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `etd` date DEFAULT NULL COMMENT 'Estimated Date of Departure',
  `quotation_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_417D8BB3CBC7D824` (`user_id_created`),
  KEY `IDX_417D8BB3A6AB49AC` (`user_id_modified`),
  KEY `IDX_417D8BB3979B1AD6` (`company_id`),
  KEY `IDX_417D8BB3E7A1254A` (`contact_id`),
  KEY `IDX_417D8BB3BD918DFF` (`enquiry_id`),
  KEY `IDX_417D8BB3B4EA4E60` (`quotation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `trading_order`
--

INSERT INTO `trading_order` (`id`, `code`, `company_id`, `contact_id`, `order_details`, `enquiry_id`, `client_order_ref`, `order_date`, `status`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`, `etd`, `quotation_id`) VALUES
(1, 'O10001', 66, NULL, 'adadasd', NULL, 'CAYR121221212', '2016-02-03', 'Confirmed', NULL, NULL, NULL, '2016-04-14 00:00:00', '2016-04-14 00:00:00', '2016-04-30', 1);

-- --------------------------------------------------------

--
-- Table structure for table `translation`
--

CREATE TABLE IF NOT EXISTS `translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_text` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `chi_value` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `description` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B469456FCBC7D824` (`user_id_created`),
  KEY `IDX_B469456FA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `translation`
--

INSERT INTO `translation` (`id`, `key_text`, `value`, `chi_value`, `creation_date`, `modification_date`, `group_name`, `flag`, `user_id_created`, `user_id_modified`, `description`) VALUES
(1, 'cp.companyName', 'Pilot Simple Software', 0, '2016-03-01 00:00:00', '2016-03-07 07:17:45', '', 0, 1, 1, 'This is a test record');

-- --------------------------------------------------------

--
-- Table structure for table `valuelist`
--

CREATE TABLE IF NOT EXISTS `valuelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(1000) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `is_group_heading` tinyint(1) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `valuelist_name` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_5A566B0ECBC7D824` (`user_id_created`),
  KEY `IDX_5A566B0EA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `valuelist`
--

INSERT INTO `valuelist` (`id`, `value`, `creation_date`, `modification_date`, `sort_order`, `is_group_heading`, `code`, `flag`, `user_id_created`, `user_id_modified`, `valuelist_name`) VALUES
(1, 'Category 1', '0000-00-00 00:00:00', '2016-03-09 18:45:37', 1, 0, '', 0, NULL, 1, 'approvedComponentCategory'),
(2, 'Category 2', '2016-03-09 07:03:45', '2016-03-09 18:45:28', NULL, NULL, NULL, NULL, 1, 1, 'approvedPackagingCategory'),
(3, 'Category 3', '2016-03-09 09:31:52', '2016-03-09 18:16:39', NULL, NULL, NULL, NULL, 1, 1, 'approvedComponentCategory'),
(4, 'Category 4', '2016-03-09 09:32:37', '2016-03-09 19:06:54', NULL, NULL, NULL, NULL, 1, 1, 'approvedMaterialCategory'),
(5, 'Category 5', '2016-03-09 09:55:11', '2016-03-09 19:06:00', NULL, NULL, NULL, NULL, 1, 1, 'approvedMaterialCategory'),
(6, 'Pre-Production', '2016-03-09 21:46:40', '2016-03-09 21:59:21', NULL, NULL, NULL, NULL, 1, 1, 'masterChecklistCategory'),
(7, 'Proto Type Definition', '2016-03-09 21:46:57', '2016-03-09 21:58:59', NULL, NULL, NULL, NULL, 1, 1, 'masterChecklistCategory'),
(8, 'Research', '2016-03-09 21:47:09', '2016-03-09 21:58:33', NULL, NULL, NULL, NULL, 1, 1, 'masterChecklistCategory'),
(9, 'Enquiry', '2016-03-09 21:51:38', '2016-03-09 21:58:17', NULL, NULL, NULL, NULL, 1, 1, 'masterChecklistCategory'),
(10, 'After Sales', '2016-03-09 21:59:39', '2016-03-09 21:59:39', NULL, NULL, NULL, NULL, 1, 1, 'masterChecklistCategory'),
(11, 'Approved Material Category 3', '2016-04-19 00:37:52', '2016-04-19 00:37:52', NULL, NULL, NULL, NULL, 1, 1, 'approvedMaterialCategory'),
(12, 'Bags', '2016-04-22 17:51:59', '2016-04-22 18:00:45', NULL, NULL, '01', NULL, 1, 1, 'productFamily'),
(13, 'Backpacks', '2016-04-22 17:52:35', '2016-04-22 18:00:38', NULL, NULL, '02', NULL, 1, 1, 'productFamily'),
(14, 'Tents', '2016-04-22 17:52:57', '2016-04-22 18:00:27', NULL, NULL, '03', NULL, 1, 1, 'productFamily'),
(15, 'Garments', '2016-04-22 17:53:49', '2016-04-22 18:00:16', NULL, NULL, '04', NULL, 1, 1, 'productFamily'),
(16, 'Sleeping Bags', '2016-04-22 17:54:26', '2016-04-22 18:00:08', NULL, NULL, '05', NULL, 1, 1, 'productFamily'),
(17, 'Accessories', '2016-04-22 17:54:54', '2016-04-22 17:59:57', NULL, NULL, '06', NULL, 1, 1, 'productFamily'),
(43, 'Kitchenware', '2016-04-23 12:55:27', '2016-04-23 12:55:27', NULL, NULL, '7', NULL, 1, 1, 'productFamily'),
(44, 'Luggage', '2016-04-23 12:55:48', '2016-04-23 12:55:48', NULL, NULL, '08', NULL, 1, 1, 'productFamily');

-- --------------------------------------------------------

--
-- Table structure for table `vendor_invoice`
--

CREATE TABLE IF NOT EXISTS `vendor_invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trading_order_id` int(11) DEFAULT NULL,
  `company_id_supplier` int(11) DEFAULT NULL,
  `delivery_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `payment_terms` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipping_medium` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `port_of_delivery` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `final_destination` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `required_delivery_date` date DEFAULT NULL,
  `warranty_and_claims` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notes` varchar(300) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flag` int(11) DEFAULT NULL,
  `user_id_created` int(11) DEFAULT NULL,
  `user_id_modified` int(11) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modification_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8E122F2BCBC7D824` (`user_id_created`),
  KEY `IDX_8E122F2BA6AB49AC` (`user_id_modified`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `vendor_invoice`
--

INSERT INTO `vendor_invoice` (`id`, `code`, `trading_order_id`, `company_id_supplier`, `delivery_terms`, `payment_terms`, `shipping_medium`, `port_of_delivery`, `final_destination`, `required_delivery_date`, `warranty_and_claims`, `notes`, `flag`, `user_id_created`, `user_id_modified`, `creation_date`, `modification_date`) VALUES
(1, NULL, NULL, NULL, 'asdad', NULL, NULL, NULL, NULL, '2015-01-02', NULL, NULL, NULL, 1, 1, '2016-03-06 18:47:51', '2016-03-06 18:47:51');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `accounts_payable`
--
ALTER TABLE `accounts_payable`
  ADD CONSTRAINT `FK_12741D3CA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_12741D3CCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `accounts_receivable`
--
ALTER TABLE `accounts_receivable`
  ADD CONSTRAINT `FK_4A2FD289A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_4A2FD289CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `activity`
--
ALTER TABLE `activity`
  ADD CONSTRAINT `FK_AC74095AA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_AC74095ACBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `approved_component`
--
ALTER TABLE `approved_component`
  ADD CONSTRAINT `FK_DC6A9B3437031EE` FOREIGN KEY (`parts_location_id`) REFERENCES `parts_location` (`id`),
  ADD CONSTRAINT `FK_DC6A9B3979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_DC6A9B3A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_DC6A9B3CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `approved_material`
--
ALTER TABLE `approved_material`
  ADD CONSTRAINT `FK_409BE2F9437031EE` FOREIGN KEY (`parts_location_id`) REFERENCES `parts_location` (`id`),
  ADD CONSTRAINT `FK_409BE2F9979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_409BE2F9A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_409BE2F9CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `approved_packaging`
--
ALTER TABLE `approved_packaging`
  ADD CONSTRAINT `FK_FBB30561437031EE` FOREIGN KEY (`parts_location_id`) REFERENCES `parts_location` (`id`),
  ADD CONSTRAINT `FK_FBB30561979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_FBB30561A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_FBB30561CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `branch`
--
ALTER TABLE `branch`
  ADD CONSTRAINT `FK_BB861B1F979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`);

--
-- Constraints for table `claim`
--
ALTER TABLE `claim`
  ADD CONSTRAINT `FK_A769DE27579AC939` FOREIGN KEY (`trading_order_id`) REFERENCES `trading_order` (`id`),
  ADD CONSTRAINT `FK_A769DE27979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_A769DE27A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_A769DE27CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `color_master`
--
ALTER TABLE `color_master`
  ADD CONSTRAINT `FK_817BD24A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_817BD24CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526CA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_9474526CCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_9474526CE7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`);

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `FK_4FBF094FA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_4FBF094FCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `contact`
--
ALTER TABLE `contact`
  ADD CONSTRAINT `FK_4C62E638979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_4C62E638A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_4C62E638CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `credit_debit_note`
--
ALTER TABLE `credit_debit_note`
  ADD CONSTRAINT `FK_96E32F1C579AC939` FOREIGN KEY (`trading_order_id`) REFERENCES `trading_order` (`id`),
  ADD CONSTRAINT `FK_96E32F1C979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_96E32F1CA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_96E32F1CCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `enquiry`
--
ALTER TABLE `enquiry`
  ADD CONSTRAINT `FK_9D996984979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_9D996984A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_9D996984CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_9D996984E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`);

--
-- Constraints for table `gantt_row`
--
ALTER TABLE `gantt_row`
  ADD CONSTRAINT `FK_6D3D56BBA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_6D3D56BBCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `gantt_task`
--
ALTER TABLE `gantt_task`
  ADD CONSTRAINT `FK_1F25B7DD1BC3626F` FOREIGN KEY (`gantt_row_id`) REFERENCES `gantt_row` (`id`),
  ADD CONSTRAINT `FK_1F25B7DDA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_1F25B7DDCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `interest_group`
--
ALTER TABLE `interest_group`
  ADD CONSTRAINT `FK_8960A54EA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_8960A54ECBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `FK_90651744579AC939` FOREIGN KEY (`trading_order_id`) REFERENCES `trading_order` (`id`),
  ADD CONSTRAINT `FK_90651744A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_90651744CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `manufacturing_order`
--
ALTER TABLE `manufacturing_order`
  ADD CONSTRAINT `FK_34010DB1579AC939` FOREIGN KEY (`trading_order_id`) REFERENCES `trading_order` (`id`),
  ADD CONSTRAINT `FK_34010DB1A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_34010DB1CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_34010DB1D2203873` FOREIGN KEY (`company_id_supplier`) REFERENCES `company` (`id`);

--
-- Constraints for table `master_checklist`
--
ALTER TABLE `master_checklist`
  ADD CONSTRAINT `FK_2FF6CE62A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_2FF6CE62CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `master_compliance`
--
ALTER TABLE `master_compliance`
  ADD CONSTRAINT `FK_ACD75F5DA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_ACD75F5DCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `master_performance`
--
ALTER TABLE `master_performance`
  ADD CONSTRAINT `FK_88DF2604A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_88DF2604CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `ma_role`
--
ALTER TABLE `ma_role`
  ADD CONSTRAINT `FK_78CB23C8A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_78CB23C8CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `ma_user_role`
--
ALTER TABLE `ma_user_role`
  ADD CONSTRAINT `FK_48CF61B4D60322AC` FOREIGN KEY (`role_id`) REFERENCES `ma_role` (`id`),
  ADD CONSTRAINT `FK_DF8CEDB9A76ED395` FOREIGN KEY (`user_id`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `media_history`
--
ALTER TABLE `media_history`
  ADD CONSTRAINT `FK_466DF60216839E83` FOREIGN KEY (`proto_type_id`) REFERENCES `proto_type` (`id`),
  ADD CONSTRAINT `FK_466DF6021D22091A` FOREIGN KEY (`sales_confirmation_id`) REFERENCES `sales_confirmation` (`id`),
  ADD CONSTRAINT `FK_466DF60227B17E93` FOREIGN KEY (`master_compliance_id`) REFERENCES `master_compliance` (`id`),
  ADD CONSTRAINT `FK_466DF6022989F1FD` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `FK_466DF6022CB577B8` FOREIGN KEY (`master_checklist_id`) REFERENCES `master_checklist` (`id`),
  ADD CONSTRAINT `FK_466DF602437031EE` FOREIGN KEY (`parts_location_id`) REFERENCES `parts_location` (`id`),
  ADD CONSTRAINT `FK_466DF6024B72AF` FOREIGN KEY (`approved_packaging_id`) REFERENCES `approved_packaging` (`id`),
  ADD CONSTRAINT `FK_466DF602579AC939` FOREIGN KEY (`trading_order_id`) REFERENCES `trading_order` (`id`),
  ADD CONSTRAINT `FK_466DF6025862141D` FOREIGN KEY (`credit_debit_note_id`) REFERENCES `credit_debit_note` (`id`),
  ADD CONSTRAINT `FK_466DF60260741FF8` FOREIGN KEY (`manufacturing_order_id`) REFERENCES `manufacturing_order` (`id`),
  ADD CONSTRAINT `FK_466DF6027909E1ED` FOREIGN KEY (`research_id`) REFERENCES `research` (`id`),
  ADD CONSTRAINT `FK_466DF60282874C87` FOREIGN KEY (`interest_group_id`) REFERENCES `interest_group` (`id`),
  ADD CONSTRAINT `FK_466DF6028BBD7E36` FOREIGN KEY (`color_master_id`) REFERENCES `color_master` (`id`),
  ADD CONSTRAINT `FK_466DF602907A7FB1` FOREIGN KEY (`accounts_receivable_id`) REFERENCES `accounts_receivable` (`id`),
  ADD CONSTRAINT `FK_466DF602979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_466DF602A45D7E6A` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`),
  ADD CONSTRAINT `FK_466DF602A76ED395` FOREIGN KEY (`user_id`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_466DF602AC9BE551` FOREIGN KEY (`approved_component_id`) REFERENCES `approved_component` (`id`),
  ADD CONSTRAINT `FK_466DF602B3FC259A` FOREIGN KEY (`vendor_invoice_id`) REFERENCES `vendor_invoice` (`id`),
  ADD CONSTRAINT `FK_466DF602B4EA4E60` FOREIGN KEY (`quotation_id`) REFERENCES `quotation` (`id`),
  ADD CONSTRAINT `FK_466DF602B4FA2105` FOREIGN KEY (`master_performance_id`) REFERENCES `master_performance` (`id`),
  ADD CONSTRAINT `FK_466DF602BD918DFF` FOREIGN KEY (`enquiry_id`) REFERENCES `enquiry` (`id`),
  ADD CONSTRAINT `FK_466DF602D7C1568D` FOREIGN KEY (`approved_material_id`) REFERENCES `approved_material` (`id`),
  ADD CONSTRAINT `FK_466DF602E33400EF` FOREIGN KEY (`accounts_payable_id`) REFERENCES `accounts_payable` (`id`),
  ADD CONSTRAINT `FK_466DF602E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`),
  ADD CONSTRAINT `FK_466DF602EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`);

--
-- Constraints for table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK_B6BD307F16839E83` FOREIGN KEY (`proto_type_id`) REFERENCES `proto_type` (`id`),
  ADD CONSTRAINT `FK_B6BD307F1D22091A` FOREIGN KEY (`sales_confirmation_id`) REFERENCES `sales_confirmation` (`id`),
  ADD CONSTRAINT `FK_B6BD307F27B17E93` FOREIGN KEY (`master_compliance_id`) REFERENCES `master_compliance` (`id`),
  ADD CONSTRAINT `FK_B6BD307F2989F1FD` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `FK_B6BD307F2CB577B8` FOREIGN KEY (`master_checklist_id`) REFERENCES `master_checklist` (`id`),
  ADD CONSTRAINT `FK_B6BD307F437031EE` FOREIGN KEY (`parts_location_id`) REFERENCES `parts_location` (`id`),
  ADD CONSTRAINT `FK_B6BD307F4B72AF` FOREIGN KEY (`approved_packaging_id`) REFERENCES `approved_packaging` (`id`),
  ADD CONSTRAINT `FK_B6BD307F579AC939` FOREIGN KEY (`trading_order_id`) REFERENCES `trading_order` (`id`),
  ADD CONSTRAINT `FK_B6BD307F5862141D` FOREIGN KEY (`credit_debit_note_id`) REFERENCES `credit_debit_note` (`id`),
  ADD CONSTRAINT `FK_B6BD307F60741FF8` FOREIGN KEY (`manufacturing_order_id`) REFERENCES `manufacturing_order` (`id`),
  ADD CONSTRAINT `FK_B6BD307F7909E1ED` FOREIGN KEY (`research_id`) REFERENCES `research` (`id`),
  ADD CONSTRAINT `FK_B6BD307F82874C87` FOREIGN KEY (`interest_group_id`) REFERENCES `interest_group` (`id`),
  ADD CONSTRAINT `FK_B6BD307F8BBD7E36` FOREIGN KEY (`color_master_id`) REFERENCES `color_master` (`id`),
  ADD CONSTRAINT `FK_B6BD307F907A7FB1` FOREIGN KEY (`accounts_receivable_id`) REFERENCES `accounts_receivable` (`id`),
  ADD CONSTRAINT `FK_B6BD307F979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_B6BD307FA45D7E6A` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`),
  ADD CONSTRAINT `FK_B6BD307FA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_B6BD307FAC9BE551` FOREIGN KEY (`approved_component_id`) REFERENCES `approved_component` (`id`),
  ADD CONSTRAINT `FK_B6BD307FB3FC259A` FOREIGN KEY (`vendor_invoice_id`) REFERENCES `vendor_invoice` (`id`),
  ADD CONSTRAINT `FK_B6BD307FB4EA4E60` FOREIGN KEY (`quotation_id`) REFERENCES `quotation` (`id`),
  ADD CONSTRAINT `FK_B6BD307FB4FA2105` FOREIGN KEY (`master_performance_id`) REFERENCES `master_performance` (`id`),
  ADD CONSTRAINT `FK_B6BD307FBD918DFF` FOREIGN KEY (`enquiry_id`) REFERENCES `enquiry` (`id`),
  ADD CONSTRAINT `FK_B6BD307FCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_B6BD307FD7C1568D` FOREIGN KEY (`approved_material_id`) REFERENCES `approved_material` (`id`),
  ADD CONSTRAINT `FK_B6BD307FE33400EF` FOREIGN KEY (`accounts_payable_id`) REFERENCES `accounts_payable` (`id`),
  ADD CONSTRAINT `FK_B6BD307FE7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`);

--
-- Constraints for table `message_reply`
--
ALTER TABLE `message_reply`
  ADD CONSTRAINT `FK_20E390B537A1329` FOREIGN KEY (`message_id`) REFERENCES `message` (`id`),
  ADD CONSTRAINT `FK_20E390BA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_20E390BCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `FK_CFBDFA1416839E83` FOREIGN KEY (`proto_type_id`) REFERENCES `proto_type` (`id`),
  ADD CONSTRAINT `FK_CFBDFA141D22091A` FOREIGN KEY (`sales_confirmation_id`) REFERENCES `sales_confirmation` (`id`),
  ADD CONSTRAINT `FK_CFBDFA1427B17E93` FOREIGN KEY (`master_compliance_id`) REFERENCES `master_compliance` (`id`),
  ADD CONSTRAINT `FK_CFBDFA142989F1FD` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `FK_CFBDFA142CB577B8` FOREIGN KEY (`master_checklist_id`) REFERENCES `master_checklist` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14437031EE` FOREIGN KEY (`parts_location_id`) REFERENCES `parts_location` (`id`),
  ADD CONSTRAINT `FK_CFBDFA144B72AF` FOREIGN KEY (`approved_packaging_id`) REFERENCES `approved_packaging` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14579AC939` FOREIGN KEY (`trading_order_id`) REFERENCES `trading_order` (`id`),
  ADD CONSTRAINT `FK_CFBDFA145862141D` FOREIGN KEY (`credit_debit_note_id`) REFERENCES `credit_debit_note` (`id`),
  ADD CONSTRAINT `FK_CFBDFA1460741FF8` FOREIGN KEY (`manufacturing_order_id`) REFERENCES `manufacturing_order` (`id`),
  ADD CONSTRAINT `FK_CFBDFA147909E1ED` FOREIGN KEY (`research_id`) REFERENCES `research` (`id`),
  ADD CONSTRAINT `FK_CFBDFA1482874C87` FOREIGN KEY (`interest_group_id`) REFERENCES `interest_group` (`id`),
  ADD CONSTRAINT `FK_CFBDFA148BBD7E36` FOREIGN KEY (`color_master_id`) REFERENCES `color_master` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14907A7FB1` FOREIGN KEY (`accounts_receivable_id`) REFERENCES `accounts_receivable` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14A45D7E6A` FOREIGN KEY (`purchase_order_id`) REFERENCES `purchase_order` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14AC9BE551` FOREIGN KEY (`approved_component_id`) REFERENCES `approved_component` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14B3FC259A` FOREIGN KEY (`vendor_invoice_id`) REFERENCES `vendor_invoice` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14B4EA4E60` FOREIGN KEY (`quotation_id`) REFERENCES `quotation` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14B4FA2105` FOREIGN KEY (`master_performance_id`) REFERENCES `master_performance` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14BD918DFF` FOREIGN KEY (`enquiry_id`) REFERENCES `enquiry` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14D7C1568D` FOREIGN KEY (`approved_material_id`) REFERENCES `approved_material` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14E33400EF` FOREIGN KEY (`accounts_payable_id`) REFERENCES `accounts_payable` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`);

--
-- Constraints for table `parts_location`
--
ALTER TABLE `parts_location`
  ADD CONSTRAINT `FK_E51211DDA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_E51211DDCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `proto_type`
--
ALTER TABLE `proto_type`
  ADD CONSTRAINT `FK_34012D32A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_34012D32BD918DFF` FOREIGN KEY (`enquiry_id`) REFERENCES `enquiry` (`id`),
  ADD CONSTRAINT `FK_34012D32CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `proto_type_color_master`
--
ALTER TABLE `proto_type_color_master`
  ADD CONSTRAINT `FK_1361A07D16839E83` FOREIGN KEY (`proto_type_id`) REFERENCES `proto_type` (`id`),
  ADD CONSTRAINT `FK_1361A07D8BBD7E36` FOREIGN KEY (`color_master_id`) REFERENCES `color_master` (`id`),
  ADD CONSTRAINT `FK_1361A07DA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_1361A07DCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD CONSTRAINT `FK_21E210B2579AC939` FOREIGN KEY (`trading_order_id`) REFERENCES `trading_order` (`id`),
  ADD CONSTRAINT `FK_21E210B2A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_21E210B2CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_21E210B2D2203873` FOREIGN KEY (`company_id_supplier`) REFERENCES `company` (`id`);

--
-- Constraints for table `quotation`
--
ALTER TABLE `quotation`
  ADD CONSTRAINT `FK_474A8DB9979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_474A8DB9A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_474A8DB9BD918DFF` FOREIGN KEY (`enquiry_id`) REFERENCES `enquiry` (`id`),
  ADD CONSTRAINT `FK_474A8DB9CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `research`
--
ALTER TABLE `research`
  ADD CONSTRAINT `FK_57EB50C216839E83` FOREIGN KEY (`proto_type_id`) REFERENCES `proto_type` (`id`),
  ADD CONSTRAINT `FK_57EB50C2979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_57EB50C2A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_57EB50C2BD918DFF` FOREIGN KEY (`enquiry_id`) REFERENCES `enquiry` (`id`),
  ADD CONSTRAINT `FK_57EB50C2CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `sales_confirmation`
--
ALTER TABLE `sales_confirmation`
  ADD CONSTRAINT `FK_D4701F84579AC939` FOREIGN KEY (`trading_order_id`) REFERENCES `trading_order` (`id`),
  ADD CONSTRAINT `FK_D4701F84979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_D4701F84A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_D4701F84CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_D4701F84E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`);

--
-- Constraints for table `setting`
--
ALTER TABLE `setting`
  ADD CONSTRAINT `FK_9F74B898A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_9F74B898CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `trading_order`
--
ALTER TABLE `trading_order`
  ADD CONSTRAINT `FK_417D8BB3979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`),
  ADD CONSTRAINT `FK_417D8BB3A6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_417D8BB3B4EA4E60` FOREIGN KEY (`quotation_id`) REFERENCES `quotation` (`id`),
  ADD CONSTRAINT `FK_417D8BB3BD918DFF` FOREIGN KEY (`enquiry_id`) REFERENCES `enquiry` (`id`),
  ADD CONSTRAINT `FK_417D8BB3CBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_417D8BB3E7A1254A` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`);

--
-- Constraints for table `translation`
--
ALTER TABLE `translation`
  ADD CONSTRAINT `FK_B469456FA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_B469456FCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `valuelist`
--
ALTER TABLE `valuelist`
  ADD CONSTRAINT `FK_5A566B0EA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_5A566B0ECBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

--
-- Constraints for table `vendor_invoice`
--
ALTER TABLE `vendor_invoice`
  ADD CONSTRAINT `FK_8E122F2BA6AB49AC` FOREIGN KEY (`user_id_modified`) REFERENCES `ma_user` (`id`),
  ADD CONSTRAINT `FK_8E122F2BCBC7D824` FOREIGN KEY (`user_id_created`) REFERENCES `ma_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
