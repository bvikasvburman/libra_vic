//variables
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var wrap = require('gulp-wrap');
var notify = require('gulp-notify');
var sass = require('gulp-sass');


var input = [
    // './web/css/vendors/*.css'
    "./web/js/vendors/angular-material/angular-material.min.css",
    "./web/js/vendors/bootstrap/dist/css/bootstrap.min.css",
    "./web/js/vendors/bootstrap/dist/css/bootstrap-theme.min.css",
    "./web/js/vendors/font-awesome/css/font-awesome.min.css",
    "./web/js/vendors/angular-loading-bar/src/loading-bar.css",
    "./web/js/vendors/angular-gantt/dist/angular-gantt.css",
    "./web/js/vendors/angular-gantt/dist/angular-gantt-plugins.css",
    "./web/js/vendors/angular-hotkeys/build/hotkeys.css",
    "./web/js/vendors/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.css",
    "./web/js/vendors/angular-material-data-table/dist/md-data-table.min.css"
];
var output = './web/css';

gulp.task('css-vendors', function () {
    gulp.src(input)
    // .pipe(sourcemaps.init())
    .pipe(wrap('\n/* <%= file.path %> */\n<%= contents %>'))
    .pipe(concat('vendors.css'))
    .pipe(uglify())
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(output));
});

