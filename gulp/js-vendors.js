//variables
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var wrap = require('gulp-wrap');

gulp.task('js-vendors', function () {
    gulp.src([
    // "web/js/vendors/requirejs/require.js",
    "web/js/vendors/jquery/dist/jquery.js",
    "web/js/vendors/underscore/underscore.js",
    "web/js/vendors/underscore.string/dist/underscore.string.js",
    "web/js/vendors/bootstrap/dist/js/bootstrap.js",
    "web/js/vendors/easypie/dist/easypiechart.js",
    "web/js/vendors/easypie/dist/jquery.easypiechart.js",
    "web/js/vendors/angular/angular.js",
    "web/js/vendors/moment/moment.js",
    "web/js/vendors/moment-range/dist/moment-range.js",
    "web/js/vendors/angular-aria/angular-aria.js",
    "web/js/vendors/angular-animate/angular-animate.js",
    "web/js/vendors/angular-messages/angular-messages.js",
    "web/js/vendors/angular-material/angular-material.js",
    "web/js/vendors/angular-bootstrap/ui-bootstrap-tpls.js",
    "web/js/vendors/angular-file-upload/dist/angular-file-upload.min.js",
    "web/js/vendors/angular-mocks/angular-mocks.js",
    "web/js/vendors/angular-sanitize/angular-sanitize.js",
    "web/js/vendors/angular/angular-route.js",
    "web/js/vendors/angular-ui-router/release/angular-ui-router.js",
    "web/js/vendors/angular-ui-tree/dist/angular-ui-tree.js",
    "web/js/vendors/angular-hotkeys/build/hotkeys.js",
    "web/js/vendors/angular-moment/angular-moment.js",
    "web/js/vendors/easypie/dist/angular.easypiechart.js",
    "web/js/vendors/slimScroll/jquery.slimscroll.min.js",
    "web/js/vendors/angularUtils-pagination/dirPagination.js",
    "web/js/vendors/dragular/dist/dragular.js",
    "web/js/vendors/angular-loading-bar/src/loading-bar.js",
    "web/js/vendors/angular-bind-html-compile/angular-bind-html-compile.js",

    // "web/js/vendors/element-queries/src/detect-resize.js",
    // "web/js/vendors/element-queries/src/element-queries.js",
    // "web/js/vendors/element-queries/dist/element-queries.min.js",
    "web/js/vendors/angular-native-dragdrop/draganddrop.js",
    "web/js/vendors/angular-gantt/dist/angular-gantt.js",
    "web/js/vendors/angular-gantt/dist/angular-gantt-plugins.js",
    "web/js/vendors/jsPlumb/dist/js/jsPlumb-2.0.7.js",

    "web/js/vendors/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.js",
    "web/js/vendors/angular-material-data-table/dist/md-data-table.min.js"
    ])
    // .pipe(sourcemaps.init())
    .pipe(wrap('\n//<%= file.path %>\n<%= contents %>'))
    .pipe(concat('vendors.js'))
    .pipe(ngAnnotate())
    .pipe(uglify())
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest('web/js'));
});