var gulp = require('gulp');
var bower = require('gulp-bower');
 
gulp.task('bower', function() {
  return bower({cwd: 'web/js', cmd: 'update'})
    .pipe(gulp.dest('lib/'));
});
