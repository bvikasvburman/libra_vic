//variables
var gulp            = require('gulp');
var concat          = require('gulp-concat');
var uglify          = require('gulp-uglify');
var sourcemaps      = require('gulp-sourcemaps');
var ngAnnotate      = require('gulp-ng-annotate');
var wrap            = require('gulp-wrap');
var notify          = require('gulp-notify');
var angularFilesort = require('gulp-angular-filesort');
var inject          = require('gulp-inject');
var livereload      = require('gulp-livereload');
var gutil = require('gulp-util');

gulp.task('js', function() {
    gulp.src([
        'web/js/app/configs/*.js',
        'web/js/app/configs/**/*.js',
        'web/js/app/bundles/**/config/routing.js',
        // 'web/js/app/bundles/**/bundle.js',
        'web/js/app/componentsp/persistenceJar/**/*.js',
        'web/js/app/componentsp/persistenceJar/**/**/*.js',
        'web/js/app/app.js',
        'web/js/app/components/**/**/*.js',
        'web/js/app/bundles/**/**/*.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(wrap('\n//<%= file.path %>\n<%= contents %>'))
    // .pipe(angularFilesort())
    .pipe(concat('app.js'))
    .pipe(
        ngAnnotate().on("error",
            notify.onError({
                message: 'Error: <%= error.line %>: <%= error.message %>'
            })
        )
    )
    // .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('web/js'));
});

