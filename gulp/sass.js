var gulp       = require('gulp');
var concat     = require('gulp-concat');
var uglify     = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var ngAnnotate = require('gulp-ng-annotate');
var wrap       = require('gulp-wrap');
var notify     = require('gulp-notify');
var sass       = require('gulp-sass');
var livereload = require('gulp-livereload');

var input = [
    // './web/css/vendors/ioadmin-theme/styles.scss',
    './web/css/bundles/app/styles.scss'
];
var output = './web/css';

gulp.task('sass', function() {
    gulp.src(input)
    .pipe(sourcemaps.init())
    // .pipe(sass.sync().on('error', sass.logError))
    .pipe(sass().on('error', sass.logError))
    .pipe(wrap('\n/* <%= file.path %> */\n<%= contents %>'))
    // .pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(concat('app.css'))
    // .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(output))
    .pipe(livereload());
});

