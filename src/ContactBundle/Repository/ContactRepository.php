<?php
namespace ContactBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ContactRepository extends AbstractRepository
{
    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('c')
              ->select('c, com, uc, um')
              ->leftjoin('c.company', 'com')
              ->leftjoin('c.userCreated', 'uc')
              ->leftjoin('c.userModified', 'um');
        if ($id != null) {
            $qb = $dbUtil->setWhere($qb, 'c.id', $id);
        }

        if ($sVars) {
            $qb = $dbUtil->setWhere($qb, 'c.companyId', $sVars->criteria);
        }

        return $qb;
    }

    public function getQBLinkNote($id, $idl, $sVars, $linkInfo, $qb) {
        $expr = $qb->expr()->eq('n.module', $qb->expr()->literal('company'));

        $qb->select('n')
            ->from('NoteBundle:Note', 'n')
            ->join("n.contact", 'c');
        $qb = $this->dbUtil->setWhere($qb, 'n.contactId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'n.id', $idl);
        return $qb;
    }


    public function getQBLinkMessage($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('m, c')
            ->from('MessageBundle:Message', 'm')
            ->leftjoin('m.contact', 'c');
        $qb = $this->dbUtil->setWhere($qb, 'm.contactId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'm.id', $idl);
        return $qb;
    }
}
