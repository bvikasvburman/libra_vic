<?php

namespace ContactBundle\Entity;


class Contact
{
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'CO';
    }

    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $fax;

    /**
     * @var string
     */
    private $email;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var string
     */
    private $position;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $country;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $ganttStartDate;

    /**
     * @var \DateTime
     */
    private $ganttEndDate;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $enquiryM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mediaHistory;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->enquiryM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mediaHistory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Contact
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return Contact
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return Contact
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Contact
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Contact
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Contact
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return Contact
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Contact
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Contact
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Contact
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Contact
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Contact
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Contact
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return Contact
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return Contact
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return Contact
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set ganttStartDate
     *
     * @param \DateTime $ganttStartDate
     * @return Contact
     */
    public function setGanttStartDate($ganttStartDate)
    {
        $this->ganttStartDate = $ganttStartDate;

        return $this;
    }

    /**
     * Get ganttStartDate
     *
     * @return \DateTime
     */
    public function getGanttStartDate()
    {
        return $this->ganttStartDate;
    }

    /**
     * Set ganttEndDate
     *
     * @param \DateTime $ganttEndDate
     * @return Contact
     */
    public function setGanttEndDate($ganttEndDate)
    {
        $this->ganttEndDate = $ganttEndDate;

        return $this;
    }

    /**
     * Get ganttEndDate
     *
     * @return \DateTime
     */
    public function getGanttEndDate()
    {
        return $this->ganttEndDate;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Contact
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return Contact
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add enquiryM
     *
     * @param \EnquiryBundle\Entity\Enquiry $enquiryM
     * @return Contact
     */
    public function addEnquiryM(\EnquiryBundle\Entity\Enquiry $enquiryM)
    {
        $this->enquiryM[] = $enquiryM;

        return $this;
    }

    /**
     * Remove enquiryM
     *
     * @param \EnquiryBundle\Entity\Enquiry $enquiryM
     */
    public function removeEnquiryM(\EnquiryBundle\Entity\Enquiry $enquiryM)
    {
        $this->enquiryM->removeElement($enquiryM);
    }

    /**
     * Get enquiryM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnquiryM()
    {
        return $this->enquiryM;
    }

    /**
     * Add mediaHistory
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaHistory
     * @return Contact
     */
    public function addMediaHistory(\MediaBundle\Entity\MediaHistory $mediaHistory)
    {
        $this->mediaHistory[] = $mediaHistory;

        return $this;
    }

    /**
     * Remove mediaHistory
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaHistory
     */
    public function removeMediaHistory(\MediaBundle\Entity\MediaHistory $mediaHistory)
    {
        $this->mediaHistory->removeElement($mediaHistory);
    }

    /**
     * Get mediaHistory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMediaHistory()
    {
        return $this->mediaHistory;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return Contact
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return Contact
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return Contact
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;


    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return Contact
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return Contact
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }
}
