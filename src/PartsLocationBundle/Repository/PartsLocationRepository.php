<?php
namespace PartsLocationBundle\Repository;

use AppBundle\Component\AbstractRepository;

class PartsLocationRepository extends AbstractRepository
{
    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('pl')
              ->select('pl, uc, um')
              ->leftjoin('pl.userCreated', 'uc')
              ->leftjoin('pl.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'pl.id', $id);
        }

        return $qb;
    }
}