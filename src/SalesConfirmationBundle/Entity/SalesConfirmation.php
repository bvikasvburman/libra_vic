<?php

namespace SalesConfirmationBundle\Entity;

class SalesConfirmation
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'SC';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $contactId;

    /**
     * @var integer
     */
    private $enquiryId;

    /**
     * @var integer
     */
    private $tradingOrderId;

    /**
     * @var string
     */
    private $specification;

    /**
     * @var string
     */
    private $clientOrderRef;

    /**
     * @var string
     */
    private $deliveryTerms;

    /**
     * @var string
     */
    private $paymentTerms;

    /**
     * @var \DateTime
     */
    private $expectedDateDeparture;

    /**
     * @var \DateTime
     */
    private $expectedDateArrival;

    /**
     * @var string
     */
    private $shippingMedium;

    /**
     * @var string
     */
    private $portOfDelivery;

    /**
     * @var string
     */
    private $finalDestination;

    /**
     * @var string
     */
    private $warrantyAndClaims;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var string
     */
    private $status;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \ContactBundle\Entity\Contact
     */
    private $contact;

    /**
     * @var \TradingOrderBundle\Entity\TradingOrder
     */
    private $tradingOrder;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return SalesConfirmation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return SalesConfirmation
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set contactId
     *
     * @param integer $contactId
     * @return SalesConfirmation
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;

        return $this;
    }

    /**
     * Get contactId
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set enquiryId
     *
     * @param integer $enquiryId
     * @return SalesConfirmation
     */
    public function setEnquiryId($enquiryId)
    {
        $this->enquiryId = $enquiryId;

        return $this;
    }

    /**
     * Get enquiryId
     *
     * @return integer
     */
    public function getEnquiryId()
    {
        return $this->enquiryId;
    }

    /**
     * Set tradingOrderId
     *
     * @param integer $tradingOrderId
     * @return SalesConfirmation
     */
    public function setTradingOrderId($tradingOrderId)
    {
        $this->tradingOrderId = $tradingOrderId;

        return $this;
    }

    /**
     * Get tradingOrderId
     *
     * @return integer
     */
    public function getTradingOrderId()
    {
        return $this->tradingOrderId;
    }

    /**
     * Set specification
     *
     * @param string $specification
     * @return SalesConfirmation
     */
    public function setSpecification($specification)
    {
        $this->specification = $specification;

        return $this;
    }

    /**
     * Get specification
     *
     * @return string
     */
    public function getSpecification()
    {
        return $this->specification;
    }

    /**
     * Set clientOrderRef
     *
     * @param string $clientOrderRef
     * @return SalesConfirmation
     */
    public function setClientOrderRef($clientOrderRef)
    {
        $this->clientOrderRef = $clientOrderRef;

        return $this;
    }

    /**
     * Get clientOrderRef
     *
     * @return string
     */
    public function getClientOrderRef()
    {
        return $this->clientOrderRef;
    }

    /**
     * Set deliveryTerms
     *
     * @param string $deliveryTerms
     * @return SalesConfirmation
     */
    public function setDeliveryTerms($deliveryTerms)
    {
        $this->deliveryTerms = $deliveryTerms;

        return $this;
    }

    /**
     * Get deliveryTerms
     *
     * @return string
     */
    public function getDeliveryTerms()
    {
        return $this->deliveryTerms;
    }

    /**
     * Set paymentTerms
     *
     * @param string $paymentTerms
     * @return SalesConfirmation
     */
    public function setPaymentTerms($paymentTerms)
    {
        $this->paymentTerms = $paymentTerms;

        return $this;
    }

    /**
     * Get paymentTerms
     *
     * @return string
     */
    public function getPaymentTerms()
    {
        return $this->paymentTerms;
    }

    /**
     * Set expectedDateDeparture
     *
     * @param \DateTime $expectedDateDeparture
     * @return SalesConfirmation
     */
    public function setExpectedDateDeparture($expectedDateDeparture)
    {
        $this->expectedDateDeparture = $expectedDateDeparture;

        return $this;
    }

    /**
     * Get expectedDateDeparture
     *
     * @return \DateTime
     */
    public function getExpectedDateDeparture()
    {
        return $this->expectedDateDeparture;
    }

    /**
     * Set expectedDateArrival
     *
     * @param \DateTime $expectedDateArrival
     * @return SalesConfirmation
     */
    public function setExpectedDateArrival($expectedDateArrival)
    {
        $this->expectedDateArrival = $expectedDateArrival;

        return $this;
    }

    /**
     * Get expectedDateArrival
     *
     * @return \DateTime
     */
    public function getExpectedDateArrival()
    {
        return $this->expectedDateArrival;
    }

    /**
     * Set shippingMedium
     *
     * @param string $shippingMedium
     * @return SalesConfirmation
     */
    public function setShippingMedium($shippingMedium)
    {
        $this->shippingMedium = $shippingMedium;

        return $this;
    }

    /**
     * Get shippingMedium
     *
     * @return string
     */
    public function getShippingMedium()
    {
        return $this->shippingMedium;
    }

    /**
     * Set portOfDelivery
     *
     * @param string $portOfDelivery
     * @return SalesConfirmation
     */
    public function setPortOfDelivery($portOfDelivery)
    {
        $this->portOfDelivery = $portOfDelivery;

        return $this;
    }

    /**
     * Get portOfDelivery
     *
     * @return string
     */
    public function getPortOfDelivery()
    {
        return $this->portOfDelivery;
    }

    /**
     * Set finalDestination
     *
     * @param string $finalDestination
     * @return SalesConfirmation
     */
    public function setFinalDestination($finalDestination)
    {
        $this->finalDestination = $finalDestination;

        return $this;
    }

    /**
     * Get finalDestination
     *
     * @return string
     */
    public function getFinalDestination()
    {
        return $this->finalDestination;
    }

    /**
     * Set warrantyAndClaims
     *
     * @param string $warrantyAndClaims
     * @return SalesConfirmation
     */
    public function setWarrantyAndClaims($warrantyAndClaims)
    {
        $this->warrantyAndClaims = $warrantyAndClaims;

        return $this;
    }

    /**
     * Get warrantyAndClaims
     *
     * @return string
     */
    public function getWarrantyAndClaims()
    {
        return $this->warrantyAndClaims;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return SalesConfirmation
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return SalesConfirmation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return SalesConfirmation
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return SalesConfirmation
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return SalesConfirmation
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return SalesConfirmation
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return SalesConfirmation
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return SalesConfirmation
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return SalesConfirmation
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return SalesConfirmation
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contact
     *
     * @param \ContactBundle\Entity\Contact $contact
     * @return SalesConfirmation
     */
    public function setContact(\ContactBundle\Entity\Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \ContactBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set tradingOrder
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrder
     * @return SalesConfirmation
     */
    public function setTradingOrder(\TradingOrderBundle\Entity\TradingOrder $tradingOrder = null)
    {
        $this->tradingOrder = $tradingOrder;

        return $this;
    }

    /**
     * Get tradingOrder
     *
     * @return \TradingOrderBundle\Entity\TradingOrder
     */
    public function getTradingOrder()
    {
        return $this->tradingOrder;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return SalesConfirmation
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return SalesConfirmation
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
