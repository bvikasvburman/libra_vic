<?php
namespace SalesConfirmationBundle\Repository;

use AppBundle\Component\AbstractRepository;

class SalesConfirmationRepository extends AbstractRepository
{
    public function getAlias() {
        return 'sc';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('sc')
              ->select('sc, uc, um')
              ->leftjoin('sc.userCreated', 'uc')
              ->leftjoin('sc.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'sc.id', $id);
        }
        return $qb;
    }
}
