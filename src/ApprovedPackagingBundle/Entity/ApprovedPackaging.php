<?php

namespace ApprovedPackagingBundle\Entity;

class ApprovedPackaging
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getBarcode()
    {
        return 'AP' . $this->id;
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $packagingMaterial;

    /**
     * @var string
     */
    private $category;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $approved;

    /**
     * @var integer
     */
    private $partsLocationId;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \PartsLocationBundle\Entity\PartsLocation
     */
    private $partsLocation;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set packagingMaterial
     *
     * @param string $packagingMaterial
     * @return ApprovedPackaging
     */
    public function setPackagingMaterial($packagingMaterial)
    {
        $this->packagingMaterial = $packagingMaterial;

        return $this;
    }

    /**
     * Get packagingMaterial
     *
     * @return string 
     */
    public function getPackagingMaterial()
    {
        return $this->packagingMaterial;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return ApprovedPackaging
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return ApprovedPackaging
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set approved
     *
     * @param integer $approved
     * @return ApprovedPackaging
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return integer 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set partsLocationId
     *
     * @param integer $partsLocationId
     * @return ApprovedPackaging
     */
    public function setPartsLocationId($partsLocationId)
    {
        $this->partsLocationId = $partsLocationId;

        return $this;
    }

    /**
     * Get partsLocationId
     *
     * @return integer 
     */
    public function getPartsLocationId()
    {
        return $this->partsLocationId;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return ApprovedPackaging
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer 
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return ApprovedPackaging
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer 
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return ApprovedPackaging
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return ApprovedPackaging
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return ApprovedPackaging
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return ApprovedPackaging
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return ApprovedPackaging
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set partsLocation
     *
     * @param \PartsLocationBundle\Entity\PartsLocation $partsLocation
     * @return ApprovedPackaging
     */
    public function setPartsLocation(\PartsLocationBundle\Entity\PartsLocation $partsLocation = null)
    {
        $this->partsLocation = $partsLocation;

        return $this;
    }

    /**
     * Get partsLocation
     *
     * @return \PartsLocationBundle\Entity\PartsLocation 
     */
    public function getPartsLocation()
    {
        return $this->partsLocation;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return ApprovedPackaging
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return ApprovedPackaging
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
