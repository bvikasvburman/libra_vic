<?php
namespace AccountsPayableBundle\Repository;

use AppBundle\Component\AbstractRepository;

class AccountsPayableRepository extends AbstractRepository
{
    public function getAlias() {
        return 'ap';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('ap')
              ->select('ap, uc, um')
              ->leftjoin('ap.userCreated', 'uc')
              ->leftjoin('ap.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'ap.id', $id);
        }
        return $qb;
    }
}
