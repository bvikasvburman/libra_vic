<?php
namespace ManufacturingOrderBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ManufacturingOrderRepository extends AbstractRepository
{
    public function getAlias() {
        return 'mo';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('mo')
              ->select('mo, uc, um')
              ->leftjoin('mo.userCreated', 'uc')
              ->leftjoin('mo.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'mo.id', $id);
        }
        return $qb;
    }
}
