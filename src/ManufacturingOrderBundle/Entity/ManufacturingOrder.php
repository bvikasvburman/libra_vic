<?php

namespace ManufacturingOrderBundle\Entity;

class ManufacturingOrder
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'MO';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $tradingOrderId;

    /**
     * @var integer
     */
    private $companyIdSupplier;

    /**
     * @var string
     */
    private $deliveryTerms;

    /**
     * @var string
     */
    private $paymentTerms;

    /**
     * @var string
     */
    private $shippingMedium;

    /**
     * @var string
     */
    private $portOfDelivery;

    /**
     * @var string
     */
    private $finalDestination;

    /**
     * @var \DateTime
     */
    private $requiredDeliveryDate;

    /**
     * @var string
     */
    private $warrantyAndClaims;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \TradingOrderBundle\Entity\TradingOrder
     */
    private $tradingOrder;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return ManufacturingOrder
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set tradingOrderId
     *
     * @param integer $tradingOrderId
     * @return ManufacturingOrder
     */
    public function setTradingOrderId($tradingOrderId)
    {
        $this->tradingOrderId = $tradingOrderId;

        return $this;
    }

    /**
     * Get tradingOrderId
     *
     * @return integer
     */
    public function getTradingOrderId()
    {
        return $this->tradingOrderId;
    }

    /**
     * Set companyIdSupplier
     *
     * @param integer $companyIdSupplier
     * @return ManufacturingOrder
     */
    public function setCompanyIdSupplier($companyIdSupplier)
    {
        $this->companyIdSupplier = $companyIdSupplier;

        return $this;
    }

    /**
     * Get companyIdSupplier
     *
     * @return integer
     */
    public function getCompanyIdSupplier()
    {
        return $this->companyIdSupplier;
    }

    /**
     * Set deliveryTerms
     *
     * @param string $deliveryTerms
     * @return ManufacturingOrder
     */
    public function setDeliveryTerms($deliveryTerms)
    {
        $this->deliveryTerms = $deliveryTerms;

        return $this;
    }

    /**
     * Get deliveryTerms
     *
     * @return string
     */
    public function getDeliveryTerms()
    {
        return $this->deliveryTerms;
    }

    /**
     * Set paymentTerms
     *
     * @param string $paymentTerms
     * @return ManufacturingOrder
     */
    public function setPaymentTerms($paymentTerms)
    {
        $this->paymentTerms = $paymentTerms;

        return $this;
    }

    /**
     * Get paymentTerms
     *
     * @return string
     */
    public function getPaymentTerms()
    {
        return $this->paymentTerms;
    }

    /**
     * Set shippingMedium
     *
     * @param string $shippingMedium
     * @return ManufacturingOrder
     */
    public function setShippingMedium($shippingMedium)
    {
        $this->shippingMedium = $shippingMedium;

        return $this;
    }

    /**
     * Get shippingMedium
     *
     * @return string
     */
    public function getShippingMedium()
    {
        return $this->shippingMedium;
    }

    /**
     * Set portOfDelivery
     *
     * @param string $portOfDelivery
     * @return ManufacturingOrder
     */
    public function setPortOfDelivery($portOfDelivery)
    {
        $this->portOfDelivery = $portOfDelivery;

        return $this;
    }

    /**
     * Get portOfDelivery
     *
     * @return string
     */
    public function getPortOfDelivery()
    {
        return $this->portOfDelivery;
    }

    /**
     * Set finalDestination
     *
     * @param string $finalDestination
     * @return ManufacturingOrder
     */
    public function setFinalDestination($finalDestination)
    {
        $this->finalDestination = $finalDestination;

        return $this;
    }

    /**
     * Get finalDestination
     *
     * @return string
     */
    public function getFinalDestination()
    {
        return $this->finalDestination;
    }

    /**
     * Set requiredDeliveryDate
     *
     * @param \DateTime $requiredDeliveryDate
     * @return ManufacturingOrder
     */
    public function setRequiredDeliveryDate($requiredDeliveryDate)
    {
        $this->requiredDeliveryDate = $requiredDeliveryDate;

        return $this;
    }

    /**
     * Get requiredDeliveryDate
     *
     * @return \DateTime
     */
    public function getRequiredDeliveryDate()
    {
        return $this->requiredDeliveryDate;
    }

    /**
     * Set warrantyAndClaims
     *
     * @param string $warrantyAndClaims
     * @return ManufacturingOrder
     */
    public function setWarrantyAndClaims($warrantyAndClaims)
    {
        $this->warrantyAndClaims = $warrantyAndClaims;

        return $this;
    }

    /**
     * Get warrantyAndClaims
     *
     * @return string
     */
    public function getWarrantyAndClaims()
    {
        return $this->warrantyAndClaims;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return ManufacturingOrder
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return ManufacturingOrder
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return ManufacturingOrder
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return ManufacturingOrder
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return ManufacturingOrder
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return ManufacturingOrder
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return ManufacturingOrder
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return ManufacturingOrder
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return ManufacturingOrder
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set tradingOrder
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrder
     * @return ManufacturingOrder
     */
    public function setTradingOrder(\TradingOrderBundle\Entity\TradingOrder $tradingOrder = null)
    {
        $this->tradingOrder = $tradingOrder;

        return $this;
    }

    /**
     * Get tradingOrder
     *
     * @return \TradingOrderBundle\Entity\TradingOrder
     */
    public function getTradingOrder()
    {
        return $this->tradingOrder;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return ManufacturingOrder
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return ManufacturingOrder
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
