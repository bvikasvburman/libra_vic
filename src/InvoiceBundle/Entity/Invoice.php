<?php

namespace InvoiceBundle\Entity;

class Invoice
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'I';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $tradingOrderId;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $enquiryId;

    /**
     * @var integer
     */
    private $salesConfirmationId;

    /**
     * @var string
     */
    private $shippingMedium;

    /**
     * @var string
     */
    private $vesselName;

    /**
     * @var string
     */
    private $voyageNo;

    /**
     * @var \DateTime
     */
    private $sailingOnDate;

    /**
     * @var \DateTime
     */
    private $invoiceDate;

    /**
     * @var string
     */
    private $portOfLoading;

    /**
     * @var string
     */
    private $portOfDelivery;

    /**
     * @var string
     */
    private $finalDestination;

    /**
     * @var string
     */
    private $containerNo;

    /**
     * @var string
     */
    private $lcNo;

    /**
     * @var string
     */
    private $blNo;

    /**
     * @var string
     */
    private $blIssuedBy;

    /**
     * @var string
     */
    private $insurancePolicyNo;

    /**
     * @var string
     */
    private $policyNo;

    /**
     * @var string
     */
    private $paymentTerms;

    /**
     * @var string
     */
    private $deliveryTerms;

    /**
     * @var string
     */
    private $clientAddress;

    /**
     * @var string
     */
    private $ourBankAccount;

    /**
     * @var string
     */
    private $ourBankDetails;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $paidDate;

    /**
     * @var string
     */
    private $goodsDescription;

    /**
     * @var integer
     */
    private $noOfCtns;

    /**
     * @var string
     */
    private $grossWeight;

    /**
     * @var string
     */
    private $measurement;

    /**
     * @var string
     */
    private $shippingMarkFront;

    /**
     * @var string
     */
    private $shippingMarkSide;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Invoice
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set tradingOrderId
     *
     * @param integer $tradingOrderId
     * @return Invoice
     */
    public function setTradingOrderId($tradingOrderId)
    {
        $this->tradingOrderId = $tradingOrderId;

        return $this;
    }

    /**
     * Get tradingOrderId
     *
     * @return integer
     */
    public function getTradingOrderId()
    {
        return $this->tradingOrderId;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return Invoice
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set enquiryId
     *
     * @param integer $enquiryId
     * @return Invoice
     */
    public function setEnquiryId($enquiryId)
    {
        $this->enquiryId = $enquiryId;

        return $this;
    }

    /**
     * Get enquiryId
     *
     * @return integer
     */
    public function getEnquiryId()
    {
        return $this->enquiryId;
    }

    /**
     * Set salesConfirmationId
     *
     * @param integer $salesConfirmationId
     * @return Invoice
     */
    public function setSalesConfirmationId($salesConfirmationId)
    {
        $this->salesConfirmationId = $salesConfirmationId;

        return $this;
    }

    /**
     * Get salesConfirmationId
     *
     * @return integer
     */
    public function getSalesConfirmationId()
    {
        return $this->salesConfirmationId;
    }

    /**
     * Set shippingMedium
     *
     * @param string $shippingMedium
     * @return Invoice
     */
    public function setShippingMedium($shippingMedium)
    {
        $this->shippingMedium = $shippingMedium;

        return $this;
    }

    /**
     * Get shippingMedium
     *
     * @return string
     */
    public function getShippingMedium()
    {
        return $this->shippingMedium;
    }

    /**
     * Set vesselName
     *
     * @param string $vesselName
     * @return Invoice
     */
    public function setVesselName($vesselName)
    {
        $this->vesselName = $vesselName;

        return $this;
    }

    /**
     * Get vesselName
     *
     * @return string
     */
    public function getVesselName()
    {
        return $this->vesselName;
    }

    /**
     * Set voyageNo
     *
     * @param string $voyageNo
     * @return Invoice
     */
    public function setVoyageNo($voyageNo)
    {
        $this->voyageNo = $voyageNo;

        return $this;
    }

    /**
     * Get voyageNo
     *
     * @return string
     */
    public function getVoyageNo()
    {
        return $this->voyageNo;
    }

    /**
     * Set sailingOnDate
     *
     * @param \DateTime $sailingOnDate
     * @return Invoice
     */
    public function setSailingOnDate($sailingOnDate)
    {
        $this->sailingOnDate = $sailingOnDate;

        return $this;
    }

    /**
     * Get sailingOnDate
     *
     * @return \DateTime
     */
    public function getSailingOnDate()
    {
        return $this->sailingOnDate;
    }

    /**
     * Set invoiceDate
     *
     * @param \DateTime $invoiceDate
     * @return Invoice
     */
    public function setInvoiceDate($invoiceDate)
    {
        $this->invoiceDate = $invoiceDate;

        return $this;
    }

    /**
     * Get invoiceDate
     *
     * @return \DateTime
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    /**
     * Set portOfLoading
     *
     * @param string $portOfLoading
     * @return Invoice
     */
    public function setPortOfLoading($portOfLoading)
    {
        $this->portOfLoading = $portOfLoading;

        return $this;
    }

    /**
     * Get portOfLoading
     *
     * @return string
     */
    public function getPortOfLoading()
    {
        return $this->portOfLoading;
    }

    /**
     * Set portOfDelivery
     *
     * @param string $portOfDelivery
     * @return Invoice
     */
    public function setPortOfDelivery($portOfDelivery)
    {
        $this->portOfDelivery = $portOfDelivery;

        return $this;
    }

    /**
     * Get portOfDelivery
     *
     * @return string
     */
    public function getPortOfDelivery()
    {
        return $this->portOfDelivery;
    }

    /**
     * Set finalDestination
     *
     * @param string $finalDestination
     * @return Invoice
     */
    public function setFinalDestination($finalDestination)
    {
        $this->finalDestination = $finalDestination;

        return $this;
    }

    /**
     * Get finalDestination
     *
     * @return string
     */
    public function getFinalDestination()
    {
        return $this->finalDestination;
    }

    /**
     * Set containerNo
     *
     * @param string $containerNo
     * @return Invoice
     */
    public function setContainerNo($containerNo)
    {
        $this->containerNo = $containerNo;

        return $this;
    }

    /**
     * Get containerNo
     *
     * @return string
     */
    public function getContainerNo()
    {
        return $this->containerNo;
    }

    /**
     * Set lcNo
     *
     * @param string $lcNo
     * @return Invoice
     */
    public function setLcNo($lcNo)
    {
        $this->lcNo = $lcNo;

        return $this;
    }

    /**
     * Get lcNo
     *
     * @return string
     */
    public function getLcNo()
    {
        return $this->lcNo;
    }

    /**
     * Set blNo
     *
     * @param string $blNo
     * @return Invoice
     */
    public function setBlNo($blNo)
    {
        $this->blNo = $blNo;

        return $this;
    }

    /**
     * Get blNo
     *
     * @return string
     */
    public function getBlNo()
    {
        return $this->blNo;
    }

    /**
     * Set blIssuedBy
     *
     * @param string $blIssuedBy
     * @return Invoice
     */
    public function setBlIssuedBy($blIssuedBy)
    {
        $this->blIssuedBy = $blIssuedBy;

        return $this;
    }

    /**
     * Get blIssuedBy
     *
     * @return string
     */
    public function getBlIssuedBy()
    {
        return $this->blIssuedBy;
    }

    /**
     * Set insurancePolicyNo
     *
     * @param string $insurancePolicyNo
     * @return Invoice
     */
    public function setInsurancePolicyNo($insurancePolicyNo)
    {
        $this->insurancePolicyNo = $insurancePolicyNo;

        return $this;
    }

    /**
     * Get insurancePolicyNo
     *
     * @return string
     */
    public function getInsurancePolicyNo()
    {
        return $this->insurancePolicyNo;
    }

    /**
     * Set policyNo
     *
     * @param string $policyNo
     * @return Invoice
     */
    public function setPolicyNo($policyNo)
    {
        $this->policyNo = $policyNo;

        return $this;
    }

    /**
     * Get policyNo
     *
     * @return string
     */
    public function getPolicyNo()
    {
        return $this->policyNo;
    }

    /**
     * Set paymentTerms
     *
     * @param string $paymentTerms
     * @return Invoice
     */
    public function setPaymentTerms($paymentTerms)
    {
        $this->paymentTerms = $paymentTerms;

        return $this;
    }

    /**
     * Get paymentTerms
     *
     * @return string
     */
    public function getPaymentTerms()
    {
        return $this->paymentTerms;
    }

    /**
     * Set deliveryTerms
     *
     * @param string $deliveryTerms
     * @return Invoice
     */
    public function setDeliveryTerms($deliveryTerms)
    {
        $this->deliveryTerms = $deliveryTerms;

        return $this;
    }

    /**
     * Get deliveryTerms
     *
     * @return string
     */
    public function getDeliveryTerms()
    {
        return $this->deliveryTerms;
    }

    /**
     * Set clientAddress
     *
     * @param string $clientAddress
     * @return Invoice
     */
    public function setClientAddress($clientAddress)
    {
        $this->clientAddress = $clientAddress;

        return $this;
    }

    /**
     * Get clientAddress
     *
     * @return string
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * Set ourBankAccount
     *
     * @param string $ourBankAccount
     * @return Invoice
     */
    public function setOurBankAccount($ourBankAccount)
    {
        $this->ourBankAccount = $ourBankAccount;

        return $this;
    }

    /**
     * Get ourBankAccount
     *
     * @return string
     */
    public function getOurBankAccount()
    {
        return $this->ourBankAccount;
    }

    /**
     * Set ourBankDetails
     *
     * @param string $ourBankDetails
     * @return Invoice
     */
    public function setOurBankDetails($ourBankDetails)
    {
        $this->ourBankDetails = $ourBankDetails;

        return $this;
    }

    /**
     * Get ourBankDetails
     *
     * @return string
     */
    public function getOurBankDetails()
    {
        return $this->ourBankDetails;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return Invoice
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Invoice
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set paidDate
     *
     * @param \DateTime $paidDate
     * @return Invoice
     */
    public function setPaidDate($paidDate)
    {
        $this->paidDate = $paidDate;

        return $this;
    }

    /**
     * Get paidDate
     *
     * @return \DateTime
     */
    public function getPaidDate()
    {
        return $this->paidDate;
    }

    /**
     * Set goodsDescription
     *
     * @param string $goodsDescription
     * @return Invoice
     */
    public function setGoodsDescription($goodsDescription)
    {
        $this->goodsDescription = $goodsDescription;

        return $this;
    }

    /**
     * Get goodsDescription
     *
     * @return string
     */
    public function getGoodsDescription()
    {
        return $this->goodsDescription;
    }

    /**
     * Set noOfCtns
     *
     * @param integer $noOfCtns
     * @return Invoice
     */
    public function setNoOfCtns($noOfCtns)
    {
        $this->noOfCtns = $noOfCtns;

        return $this;
    }

    /**
     * Get noOfCtns
     *
     * @return integer
     */
    public function getNoOfCtns()
    {
        return $this->noOfCtns;
    }

    /**
     * Set grossWeight
     *
     * @param string $grossWeight
     * @return Invoice
     */
    public function setGrossWeight($grossWeight)
    {
        $this->grossWeight = $grossWeight;

        return $this;
    }

    /**
     * Get grossWeight
     *
     * @return string
     */
    public function getGrossWeight()
    {
        return $this->grossWeight;
    }

    /**
     * Set measurement
     *
     * @param string $measurement
     * @return Invoice
     */
    public function setMeasurement($measurement)
    {
        $this->measurement = $measurement;

        return $this;
    }

    /**
     * Get measurement
     *
     * @return string
     */
    public function getMeasurement()
    {
        return $this->measurement;
    }

    /**
     * Set shippingMarkFront
     *
     * @param string $shippingMarkFront
     * @return Invoice
     */
    public function setShippingMarkFront($shippingMarkFront)
    {
        $this->shippingMarkFront = $shippingMarkFront;

        return $this;
    }

    /**
     * Get shippingMarkFront
     *
     * @return string
     */
    public function getShippingMarkFront()
    {
        return $this->shippingMarkFront;
    }

    /**
     * Set shippingMarkSide
     *
     * @param string $shippingMarkSide
     * @return Invoice
     */
    public function setShippingMarkSide($shippingMarkSide)
    {
        $this->shippingMarkSide = $shippingMarkSide;

        return $this;
    }

    /**
     * Get shippingMarkSide
     *
     * @return string
     */
    public function getShippingMarkSide()
    {
        return $this->shippingMarkSide;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return Invoice
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return Invoice
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return Invoice
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Invoice
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return Invoice
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return Invoice
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return Invoice
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return Invoice
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return Invoice
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
    /**
     * @var \TradingOrderBundle\Entity\TradingOrder
     */
    private $tradingOrder;


    /**
     * Set tradingOrder
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrder
     * @return Invoice
     */
    public function setTradingOrder(\TradingOrderBundle\Entity\TradingOrder $tradingOrder = null)
    {
        $this->tradingOrder = $tradingOrder;

        return $this;
    }

    /**
     * Get tradingOrder
     *
     * @return \TradingOrderBundle\Entity\TradingOrder
     */
    public function getTradingOrder()
    {
        return $this->tradingOrder;
    }
}
