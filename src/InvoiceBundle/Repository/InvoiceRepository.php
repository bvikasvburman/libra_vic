<?php
namespace InvoiceBundle\Repository;

use AppBundle\Component\AbstractRepository;

class InvoiceRepository extends AbstractRepository
{
    public function getAlias() {
        return 'i';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('i')
              ->select('i, uc, um')
              ->leftjoin('i.userCreated', 'uc')
              ->leftjoin('i.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'i.id', $id);
        }
        return $qb;
    }
}
