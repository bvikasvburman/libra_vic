<?php

namespace VendorInvoiceBundle\Entity;

class VendorInvoice
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'VI';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $tradingOrderId;

    /**
     * @var integer
     */
    private $companyIdSupplier;

    /**
     * @var string
     */
    private $deliveryTerms;

    /**
     * @var string
     */
    private $paymentTerms;

    /**
     * @var string
     */
    private $shippingMedium;

    /**
     * @var string
     */
    private $portOfDelivery;

    /**
     * @var string
     */
    private $finalDestination;

    /**
     * @var \DateTime
     */
    private $requiredDeliveryDate;

    /**
     * @var string
     */
    private $warrantyAndClaims;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return VendorInvoice
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set tradingOrderId
     *
     * @param integer $tradingOrderId
     * @return VendorInvoice
     */
    public function setTradingOrderId($tradingOrderId)
    {
        $this->tradingOrderId = $tradingOrderId;

        return $this;
    }

    /**
     * Get tradingOrderId
     *
     * @return integer
     */
    public function getTradingOrderId()
    {
        return $this->tradingOrderId;
    }

    /**
     * Set companyIdSupplier
     *
     * @param integer $companyIdSupplier
     * @return VendorInvoice
     */
    public function setCompanyIdSupplier($companyIdSupplier)
    {
        $this->companyIdSupplier = $companyIdSupplier;

        return $this;
    }

    /**
     * Get companyIdSupplier
     *
     * @return integer
     */
    public function getCompanyIdSupplier()
    {
        return $this->companyIdSupplier;
    }

    /**
     * Set deliveryTerms
     *
     * @param string $deliveryTerms
     * @return VendorInvoice
     */
    public function setDeliveryTerms($deliveryTerms)
    {
        $this->deliveryTerms = $deliveryTerms;

        return $this;
    }

    /**
     * Get deliveryTerms
     *
     * @return string
     */
    public function getDeliveryTerms()
    {
        return $this->deliveryTerms;
    }

    /**
     * Set paymentTerms
     *
     * @param string $paymentTerms
     * @return VendorInvoice
     */
    public function setPaymentTerms($paymentTerms)
    {
        $this->paymentTerms = $paymentTerms;

        return $this;
    }

    /**
     * Get paymentTerms
     *
     * @return string
     */
    public function getPaymentTerms()
    {
        return $this->paymentTerms;
    }

    /**
     * Set shippingMedium
     *
     * @param string $shippingMedium
     * @return VendorInvoice
     */
    public function setShippingMedium($shippingMedium)
    {
        $this->shippingMedium = $shippingMedium;

        return $this;
    }

    /**
     * Get shippingMedium
     *
     * @return string
     */
    public function getShippingMedium()
    {
        return $this->shippingMedium;
    }

    /**
     * Set portOfDelivery
     *
     * @param string $portOfDelivery
     * @return VendorInvoice
     */
    public function setPortOfDelivery($portOfDelivery)
    {
        $this->portOfDelivery = $portOfDelivery;

        return $this;
    }

    /**
     * Get portOfDelivery
     *
     * @return string
     */
    public function getPortOfDelivery()
    {
        return $this->portOfDelivery;
    }

    /**
     * Set finalDestination
     *
     * @param string $finalDestination
     * @return VendorInvoice
     */
    public function setFinalDestination($finalDestination)
    {
        $this->finalDestination = $finalDestination;

        return $this;
    }

    /**
     * Get finalDestination
     *
     * @return string
     */
    public function getFinalDestination()
    {
        return $this->finalDestination;
    }

    /**
     * Set requiredDeliveryDate
     *
     * @param \DateTime $requiredDeliveryDate
     * @return VendorInvoice
     */
    public function setRequiredDeliveryDate($requiredDeliveryDate)
    {
        $this->requiredDeliveryDate = $requiredDeliveryDate;

        return $this;
    }

    /**
     * Get requiredDeliveryDate
     *
     * @return \DateTime
     */
    public function getRequiredDeliveryDate()
    {
        return $this->requiredDeliveryDate;
    }

    /**
     * Set warrantyAndClaims
     *
     * @param string $warrantyAndClaims
     * @return VendorInvoice
     */
    public function setWarrantyAndClaims($warrantyAndClaims)
    {
        $this->warrantyAndClaims = $warrantyAndClaims;

        return $this;
    }

    /**
     * Get warrantyAndClaims
     *
     * @return string
     */
    public function getWarrantyAndClaims()
    {
        return $this->warrantyAndClaims;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return VendorInvoice
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return VendorInvoice
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return VendorInvoice
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return VendorInvoice
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return VendorInvoice
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return VendorInvoice
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return VendorInvoice
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return VendorInvoice
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return VendorInvoice
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return VendorInvoice
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
