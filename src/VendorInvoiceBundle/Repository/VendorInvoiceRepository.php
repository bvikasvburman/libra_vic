<?php
namespace VendorInvoiceBundle\Repository;

use AppBundle\Component\AbstractRepository;

class VendorInvoiceRepository extends AbstractRepository
{
    public function getAlias() {
        return 'vi';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('vi')
              ->select('vi, uc, um')
              ->leftjoin('vi.userCreated', 'uc')
              ->leftjoin('vi.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'vi.id', $id);
        }
        return $qb;
    }
}
