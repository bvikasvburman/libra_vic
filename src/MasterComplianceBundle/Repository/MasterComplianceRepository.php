<?php
namespace MasterComplianceBundle\Repository;

use AppBundle\Component\AbstractRepository;

class MasterComplianceRepository extends AbstractRepository
{
    public function getAlias() {
        return 'mc';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('mc')
              ->select('mc, uc, um')
              ->leftjoin('mc.userCreated', 'uc')
              ->leftjoin('mc.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'mc.id', $id);
        }
        return $qb;
    }
}
