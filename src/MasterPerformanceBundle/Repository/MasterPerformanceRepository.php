<?php
namespace MasterPerformanceBundle\Repository;

use AppBundle\Component\AbstractRepository;

class MasterPerformanceRepository extends AbstractRepository
{
    public function getAlias() {
        return 'mp';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('mp')
              ->select('mp, uc, um')
              ->leftjoin('mp.userCreated', 'uc')
              ->leftjoin('mp.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'mp.id', $id);
        }
        return $qb;
    }
}
