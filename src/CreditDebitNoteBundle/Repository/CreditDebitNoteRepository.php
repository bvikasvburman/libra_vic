<?php
namespace CreditDebitNoteBundle\Repository;

use AppBundle\Component\AbstractRepository;

class CreditDebitNoteRepository extends AbstractRepository
{
    public function getAlias() {
        return 'cdn';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('cdn')
              ->select('cdn, uc, um')
              ->leftjoin('cdn.userCreated', 'uc')
              ->leftjoin('cdn.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'cdn.id', $id);
        }
        return $qb;
    }
}
