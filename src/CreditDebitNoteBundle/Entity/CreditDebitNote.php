<?php

namespace CreditDebitNoteBundle\Entity;

class CreditDebitNote
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'CD';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $tradingOrderId;

    /**
     * @var integer
     */
    private $purchaseOrderId;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var string
     */
    private $noteType;

    /**
     * @var integer
     */
    private $amount;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var string
     */
    private $status;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \TradingOrderBundle\Entity\TradingOrder
     */
    private $tradingOrder;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return CreditDebitNote
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set tradingOrderId
     *
     * @param integer $tradingOrderId
     * @return CreditDebitNote
     */
    public function setTradingOrderId($tradingOrderId)
    {
        $this->tradingOrderId = $tradingOrderId;

        return $this;
    }

    /**
     * Get tradingOrderId
     *
     * @return integer
     */
    public function getTradingOrderId()
    {
        return $this->tradingOrderId;
    }

    /**
     * Set purchaseOrderId
     *
     * @param integer $purchaseOrderId
     * @return CreditDebitNote
     */
    public function setPurchaseOrderId($purchaseOrderId)
    {
        $this->purchaseOrderId = $purchaseOrderId;

        return $this;
    }

    /**
     * Get purchaseOrderId
     *
     * @return integer
     */
    public function getPurchaseOrderId()
    {
        return $this->purchaseOrderId;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return CreditDebitNote
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set noteType
     *
     * @param string $noteType
     * @return CreditDebitNote
     */
    public function setNoteType($noteType)
    {
        $this->noteType = $noteType;

        return $this;
    }

    /**
     * Get noteType
     *
     * @return string
     */
    public function getNoteType()
    {
        return $this->noteType;
    }

    /**
     * Set amount
     *
     * @param integer $amount
     * @return CreditDebitNote
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return integer
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set notes
     *
     * @param string $notes
     * @return CreditDebitNote
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return CreditDebitNote
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return CreditDebitNote
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return CreditDebitNote
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return CreditDebitNote
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return CreditDebitNote
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return CreditDebitNote
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return CreditDebitNote
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return CreditDebitNote
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set tradingOrder
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrder
     * @return CreditDebitNote
     */
    public function setTradingOrder(\TradingOrderBundle\Entity\TradingOrder $tradingOrder = null)
    {
        $this->tradingOrder = $tradingOrder;

        return $this;
    }

    /**
     * Get tradingOrder
     *
     * @return \TradingOrderBundle\Entity\TradingOrder
     */
    public function getTradingOrder()
    {
        return $this->tradingOrder;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return CreditDebitNote
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return CreditDebitNote
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return CreditDebitNote
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
