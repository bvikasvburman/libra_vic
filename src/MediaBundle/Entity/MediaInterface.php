<?php
namespace MediaBundle\Entity;

/**
 * The interface contains the minimum set of methods required to handle media.
 */
interface MediaInterface
{
    /**
     * Get the original file object.
     */
    public function getFile();

    /**
     * Set the original file object.
     */
    public function setFile($file);

    /**
     * Get the original file name.
     */
    public function getPath();

    /**
     * Set the original file name.
     */
    public function setPath($path);

    /**
     * Should return the absolut path of the file.
     */
    public function getAbsolutePath();

    /**
     * Should return the web path of the file.
     */
    public function getWebPath();

    /**
     * Should return the absolute directory path where uploaded file is saved.
     */
    public function getUploadRootDir();

    /**
     * Should return the name of the directory path where the file is saved,
     * but not absolute.
     */
    // public function getUploadDir();
}