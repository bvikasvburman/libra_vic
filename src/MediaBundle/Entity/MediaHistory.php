<?php

namespace MediaBundle\Entity;

class MediaHistory
{
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $mediaId;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $contactId;

    /**
     * @var integer
     */
    private $colorMasterId;

    /**
     * @var integer
     */
    private $partsLocationId;

    /**
     * @var integer
     */
    private $approvedMaterialId;

    /**
     * @var integer
     */
    private $approvedComponentId;

    /**
     * @var integer
     */
    private $approvedPackagingId;

    /**
     * @var integer
     */
    private $masterPerformanceId;

    /**
     * @var integer
     */
    private $masterComplianceId;

    /**
     * @var integer
     */
    private $masterChecklistId;

    /**
     * @var integer
     */
    private $enquiryId;

    /**
     * @var integer
     */
    private $quotationId;

    /**
     * @var integer
     */
    private $tradingOrderId;

    /**
     * @var integer
     */
    private $salesConfirmationId;

    /**
     * @var integer
     */
    private $purchaseOrderId;

    /**
     * @var integer
     */
    private $manufacturingOrderId;

    /**
     * @var integer
     */
    private $invoiceId;

    /**
     * @var integer
     */
    private $vendorInvoiceId;

    /**
     * @var integer
     */
    private $accountsReceivableId;

    /**
     * @var integer
     */
    private $accountsPayableId;

    /**
     * @var integer
     */
    private $creditDebitNoteId;

    /**
     * @var string
     */
    private $mediaType;

    /**
     * @var string
     */
    private $module;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \ContactBundle\Entity\Contact
     */
    private $contact;

    /**
     * @var \ColorMasterBundle\Entity\ColorMaster
     */
    private $colorMaster;

    /**
     * @var \PartsLocationBundle\Entity\PartsLocation
     */
    private $partsLocation;

    /**
     * @var \ApprovedMaterialBundle\Entity\ApprovedMaterial
     */
    private $approvedMaterial;

    /**
     * @var \ApprovedComponentBundle\Entity\ApprovedComponent
     */
    private $approvedComponent;

    /**
     * @var \ApprovedPackagingBundle\Entity\ApprovedPackaging
     */
    private $approvedPackaging;

    /**
     * @var \MasterPerformanceBundle\Entity\MasterPerformance
     */
    private $masterPerformance;

    /**
     * @var \MasterComplianceBundle\Entity\MasterCompliance
     */
    private $masterCompliance;

    /**
     * @var \MasterChecklistBundle\Entity\MasterChecklist
     */
    private $masterChecklist;

    /**
     * @var \EnquiryBundle\Entity\Enquiry
     */
    private $enquiry;

    /**
     * @var \QuotationBundle\Entity\Quotation
     */
    private $quotation;

    /**
     * @var \TradingOrderBundle\Entity\TradingOrder
     */
    private $tradingOrder;

    /**
     * @var \SalesConfirmationBundle\Entity\SalesConfirmation
     */
    private $salesConfirmation;

    /**
     * @var \PurchaseOrderBundle\Entity\PurchaseOrder
     */
    private $purchaseOrder;

    /**
     * @var \ManufacturingOrderBundle\Entity\ManufacturingOrder
     */
    private $manufacturingOrder;

    /**
     * @var \InvoiceBundle\Entity\Invoice
     */
    private $invoice;

    /**
     * @var \VendorInvoiceBundle\Entity\VendorInvoice
     */
    private $vendorInvoice;

    /**
     * @var \AccountsReceivableBundle\Entity\AccountsReceivable
     */
    private $accountsReceivable;

    /**
     * @var \AccountsPayableBundle\Entity\AccountsPayable
     */
    private $accountsPayable;

    /**
     * @var \CreditDebitNoteBundle\Entity\CreditDebitNote
     */
    private $creditDebitNote;

    /**
     * @var \MediaBundle\Entity\Media
     */
    private $media;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mediaId
     *
     * @param integer $mediaId
     * @return MediaHistory
     */
    public function setMediaId($mediaId)
    {
        $this->mediaId = $mediaId;

        return $this;
    }

    /**
     * Get mediaId
     *
     * @return integer 
     */
    public function getMediaId()
    {
        return $this->mediaId;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return MediaHistory
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer 
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set contactId
     *
     * @param integer $contactId
     * @return MediaHistory
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;

        return $this;
    }

    /**
     * Get contactId
     *
     * @return integer 
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set colorMasterId
     *
     * @param integer $colorMasterId
     * @return MediaHistory
     */
    public function setColorMasterId($colorMasterId)
    {
        $this->colorMasterId = $colorMasterId;

        return $this;
    }

    /**
     * Get colorMasterId
     *
     * @return integer 
     */
    public function getColorMasterId()
    {
        return $this->colorMasterId;
    }

    /**
     * Set partsLocationId
     *
     * @param integer $partsLocationId
     * @return MediaHistory
     */
    public function setPartsLocationId($partsLocationId)
    {
        $this->partsLocationId = $partsLocationId;

        return $this;
    }

    /**
     * Get partsLocationId
     *
     * @return integer 
     */
    public function getPartsLocationId()
    {
        return $this->partsLocationId;
    }

    /**
     * Set approvedMaterialId
     *
     * @param integer $approvedMaterialId
     * @return MediaHistory
     */
    public function setApprovedMaterialId($approvedMaterialId)
    {
        $this->approvedMaterialId = $approvedMaterialId;

        return $this;
    }

    /**
     * Get approvedMaterialId
     *
     * @return integer 
     */
    public function getApprovedMaterialId()
    {
        return $this->approvedMaterialId;
    }

    /**
     * Set approvedComponentId
     *
     * @param integer $approvedComponentId
     * @return MediaHistory
     */
    public function setApprovedComponentId($approvedComponentId)
    {
        $this->approvedComponentId = $approvedComponentId;

        return $this;
    }

    /**
     * Get approvedComponentId
     *
     * @return integer 
     */
    public function getApprovedComponentId()
    {
        return $this->approvedComponentId;
    }

    /**
     * Set approvedPackagingId
     *
     * @param integer $approvedPackagingId
     * @return MediaHistory
     */
    public function setApprovedPackagingId($approvedPackagingId)
    {
        $this->approvedPackagingId = $approvedPackagingId;

        return $this;
    }

    /**
     * Get approvedPackagingId
     *
     * @return integer 
     */
    public function getApprovedPackagingId()
    {
        return $this->approvedPackagingId;
    }

    /**
     * Set masterPerformanceId
     *
     * @param integer $masterPerformanceId
     * @return MediaHistory
     */
    public function setMasterPerformanceId($masterPerformanceId)
    {
        $this->masterPerformanceId = $masterPerformanceId;

        return $this;
    }

    /**
     * Get masterPerformanceId
     *
     * @return integer 
     */
    public function getMasterPerformanceId()
    {
        return $this->masterPerformanceId;
    }

    /**
     * Set masterComplianceId
     *
     * @param integer $masterComplianceId
     * @return MediaHistory
     */
    public function setMasterComplianceId($masterComplianceId)
    {
        $this->masterComplianceId = $masterComplianceId;

        return $this;
    }

    /**
     * Get masterComplianceId
     *
     * @return integer 
     */
    public function getMasterComplianceId()
    {
        return $this->masterComplianceId;
    }

    /**
     * Set masterChecklistId
     *
     * @param integer $masterChecklistId
     * @return MediaHistory
     */
    public function setMasterChecklistId($masterChecklistId)
    {
        $this->masterChecklistId = $masterChecklistId;

        return $this;
    }

    /**
     * Get masterChecklistId
     *
     * @return integer 
     */
    public function getMasterChecklistId()
    {
        return $this->masterChecklistId;
    }

    /**
     * Set enquiryId
     *
     * @param integer $enquiryId
     * @return MediaHistory
     */
    public function setEnquiryId($enquiryId)
    {
        $this->enquiryId = $enquiryId;

        return $this;
    }

    /**
     * Get enquiryId
     *
     * @return integer 
     */
    public function getEnquiryId()
    {
        return $this->enquiryId;
    }

    /**
     * Set quotationId
     *
     * @param integer $quotationId
     * @return MediaHistory
     */
    public function setQuotationId($quotationId)
    {
        $this->quotationId = $quotationId;

        return $this;
    }

    /**
     * Get quotationId
     *
     * @return integer 
     */
    public function getQuotationId()
    {
        return $this->quotationId;
    }

    /**
     * Set tradingOrderId
     *
     * @param integer $tradingOrderId
     * @return MediaHistory
     */
    public function setTradingOrderId($tradingOrderId)
    {
        $this->tradingOrderId = $tradingOrderId;

        return $this;
    }

    /**
     * Get tradingOrderId
     *
     * @return integer 
     */
    public function getTradingOrderId()
    {
        return $this->tradingOrderId;
    }

    /**
     * Set salesConfirmationId
     *
     * @param integer $salesConfirmationId
     * @return MediaHistory
     */
    public function setSalesConfirmationId($salesConfirmationId)
    {
        $this->salesConfirmationId = $salesConfirmationId;

        return $this;
    }

    /**
     * Get salesConfirmationId
     *
     * @return integer 
     */
    public function getSalesConfirmationId()
    {
        return $this->salesConfirmationId;
    }

    /**
     * Set purchaseOrderId
     *
     * @param integer $purchaseOrderId
     * @return MediaHistory
     */
    public function setPurchaseOrderId($purchaseOrderId)
    {
        $this->purchaseOrderId = $purchaseOrderId;

        return $this;
    }

    /**
     * Get purchaseOrderId
     *
     * @return integer 
     */
    public function getPurchaseOrderId()
    {
        return $this->purchaseOrderId;
    }

    /**
     * Set manufacturingOrderId
     *
     * @param integer $manufacturingOrderId
     * @return MediaHistory
     */
    public function setManufacturingOrderId($manufacturingOrderId)
    {
        $this->manufacturingOrderId = $manufacturingOrderId;

        return $this;
    }

    /**
     * Get manufacturingOrderId
     *
     * @return integer 
     */
    public function getManufacturingOrderId()
    {
        return $this->manufacturingOrderId;
    }

    /**
     * Set invoiceId
     *
     * @param integer $invoiceId
     * @return MediaHistory
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * Get invoiceId
     *
     * @return integer 
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * Set vendorInvoiceId
     *
     * @param integer $vendorInvoiceId
     * @return MediaHistory
     */
    public function setVendorInvoiceId($vendorInvoiceId)
    {
        $this->vendorInvoiceId = $vendorInvoiceId;

        return $this;
    }

    /**
     * Get vendorInvoiceId
     *
     * @return integer 
     */
    public function getVendorInvoiceId()
    {
        return $this->vendorInvoiceId;
    }

    /**
     * Set accountsReceivableId
     *
     * @param integer $accountsReceivableId
     * @return MediaHistory
     */
    public function setAccountsReceivableId($accountsReceivableId)
    {
        $this->accountsReceivableId = $accountsReceivableId;

        return $this;
    }

    /**
     * Get accountsReceivableId
     *
     * @return integer 
     */
    public function getAccountsReceivableId()
    {
        return $this->accountsReceivableId;
    }

    /**
     * Set accountsPayableId
     *
     * @param integer $accountsPayableId
     * @return MediaHistory
     */
    public function setAccountsPayableId($accountsPayableId)
    {
        $this->accountsPayableId = $accountsPayableId;

        return $this;
    }

    /**
     * Get accountsPayableId
     *
     * @return integer 
     */
    public function getAccountsPayableId()
    {
        return $this->accountsPayableId;
    }

    /**
     * Set creditDebitNoteId
     *
     * @param integer $creditDebitNoteId
     * @return MediaHistory
     */
    public function setCreditDebitNoteId($creditDebitNoteId)
    {
        $this->creditDebitNoteId = $creditDebitNoteId;

        return $this;
    }

    /**
     * Get creditDebitNoteId
     *
     * @return integer 
     */
    public function getCreditDebitNoteId()
    {
        return $this->creditDebitNoteId;
    }

    /**
     * Set mediaType
     *
     * @param string $mediaType
     * @return MediaHistory
     */
    public function setMediaType($mediaType)
    {
        $this->mediaType = $mediaType;

        return $this;
    }

    /**
     * Get mediaType
     *
     * @return string 
     */
    public function getMediaType()
    {
        return $this->mediaType;
    }

    /**
     * Set module
     *
     * @param string $module
     * @return MediaHistory
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return string 
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return MediaHistory
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return MediaHistory
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contact
     *
     * @param \ContactBundle\Entity\Contact $contact
     * @return MediaHistory
     */
    public function setContact(\ContactBundle\Entity\Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \ContactBundle\Entity\Contact 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set colorMaster
     *
     * @param \ColorMasterBundle\Entity\ColorMaster $colorMaster
     * @return MediaHistory
     */
    public function setColorMaster(\ColorMasterBundle\Entity\ColorMaster $colorMaster = null)
    {
        $this->colorMaster = $colorMaster;

        return $this;
    }

    /**
     * Get colorMaster
     *
     * @return \ColorMasterBundle\Entity\ColorMaster 
     */
    public function getColorMaster()
    {
        return $this->colorMaster;
    }

    /**
     * Set partsLocation
     *
     * @param \PartsLocationBundle\Entity\PartsLocation $partsLocation
     * @return MediaHistory
     */
    public function setPartsLocation(\PartsLocationBundle\Entity\PartsLocation $partsLocation = null)
    {
        $this->partsLocation = $partsLocation;

        return $this;
    }

    /**
     * Get partsLocation
     *
     * @return \PartsLocationBundle\Entity\PartsLocation 
     */
    public function getPartsLocation()
    {
        return $this->partsLocation;
    }

    /**
     * Set approvedMaterial
     *
     * @param \ApprovedMaterialBundle\Entity\ApprovedMaterial $approvedMaterial
     * @return MediaHistory
     */
    public function setApprovedMaterial(\ApprovedMaterialBundle\Entity\ApprovedMaterial $approvedMaterial = null)
    {
        $this->approvedMaterial = $approvedMaterial;

        return $this;
    }

    /**
     * Get approvedMaterial
     *
     * @return \ApprovedMaterialBundle\Entity\ApprovedMaterial 
     */
    public function getApprovedMaterial()
    {
        return $this->approvedMaterial;
    }

    /**
     * Set approvedComponent
     *
     * @param \ApprovedComponentBundle\Entity\ApprovedComponent $approvedComponent
     * @return MediaHistory
     */
    public function setApprovedComponent(\ApprovedComponentBundle\Entity\ApprovedComponent $approvedComponent = null)
    {
        $this->approvedComponent = $approvedComponent;

        return $this;
    }

    /**
     * Get approvedComponent
     *
     * @return \ApprovedComponentBundle\Entity\ApprovedComponent 
     */
    public function getApprovedComponent()
    {
        return $this->approvedComponent;
    }

    /**
     * Set approvedPackaging
     *
     * @param \ApprovedPackagingBundle\Entity\ApprovedPackaging $approvedPackaging
     * @return MediaHistory
     */
    public function setApprovedPackaging(\ApprovedPackagingBundle\Entity\ApprovedPackaging $approvedPackaging = null)
    {
        $this->approvedPackaging = $approvedPackaging;

        return $this;
    }

    /**
     * Get approvedPackaging
     *
     * @return \ApprovedPackagingBundle\Entity\ApprovedPackaging 
     */
    public function getApprovedPackaging()
    {
        return $this->approvedPackaging;
    }

    /**
     * Set masterPerformance
     *
     * @param \MasterPerformanceBundle\Entity\MasterPerformance $masterPerformance
     * @return MediaHistory
     */
    public function setMasterPerformance(\MasterPerformanceBundle\Entity\MasterPerformance $masterPerformance = null)
    {
        $this->masterPerformance = $masterPerformance;

        return $this;
    }

    /**
     * Get masterPerformance
     *
     * @return \MasterPerformanceBundle\Entity\MasterPerformance 
     */
    public function getMasterPerformance()
    {
        return $this->masterPerformance;
    }

    /**
     * Set masterCompliance
     *
     * @param \MasterComplianceBundle\Entity\MasterCompliance $masterCompliance
     * @return MediaHistory
     */
    public function setMasterCompliance(\MasterComplianceBundle\Entity\MasterCompliance $masterCompliance = null)
    {
        $this->masterCompliance = $masterCompliance;

        return $this;
    }

    /**
     * Get masterCompliance
     *
     * @return \MasterComplianceBundle\Entity\MasterCompliance 
     */
    public function getMasterCompliance()
    {
        return $this->masterCompliance;
    }

    /**
     * Set masterChecklist
     *
     * @param \MasterChecklistBundle\Entity\MasterChecklist $masterChecklist
     * @return MediaHistory
     */
    public function setMasterChecklist(\MasterChecklistBundle\Entity\MasterChecklist $masterChecklist = null)
    {
        $this->masterChecklist = $masterChecklist;

        return $this;
    }

    /**
     * Get masterChecklist
     *
     * @return \MasterChecklistBundle\Entity\MasterChecklist 
     */
    public function getMasterChecklist()
    {
        return $this->masterChecklist;
    }

    /**
     * Set enquiry
     *
     * @param \EnquiryBundle\Entity\Enquiry $enquiry
     * @return MediaHistory
     */
    public function setEnquiry(\EnquiryBundle\Entity\Enquiry $enquiry = null)
    {
        $this->enquiry = $enquiry;

        return $this;
    }

    /**
     * Get enquiry
     *
     * @return \EnquiryBundle\Entity\Enquiry 
     */
    public function getEnquiry()
    {
        return $this->enquiry;
    }

    /**
     * Set quotation
     *
     * @param \QuotationBundle\Entity\Quotation $quotation
     * @return MediaHistory
     */
    public function setQuotation(\QuotationBundle\Entity\Quotation $quotation = null)
    {
        $this->quotation = $quotation;

        return $this;
    }

    /**
     * Get quotation
     *
     * @return \QuotationBundle\Entity\Quotation 
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * Set tradingOrder
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrder
     * @return MediaHistory
     */
    public function setTradingOrder(\TradingOrderBundle\Entity\TradingOrder $tradingOrder = null)
    {
        $this->tradingOrder = $tradingOrder;

        return $this;
    }

    /**
     * Get tradingOrder
     *
     * @return \TradingOrderBundle\Entity\TradingOrder 
     */
    public function getTradingOrder()
    {
        return $this->tradingOrder;
    }

    /**
     * Set salesConfirmation
     *
     * @param \SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmation
     * @return MediaHistory
     */
    public function setSalesConfirmation(\SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmation = null)
    {
        $this->salesConfirmation = $salesConfirmation;

        return $this;
    }

    /**
     * Get salesConfirmation
     *
     * @return \SalesConfirmationBundle\Entity\SalesConfirmation 
     */
    public function getSalesConfirmation()
    {
        return $this->salesConfirmation;
    }

    /**
     * Set purchaseOrder
     *
     * @param \PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrder
     * @return MediaHistory
     */
    public function setPurchaseOrder(\PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrder = null)
    {
        $this->purchaseOrder = $purchaseOrder;

        return $this;
    }

    /**
     * Get purchaseOrder
     *
     * @return \PurchaseOrderBundle\Entity\PurchaseOrder 
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * Set manufacturingOrder
     *
     * @param \ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrder
     * @return MediaHistory
     */
    public function setManufacturingOrder(\ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrder = null)
    {
        $this->manufacturingOrder = $manufacturingOrder;

        return $this;
    }

    /**
     * Get manufacturingOrder
     *
     * @return \ManufacturingOrderBundle\Entity\ManufacturingOrder 
     */
    public function getManufacturingOrder()
    {
        return $this->manufacturingOrder;
    }

    /**
     * Set invoice
     *
     * @param \InvoiceBundle\Entity\Invoice $invoice
     * @return MediaHistory
     */
    public function setInvoice(\InvoiceBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \InvoiceBundle\Entity\Invoice 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set vendorInvoice
     *
     * @param \VendorInvoiceBundle\Entity\VendorInvoice $vendorInvoice
     * @return MediaHistory
     */
    public function setVendorInvoice(\VendorInvoiceBundle\Entity\VendorInvoice $vendorInvoice = null)
    {
        $this->vendorInvoice = $vendorInvoice;

        return $this;
    }

    /**
     * Get vendorInvoice
     *
     * @return \VendorInvoiceBundle\Entity\VendorInvoice 
     */
    public function getVendorInvoice()
    {
        return $this->vendorInvoice;
    }

    /**
     * Set accountsReceivable
     *
     * @param \AccountsReceivableBundle\Entity\AccountsReceivable $accountsReceivable
     * @return MediaHistory
     */
    public function setAccountsReceivable(\AccountsReceivableBundle\Entity\AccountsReceivable $accountsReceivable = null)
    {
        $this->accountsReceivable = $accountsReceivable;

        return $this;
    }

    /**
     * Get accountsReceivable
     *
     * @return \AccountsReceivableBundle\Entity\AccountsReceivable 
     */
    public function getAccountsReceivable()
    {
        return $this->accountsReceivable;
    }

    /**
     * Set accountsPayable
     *
     * @param \AccountsPayableBundle\Entity\AccountsPayable $accountsPayable
     * @return MediaHistory
     */
    public function setAccountsPayable(\AccountsPayableBundle\Entity\AccountsPayable $accountsPayable = null)
    {
        $this->accountsPayable = $accountsPayable;

        return $this;
    }

    /**
     * Get accountsPayable
     *
     * @return \AccountsPayableBundle\Entity\AccountsPayable 
     */
    public function getAccountsPayable()
    {
        return $this->accountsPayable;
    }

    /**
     * Set creditDebitNote
     *
     * @param \CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNote
     * @return MediaHistory
     */
    public function setCreditDebitNote(\CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNote = null)
    {
        $this->creditDebitNote = $creditDebitNote;

        return $this;
    }

    /**
     * Get creditDebitNote
     *
     * @return \CreditDebitNoteBundle\Entity\CreditDebitNote 
     */
    public function getCreditDebitNote()
    {
        return $this->creditDebitNote;
    }

    /**
     * Set media
     *
     * @param \MediaBundle\Entity\Media $media
     * @return MediaHistory
     */
    public function setMedia(\MediaBundle\Entity\Media $media = null)
    {
        $this->media = $media;

        return $this;
    }

    /**
     * Get media
     *
     * @return \MediaBundle\Entity\Media 
     */
    public function getMedia()
    {
        return $this->media;
    }
    /**
     * @var \UserBundle\Entity\User
     */
    private $user;


    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     * @return MediaHistory
     */
    public function setUser(\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @var integer
     */
    private $researchId;

    /**
     * @var \ResearchBundle\Entity\Research
     */
    private $research;


    /**
     * Set researchId
     *
     * @param integer $researchId
     * @return MediaHistory
     */
    public function setResearchId($researchId)
    {
        $this->researchId = $researchId;

        return $this;
    }

    /**
     * Get researchId
     *
     * @return integer 
     */
    public function getResearchId()
    {
        return $this->researchId;
    }

    /**
     * Set research
     *
     * @param \ResearchBundle\Entity\Research $research
     * @return MediaHistory
     */
    public function setResearch(\ResearchBundle\Entity\Research $research = null)
    {
        $this->research = $research;

        return $this;
    }

    /**
     * Get research
     *
     * @return \ResearchBundle\Entity\Research 
     */
    public function getResearch()
    {
        return $this->research;
    }
    /**
     * @var integer
     */
    private $protoTypeId;

    /**
     * @var \ProtoTypeBundle\Entity\ProtoType
     */
    private $protoType;


    /**
     * Set protoTypeId
     *
     * @param integer $protoTypeId
     * @return MediaHistory
     */
    public function setProtoTypeId($protoTypeId)
    {
        $this->protoTypeId = $protoTypeId;

        return $this;
    }

    /**
     * Get protoTypeId
     *
     * @return integer 
     */
    public function getProtoTypeId()
    {
        return $this->protoTypeId;
    }

    /**
     * Set protoType
     *
     * @param \ProtoTypeBundle\Entity\ProtoType $protoType
     * @return MediaHistory
     */
    public function setProtoType(\ProtoTypeBundle\Entity\ProtoType $protoType = null)
    {
        $this->protoType = $protoType;

        return $this;
    }

    /**
     * Get protoType
     *
     * @return \ProtoTypeBundle\Entity\ProtoType 
     */
    public function getProtoType()
    {
        return $this->protoType;
    }
    /**
     * @var integer
     */
    private $interestGroupId;

    /**
     * @var \InterestGroupBundle\Entity\InterestGroup
     */
    private $interestGroup;


    /**
     * Set interestGroupId
     *
     * @param integer $interestGroupId
     * @return MediaHistory
     */
    public function setInterestGroupId($interestGroupId)
    {
        $this->interestGroupId = $interestGroupId;

        return $this;
    }

    /**
     * Get interestGroupId
     *
     * @return integer 
     */
    public function getInterestGroupId()
    {
        return $this->interestGroupId;
    }

    /**
     * Set interestGroup
     *
     * @param \InterestGroupBundle\Entity\InterestGroup $interestGroup
     * @return MediaHistory
     */
    public function setInterestGroup(\InterestGroupBundle\Entity\InterestGroup $interestGroup = null)
    {
        $this->interestGroup = $interestGroup;

        return $this;
    }

    /**
     * Get interestGroup
     *
     * @return \InterestGroupBundle\Entity\InterestGroup 
     */
    public function getInterestGroup()
    {
        return $this->interestGroup;
    }
}
