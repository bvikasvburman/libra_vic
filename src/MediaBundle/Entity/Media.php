<?php

namespace MediaBundle\Entity;

use AppBundle\Entity\Media as AbstractMedia;
use MediaBundle\Entity\MediaInterface;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;

class Media implements MediaInterface
{
    /**
     * The actual file.
     *
     * @var \Symfony\Component\HttpFoundation\File\UploadedFile
     */
    protected $file;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get the absolut actualFileName of the file.
     *
     * @return string
     */
    public function getAbsolutePath()
    {
        return null === $this->fileName
            ? null
            : $this->getUploadRootDir() . '/' . $this->fileName;
    }

    /**
     * Get the web actualFileName of the file.
     *
     * @return string
     */
    public function getWebPath()
    {
        return null === $this->fileName
            ? null
            : '/media/normal/' . $this->actualFileName;
    }

    /**
     * Get the absolut directory where the file is saved.
     *
     * @return string
     */
    public function getUploadRootDir()
    {
        // the absolute directory actualFileName where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/media/normal/';
    }

    /**
     * Upload the file.
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        $filename = md5(uniqid()) . '.' . $this->file->guessExtension();

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );


        // set the actualFileName property to the filename where you've saved the file
        $this->setPath($this->getFile()->getClientOriginalName());

        $this->setFileName($filename)
             ->setDisplayTitle($this->getActualFileName());

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    public function renameUploadedFile() {
        //ex: 23_beautiful_photo.jpg
        $newFileName = $this->getId() . '_' . $this->getSanitizedFileName($this->getActualFileName());
        $existingFilePath = $this->getUploadRootDir() . $this->getFileName();
        $newFilePath = $this->getUploadRootDir() . $newFileName;

        $fileSize = filesize($existingFilePath);

        $fs = new Filesystem();
        try {
            $fs->rename($existingFilePath, $newFilePath);
            $this->setFileName($newFileName);
            $this->setMediaSize($fileSize);
        } catch (IOExceptionInterface $e) {
            echo "An error occurred while renaming the file " . $e->getPath();
        }
    }

    public function getSanitizedFileName($fileName) {
        $sanitized = preg_replace('/[^a-zA-Z0-9-_\.]/', '', $fileName);
        return $sanitized;
    }

    /**
     * Get the original file name.
     *
     * @return string
     */
    public function getPath() {
        return $this->actualFileName;
    }

    /**
     * Set the original file name
     *
     * @param string $actualFileName
     * @return \AppBundle\Entity\Media
     */
    public function setPath($actualFileName) {
        $this->actualFileName = $actualFileName;
        return $this;
    }

    /**
     * Get the file object.
     *
     * @return string
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * Set the file object.
     *
     * @param mixed $file
     * @return \AppBundle\Entity\Media
     */
    public function setFile($file) {
        $this->file = $file;
        return $this;
    }

    public function deleteMediaFile() {
        $file = $this->getAbsolutePath();
        if ($file) {
            unlink($file);
        }
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $actualFileName;

    /**
     * @var string
     */
    private $fileName;

    /**
     * @var string
     */
    private $displayTitle;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $contentType;

    /**
     * @var integer
     */
    private $mediaSize;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mediaCompany;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mediaContact;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mediaCompany = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mediaContact = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actualFileName
     *
     * @param string $actualFileName
     * @return Media
     */
    public function setActualFileName($actualFileName)
    {
        $this->actualFileName = $actualFileName;

        return $this;
    }

    /**
     * Get actualFileName
     *
     * @return string
     */
    public function getActualFileName()
    {
        return $this->actualFileName;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     * @return Media
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set displayTitle
     *
     * @param string $displayTitle
     * @return Media
     */
    public function setDisplayTitle($displayTitle)
    {
        $this->displayTitle = $displayTitle;

        return $this;
    }

    /**
     * Get displayTitle
     *
     * @return string
     */
    public function getDisplayTitle()
    {
        return $this->displayTitle;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Media
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set contentType
     *
     * @param string $contentType
     * @return Media
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get contentType
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Set mediaSize
     *
     * @param integer $mediaSize
     * @return Media
     */
    public function setMediaSize($mediaSize)
    {
        $this->mediaSize = $mediaSize;

        return $this;
    }

    /**
     * Get mediaSize
     *
     * @return integer
     */
    public function getMediaSize()
    {
        return $this->mediaSize;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Media
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return Media
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add mediaCompany
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaCompany
     * @return Media
     */
    public function addMediaCompany(\MediaBundle\Entity\MediaHistory $mediaCompany)
    {
        $this->mediaCompany[] = $mediaCompany;

        return $this;
    }

    /**
     * Remove mediaCompany
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaCompany
     */
    public function removeMediaCompany(\MediaBundle\Entity\MediaHistory $mediaCompany)
    {
        $this->mediaCompany->removeElement($mediaCompany);
    }

    /**
     * Get mediaCompany
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMediaCompany()
    {
        return $this->mediaCompany;
    }

    /**
     * Add mediaContact
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaContact
     * @return Media
     */
    public function addMediaContact(\MediaBundle\Entity\MediaHistory $mediaContact)
    {
        $this->mediaContact[] = $mediaContact;

        return $this;
    }

    /**
     * Remove mediaContact
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaContact
     */
    public function removeMediaContact(\MediaBundle\Entity\MediaHistory $mediaContact)
    {
        $this->mediaContact->removeElement($mediaContact);
    }

    /**
     * Get mediaContact
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMediaContact()
    {
        return $this->mediaContact;
    }
}
