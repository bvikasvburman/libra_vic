<?php
namespace NoteBundle\Controller;

use AppBundle\Component\AbstractController;

class NoteController extends AbstractController
{
    public function getPostData()
    {
        $postData = parent::getPostData();
        $params = $this->req->request->get('params');
        $bundle = $params['bundle'];

        $relTableObj = ['id' => $params['bundleEntityId']];

        $postData['module'] = $bundle;
        $postData[$bundle] = $relTableObj;

        // print_r($postData);
        // die();
        /* Format of the above $postData would be something like this.
           Doctrine would accept this after serializing this in to note entity (in the save action).
        [
            note => [
                id => 1,
                notes => 'bla bla bla',
                [
                    company => [
                        id => 121
                    ]
                ]
            ]
        ]
        */

        return $postData;
    }

}
