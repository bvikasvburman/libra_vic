<?php
namespace NoteBundle\Repository;

use AppBundle\Component\AbstractRepository;

class NoteRepository extends AbstractRepository
{
    public function getAlias() {
        return 'n';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('n')
              ->select('n');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'n.id', $id);
        }
        return $qb;
    }
}
