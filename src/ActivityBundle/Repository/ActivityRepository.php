<?php
namespace ActivityBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ActivityRepository extends AbstractRepository
{
    public function getAlias() {
        return 'a';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('a')
              ->select('a, uc, um')
              ->leftjoin('a.userCreated', 'uc')
              ->leftjoin('a.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'a.id', $id);
        }
        return $qb;
    }
}
