<?php
namespace ClaimBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ClaimRepository extends AbstractRepository
{
    public function getAlias() {
        return 'c';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('c')
              ->select('c, uc, um')
              ->leftjoin('c.userCreated', 'uc')
              ->leftjoin('c.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'c.id', $id);
        }
        return $qb;
    }
}
