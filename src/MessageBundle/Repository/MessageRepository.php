<?php
namespace MessageBundle\Repository;

use AppBundle\Component\AbstractRepository;

class MessageRepository extends AbstractRepository
{
    public function getAlias() {
        return 'm';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('m')
              ->select('m, uc, um')
              ->leftjoin('m.userCreated', 'uc')
              ->leftjoin('m.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'm.id', $id);
        }
        return $qb;
    }
}
