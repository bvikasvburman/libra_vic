<?php
namespace MessageBundle\Controller;

use AppBundle\Component\AbstractController;

class MessageController extends AbstractController
{
    public function getPostData()
    {
        $postData = parent::getPostData();

        $params = $this->req->request->get('params');
        $bundle = $params['bundle'];

        $relTableObj = ['id' => $params['bundleEntityId']];

        $postData['module'] = $bundle;
        $postData[$bundle]  = $relTableObj; //ex: $postData['company'] = ['id' => 66];

        // this is why the company field is saving.
        // We are setting company as an object for use within doctrine.
        // To set a relation (ex: company relation) as an object to Message entity,
        // all you need is the id of the company -- $postData['company'] = ['id' => 66];
        // Then the doctrine will handle the rest.

        // unset($postData['companyId']);
        // print_r($postData);
        // die();
        return $postData;
    }
}
