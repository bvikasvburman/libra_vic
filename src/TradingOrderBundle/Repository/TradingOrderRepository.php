<?php
namespace TradingOrderBundle\Repository;

use AppBundle\Component\AbstractRepository;

class TradingOrderRepository extends AbstractRepository
{
    public function getAlias() {
        return 'to';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('to')
              ->select('to, uc, um')
              ->leftjoin('to.userCreated', 'uc')
              ->leftjoin('to.userModified', 'um');
        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'to.id', $id);
        }
        return $qb;
    }
}
