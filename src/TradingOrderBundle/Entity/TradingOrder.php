<?php

namespace TradingOrderBundle\Entity;

class TradingOrder
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'TO';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $contactId;

    /**
     * @var integer
     */
    private $enquiryId;

    /**
     * @var integer
     */
    private $quotationId;

    /**
     * @var string
     */
    private $orderDetails;

    /**
     * @var string
     */
    private $clientOrderRef;

    /**
     * @var \DateTime
     */
    private $orderDate;

    /**
     * @var \DateTime
     */
    private $etd;

    /**
     * @var string
     */
    private $status;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $salesConfirmationM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \ContactBundle\Entity\Contact
     */
    private $contact;

    /**
     * @var \EnquiryBundle\Entity\Enquiry
     */
    private $enquiry;

    /**
     * @var \QuotationBundle\Entity\Quotation
     */
    private $quotation;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->salesConfirmationM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return TradingOrder
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return TradingOrder
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set contactId
     *
     * @param integer $contactId
     * @return TradingOrder
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;

        return $this;
    }

    /**
     * Get contactId
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set enquiryId
     *
     * @param integer $enquiryId
     * @return TradingOrder
     */
    public function setEnquiryId($enquiryId)
    {
        $this->enquiryId = $enquiryId;

        return $this;
    }

    /**
     * Get enquiryId
     *
     * @return integer
     */
    public function getEnquiryId()
    {
        return $this->enquiryId;
    }

    /**
     * Set quotationId
     *
     * @param integer $quotationId
     * @return TradingOrder
     */
    public function setQuotationId($quotationId)
    {
        $this->quotationId = $quotationId;

        return $this;
    }

    /**
     * Get quotationId
     *
     * @return integer
     */
    public function getQuotationId()
    {
        return $this->quotationId;
    }

    /**
     * Set orderDetails
     *
     * @param string $orderDetails
     * @return TradingOrder
     */
    public function setOrderDetails($orderDetails)
    {
        $this->orderDetails = $orderDetails;

        return $this;
    }

    /**
     * Get orderDetails
     *
     * @return string
     */
    public function getOrderDetails()
    {
        return $this->orderDetails;
    }

    /**
     * Set clientOrderRef
     *
     * @param string $clientOrderRef
     * @return TradingOrder
     */
    public function setClientOrderRef($clientOrderRef)
    {
        $this->clientOrderRef = $clientOrderRef;

        return $this;
    }

    /**
     * Get clientOrderRef
     *
     * @return string
     */
    public function getClientOrderRef()
    {
        return $this->clientOrderRef;
    }

    /**
     * Set orderDate
     *
     * @param \DateTime $orderDate
     * @return TradingOrder
     */
    public function setOrderDate($orderDate)
    {
        $this->orderDate = $orderDate;

        return $this;
    }

    /**
     * Get orderDate
     *
     * @return \DateTime
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * Set etd
     *
     * @param \DateTime $etd
     * @return TradingOrder
     */
    public function setEtd($etd)
    {
        $this->etd = $etd;

        return $this;
    }

    /**
     * Get etd
     *
     * @return \DateTime
     */
    public function getEtd()
    {
        return $this->etd;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return TradingOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return TradingOrder
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return TradingOrder
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return TradingOrder
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return TradingOrder
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return TradingOrder
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add salesConfirmationM
     *
     * @param \SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmationM
     * @return TradingOrder
     */
    public function addSalesConfirmationM(\SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmationM)
    {
        $this->salesConfirmationM[] = $salesConfirmationM;

        return $this;
    }

    /**
     * Remove salesConfirmationM
     *
     * @param \SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmationM
     */
    public function removeSalesConfirmationM(\SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmationM)
    {
        $this->salesConfirmationM->removeElement($salesConfirmationM);
    }

    /**
     * Get salesConfirmationM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSalesConfirmationM()
    {
        return $this->salesConfirmationM;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return TradingOrder
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return TradingOrder
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return TradingOrder
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contact
     *
     * @param \ContactBundle\Entity\Contact $contact
     * @return TradingOrder
     */
    public function setContact(\ContactBundle\Entity\Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \ContactBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set enquiry
     *
     * @param \EnquiryBundle\Entity\Enquiry $enquiry
     * @return TradingOrder
     */
    public function setEnquiry(\EnquiryBundle\Entity\Enquiry $enquiry = null)
    {
        $this->enquiry = $enquiry;

        return $this;
    }

    /**
     * Get enquiry
     *
     * @return \EnquiryBundle\Entity\Enquiry
     */
    public function getEnquiry()
    {
        return $this->enquiry;
    }

    /**
     * Set quotation
     *
     * @param \QuotationBundle\Entity\Quotation $quotation
     * @return TradingOrder
     */
    public function setQuotation(\QuotationBundle\Entity\Quotation $quotation = null)
    {
        $this->quotation = $quotation;

        return $this;
    }

    /**
     * Get quotation
     *
     * @return \QuotationBundle\Entity\Quotation
     */
    public function getQuotation()
    {
        return $this->quotation;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return TradingOrder
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return TradingOrder
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $purchaseOrderM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $manufacturingOrderM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $invoiceM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $claimM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $creditDebitNoteM;


    /**
     * Add purchaseOrderM
     *
     * @param \PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrderM
     * @return TradingOrder
     */
    public function addPurchaseOrderM(\PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrderM)
    {
        $this->purchaseOrderM[] = $purchaseOrderM;

        return $this;
    }

    /**
     * Remove purchaseOrderM
     *
     * @param \PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrderM
     */
    public function removePurchaseOrderM(\PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrderM)
    {
        $this->purchaseOrderM->removeElement($purchaseOrderM);
    }

    /**
     * Get purchaseOrderM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchaseOrderM()
    {
        return $this->purchaseOrderM;
    }

    /**
     * Add manufacturingOrderM
     *
     * @param \ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrderM
     * @return TradingOrder
     */
    public function addManufacturingOrderM(\ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrderM)
    {
        $this->manufacturingOrderM[] = $manufacturingOrderM;

        return $this;
    }

    /**
     * Remove manufacturingOrderM
     *
     * @param \ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrderM
     */
    public function removeManufacturingOrderM(\ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrderM)
    {
        $this->manufacturingOrderM->removeElement($manufacturingOrderM);
    }

    /**
     * Get manufacturingOrderM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getManufacturingOrderM()
    {
        return $this->manufacturingOrderM;
    }

    /**
     * Add invoiceM
     *
     * @param \InvoiceBundle\Entity\Invoice $invoiceM
     * @return TradingOrder
     */
    public function addInvoiceM(\InvoiceBundle\Entity\Invoice $invoiceM)
    {
        $this->invoiceM[] = $invoiceM;

        return $this;
    }

    /**
     * Remove invoiceM
     *
     * @param \InvoiceBundle\Entity\Invoice $invoiceM
     */
    public function removeInvoiceM(\InvoiceBundle\Entity\Invoice $invoiceM)
    {
        $this->invoiceM->removeElement($invoiceM);
    }

    /**
     * Get invoiceM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoiceM()
    {
        return $this->invoiceM;
    }

    /**
     * Add claimM
     *
     * @param \ClaimBundle\Entity\Claim $claimM
     * @return TradingOrder
     */
    public function addClaimM(\ClaimBundle\Entity\Claim $claimM)
    {
        $this->claimM[] = $claimM;

        return $this;
    }

    /**
     * Remove claimM
     *
     * @param \ClaimBundle\Entity\Claim $claimM
     */
    public function removeClaimM(\ClaimBundle\Entity\Claim $claimM)
    {
        $this->claimM->removeElement($claimM);
    }

    /**
     * Get claimM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClaimM()
    {
        return $this->claimM;
    }

    /**
     * Add creditDebitNoteM
     *
     * @param \CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNoteM
     * @return TradingOrder
     */
    public function addCreditDebitNoteM(\CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNoteM)
    {
        $this->creditDebitNoteM[] = $creditDebitNoteM;

        return $this;
    }

    /**
     * Remove creditDebitNoteM
     *
     * @param \CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNoteM
     */
    public function removeCreditDebitNoteM(\CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNoteM)
    {
        $this->creditDebitNoteM->removeElement($creditDebitNoteM);
    }

    /**
     * Get creditDebitNoteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCreditDebitNoteM()
    {
        return $this->creditDebitNoteM;
    }
}
