<?php
namespace TranslationBundle\Repository;

use AppBundle\Component\AbstractRepository;

class TranslationRepository extends AbstractRepository
{
    public function getAlias() {
        return 't';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('t')
              ->select('t, uc, um')
              ->leftjoin('t.userCreated', 'uc')
              ->leftjoin('t.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 't.id', $id);
        }
        return $qb;
    }
}
