<?php

namespace QuotationBundle\Entity;

class Quotation
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'Q';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $contactId;

    /**
     * @var integer
     */
    private $enquiryId;

    /**
     * @var string
     */
    private $salesDeliveryTerms;

    /**
     * @var string
     */
    private $portOfDelivery;

    /**
     * @var string
     */
    private $shippingMedium;

    /**
     * @var integer
     */
    private $offerValidity;

    /**
     * @var string
     */
    private $salesPaymentTerms;

    /**
     * @var integer
     */
    private $leadTime;

    /**
     * @var \DateTime
     */
    private $quotationDate;

    /**
     * @var string
     */
    private $status;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tradingOrderM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \EnquiryBundle\Entity\Enquiry
     */
    private $enquiry;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tradingOrderM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Quotation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return Quotation
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set contactId
     *
     * @param integer $contactId
     * @return Quotation
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;

        return $this;
    }

    /**
     * Get contactId
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set enquiryId
     *
     * @param integer $enquiryId
     * @return Quotation
     */
    public function setEnquiryId($enquiryId)
    {
        $this->enquiryId = $enquiryId;

        return $this;
    }

    /**
     * Get enquiryId
     *
     * @return integer
     */
    public function getEnquiryId()
    {
        return $this->enquiryId;
    }

    /**
     * Set salesDeliveryTerms
     *
     * @param string $salesDeliveryTerms
     * @return Quotation
     */
    public function setSalesDeliveryTerms($salesDeliveryTerms)
    {
        $this->salesDeliveryTerms = $salesDeliveryTerms;

        return $this;
    }

    /**
     * Get salesDeliveryTerms
     *
     * @return string
     */
    public function getSalesDeliveryTerms()
    {
        return $this->salesDeliveryTerms;
    }

    /**
     * Set portOfDelivery
     *
     * @param string $portOfDelivery
     * @return Quotation
     */
    public function setPortOfDelivery($portOfDelivery)
    {
        $this->portOfDelivery = $portOfDelivery;

        return $this;
    }

    /**
     * Get portOfDelivery
     *
     * @return string
     */
    public function getPortOfDelivery()
    {
        return $this->portOfDelivery;
    }

    /**
     * Set shippingMedium
     *
     * @param string $shippingMedium
     * @return Quotation
     */
    public function setShippingMedium($shippingMedium)
    {
        $this->shippingMedium = $shippingMedium;

        return $this;
    }

    /**
     * Get shippingMedium
     *
     * @return string
     */
    public function getShippingMedium()
    {
        return $this->shippingMedium;
    }

    /**
     * Set offerValidity
     *
     * @param integer $offerValidity
     * @return Quotation
     */
    public function setOfferValidity($offerValidity)
    {
        $this->offerValidity = $offerValidity;

        return $this;
    }

    /**
     * Get offerValidity
     *
     * @return integer
     */
    public function getOfferValidity()
    {
        return $this->offerValidity;
    }

    /**
     * Set salesPaymentTerms
     *
     * @param string $salesPaymentTerms
     * @return Quotation
     */
    public function setSalesPaymentTerms($salesPaymentTerms)
    {
        $this->salesPaymentTerms = $salesPaymentTerms;

        return $this;
    }

    /**
     * Get salesPaymentTerms
     *
     * @return string
     */
    public function getSalesPaymentTerms()
    {
        return $this->salesPaymentTerms;
    }

    /**
     * Set leadTime
     *
     * @param integer $leadTime
     * @return Quotation
     */
    public function setLeadTime($leadTime)
    {
        $this->leadTime = $leadTime;

        return $this;
    }

    /**
     * Get leadTime
     *
     * @return integer
     */
    public function getLeadTime()
    {
        return $this->leadTime;
    }

    /**
     * Set quotationDate
     *
     * @param \DateTime $quotationDate
     * @return Quotation
     */
    public function setQuotationDate($quotationDate)
    {
        $this->quotationDate = $quotationDate;

        return $this;
    }

    /**
     * Get quotationDate
     *
     * @return \DateTime
     */
    public function getQuotationDate()
    {
        return $this->quotationDate;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Quotation
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return Quotation
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return Quotation
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return Quotation
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Quotation
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return Quotation
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add tradingOrderM
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrderM
     * @return Quotation
     */
    public function addTradingOrderM(\TradingOrderBundle\Entity\TradingOrder $tradingOrderM)
    {
        $this->tradingOrderM[] = $tradingOrderM;

        return $this;
    }

    /**
     * Remove tradingOrderM
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrderM
     */
    public function removeTradingOrderM(\TradingOrderBundle\Entity\TradingOrder $tradingOrderM)
    {
        $this->tradingOrderM->removeElement($tradingOrderM);
    }

    /**
     * Get tradingOrderM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTradingOrderM()
    {
        return $this->tradingOrderM;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return Quotation
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return Quotation
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return Quotation
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set enquiry
     *
     * @param \EnquiryBundle\Entity\Enquiry $enquiry
     * @return Quotation
     */
    public function setEnquiry(\EnquiryBundle\Entity\Enquiry $enquiry = null)
    {
        $this->enquiry = $enquiry;

        return $this;
    }

    /**
     * Get enquiry
     *
     * @return \EnquiryBundle\Entity\Enquiry
     */
    public function getEnquiry()
    {
        return $this->enquiry;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return Quotation
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return Quotation
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
