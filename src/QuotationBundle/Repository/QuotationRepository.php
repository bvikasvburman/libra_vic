<?php
namespace QuotationBundle\Repository;

use AppBundle\Component\AbstractRepository;

class QuotationRepository extends AbstractRepository
{
    public function getAlias() {
        return 'q';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('q')
              ->select('q,
                        partial e.{id, code},
                        partial c.{id, code, name},
                        uc, um')
              ->leftjoin('q.enquiry', 'e')
              ->leftjoin('q.company', 'c')
              ->leftjoin('q.userCreated', 'uc')
              ->leftjoin('q.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'q.id', $id);
        }
        return $qb;
    }
}
