<?php
namespace AccountsReceivableBundle\Repository;

use AppBundle\Component\AbstractRepository;

class AccountsReceivableRepository extends AbstractRepository
{
    public function getAlias() {
        return 'ar';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('ar')
              ->select('ar, uc, um')
              ->leftjoin('ar.userCreated', 'uc')
              ->leftjoin('ar.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'ar.id', $id);
        }
        return $qb;
    }
}
