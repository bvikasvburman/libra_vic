<?php

namespace AppBundle\EntityListener;

use Doctrine\ORM\Event\LifecycleEventArgs;

class UniqueCodeGenerator
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        //if an entity contains a method called getCodePrefix then we shall generate the code.
        if (method_exists($entity, 'getCodePrefix')) {
            $codePrefix = $entity->getCodePrefix();
            $em = $args->getEntityManager();
            $code = $codePrefix . str_pad($entity->getId(), 6, "0", STR_PAD_LEFT);
            $entity->setCode($code);
            $em->persist($entity);
            $em->flush();
        }
    }
}

