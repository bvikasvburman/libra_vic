<?php

namespace AppBundle\Controller;

use \Symfony\Component\HttpFoundation\Request;

/**
 * This interface contains a minimum set of methods needed for the RESTful API.
 *
 * Each controller that acts as an interface between the client and one of the app
 * entities should follow the Richardson Maturity Model Level 2 and offers methods
 * to fetch and save model data.
 *
 */
interface ModelRestInterface
{
    /**
     * Fetch one.
     *
     * @methods GET
     */
    public function getOneAction($id);

    /**
     * Fetch all.
     *
     * @methods GET
     */
    public function getListAction();

    /**
     * Create or update.
     *
     * @methods [PUT, POST]
     */
    public function saveAction(Request $request, $id);
}
