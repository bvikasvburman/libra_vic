<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

use Jaspersoft\Client\Client;

class PrintController extends Controller
{

    public $client;
    private $mimeTypes = [
        'html' => 'text/html',
        'pdf'  => 'application/pdf',
        'xls'  => 'application/vnd.ms-excel',
        'csv'  => 'text/csv',
        'docx' => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'rtf'  => 'text/rtf',
        'odt'  => 'application/vnd.oasis.opendocument.text',
        'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
        'xlsx' => 'application/vnd.ms-excel'
    ];

    public function testAction()
    {
        $this->setClient();

        // fast jasperserver testing
        //print_r($this->client->serverInfo()); die;
        echo $this->client->reportService()
                ->runReport('/reports/interactive/MapReport', 'html');
        exit;
    }

    public function listAction($reportName, $format)
    {
        $this->setClient();

        $report = $this->client->reportService()
            ->runReport("/reports/$reportName", $format);

        if ($format !== 'html') {
            $this->setHeaders(strlen($report), $format);
        }

        echo $report;

        exit;
    }

    public function index2Action($reportName, $format)
    {
        $this->setClient();

        // fast jasperserver testing
        //print_r($this->client->serverInfo()); die;
        //echo $this->client->reportService()->runReport('/reports/interactive/MapReport', 'html');
        //die;

        $report = $this->client->reportService()
            ->runReport("/reports/$reportName/", $format);

        if ($format !== 'html') {
            $this->setHeaders(strlen($report), $format);
        }

        echo $report;

        exit;
    }

    private function setClient()
    {
        $this->client = new Client(
            $this->getParameter('jasper_host'),
            $this->getParameter('jasper_user'),
            $this->getParameter('jasper_password')
        );
    }

    private function setHeaders($length, $format)
    {
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=report.'.$format);
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . $length);
        if(isset($this->mimeTypes[$format])) {
            header('Content-Type: ' . $this->mimeTypes[$format]);
        } else {
            header('Content-Type: application/octet-stream');
        }
    }
}


