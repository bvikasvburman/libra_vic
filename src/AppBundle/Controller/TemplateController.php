<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * TemplateController is used to load twig templates by AngularJS on the frontend.
 *
 */
class TemplateController extends Controller
{
    /**
     * Load a template using the template path.
     *
     * @param Request $request
     * @param string $templatePath
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getAction(Request $request, $templatePath)
    {
        $tplPathArr = explode('/', $templatePath);

        $pathVars = $this->getPathVars($tplPathArr);

        //ex: template path should be like ex: /company/company/list OR
        //ex: /dashboard
        if (count($tplPathArr) < 2) {
            return $this->render('AppBundle:Template:template-not-found.html.twig', array(
                'templatePath' => $templatePath, 'generatedPath' => ''
            ));
        }


        $path = $this->createTemplatePathFromElements($tplPathArr);
        $templName = $tplPathArr[sizeof($tplPathArr) - 1];

        //Add to global twig variables
        $twig = $this->get('twig');
        $cpBundle = $pathVars['bundle'];
        $cpView   = $pathVars['view'];
        $cpTab    = $pathVars['tab'];

        $cpBundleName = ucwords($cpBundle);
        $cpViewName   = ucwords($cpView);
        $cpTabName    = ucwords($cpTab);

        $twig->addGlobal('cpBundle', $cpBundle);
        $twig->addGlobal('cpView', $cpView);
        $twig->addGlobal('cpTab', $cpTab);
        $twig->addGlobal('cpBundleName', $cpBundleName);
        $twig->addGlobal('cpViewName', $cpViewName);
        $twig->addGlobal('cpTabName', $cpTabName);
        $twig->addGlobal('cpLink', $cpTab);

        if ($this->get('templating')->exists($path)) {
            return $this->render($path);
        } else {
            return $this->render('AppBundle:Template:template-not-found.html.twig', array(
                'templatePath' => $templatePath, 'generatedPath' => $path
            ));
        }

    }

    /**
     * Get path vars from static path
     *
     * @param array $tplPathArr
     * @return array
     */
    public function getPathVars(array $tplPathArr)
    {
        $tplType    = $tplPathArr[0]; // main / link
        $bundle     = isset($tplPathArr[1]) ? $tplPathArr[1] : '';
        $controller = isset($tplPathArr[2]) ? $tplPathArr[2] : '';
        $view       = isset($tplPathArr[3]) ? $tplPathArr[3] : '';
        $tab        = isset($tplPathArr[4]) ? $tplPathArr[4] : '';

        return array(
            'tplType' => $tplType, //main / link
            'bundle' => $bundle,
            'controller' => $controller,
            'view' => $view, //it can be a main view or link view
            'tab' => $tab, //this is tab/link within main bundle. there's no tab within link edit/detail
        );

    }

    /**
     * Create the real path to a twig file based on the simple path provided.
     *
     * @param array $tplPathArr
     * @return string
     */
    public function createTemplatePathFromElements(array $tplPathArr)
    {
        $pathSize = sizeof($tplPathArr);

        $pathVars = $this->getPathVars($tplPathArr);

        $tplType    = $pathVars['tplType'];
        $bundle     = $pathVars['bundle'];
        $controller = $pathVars['controller'];
        $view       = $pathVars['view'];
        $tab        = $pathVars['tab'];

        $path = '';
        if ($tplType == 'link') {
            //$bundle here indicates the bundle name of the link room
            // if we Company>Contact relation then the $bundle = Contact

            //ContactBundle:Contact:link-edit.html.twig
            $path = ucfirst($bundle) . 'Bundle' . ':' .
                    ucfirst($controller) . ':';
            $path .= 'link-' . $view . '.html.twig';

        } else if ($tplType == 'main') {
            $path = ucfirst($bundle) . 'Bundle' . ':' .
                    ucfirst($controller) . ':';

            if ($pathSize >= 5) {
                // ex URL: /company/company/detail/main
                // bundle:company
                // controller:company
                // view:detail
                // tab (or link): main
                //template for tab / link
                $path2 = $path . 'tabs/' . $tab . '.html.twig';
                if ($this->get('templating')->exists($path2)) {
                    $path = $path2;
                } else {
                    $path = $path . 'tabs/link-tab.html.twig';
                }

            } else if ($pathSize >= 4) {
                // ex URL: /company/company/detail
                //template for list, detail, edit, new
                $path .= $view . '.html.twig';
            }

        } else if ($tplType == 'other') {
            //just for clarity re-assign the variables with proper names
            $page    = $pathVars['bundle'];
            $subPage = $pathVars['controller'];
            $action  = $pathVars['view'];
            $page2   = ucfirst($page);

            if ($page == 'dashboard') {
                $path = 'AppBundle:Dashboard:index.html.twig';
            } else {
                //AppBundle:Gantt:task-edit.html.twig
                $path = "AppBundle:{$page2}:{$subPage}-{$action}.html.twig";
            }
        }

        return $path;
    }

}
