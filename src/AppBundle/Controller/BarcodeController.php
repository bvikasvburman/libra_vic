<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BarcodeController extends Controller
{
    /**
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    // public function indexAction($name, $value)
    // {

    //     // $name
    //     $options = array(
    //         'code'   => $value,
    //         'type'   => 'c39',
    //         'format' => 'png',
    //         'width'  => 250,
    //         'height' => 45,
    //         'color'  => array(0, 0, 0),
    //     );

    //     $barcode = $this->get('sgk_barcode.generator')->generate($options);

    //     return new Response($barcode);

    // }

    public function indexAction($name, $value)
    {
        $type = 'code39';
        $bservice = $this->container->get('mopa_barcode.barcode_service');

        $options = array(
            'useOverlay' => true,
            'noCache' => false,
            'barcodeOptions' => array(
                'barHeight' => 40,
                'drawText' => false,
                'barThickWidth' => 5,
                'barThinWidth' => 2,
                'withQuietZones' => false,
            ),
            'rendererOptions' => array(
                'imageType' => 'png',
                'height' => 40,
                'width' => 220,
                ),
        );

        return new Response(
            file_get_contents($file = $bservice->get($type, $value, true, $options)),
            200,
            array(
                'Content-Type'          => 'image/png',
                'Content-Disposition'   => 'filename="'.$file.'"'
            )
        );


    }
}


