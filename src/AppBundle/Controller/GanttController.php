<?php
namespace AppBundle\Controller;

use AppBundle\Component\AbstractController;
use AppBundle\Component\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class GanttController extends AbstractController
{
    public function indexAction($ganttName, $ganttId)
    {
        $sVars = $this->dbUtil->getSearchVarsArr($this->req);
        $sVars->criteria = [
            'ganttName' => $ganttName,
            'ganttId' => $ganttId,
        ];
        $sOpts = $this->dbUtil->getSearchOpts($this->req);

        $ganttRows = $this->getOperation('gantt')->getGanttData($sVars, $sOpts);
        return new JsonResponse($this->container, $ganttRows);
    }

    public function saveTaskAction(Request $req)
    {
        $postData = $req->request->get('ganttTask');
        $deSerTask = $this->getOperation('gantt')->saveTask($postData);

        $retArr = array('taskId' => $deSerTask->getId());
        return new JsonResponse($this->container, $retArr);
    }

    public function deleteTaskAction(Request $req, $taskId)
    {
        $this->getOperation('gantt')->deleteTask($taskId);
        return new Response();
    }

    public function saveRowAction(Request $req, $ganttName, $ganttId)
    {
        $postData = $req->request->get('ganttRow');
        $ganttRow = $this->getOperation('gantt')->saveRow($postData, $ganttName, $ganttId);
        $ganttRow = ['ganttRow' => $ganttRow];

        return new JsonResponse($this->container, $ganttRow);
    }

    public function deleteRowAction(Request $req, $rowId)
    {
        try {
            $this->getOperation('gantt')->deleteRow($rowId);
        } catch (\Exception $e) {
            return new JsonResponse($this->container, [
                    'status' => 'error',
                    'message' => "You must delete all the tasks in a row before deleting it."
                ],
                ['status' => 500]
            );
        }

        return new JsonResponse($this->container, []);
    }

}

