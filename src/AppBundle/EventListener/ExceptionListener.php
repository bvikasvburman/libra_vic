<?php

namespace AppBundle\EventListener;

use Symfony\Component\Debug\ExceptionHandler;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;

class ExceptionListener extends ExceptionHandler
{
    private $logger;
    private $prevExceptionHandler;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

        // Set our handle method as fatal exception handler.
        // It is required to extend Symfony\Component\Debug\ExceptionHandler
        $this->prevExceptionHandler = set_exception_handler(array($this, 'handle'));
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
    }

    /**
     * Handles non fatal exceptions (normal way).
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();

        // Log exception.
        $this->logger->error($exception->getMessage());

        // ...
    }

    /**
     * Overwrite ExceptionHandler method.
     */
    public function handle(\Exception $exception) {
        // Call our custom handler.
        $this->onFatalErrorException($exception);

        // Call exception handler that was overridden.
        // Or try to call parent::handle($exception)
        if (is_array($this->prevExceptionHandler) && $this->prevExceptionHandler[0] instanceof ExceptionHandler) {
            $this->prevExceptionHandler[0]->handle($exception);
        }
    }

    public function onFatalErrorException(\Exception $exception)
    {
        // Do anything you want...
        $this->logger->error('Hey, I got it: '. $exception->getMessage());
    }
}

