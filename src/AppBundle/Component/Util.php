<?php

namespace AppBundle\Component;

use Underscore\Types\Strings;
use \Symfony\Component\DependencyInjection\ContainerAware;


class Util
{
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public static function fixJSON($json)
    {
        $regex = <<<'REGEX'
~
    "[^"\\]*(?:\\.|[^"\\]*)*"
    (*SKIP)(*F)
  | '([^'\\]*(?:\\.|[^'\\]*)*)'
~x
REGEX;

        return preg_replace_callback($regex, function($matches) {
            return '"' . preg_replace('~\\\\.(*SKIP)(*F)|"~', '\\"', $matches[1]) . '"';
        }, $json);
    }

    public static function camelToUnderScore($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    public static function humanizeStr($str)
    {
        $str = Strings::title(Strings::toSnakeCase($str));
        $str = str_replace('_', ' ', $str);

        return $str;
    }

    public function getCurrentBundleName($camelCase = false)
    {
        $request = $this->container->get('request');
        $route = $request->get('_route'); // supposed to be for ex: api_company
        $routeArr = split('_', $route);

        $bundleName = ucfirst($routeArr[1]); // Company
        $bundleNameCC = $routeArr[1]; // company or colorMaster

        if ($camelCase) {
            return $bundleNameCC;
        } else {
            return $bundleName;
        }
    }

    public function getCurrentLinkName($camelCase = false)
    {
        $request = $this->container->get('request');
        $route = $request->get('_route'); // supposed to be for ex: api_company_link_contact
        $routeArr = split('_', $route);

        $linkName   = ucfirst($routeArr[3]); // Contact
        $linkNameCC = $routeArr[3]; // contact

        if ($camelCase) {
            return $linkNameCC;
        } else {
            return $linkName;
        }
    }

    /**
     * Gives the current URL state information like BundleName, LinkName, Link Type etc
     *
     * @return array Array of information
     */
    public function getCurrentStateInfo($camelCase = false)
    {
        $request = $this->container->get('request');
        $route = $request->get('_route'); // supposed to be for ex: api_company_link_contact
        $routeArr = split('_', $route);

        $arr = array();
        $bundleName          = ucfirst($routeArr[1]); // Company
        $bundleNameCC        = $routeArr[1]; // company
        $linkName            = ucfirst($routeArr[3]); // Contact
        $linkNameCC          = $routeArr[3]; // contact
        $linkTableManyToMany = $routeArr[3]; // contact

        $arr['bundleName']          = $bundleName;
        $arr['bundleNameCC']        = $bundleNameCC;
        $arr['linkName']            = $linkName;
        $arr['linkNameCC']          = $linkNameCC;
        $arr['linkTableOneToMany']  = $linkTableOneToMany ;
        $arr['linkTableManyToMany'] = $linkTableManyToMany;
    }

    public function hasStringKeys(array $array)
    {
        return count(array_filter(array_keys($array), 'is_string')) > 0;
    }

}
