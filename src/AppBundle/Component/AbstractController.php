<?php

namespace AppBundle\Component;

use Symfony\Component\HttpFoundation\JsonResponse as SymfonyJsonResponse;
use AppBundle\Component\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Controller\ModelRestInterface;
use AppBundle\Component\Util;
use AppBundle\Model\InitializableControllerInterface;

use Doctrine\ORM\Query;

/**
 * Abstract type Controller class to make certain repeated things common
 *
 */
class AbstractController extends Controller implements ModelRestInterface, InitializableControllerInterface
{
    protected $util;
    protected $req;
    protected $dbUtil;

    public function initialize(Request $req)
    {
        $this->util   = $this->get('app.util');
        $this->dbUtil = $this->container->get('app.dbutil');
        $this->em     = $this->container->get('doctrine')->getEntityManager();
        $this->req = $req;
    }

    public function getOneAction($id)
    {
        $bundleName = $this->util->getCurrentBundleName($this->container);
        $retType = $this->req->query->get('retType');
        $searchOpts = $this->dbUtil->getSearchOpts($this->req);

        $row = $this->getOperation($bundleName)->getOne($id, $searchOpts);
        // \Doctrine\Common\Util\Debug::dump($row);
        // die();
        return new JsonResponse($this->container, $row, ['group' => $retType]);
    }

    public function getListAction()
    {

        $bundleName = $this->util->getCurrentBundleName();
        $retType = $this->req->query->get('retType');
        $searchVars = $this->dbUtil->getSearchVarsArr($this->req);
        $searchOpts = $this->dbUtil->getSearchOpts($this->req);

        $rows = $this->getOperation($bundleName)->getList($searchVars, $searchOpts);
        return new JsonResponse($this->container, $rows, ['group' => $retType]);
    }

    public function getListLinkAction($id)
    {
        $searchVars = $this->dbUtil->getSearchVarsArr($this->req);

        $bundleName = $this->util->getCurrentBundleName();
        $linkRows = $this->getOperation($bundleName)->getListLink($id, $searchVars);
        return new JsonResponse($this->container, $linkRows);
    }

    public function getOneLinkAction($id, $idl)
    {
        $searchVars = $this->dbUtil->getSearchVarsArr($this->req);

        $bundleName = $this->util->getCurrentBundleName(); //bundle: company, link: contact

        $linkRows = $this->getOperation($bundleName)->getOneLink($id, $idl, $searchVars);

        return new JsonResponse($this->container, $linkRows);
    }

    public function getPostData()
    {
        $bundleNameCC = $this->util->getCurrentBundleName(true);
        $postData     = $this->req->request->get($bundleNameCC);
        return $postData;
    }

    public function saveAction(Request $req, $id)
    {
        $bundleName   = $this->util->getCurrentBundleName();
        $postData     = $this->getPostData();

        if (!$postData['id']) {
            unset($postData['id']);
        }
        $currBundleRow = $this->getOperation($bundleName)->save($postData);
        return new JsonResponse($this->container, $currBundleRow);
    }

    public function saveLinkAction($id, $idl = null, $relEntity = null)
    {

        $bundleName = $this->util->getCurrentBundleName();
        $linkNameCC = $this->container->get('app.util')->getCurrentLinkName(true);
        $postData   = $this->req->request->get($linkNameCC);

        // print_r($postData);
        // die();

        //saving an link entity new record
        if (!$postData['id']) {
            unset($postData['id']);
        }
        $currBundleRow = $this->getOperation($bundleName)->saveLink($postData, $id, $idl, $relEntity);
        return new JsonResponse($this->container, $currBundleRow);
    }

    public function uploadMediaAction($id, $relEntity = null)
    {
        // \Doctrine\Common\Util\Debug::dump($this->req->files);
        $bundleName = $this->util->getCurrentBundleName();
        $mediaRows  = $this->getOperation($bundleName)->uploadMedia($this->req->files, $id, $relEntity);

        return new JsonResponse($this->container, $mediaRows);
    }

    public function deleteAction(Request $req, $id)
    {
        $bundleName = $this->util->getCurrentBundleName(true);
        $this->getOperation($bundleName)->delete($id);

        return new Response();
    }

    public function deleteLinkAction(Request $req, $id, $idl,
                                     $relEntity = null, $deleteTarget = false, $targetEntity = null)
    {
        $bundleName = $this->util->getCurrentBundleName(true);
        $this->getOperation($bundleName)->deleteLink($id, $idl, $relEntity, $deleteTarget, $targetEntity);

        return new Response();
    }

    public function getOperation($bundleName)
    {
        return $this->get("{$bundleName}Op");
    }
}
