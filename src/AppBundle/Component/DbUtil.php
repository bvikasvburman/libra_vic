<?php

namespace AppBundle\Component;

use Underscore\Types\Strings;
use Symfony\Component\HttpFoundation\Request;

class DbUtil
{

    protected $util;

    public function __construct(\AppBundle\Component\Util $util)
    {
        $this->util = $util;
    }

    public function setWhere($qb, $fldName, $criteria, $searchType = '')
    {
        $fldNameArr = explode('.', $fldName);
        $alias = '';
        if (count($fldNameArr) > 1) {
            //ex: c.company_id
            $alias   = $fldNameArr[0]; // c
            $fldName = $fldNameArr[1]; // company_id
        }

        if (is_array($criteria)) {
            if (isset($criteria[$fldName])) {
                $qb->where("{$alias}.{$fldName} = :{$fldName}")
                   ->setParameter(":{$fldName}", $criteria[$fldName]);
            }
        } else { //directly sending a value ex: $id
            $value = $criteria;
            if ($value) {
                $qb->andWhere("{$alias}.{$fldName} = :{$fldName}")
                   ->setParameter(":{$fldName}", $value);
            }
        }

        return $qb;
    }

    public function setOrderBy($qb, $sortArr)
    {
        foreach ($sortArr as $sortCol => $sortOrder) {
            $qb->addOrderBy($sortCol, $sortOrder);
        }

        return $qb;
    }

    public function getReferenceIdNameFromBundle($bundleName)
    {
        //if $bundleName is TradingOrder then it will return trading_order_id
        $idName = Strings::toSnakeCase($bundleName) . '_id';

        // The function toSnakeCase somehow wrongly returns _trading_order_id.
        // So fix it by removing the first underscore
        if (substr($idName, 0, 1) == '_') {
            $idName = substr($idName, 1);
        }

        return $idName;
    }

    public function getIdNameFromTargetEntity($entityShortName)
    {
        $tmpArr = explode(":", $entityShortName); //ex: MediaBundle:Media
        $idName = Strings::toPascalCase($tmpArr[1]) . 'Id'; //MediaId or TradingOrderId

        return $idName;
    }

    public static function getAliasFromEntity($entityName)
    {
        //if $bundleName is TradingOrder then it should return 'to'
        $entity = Strings::toSnakeCase($entityName);

        // The function toSnakeCase somehow wrongly returns _trading_order_id.
        // So fix it by removing the first underscore
        if (substr($entity, 0, 1) == '_') {
            $entity = substr($entity, 1);
        }

        $arr = explode('_', $entity);
        $alias = '';
        foreach ($arr as $value) {
            $alias .= $value[0];
        }

        return $alias;
    }

    public function getEntityPathFromShortName($relEntity, $defaultEntity)
    {
        $entityPath = '';

        if ($relEntity) {
            $tmpArr = explode(":", $relEntity); //ex: MediaBundle:MediaHistory
            $entityPath = $tmpArr[0] . "\Entity\\" . $tmpArr[1];
        } else {
            $entityPath = "{$defaultEntity}Bundle\Entity\\{$defaultEntity}";
        }

        return $entityPath;
    }

    public function setRecordCreationDetails($entityDeSerialized, $container)
    {
        $user = $container->get('security.context')->getToken()->getUser();

        if (method_exists($entityDeSerialized, 'setUserCreated')) {
            $entityDeSerialized->setUserModified($user);
            if (!$entityDeSerialized->getId()) { //new record
                $entityDeSerialized->setUserCreated($user);
            }
        }

        return $entityDeSerialized;
    }

    public function getSearchVarsArr(Request $req, $criteria = [], $sort = [], $limit = '', $offset = '')
    {
        $retObj = new \stdClass();
        $retObj->criteria = [];
        $retObj->sort = [];
        $retObj->limit = '';
        $retObj->offset = '';

        if (count($criteria) == 0) {
            $retObj->criteria = $req->query->get('criteria') ? $req->query->get('criteria') : [];
        }
        if (count($sort) == 0) {
            $retObj->sort = $req->query->get('sort') ? $req->query->get('sort') : [];
            $retObj->sort = $this->getNormalizedSortArr($retObj->sort);
        }
        if ($limit == '') {
            $retObj->limit = $req->query->get('limit') ? $req->query->get('limit') : '';
        }
        if ($offset == '') {
            $retObj->offset = $req->query->get('offset') ? $req->query->get('offset') : '';
        }

        return $retObj;
    }

    public function getSearchOpts(Request $req)
    {
        $retType = $req->query->get('retType');

        $retObj = new \stdClass();
        $retObj->distinct = false;
        $retObj->retType  = $retType;

        return $retObj;
    }

    public function getNormalizedSortArr(array $arr)
    {
        $retArr = [];
        $hasStringKeys = $this->util->hasStringKeys($arr);

        // ex: $sortArr = array("0" => "title", "0" => "code")
        // ex2: $sortArr = array("title" => "asc", "code" => "desc")

        foreach ($arr as $key => $value) {
            if ($hasStringKeys) {
                $retArr[$key] = $value;
            } else {
                $retArr[$value] = 'asc';
            }
        }

        // ex return array: $retArr = array("title" => "asc", "code" => "desc") however the input be.
        return $retArr;
    }

    function getEntityColumnValues($entity, $em){
        $cols = $em->getClassMetadata(get_class($entity))->getColumnNames();
        $values = array();
        foreach($cols as $col){
            $colCC = Strings::toCamelCase($col);
            $col = Strings::toPascalCase($col);
            $getter = 'get' . ucfirst($col); //getFirstName()

            $value  = $entity->$getter();
            if ($value instanceof \DateTime) {
                $value = $value->format('Y-m-d\TH:i:sO');
            }
            $values[$colCC] = $value;
        }

        $cols = $em->getClassMetadata(get_class($entity))->getAssociationNames();
        foreach($cols as $col){
            $colCC = Strings::toCamelCase($col);
            $col = Strings::toPascalCase($col);
            $getter = 'get' . ucfirst($col); //getUserCreated()

            $value  = $entity->$getter();
            $values[$colCC] = $value;
        }
        return $values;
    }

    /**
     * When the DQL contains some related cols from a JOIN with no unique id
     * the doctrine will put the result cols with no id as below for ex:
     * [{
        "0": {"id":2,"artNo":"100001","productName":"Rucksack 1","sampleReadyDate":"2016-05-25","requestedQty":1000,"weight":144,"capacity":32,"dimLength":11,"dimWidth":12,"dimHeight":12,"userIdCreated":1,"userIdModified":1,"creationDate":"2016-03-06T18:48:43+0800",...},
        "enquiry":{...},
        "userCreated":{...},
        "userModified":{...},
        "productFamily":"Bags",
        "productType":"Weel bag"}]
     *
     * We want the above vals merged in to one index as below for ex:
     * {
        "id":2,"artNo":"100001","productName":"Rucksack 1","sampleReadyDate":"2016-05-25",...
        "productFamily":"Bags",
        "productType":"Weel bag"
        },
     *
     * We are merging this in to one object for easy use of the this object in the angular app
     */
    function getRowAsArrayForCustomDQL($rowDb, $em) {
        $row = [];

        // return $row;
        // \Doctrine\Common\Util\Debug::dump($rowDb);
        // die();

        if (is_array($rowDb) && isset($rowDb[0])) {
            //get the main row here
            $row = $this->getEntityColumnValues($rowDb[0], $em);
            // $row = $rowDb[0];
            unset($rowDb[0]);

            //now merge the non related cols in to the main array
            foreach ($rowDb as $key => $value) {
                $row[$key] = $value;
            }
        } else {
            $row = $rowDb;
        }

        return $row;
    }

    function getRowsAsArrayForCustomDQL($rowsDb, $em) {
        $rows = [];
        foreach ($rowsDb as $rowDb) {
            $rows[] = $this->getRowAsArrayForCustomDQL($rowDb, $em);
        }

        return $rows;
    }

}
