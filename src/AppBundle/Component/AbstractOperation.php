<?php
namespace AppBundle\Component;

use \Symfony\Component\DependencyInjection\ContainerAware;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use MediaBundle\Entity\Media;

class AbstractOperation extends ContainerAware
{
    protected $dbUtil;
    protected $util;
    protected $em;

    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->util   = $this->container->get('app.util');
        $this->dbUtil = $this->container->get('app.dbutil');
        $this->em     = $this->container->get('doctrine')->getEntityManager();
    }

    /**
     * Get the service container
     *
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Get the entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        // get the document manager
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Get the JMS Serializer
     *
     * @return type
     */
    public function getJmsSerializer()
    {
        return $this->getContainer()->get('jms_serializer');
    }

    /**
     * Get all records
     *
     */
    public function getList($searchVars, $searchOpts)
    {
        $rowsDb = $this->getRepository()->getList($searchVars, $searchOpts);
        $rows = $this->dbUtil->getRowsAsArrayForCustomDQL($rowsDb, $this->em);
        return $rows;

        // return $this->getRepository()->getList($searchVars, $searchOpts);
    }

    public function getOne($id, $searchOpts)
    {
        $rowDb = $this->getRepository()->getOne($id, $searchOpts);
        $row = $this->dbUtil->getRowAsArrayForCustomDQL($rowDb, $this->em);
        return $row;

        // return $this->getRepository()->getOne($id, $searchOpts);
    }

    /**
     * Get all link records
     */
    public function getListLink($id, $searchVars = [], $linkInfo = [])
    {
        $linkName   = $this->util->getCurrentLinkName();
        $linkNameCC = $this->util->getCurrentLinkName(true);
        $bundleName = $this->util->getCurrentBundleName();
        $bundleNameCC = $this->util->getCurrentBundleName(true);

        $linkInfo['name']         = $linkName;
        $linkInfo['nameCC']       = $linkNameCC;
        $linkInfo['sourceBundle'] = "{$bundleName}Bundle";
        $linkInfo['linkBundle']   = "{$linkName}Bundle";
        $linkInfo['linkEntity']   = "{$linkName}Bundle:{$linkName}";
        $linkInfo['bundleNameCC'] = $bundleNameCC;

        $rowsDb = $this->getRepository()->getListLink($id, $searchVars, $linkInfo);
        $rows = $this->dbUtil->getRowsAsArrayForCustomDQL($rowsDb, $this->em);
        return $rows;

        // return $this->getRepository()->getListLink($id, $searchVars, $linkInfo);
    }

    public function getOneLink($id, $idl, $searchVars = [], $linkInfo = [])
    {
        $linkName   = $this->util->getCurrentLinkName();
        $linkNameCC = $this->util->getCurrentLinkName(true);
        $bundleName = $this->util->getCurrentBundleName();
        $bundleNameCC = $this->util->getCurrentBundleName(true);

        $linkInfo['name']         = $linkName;
        $linkInfo['nameCC']       = $linkNameCC;
        $linkInfo['sourceBundle'] = "{$bundleName}Bundle";
        $linkInfo['linkBundle']   = "{$linkName}Bundle";
        $linkInfo['linkEntity']   = "{$linkName}Bundle:{$linkName}";
        $linkInfo['bundleNameCC'] = $bundleNameCC;


        // $rowDb = $this->getRepository()->getOneLink($id, $idl, $searchVars, $linkInfo);
        // $row = $this->dbUtil->getRowAsArrayForCustomDQL($rowDb, $this->em);
        // return $row;

        return $this->getRepository()->getOneLink($id, $idl, $searchVars, $linkInfo);
    }

     /**
     * Save a main bundle entity ex: Company
     *
     */
    public function save($postData)
    {
        $bundleName = $this->util->getCurrentBundleName();
        $entityPath = "{$bundleName}Bundle\Entity\\{$bundleName}";
        $serializer = $this->getJmsSerializer();

        $entityDeSerialized = $serializer->deserialize(json_encode($postData), $entityPath, 'json');
        $entityDeSerialized = $this->dbUtil->setRecordCreationDetails($entityDeSerialized, $this->container);

        // \Doctrine\Common\Util\Debug::dump($entityDeSerialized);
        // die();
        return $this->getRepository()->save($entityDeSerialized);
    }

     /**
     * Save link records
     *
     */
    public function saveLink($postData, $id, $idl = null, $relEntity = null)
    {
        $linkName = $this->util->getCurrentLinkName();
        $entityPath = $this->dbUtil->getEntityPathFromShortName($relEntity, $linkName);

        $relEntityObj = $this->em->getRepository($relEntity)->find($idl);

        $serializer = $this->getJmsSerializer();
        $context    = new \JMS\Serializer\DeserializationContext();
        $context->attributes->set('target', $relEntityObj);

        $entityDeSerialized = $serializer->deserialize(json_encode($postData), $entityPath, 'json');

        return $this->getRepository()->save($entityDeSerialized);
    }

    public function uploadMedia($files, $id, $relEntity)
    {
        $linkName     = $this->util->getCurrentLinkName();
        $linkNameCC   = $this->util->getCurrentLinkName(true);
        $bundleName   = $this->util->getCurrentBundleName();
        $bundleNameCC = $this->util->getCurrentBundleName(true);

        $mediaType = $linkNameCC; //ex: attachment / image etc.

        $mediaRepo     = $this->em->getRepository('MediaBundle:Media'); //media repository
        $relEntityRepo = $this->em->getRepository($relEntity); //MediaBundle:MediaHistory

        //ex: MediaBundle/Entity/MediaHistory
        $relEntityPath   = $this->dbUtil->getEntityPathFromShortName($relEntity, $linkName);
        $sourceEntityObj = $this->getRepository()->find($id); //ex: CompanyRepository
        $setSourceEntityMethod = 'set' . $bundleName; //ex: setCompany

        $mediaArr = array();

        foreach ($files as $file) {
            // create a new media record
            $media = new Media();

            $media->setFile($file);
            $media->upload();
            $media = $mediaRepo->save($media);
            $media->renameUploadedFile();
            $media = $mediaRepo->save($media);

            // create a relation record
            $relEntityObj = new $relEntityPath();
            $relEntityObj->setMediaType($mediaType)
                         ->setMedia($media)
                         ->setModule($bundleNameCC)
                         ->$setSourceEntityMethod($sourceEntityObj);

            $relEntityObj = $relEntityRepo->save($relEntityObj);

            $mediaArr[] = $relEntityObj;
        }

        // array("attachment" => $media);
        return array($linkNameCC => $mediaArr);
    }

    /**
     * delete a bundle's main entity
     *
     */
    public function delete($id)
    {
        $row = $this->getRepository()->find($id);
        $this->em->remove($row);
        $this->em->flush();
    }

    public function deleteLink($id, $idl, $relEntity, $deleteTarget = false, $targetEntity = null)
    {
        $targetIdFld   = '';
        $targetIdValue = '';

        $linkName   = $this->util->getCurrentLinkName();
        $entityPath = $this->dbUtil->getEntityPathFromShortName($relEntity, $linkName);
        $relEntityObj = $this->em->getRepository($relEntity)->find($idl);
        if ($deleteTarget) {
            $targetIdFld = $this->dbUtil->getIdNameFromTargetEntity($targetEntity); //ex: MediaId
            $targetIdMethod = "get{$targetIdFld}"; //ex: getMediaId()
            $targetIdValue  = $relEntityObj->$targetIdMethod(); //ex: $relEntityObj->getMediaId()
        }
        $this->em->remove($relEntityObj);

        if ($deleteTarget) {
            $entityPathTarget = $this->dbUtil->getEntityPathFromShortName($targetEntity, $linkName);
            $targetEntityObj  = $this->em->getRepository($entityPathTarget)->find($targetIdValue);
            $this->em->remove($targetEntityObj);
        }

        $this->em->flush();
    }

    /**
     * Get company repository's instance.
     *
     */
    public function getRepository()
    {
        $bundleName = $this->util->getCurrentBundleName();
        $repo = $this->em->getRepository("{$bundleName}Bundle:{$bundleName}");

        //quickly set the defaults for AbstractRepository
        $repo->setDefaults($this->container, $this->dbUtil);

        return $repo;
    }
}