<?php
namespace AppBundle\Component;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

use Doctrine\ORM\Mapping\ClassMetadata;
/**
 *
 * Base class for all the Repositories in the each Bundle
 */
class AbstractRepository extends EntityRepository
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $dbUtil;

    public function setDefaults($container, $dbUtil)
    {
        $this->container = $container;
        $this->dbUtil    = $dbUtil;
    }

    /**
     * Default SQL Query Builder for repository if it's not overridden
     *
     */
    public function getQB($id = null)
    {
        // Similar to "SELECT e FROM {$bundleName}Bundle:{$bundleName} e"
        $alias = $this->getAlias();
        $qb = $this->createQueryBuilder($alias);

        if ($id) {
            $qb->where("{$alias}.id = :id")
               ->setParameter(':id', $id);
        }

        return $qb;
    }

    /**
     * Get all records.
     *
     */
    public function getList($searchVars, $searchOpts)
    {
        $qb = $this->getQB(null, $searchVars, $searchOpts);

        $query = $qb->getQuery()
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, 1);
        // $result = $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
        $result = $query->getResult();
        // \Doctrine\Common\Util\Debug::dump($result);

        if ($result) {
            return $result;
        }

        return [];
    }

    /**
     * Get a record by its id.
     *
     * @param integer $id
     *
     */
    public function getOne($id, $searchOpts)
    {
        if ($id && is_numeric($id)) {
            $query = $this->getQB($id, null, $searchOpts)->getQuery()
                ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, 1);
            // return $query->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY)[0];
            return $query->getResult(\Doctrine\ORM\Query::HYDRATE_OBJECT)[0];
        }

        return null;
    }

    /**
     * Please override this in the bundle repository for now
     */
    public function getQBLink($id, $idl, $searchVars = [], $linkInfo)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $bundleName = $this->container->get('app.util')->getCurrentBundleName();

        $linkName   = $linkInfo['name'];
        $linkEntity = $linkInfo['linkEntity']; //ContactBundle:Contact -- used in manyToOne

        $qb = $this->callQBLinkMethodName($id, $idl, $searchVars, $linkInfo);
        if ($qb) return $qb;

        $srcEntIdInRelEnt = $dbUtil->getReferenceIdNameFromBundle($bundleName); //ex: company_id

        $qb = $this->_em->createQueryBuilder();
        $alias = $dbUtil->getAliasFromEntity($linkName);
        $qb->select($alias);
        $qb->from($linkEntity, $alias);
        $qb = $dbUtil->setWhere($qb, "{$alias}.{$srcEntIdInRelEnt}", $id);

        return $qb;
    }

    /**
     * Please override this in the bundle repository for now
     */
    // public function getQBLink($id, $idl, $searchVars = [], $linkInfo)
    // {
    //     $dbUtil = $this->container->get('app.dbutil');

    //     $bundleName = $this->container->get('app.util')->getCurrentBundleName();

    //     $linkName   = $linkInfo['name'];
    //     $linkEntity = $linkInfo['linkEntity']; //ContactBundle:Contact -- used in manyToOne

    //     $srcEntIdInRelEnt = $dbUtil->getReferenceIdNameFromBundle($bundleName); //ex: company_id

    //     $qb = $this->_em->createQueryBuilder();
    //     $alias = $dbUtil->getAliasFromEntity($linkName);
    //     $qb->select($alias);
    //     $qb->from($linkEntity, $alias);
    //     $qb = $dbUtil->setWhere($qb, "{$alias}.{$srcEntIdInRelEnt}", $id);

    //     return $qb;
    // }

    /**
     * Get all link records.
     *
     */
    public function getListLink($id, $searchVars = [], $linkInfo = [])
    {
        $qb = $this->getQBLink($id, null, $searchVars, $linkInfo);
        $query = $qb->getQuery()
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, 1);

        return $query->getResult();
    }

    /**
     * Get one link record (ex: for edit link).
     *
     */
    public function getOneLink($id, $idl, $searchVars = [], $linkInfo = [])
    {
        $qb = $this->getQBLink($id, $idl, $searchVars, $linkInfo);
        $query = $qb->getQuery()
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, 1);
        return $query->getResult()[0];
    }

    /**
     * returns the alias for bundle's main entity
     * This function is kept for overriding the individual repositories
     * by default the alias is set to 'e'
     */
    public function getAlias() {
        $bundleName = $this->container->get('app.util')->getCurrentBundleName();
        $alias = strtolower(substr($bundleName, 0, 1));

        return $alias;
    }

    public function getAliasLink() {
        $bundleName = $this->container->get('app.util')->getCurrentBundleName();
        $alias = strtolower(substr($bundleName, 0, 1));

        return $alias;
    }

    public function callQBLinkMethodName($id, $idl, $searchVars, $linkInfo) {
        $linkName = $linkInfo['name'];
        $methodName = 'getQBLink' . $linkName;

        if (method_exists($this, $methodName)) {
            $qb = $this->_em->createQueryBuilder();
            return call_user_func(array($this, $methodName), $id, $idl, $searchVars, $linkInfo, $qb);
        } else {
            $qb = $this->_em->createQueryBuilder();
            //check the method exist in the LinkRepositoryCallbacks
            $linkRepoCB = $this->container->get('app.link_repo_cb');
            if (method_exists($linkRepoCB, $methodName)) {
                return call_user_func(array($linkRepoCB, $methodName),
                    $id, $idl, $searchVars, $linkInfo, $qb);
            }
        }

        return false;
    }

    /**
     * Save a record.
     */
    public function save($deSerializedEntity)
    {
        // $row = $this->getEntityManager()->merge($deSerializedEntity);

        // \Doctrine\Common\Util\Debug::dump($deSerializedEntity);
        // die();

        $row = $this->getEntityManager()->persist($deSerializedEntity);
        $this->getEntityManager()->flush();

        return $deSerializedEntity;
    }

    /**
     * Save a record.
     */
    public function getSaveLink($deSerializedEntity)
    {
        $row = $this->getEntityManager()->merge($deSerializedEntity);
        $this->getEntityManager()->flush();

        return $row;
    }


}