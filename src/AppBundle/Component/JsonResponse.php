<?php

namespace AppBundle\Component;

use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse as SymfonyJsonResponse;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Response represents an HTTP response in JSON format.
 *
 * The class uses JSM Serializer to convert data to JSON format given the
 * fact that JSM offers a higly configurable interface.
 *
 * Note that this class does not force the returned JSON content to be an
 * object. It is however recommended that you do return an object as it
 * protects yourself against XSSI and JSON-JavaScript Hijacking.
 *
 * @see https://www.owasp.org/index.php/OWASP_AJAX_Security_Guidelines#Always_return_JSON_with_an_Object_on_the_outside
 *
 */
class JsonResponse extends SymfonyJsonResponse implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     *
     * @api
     */
    protected $container;

    /**
     * Contains the serialization context for JMS Serializer
     *
     * @var SerializationContext
     */
    protected $serializationContext;

    /**
     * Constructor.
     *
     * @param ContainerInterface $container The service container object
     * @param mixed   $data    The response data
     * @param int     $status  The response status code
     * @param array   $headers An array of response headers
     * @param SerializationContext $context (optional) The serialization context for JMS Serializer
     */
    public function __construct(ContainerInterface $container, $data = null, $exp = array())
    {
        $status  = isset($exp['status']) ? $exp['status'] : 200;
        $headers = isset($exp['headers']) ? $exp['headers'] : array();
        $context = isset($exp['context']) ? $exp['context'] : null;
        $group   = isset($exp['group']) ? $exp['group'] : 'Default';

        $this->setContainer($container);

        if (null === $context) {
            $context = new SerializationContext();
        }

        $groups = array($group);
        // $groups = array('vl');
        // $groups = array('Default', 'list');
        $context->setGroups($groups);

        $this->serializationContext = $context;

        parent::__construct($data, $status, $headers);
    }

    /**
     * Sets the data to be sent as JSON. It uses JMS Serializer to convert
     * recursivelly data to JSON format.
     *
     * @param mixed $data
     *
     * @return JsonResponse
     *
     * @throws \InvalidArgumentException
     */
    public function setData($data = array())
    {
        $errorHandler = null;
        $errorHandler = set_error_handler(function () use (&$errorHandler) {
            if (JSON_ERROR_NONE !== json_last_error()) {
                return;
            }

            if ($errorHandler) {
                call_user_func_array($errorHandler, func_get_args());
            }
        });

        try {
            // Clear json_last_error()
            json_encode(null);

            // \Doctrine\Common\Util\Debug::dump($data);
            // die();
            $serializer = $this->getContainer()->get('jms_serializer');
            $this->data = $serializer->serialize($data, 'json', $this->getJMSSerializationContext());

            // \Doctrine\Common\Util\Debug::dump($this->data);
            // die();

            restore_error_handler();
        } catch (\Exception $exception) {
            restore_error_handler();

            throw $exception;
        }

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new \InvalidArgumentException($this->transformJsonError());
        }

        return $this->update();
    }

    private function transformJsonError()
    {
        if (function_exists('json_last_error_msg')) {
            return json_last_error_msg();
        }

        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                return 'Maximum stack depth exceeded.';

            case JSON_ERROR_STATE_MISMATCH:
                return 'Underflow or the modes mismatch.';

            case JSON_ERROR_CTRL_CHAR:
                return 'Unexpected control character found.';

            case JSON_ERROR_SYNTAX:
                return 'Syntax error, malformed JSON.';

            case JSON_ERROR_UTF8:
                return 'Malformed UTF-8 characters, possibly incorrectly encoded.';

            default:
                return 'Unknown error.';
        }
    }

    /**
     * Get the serialization context for JMS Serializer.
     *
     * @return \JMS\Serializer\SerializationContext
     */
    public function getJMSSerializationContext()
    {
        return $this->serializationContext;
    }

    /**
     * Sets the Container associated with this Controller.
     *
     * @param ContainerInterface $container A ContainerInterface instance
     *
     * @api
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    /**
     * Gets the service container.
     *
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }
}
