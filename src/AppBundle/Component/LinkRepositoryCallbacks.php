<?php
namespace AppBundle\Component;

class LinkRepositoryCallbacks
{
    protected $dbUtil;

    public function __construct(\AppBundle\Component\DbUtil $dbUtil)
    {
        $this->dbUtil = $dbUtil;
    }

    public function getQBLinkNote($id, $idl, $sVars, $linkInfo, $qb) {
        $bundleNameCC = $linkInfo['bundleNameCC'];

        $qb->select('n')
            ->from('NoteBundle:Note', 'n');
        $qb->expr()->eq('n.module', $qb->expr()->literal($bundleNameCC));
        $qb = $this->dbUtil->setWhere($qb, "n.{$bundleNameCC}Id", $id); //n.companyId
        $qb = $this->dbUtil->setWhere($qb, 'n.id', $idl);

        return $qb;
    }

    public function getQBLinkAttachment($id, $idl, $sVars, $linkInfo, $qb) {
        $bundleNameCC = $linkInfo['bundleNameCC'];
        $expr = $qb->expr()->andX();
        $expr->add($qb->expr()->eq("mh.module", $qb->expr()->literal($bundleNameCC) ));
        $expr->add($qb->expr()->eq("mh.mediaType", $qb->expr()->literal('attachment') ));

        $qb->select("mh, m")
            ->from('MediaBundle:MediaHistory', 'mh')
            ->join("mh.media", 'm', 'WITH', $expr);

        $qb = $this->dbUtil->setWhere($qb, "mh.id", $idl);
        $qb = $this->dbUtil->setWhere($qb, "mh.{$bundleNameCC}Id", $id);

        return $qb;
    }

    public function getQBLinkImage($id, $idl, $sVars, $linkInfo, $qb) {
        $bundleNameCC = $linkInfo['bundleNameCC'];
        $expr = $qb->expr()->andX();
        $expr->add($qb->expr()->eq("mh.module", $qb->expr()->literal($bundleNameCC) ));
        $expr->add($qb->expr()->eq("mh.mediaType", $qb->expr()->literal('image') ));

        $qb->select("mh, m")
            ->from('MediaBundle:MediaHistory', 'mh')
            ->join("mh.media", 'm', 'WITH', $expr);

        $qb = $this->dbUtil->setWhere($qb, "mh.id", $idl);
        $qb = $this->dbUtil->setWhere($qb, "mh.{$bundleNameCC}Id", $id);

        return $qb;
    }

    public function getQBLinkMessage($id, $idl, $sVars, $linkInfo, $qb) {
        $bundleNameCC = $linkInfo['bundleNameCC'];

        $qb->select('m')
            ->from('MessageBundle:Message', 'm');
        $qb->expr()->eq('m.module', $qb->expr()->literal($bundleNameCC));
        $qb = $this->dbUtil->setWhere($qb, "m.{$bundleNameCC}Id", $id); //m.companyId
        $qb = $this->dbUtil->setWhere($qb, 'm.id', $idl);
        return $qb;
    }
}