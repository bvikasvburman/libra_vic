<?php
namespace AppBundle\Repository;

use AppBundle\Component\AbstractRepository;

class GanttRowRepository extends AbstractRepository
{
    public function getAlias() {
        return 'gr';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');
        $ganttName = $sVars->criteria['ganttName'];
        $ganttId   = $sVars->criteria['ganttId'];

        $qb = $this->createQueryBuilder('gr')
              ->select('gr, gt, uc, um')
              ->leftjoin('gr.ganttTaskM', 'gt')
              ->leftjoin('gr.userCreated', 'uc')
              ->leftjoin('gr.userModified', 'um');

        $qb = $dbUtil->setWhere($qb, 'gr.ganttId', $ganttId);
        $qb = $dbUtil->setWhere($qb, 'gr.ganttName', $ganttName);

        return $qb;
    }
}
