<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Filesystem\Filesystem;
use AppBundle\Component\Util;

use Underscore\Types\Strings;
use AppBundle\Command\CopySymfonyFiles;

class DuplicateBundleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('duplicateBundle')
            ->setDescription('Greet someone')
            ->addArgument(
                'newBundleName',
                InputArgument::REQUIRED
            )
            ->addArgument(
                'options',
                InputArgument::REQUIRED
            )
            ->addOption(
                'yell',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will yell in uppercase letters'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $o)
    {
        $srcBundleName   = 'Sample';
        $srcBundleNameCC = Strings::toCamelCase($srcBundleName);

        $destBundleName   = '';
        $destBundleNameCC = '';
        $opts = '';

        $this->o = $o;
        $fs = new Filesystem();
        $app = $this->getApplication();


        $destBundleName   = $input->getArgument('newBundleName'); //ex: TradingOrder
        $destBundleNameCC = lcfirst($destBundleName); //ex: camel case TradingOrder

        $opts = $input->getArgument('options'); //ex: {firstName, lastName, email, phone}
        $opts = Util::fixJSON($opts);

        $opts = json_decode($opts);

        print_r($opts);
        // $o->writeln(print_r($this->opts));
        // die();

        if (!$opts) {
            $text = PHP_EOL . "There seems to be an issue with the options provided. Please check it and try again." . PHP_EOL .
                    "You may validate your json @ https://jsonformatter.curiousconcept.com/" . PHP_EOL;
            $o->writeln($text);
            die();
        }

        //Copy Symfony2 files
        $sourcePath = __DIR__ . "/../../../app/utils/duplicateBundle/symfony/{$srcBundleName}Bundle/";
        $destPath   = __DIR__ . "/../../../src/{$destBundleName}Bundle/";

        if ($fs->exists($destPath)) {
            $text = PHP_EOL . "The Destination folder already exists. " .
                    "Please delete it before running this command again." .
                    PHP_EOL .
                    "Destination Path: " . $destPath .
                    PHP_EOL;
            $o->writeln($text);
            die();
        }

        $finder = new Finder();
        $finder->directories()->files()->in($sourcePath);

        $text = PHP_EOL . "Duplicating {$srcBundleName}Bundle to " . $destBundleName . "Bundle..." . PHP_EOL;
        $o->writeln($text);

        $copySymfonyFiles = new CopySymfonyFiles($o, $finder, $srcBundleName,
                                $destBundleName, $destBundleNameCC, $opts, $sourcePath, $destPath, $app);
        $copySymfonyFiles->copy();


        //Copy AnjularJs javascript files
        $sourcePath = __DIR__ . "/../../../app/utils/duplicateBundle/angularjs/{$srcBundleNameCC}/";
        $destPath   = __DIR__ . "/../../../web/js/app/bundles/{$destBundleNameCC}/";

        $finder = new Finder();
        $finder->directories()->files()->in($sourcePath);

        $copyAngularJsFiles = new CopyAngularJsFiles($o, $finder, $srcBundleName,
                                $destBundleName, $destBundleNameCC, $opts, $sourcePath, $destPath);
        $copyAngularJsFiles->copy();

        $copySymfonyFiles->addBundleToAppKernel();
        $copySymfonyFiles->createEntities();


        $text = PHP_EOL . "New bundle {$destBundleName} successfully created!" . PHP_EOL;
        $o->writeln($text);
    }
}