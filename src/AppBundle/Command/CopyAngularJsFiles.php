<?php

namespace AppBundle\Command;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Filesystem\Filesystem;
use AppBundle\Component\Util;

use Underscore\Types\Strings;

class CopyAngularJsFiles
{
    var $srcBundleName = '';
    var $destBundleName   = '';
    var $destBundleNameCC = '';
    var $o; //output obj
    var $sourcePath = '';
    var $destPath = '';
    var $opts = '';
    var $spacer4 = '';
    var $spacer8 = '';
    var $spacer12 = '';
    var $spacer16 = '';

    public function __construct($o, $finder, $srcBundleName, $destBundleName,
                                $destBundleNameCC, $opts, $sourcePath, $destPath) {
        $this->o = $o;
        $this->finder = $finder;
        $this->srcBundleName = $srcBundleName;
        $this->destBundleName = $destBundleName;
        $this->destBundleNameCC = $destBundleNameCC;
        $this->opts = $opts;
        $this->sourcePath = $sourcePath;
        $this->destPath = $destPath;

        $this->spacer4  = str_repeat(' ', 4);
        $this->spacer8  = str_repeat(' ', 8);
        $this->spacer12 = str_repeat(' ', 12);
        $this->spacer16 = str_repeat(' ', 16);
    }

    function copy() {
        $fs = new Filesystem();

        $bundleTableName     = Strings::toSnakeCase($this->destBundleName);
        $listFldsArray       = $this->getListFldsArray();
        $linksArray          = $this->getLinksArray();
        $entityFields        = $this->getEntityFields();
        $linksArrayInRouting = $this->getLinksArrayInRouting();
        $linksDataArrDepInj1InController = $this->getLinksDataArrDepInj1InController();
        $linksDataArrDepInj2InController = $this->getLinksDataArrDepInj2InController();
        $linksDataArrInRowArray = $this->getLinksDataArrInRowArray();

        //$srcFIO is Symfony\Component\Finder\SplFileInfo object
        foreach ($this->finder as $srcFIO) {
            $srcFile        = $srcFIO->getRealpath();
            $srcFileName    = $srcFIO->getFilename();
            $srcRelPath     = $srcFIO->getRelativePath();
            $srcRelPathName = $srcFIO->getRelativePathname();

            $destFile        = $this->destPath . $srcRelPathName;
            $destRelPath     = $srcRelPath;
            $destRelPathName = $srcRelPathName;

            //check if the file name has got BundleName then we should replace with the new BundleName
            if (strpos($srcFile, $this->srcBundleName) !== false) {
                $destFile        = str_replace($this->srcBundleName, $this->destBundleName, $destFile);
                $destRelPath     = str_replace($this->srcBundleName, $this->destBundleName, $destRelPath);
                $destRelPathName = str_replace($this->srcBundleName, $this->destBundleName, $destRelPathName);
            }

            // die(PHP_EOL . PHP_EOL . $this->destBundleName . PHP_EOL . PHP_EOL);

            $fs->copy($srcFile, $destFile);

            //$destFIO is Symfony\Component\Finder\SplFileInfo object
            $destFIO = new SPLFileInfo($destFile, $destRelPath, $destRelPathName);

            //only copy contents to dest file if the src is a file and not a dir.
            if (!$srcFIO->isDir()) {
                //$destFO is SplFileobject. php core class.
                $destFO = $destFIO->openFile('w');
                $srcFileContent = $srcFIO->getContents();

                $srcFileContent = str_replace('<BundleName>', $this->destBundleName, $srcFileContent);
                $srcFileContent = str_replace('<BundleNameCC>', $this->destBundleNameCC, $srcFileContent);
                $srcFileContent = str_replace('<listFldsArray>', $listFldsArray, $srcFileContent);
                $srcFileContent = str_replace('<linksArray>', $linksArray, $srcFileContent);
                $srcFileContent = str_replace('<entityFields>', $entityFields, $srcFileContent);
                $srcFileContent = str_replace('<linksArrayInRouting>', $linksArrayInRouting, $srcFileContent);
                $srcFileContent = str_replace('<linksDataArrDepInj1InController>', $linksDataArrDepInj1InController, $srcFileContent);
                $srcFileContent = str_replace('<linksDataArrDepInj2InController>', $linksDataArrDepInj2InController, $srcFileContent);
                $srcFileContent = str_replace('<linksDataArrInRowArray>', $linksDataArrInRowArray, $srcFileContent);

                $destFO->fwrite($srcFileContent);
            }
            $this->o->writeln("Copying... : " . $destRelPathName);
        }

    }

    private function getListFldsArray() {
        $listFlds = $this->opts->listFlds;

        $text = '';
        foreach ($listFlds as $fldOpts) {
            $fldName = $fldOpts->name;

            $label         = isset($fldOpts->label) ? $fldOpts->label : '';
            $flex          = isset($fldOpts->flex) ? $fldOpts->flex : '';
            $hasDetailLink = isset($fldOpts->editable) ? $fldOpts->editable : '';

            $frmFldOpts = array();
            if ($label != '') {
                $frmFldOpts['label'] = $label;
            }
            if ($flex != '') {
                $frmFldOpts['flex'] = $flex;
            }

            $optsText = '';
            if (count($frmFldOpts) != 0) {
                $optsText = str_replace("\"", "'", json_encode($frmFldOpts));
                $optsText = ", " . $optsText;
            }
            $optsText = $optsText != '' ? ",{$optsText}" : '';
            $text .= $this->spacer12 . "new lf('{$fldName}'{$optsText})," . PHP_EOL;
        }
        $text = rtrim($text);

        return $text;
    }

    private function getLinksArray() {
        $links = $this->opts->links;

        $text = '';
        foreach ($links as $linkOpts) {
            $linkName = $linkOpts->name;

            $label  = isset($linkOpts->label) ? $linkOpts->label : '';

            $linkParams = array();
            if ($label != '') {
                $linkParams['label'] = $label;
            }

            $linkParamsText = '';
            if (count($linkParams) != 0) {
                $linkParamsText = str_replace("\"", "'", json_encode($linkParams));
                $linkParamsText = ", " . $linkParamsText;
            }
            $linkParamsText = $linkParamsText != '' ? ",{$linkParamsText}" : '';
            $text .= $this->spacer12 . "new LinkService('{$linkName}'{$linkParamsText})," . PHP_EOL;
        }
        $text = rtrim($text);

        return $text;
    }

    private function getLinksArrayInRouting() {
        $links = $this->opts->links;

        //Router.getLinkObjForRoute('contact'),
        $text = '';
        foreach ($links as $linkOpts) {
            $linkName = $linkOpts->name;
            $text .= $this->spacer4 . "Router.getLinkObjForRoute('{$linkName}')," . PHP_EOL;
        }
        $text = rtrim($text);

        return $text;
    }

    private function getEntityFields() {
        $entites = $this->opts->entities;
        // print_r($listFlds);

        $excludeFldArr = array('creationDate', 'modificationDate');
        $text = '';
        foreach ($entites as $fldOpts) {
            $entityName = $fldOpts->name;
            if (!in_array($entityName, $excludeFldArr)) {
                $type = $this->getJavascriptFieldType($fldOpts->type);
                $text .= $this->spacer8 . "this.add('{$entityName}', '{$type}');" . PHP_EOL;
            }
        }
        $text = rtrim($text);

        return $text;
    }

    private function getJavascriptFieldType($type) {
        $typesArr = array(
            'datetime' => 'Date',
            'DateTime' => 'Date',
            'integer' => 'number',
            'decimal' => 'number',
        );

        $retType = isset($typesArr[$type]) ? $typesArr[$type] : $type;

        return $retType;
    }

    private function getLinksDataArrDepInj1InController() {
        $links = $this->opts->links;

        $text = '';
        foreach ($links as $linkOpts) {
            $linkName = $linkOpts->name;
            $text .= $this->spacer4 . "'{$linkName}Lnk'," . PHP_EOL;
        }
        $text = rtrim($text);

        return $text;
    }

    private function getLinksDataArrDepInj2InController() {
        $links = $this->opts->links;

        $text = '';
        foreach ($links as $linkOpts) {
            $linkName = $linkOpts->name;
            $text .= $this->spacer8 . "{$linkName}Lnk," . PHP_EOL;
        }
        $text = rtrim($text);

        return $text;
    }

    private function getLinksDataArrInRowArray() {
        $links = $this->opts->links;

        // row.links.contactLnk = contactLnk;

        $text = '';
        foreach ($links as $linkOpts) {
            $linkName = $linkOpts->name;
            $text .= $this->spacer8 . "\$scope.links.{$linkName}Lnk = {$linkName}Lnk;" . PHP_EOL;
        }
        $text = rtrim($text);

        return $text;
    }
}