<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Bundle\DoctrineBundle\Mapping\DisconnectedMetadataFactory;
use Symfony\Component\Console\Input\ArrayInput;

class GenerateEntitiesCommand extends ContainerAwareCommand
{
    var $spacer4 = '';

    protected function configure()
    {
        $this
            ->setName('cpGenerateEntities')
            ->setDescription('Clear the existing entities generation and generate entities')
            ->addArgument(
                'entityPrefixes',
                InputArgument::REQUIRED
            )
            ->addOption('clear-entities-only', null, InputOption::VALUE_NONE);

    }

    protected function execute(InputInterface $input, OutputInterface $o)
    {
        $this->o = $o;
        $app = $this->getApplication();

        $entityPrefixes = $input->getArgument('entityPrefixes'); //ex: all Comp Abc X Y etc...
        if ($entityPrefixes == 'all') {
            $entityPrefixes = 'A B C D E F G H I J K L M N O P Q R S T U V W X Y Z';
        }
        $prefixesArr = explode(' ', $entityPrefixes);

        $clearEntitiesOnly = $input->getOption('clear-entities-only');

        $this->clearMethodEntriesFromEntities($prefixesArr, $input, $o);

        if ($clearEntitiesOnly) {
            $text = PHP_EOL . "Entities Cleared!" . PHP_EOL;
            $o->writeln($text);
            return;
        }

        $command = $app->find('generate:doctrine:entities');

        // sudo app/console doctrine:generate:entities CompanyBundle/Entity/Company --no-backup

        foreach ($prefixesArr as $prefix) {
            try {
                $input = new ArrayInput(array(
                    'command' => 'generate:doctrine:entities',
                    'name' => $prefix,
                    '--no-backup' => true,
                ));
                $returnCode = $command->run($input, $this->o);
            } catch (\Exception $e) {
            }
        }

        $text = PHP_EOL . "Entities Generated!" . PHP_EOL;
        $o->writeln($text);
    }

    public function clearMethodEntriesFromEntities($prefixesArr, InputInterface $input, OutputInterface $o)
    {
        $manager = new DisconnectedMetadataFactory($this->getContainer()->get('doctrine'));
        foreach ($prefixesArr as $prefix) {
            try {
                $metadata = $manager->getNamespaceMetadata($prefix);
            } catch (\Exception $e) {
            }

            $o->writeln("============= {$prefix} =============");
            $basePath = $metadata->getPath();
            foreach ($metadata->getMetadata() as $m) {
                $o->writeln("----------------------{$m->name}");

                $className = $m->name;
                $filePath = $basePath . '/' . $className . '.php';
                $filePath = str_replace('\\', '/', $filePath);
                $fileContent = file_get_contents($filePath);
                $fileContent = trim($fileContent);

                $placeholder = $this->spacer4 . '// ---AUTO GENERATION STARTS--- //';
                $end = strpos($fileContent, $placeholder);
                if ($end === false) {
                    $o->writeln("{$placeholder} --- NOT FOUND.");
                    continue;
                }
                $end += strlen($placeholder);

                // die($fileContent . "~~~~~~\n\n");

                $fileContent = substr($fileContent, 0, $end);
                $fileContent .= "\n" .
                                "}";

                // now write new content in the actual file
                $destFIO = new \SPLFileInfo($filePath);
                $destFO = $destFIO->openFile('w');
                $destFO->fwrite($fileContent);
                $destFO = null;
            }
        }
    }

}