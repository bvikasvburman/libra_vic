<?php

namespace AppBundle\Command;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Input\ArrayInput;
use AppBundle\Component\Util;
use AppBundle\Component\DbUtil;

use Underscore\Types\Strings;

class CopySymfonyFiles
{
    var $srcBundleName = '';
    var $destBundleName   = '';
    var $destBundleNameCC = '';
    var $sourcePath = '';
    var $destPath = '';
    var $entityAlias = '';
    var $o; //output obj
    var $app; //current application instance
    var $opts = '';
    var $spacer4 = '';
    var $spacer8 = '';
    var $spacer12 = '';
    var $spacer16 = '';
    var $spacer20 = '';

    public function __construct($o, $finder, $srcBundleName, $destBundleName,
                                $destBundleNameCC, $opts, $sourcePath, $destPath, $app) {
        $this->o = $o;
        $this->finder = $finder;
        $this->srcBundleName = $srcBundleName;
        $this->destBundleName = $destBundleName;
        $this->destBundleNameCC = $destBundleNameCC;
        $this->opts = $opts;
        $this->sourcePath = $sourcePath;
        $this->destPath = $destPath;
        $this->app = $app;

        $this->entityAlias = DbUtil::getAliasFromEntity($destBundleName);

        $this->spacer4  = str_repeat(' ', 4);
        $this->spacer8  = str_repeat(' ', 8);
        $this->spacer12 = str_repeat(' ', 12);
        $this->spacer16 = str_repeat(' ', 16);
        $this->spacer20 = str_repeat(' ', 20);
    }

    function copy() {
        $fs = new Filesystem();

        $bundleTableName                = Strings::toSnakeCase($this->destBundleNameCC);
        $bundleEntityFields             = $this->getEntityFields();
        $bundleEntitySerializerFields   = $this->getEntitySerializerFields();
        $bundleEditFormFieldsKeyDetails = $this->getBundleEditFormFieldsKeyDetail();
        $bundleEditFormFieldsMain       = $this->getBundleEditFormFieldsMain();
        $routingForLinksApi             = $this->getRoutingForLinksApi();

        //$srcFIO is Symfony\Component\Finder\SplFileInfo object
        foreach ($this->finder as $srcFIO) {
            $srcFile        = $srcFIO->getRealpath();
            $srcFileName    = $srcFIO->getFilename();
            $srcRelPath     = $srcFIO->getRelativePath();
            $srcRelPathName = $srcFIO->getRelativePathname();

            $destFile        = $this->destPath . $srcRelPathName;
            $destRelPath     = $srcRelPath;
            $destRelPathName = $srcRelPathName;

            //check if the file name has got BundleName then we should replace with the new BundleName
            if (strpos($srcFile, $this->srcBundleName) !== false) {
                $destFile        = str_replace($this->srcBundleName, $this->destBundleName, $destFile);
                $destRelPath     = str_replace($this->srcBundleName, $this->destBundleName, $destRelPath);
                $destRelPathName = str_replace($this->srcBundleName, $this->destBundleName, $destRelPathName);
            }

            $fs->copy($srcFile, $destFile);

            //$destFIO is Symfony\Component\Finder\SplFileInfo object
            $destFIO = new SPLFileInfo($destFile, $destRelPath, $destRelPathName);

            //only copy contents to dest file if the src is a file and not a dir.
            if (!$srcFIO->isDir()) {
                //$destFO is SplFileobject. php core class.
                $destFO = $destFIO->openFile('w');
                $srcFileContent = $srcFIO->getContents();

                $srcFileContent = str_replace('<BundleName>', $this->destBundleName, $srcFileContent);
                $srcFileContent = str_replace('<BundleNameCC>', $this->destBundleNameCC, $srcFileContent);
                $srcFileContent = str_replace('<BundleTableName>', $bundleTableName, $srcFileContent);
                $srcFileContent = str_replace('<BundleEntityFields>', $bundleEntityFields, $srcFileContent);
                $srcFileContent = str_replace('<BundleEntitySerializerFields>', $bundleEntitySerializerFields, $srcFileContent);
                $srcFileContent = str_replace('<BundleEditFormFieldsKeyDetails>', $bundleEditFormFieldsKeyDetails, $srcFileContent);
                $srcFileContent = str_replace('<BundleEditFormFieldsMain>', $bundleEditFormFieldsMain, $srcFileContent);
                $srcFileContent = str_replace('<RoutingForLinksApi>', $routingForLinksApi, $srcFileContent);
                $srcFileContent = str_replace('<EntityAlias>', $this->entityAlias, $srcFileContent);

                $destFO->fwrite($srcFileContent);
            }

            $this->o->writeln("Copying... : " . $destRelPathName);
        }

    }

    private function getEntityFields() {
        $entities = $this->opts->entities;

        $text = '';
        foreach ($entities as $fldOpts) {
            $entityName = $fldOpts->name;
            $nullable = isset($fldOpts->nullable) ? $fldOpts->nullable : 'true';
            $length   = isset($fldOpts->length) ? $fldOpts->length : '';

            $text .= $this->spacer8 . $entityName . ':' . PHP_EOL; //entity name
            $text .= $this->spacer12 . 'type: ' . $fldOpts->type . PHP_EOL;  //entity type
            if ($length != '') {
                $text .= $this->spacer12 . 'length: ' . $length . PHP_EOL;
            }
            if ($nullable != '') {
                $text .= $this->spacer12 . 'nullable: ' . strtoupper($nullable) . PHP_EOL;
            }
        }

        $text .= $this->spacer8 . 'creationDate:' . PHP_EOL .
                 $this->spacer12 . 'type: datetime' . PHP_EOL .
                 $this->spacer12 . 'gedmo:' . PHP_EOL .
                 $this->spacer16 . 'timestampable:' . PHP_EOL .
                 $this->spacer20 . 'on: create' . PHP_EOL .

                 $this->spacer8 . 'modificationDate:' . PHP_EOL .
                 $this->spacer12 . 'type: datetime' . PHP_EOL .
                 $this->spacer12 . 'gedmo:' . PHP_EOL .
                 $this->spacer16 . 'timestampable:' . PHP_EOL .
                 $this->spacer20 . 'on: update' . PHP_EOL;
        return $text;
    }

    private function getEntitySerializerFields() {
        $entities = $this->opts->entities;
        $text = '';
        foreach ($entities as $fldOpts) {
            $entityName = $fldOpts->name;
            $nullable = isset($fldOpts->nullable) ? $fldOpts->nullable : 'true';
            $length   = isset($fldOpts->length) ? $fldOpts->length : '';

            $text .= $this->spacer8 . $entityName . ':' . PHP_EOL; //entity name
            $text .= $this->spacer12 . 'type: ' . $this->getOrmSerializeFieldType($fldOpts->type) . PHP_EOL;  //entity type
            // if ($length != '') {
            //     $text .= $this->spacer12 . 'length: ' . $length . PHP_EOL;
            // }
            // if ($nullable != '') {
            //     $text .= $this->spacer12 . 'nullable: ' . strtoupper($nullable) . PHP_EOL;
            // }
        }

        return $text;
    }

    private function getOrmSerializeFieldType($type) {
        $typesArr = array(
            'datetime' => 'DateTime',
            'date' => "DateTime<'Y-m-d'>",
        );

        $retType = isset($typesArr[$type]) ? $typesArr[$type] : $type;

        return $retType;
    }

    private function getBundleEditFormFieldsKeyDetail() {
        $flds = isset($this->opts->detailFlds->keyDetail) ? $this->opts->detailFlds->keyDetail : '';
        return $this->getBundleEditFormFields($flds);
    }

    private function getBundleEditFormFieldsMain() {
        $flds = isset($this->opts->detailFlds->main) ? $this->opts->detailFlds->main : '';
        return $this->getBundleEditFormFields($flds);
    }

    private function getBundleEditFormFields($flds) {
        if ($flds == '') {
            return '';
        }

        $text = '';
        foreach ($flds as $fldOpts) {
            $fldName = $fldOpts->name;

            // $label     = isset($fldOpts->label) ? $fldOpts->label : Util::humanizeStr($fldName);

            $label     = isset($fldOpts->label) ? $fldOpts->label : '';
            $flex      = isset($fldOpts->flex) ? $fldOpts->flex : '';
            $editable  = isset($fldOpts->editable) ? $fldOpts->editable : '';
            $valuelist = isset($fldOpts->valuelist) ? $fldOpts->valuelist : '';

            $type = isset($fldOpts->type) ? $fldOpts->type : 'input';

            $frmFldOpts = array();
            if ($label != '') {
                $frmFldOpts['label'] = $label;
            }
            if ($flex != '') {
                $frmFldOpts['flex'] = $flex;
            }
            if ($editable != '') {
                $frmFldOpts['editable'] = $editable;
            }
            if ($valuelist != '') {
                $frmFldOpts['valuelist'] = $valuelist;
            }

            $optsText = '';
            if (count($frmFldOpts) != 0) {
                $optsText = str_replace("\"", "'", json_encode($frmFldOpts));
                $optsText = ", " . $optsText;
            }
            $text .= "{{ fo.{$type}('row.{$fldName}', '{$label}'{$optsText}) }}" . PHP_EOL;
        }
        $text = rtrim($text);

        return $text;
    }

    private function getRoutingForLinksApi() {
        $text = "";
        return $text;
    }

    public function addBundleToAppKernel() {
        $reflection = new \ReflectionClass('AppKernel');
        $method = $reflection->getMethod('registerBundles');

        $filename  = $method->getFileName();
        $startLine = $method->getStartLine() - 1;
        $endLine   = $method->getEndLine();
        $length    = $endLine - $startLine;

        $lines = file($filename);
        $methodArr = array_slice($lines, $startLine, $length);

        $methodContent = '';

        //get the method content, insert the new definition and hold the new content in $methodContent var
        $currBundleTextInMethod = "new {$this->destBundleName}Bundle\\{$this->destBundleName}Bundle(),";

        foreach ($methodArr as $line) {
            // if the current bundle definition is already added in the registerBundles method.
            // then it will ignore it for adding it in the last line
            if (strpos($line, $currBundleTextInMethod) === false) {
                $methodContent .= $line;
            }
        }

        $placeholder = "/* [NewBundleDefPlaceHolder] */";
        $newDefText = $currBundleTextInMethod . PHP_EOL .
                      $this->spacer12 . $placeholder;
        $methodContent = str_replace($placeholder, $newDefText, $methodContent);

        //replace the new method content generated in to the lines array (AppKernel.php file content)
        array_splice($lines, $startLine, $length, $methodContent);

        $newFileContent = implode('', $lines);

        // now write new content in the actual file
        $destFIO = new \SPLFileInfo($filename);
        $destFO = $destFIO->openFile('w');
        $destFO->fwrite($newFileContent);
        $destFO = null;
    }

    public function createEntities() {
        $command = $this->app->find('generate:doctrine:entities');

        // sudo app/console doctrine:generate:entities CompanyBundle/Entity/Company --no-backup
        $entityName = "{$this->destBundleName}Bundle/Entity/{$this->destBundleName}";
        $input = new ArrayInput(array(
            'command' => 'generate:doctrine:entities',
            'name' => $entityName,
            '--no-backup' => true,
        ));
        $returnCode = $command->run($input, $this->o);

        $filePath = __DIR__ . "/../../{$this->destBundleName}Bundle/Entity/{$this->destBundleName}.php";
        // print file_get_contents($filePath);
        // die();

        // sleep(3);

        // //update the background database structure
        // $command = $this->app->find('doctrine:schema:update');
        // $input = new ArrayInput(array(
        //     'command' => 'doctrine:schema:update',
        //     '--force' => true,
        // ));
        // $returnCode = $command->run($input, $this->o);
    }
}