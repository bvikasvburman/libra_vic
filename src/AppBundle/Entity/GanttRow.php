<?php

namespace AppBundle\Entity;

class GanttRow
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $color;

    /**
     * @var integer
     */
    private $sortOrder;

    /**
     * @var integer
     */
    private $ganttId;

    /**
     * @var string
     */
    private $ganttName;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $ganttTaskM;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->ganttTaskM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return GanttRow
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return GanttRow
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     * @return GanttRow
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer 
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set ganttId
     *
     * @param integer $ganttId
     * @return GanttRow
     */
    public function setGanttId($ganttId)
    {
        $this->ganttId = $ganttId;

        return $this;
    }

    /**
     * Get ganttId
     *
     * @return integer 
     */
    public function getGanttId()
    {
        return $this->ganttId;
    }

    /**
     * Set ganttName
     *
     * @param string $ganttName
     * @return GanttRow
     */
    public function setGanttName($ganttName)
    {
        $this->ganttName = $ganttName;

        return $this;
    }

    /**
     * Get ganttName
     *
     * @return string 
     */
    public function getGanttName()
    {
        return $this->ganttName;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return GanttRow
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer 
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return GanttRow
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer 
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return GanttRow
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return GanttRow
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add ganttTaskM
     *
     * @param \AppBundle\Entity\GanttTask $ganttTaskM
     * @return GanttRow
     */
    public function addGanttTaskM(\AppBundle\Entity\GanttTask $ganttTaskM)
    {
        $this->ganttTaskM[] = $ganttTaskM;

        return $this;
    }

    /**
     * Remove ganttTaskM
     *
     * @param \AppBundle\Entity\GanttTask $ganttTaskM
     */
    public function removeGanttTaskM(\AppBundle\Entity\GanttTask $ganttTaskM)
    {
        $this->ganttTaskM->removeElement($ganttTaskM);
    }

    /**
     * Get ganttTaskM
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGanttTaskM()
    {
        return $this->ganttTaskM;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return GanttRow
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return GanttRow
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
