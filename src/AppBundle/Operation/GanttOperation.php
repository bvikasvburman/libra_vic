<?php
namespace AppBundle\Operation;

use AppBundle\Component\AbstractOperation;
use Underscore\Types\Strings;

class GanttOperation extends AbstractOperation
{
    public function getGanttData($sVars, $sOpts) {
        $rows = [];

        $repo = $this->em->getRepository('AppBundle:GanttRow');
        $repo->setDefaults($this->container, $this->dbUtil);
        $rows = $repo->getList($sVars, $sOpts);

        $ganttRows = [];
        foreach ($rows as $key => $ganttRow) {
            $ganttTasks = [];

            foreach ($ganttRow->getGanttTaskM() as $ganttTask) {
                $ganttTask = [
                    'name' => $ganttTask->getName(),
                    'from' => $ganttTask->getFromDate()->format('Y-m-d'),
                    'to' => $ganttTask->getToDate()->format('Y-m-d'),
                    'data' => array('id' => $ganttTask->getId()),
                ];
                $ganttTasks[] = $ganttTask;
            }
            $ganttRow = [
                'id' => $ganttRow->getId(),
                'name' => $ganttRow->getName(),
                'tasks' => $ganttTasks,
                'height' => 40,
            ];
            $ganttRows[] = $ganttRow;
        }

        return $ganttRows;
    }

    public function saveTask($postData)
    {
        $entityPath = "AppBundle\Entity\GanttTask";

        //new record
        if ($postData['id'] == '') {
            unset($postData['id']);
        }


        $serializer = $this->container->get('serializer');
        $deSerTask = $serializer->deserialize(json_encode($postData), $entityPath, 'json');

        $deSerTask = $this->dbUtil->setRecordCreationDetails($deSerTask, $this->container);

        $repo = $this->em->getRepository('AppBundle:GanttTask');
        $repo->save($deSerTask);

        return $deSerTask;
    }

    public function deleteTask($taskId)
    {
        $row = $this->em->getRepository('AppBundle:GanttTask')->find($taskId);
        $this->em->remove($row);
        $this->em->flush();
    }

    public function saveRow($postData, $ganttName, $ganttId)
    {
        $entityPath = "AppBundle\Entity\GanttRow";

        //new record
        if ($postData['id'] == '') {
            unset($postData['id']);
        }

        $serializer = $this->container->get('serializer');
        $deSerRow = $serializer->deserialize(json_encode($postData), $entityPath, 'json');
        $deSerRow->setGanttName($ganttName);
        $deSerRow->setGanttId($ganttId);

        $deSerRow = $this->dbUtil->setRecordCreationDetails($deSerRow, $this->container);

        $repo = $this->em->getRepository('AppBundle:GanttRow');
        $repo->save($deSerRow);

        $ganttRow = [
            'id' => $deSerRow->getId(),
            'name' => $deSerRow->getName(),
            'height' => 40,
        ];

        return $ganttRow;
    }

    public function deleteRow($rowId)
    {
        $row = $this->em->getRepository('AppBundle:GanttRow')->find($rowId);
        $this->em->remove($row);
        $this->em->flush();
    }

}