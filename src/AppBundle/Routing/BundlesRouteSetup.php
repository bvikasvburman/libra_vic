<?php

namespace AppBundle\Routing;

class BundlesRouteSetup
{
    public $bundles = [];

    public function setBundles() {
        $bundles = $this->bundles;

        $linkObjAttachment = $this->getLinkObj('attachment', 'media',
                'MediaBundle:MediaHistory', true, 'MediaBundle:Media');
        $linkObjImage = $this->getLinkObj('image', 'media',
                'MediaBundle:MediaHistory', true, 'MediaBundle:Media');

        //-----CRM------//
        $this->bundles['company'] = $this->getBundleObj('company', [
            'contact' => $this->getLinkObj('contact'),
            'branch' => $this->getLinkObj('branch'),
            'attachment' => $linkObjAttachment,
            'enquiry' => $this->getLinkObj('enquiry'),
            'quotation' => $this->getLinkObj('quotation'),
            'tradingOrder' => $this->getLinkObj('tradingOrder'),
            'salesConfirmation' => $this->getLinkObj('salesConfirmation'),
            'purchaseOrder' => $this->getLinkObj('purchaseOrder'),
            'manufacturingOrder' => $this->getLinkObj('manufacturingOrder'),
            'invoice' => $this->getLinkObj('invoice'),
            'claim' => $this->getLinkObj('claim'),
            'creditDebitNote' => $this->getLinkObj('creditDebitNote'),
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['contact'] = $this->getBundleObj('contact', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);
        $this->bundles['branch'] = $this->getBundleObj('branch', [
        ]);

        $this->bundles['interestGroup'] = $this->getBundleObj('interestGroup', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        //----- Masters -----//
        $this->bundles['colorMaster'] = $this->getBundleObj('colorMaster', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['partsLocation'] = $this->getBundleObj('partsLocation', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['approvedMaterial'] = $this->getBundleObj('approvedMaterial', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['approvedComponent'] = $this->getBundleObj('approvedComponent', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['approvedPackaging'] = $this->getBundleObj('approvedPackaging', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['masterPerformance'] = $this->getBundleObj('masterPerformance', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['masterCompliance'] = $this->getBundleObj('masterCompliance', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['masterChecklist'] = $this->getBundleObj('masterChecklist', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['productCoding'] = $this->getBundleObj('productCoding', [
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        //----- ERM -----//
        $this->bundles['enquiry'] = $this->getBundleObj('enquiry', [
            'protoType' => $this->getLinkObj('protoType'),
            'costing' => $this->getLinkObj('costing'),
            'quotation' => $this->getLinkObj('quotation'),
            'tradingOrder' => $this->getLinkObj('tradingOrder'),
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
            'task' => $this->getLinkObj('task'),
        ]);

        $this->bundles['quotation'] = $this->getBundleObj('quotation', [
            'protoType' => $this->getLinkObj('protoType'),
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['tradingOrder'] = $this->getBundleObj('tradingOrder', [
            'protoType' => $this->getLinkObj('protoType'),
            'salesConfirmation' => $this->getLinkObj('salesConfirmation'),
            'purchaseOrder' => $this->getLinkObj('purchaseOrder'),
            'manufacturingOrder' => $this->getLinkObj('manufacturingOrder'),
            'vendorInvoice' => $this->getLinkObj('vendorInvoice'),
            'invoice' => $this->getLinkObj('invoice'),
            'claim' => $this->getLinkObj('claim'),
            'creditDebitNote' => $this->getLinkObj('creditDebitNote'),
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['salesConfirmation'] = $this->getBundleObj('salesConfirmation', [
            'protoType' => $this->getLinkObj('protoType'),
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['purchaseOrder'] = $this->getBundleObj('purchaseOrder', [
            'protoType' => $this->getLinkObj('protoType'),
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['manufacturingOrder'] = $this->getBundleObj('manufacturingOrder', [
            'protoType' => $this->getLinkObj('protoType'),
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['invoice'] = $this->getBundleObj('invoice', [
            'protoType' => $this->getLinkObj('protoType'),
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['vendorInvoice'] = $this->getBundleObj('vendorInvoice', [
            'protoType' => $this->getLinkObj('protoType'),
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['accountsReceivable'] = $this->getBundleObj('accountsReceivable', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['accountsPayable'] = $this->getBundleObj('accountsPayable', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['creditDebitNote'] = $this->getBundleObj('creditDebitNote', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['claim'] = $this->getBundleObj('claim', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['message'] = $this->getBundleObj('message', [
            'attachment' => $linkObjAttachment,
        ]);

        $this->bundles['note'] = $this->getBundleObj('note', [
        ]);

        //----- PLM -----//
        $this->bundles['protoType'] = $this->getBundleObj('protoType', [
            'research' => $this->getLinkObj('research'),
            'attachment' => $linkObjAttachment,
            'image' => $linkObjImage,
            'colorMaster' => $this->getLinkObj('colorMaster'),
            'approvedPackaging' => $this->getLinkObj('approvedPackaging'),
            'approvedMaterial' => $this->getLinkObj('approvedMaterial'),
            'approvedComponent' => $this->getLinkObj('approvedComponent'),
            'masterPerformance' => $this->getLinkObj('masterPerformance'),
            'masterCompliance' => $this->getLinkObj('masterCompliance'),
            'productAssembly' => $this->getLinkObj('productAssembly'),
            'research' => $this->getLinkObj('research'),
            'costing' => $this->getLinkObj('costing'),
            'quotation' => $this->getLinkObj('quotation'),
            'tradingOrder' => $this->getLinkObj('tradingOrder'),
            'manufacturingOrder' => $this->getLinkObj('manufacturingOrder'),
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        $this->bundles['research'] = $this->getBundleObj('research', [
            'attachment' => $linkObjAttachment,
            'note' => $this->getLinkObj('note'),
            'message' => $this->getLinkObj('message'),
        ]);

        //----- Admin -----//
        $this->bundles['role'] = $this->getBundleObj('role', [
        ]);

        $this->bundles['user'] = $this->getBundleObj('user', [
        ]);

        $this->bundles['translation'] = $this->getBundleObj('translation', [
        ]);

        $this->bundles['setting'] = $this->getBundleObj('setting', [
        ]);

        $this->bundles['valuelist'] = $this->getBundleObj('valuelist', [
        ]);

        return $this;
    }

    public function getBundles()
    {
        return $this->bundles;
    }

    public function getBundleObj($name, $links = [])
    {
        $obj = new \stdClass;
        $obj->name = $name;
        $obj->links = $links; //array of link objects

        return $obj;
    }

    public function getLinkObj($name, $linkType = '', $relEntity = '', $deleteTarget = false, $targetEntity = '')
    {

        if ($relEntity == '') {
            $relEntity = ucfirst($name) . 'Bundle:' . ucfirst($name);
        }

        $obj = new \stdClass;
        $obj->name = $name;
        $obj->linkType = $linkType;
        $obj->relEntity = $relEntity;
        $obj->deleteTarget = $deleteTarget; //used when deleting a link
        $obj->targetEntity = $targetEntity; //used when deleting or doing search in a link

        return $obj;
    }
}