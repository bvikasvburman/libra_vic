<?php

namespace AppBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use AppBundle\Routing\BundlesRouteSetup;

class CloudPilotLoader extends Loader
{
    private $routes;
    private $requirementsMain;
    private $requirementsLink;

    public function load($resource, $type = null)
    {
        $this->requirementsMain = array(
            'id' => '\d+',
        );
        $this->requirementsLink = array(
            'id' => '\d+',
            'idl' => '\d+',
        );
        $this->routes = new RouteCollection();

        $bundlesForLoader = new BundlesRouteSetup();
        $bundles = $bundlesForLoader->setBundles()->getBundles();

        foreach ($bundles as $bundle => $bundleObj) {
            $this->setDefaultAPIRoutes($bundleObj);
            foreach ($bundleObj->links as $link => $linkObj) {
                $this->setLinksAPIRoutes($bundleObj, $linkObj);
            }

            //import manual routes from Resources/config/routing.yml
            $this->importManualRoutes($bundleObj);

        }

        return $this->routes;
    }

    public function setDefaultAPIRoutes($bundleObj)
    {
        $bundle = $bundleObj->name;
        $bundleName = ucfirst($bundle);

        //Default api routes
        //--------list---------//
        $routeName = "api_{$bundle}_list"; //ex: api_company_list
        $path      = "/api/{$bundle}"; //ex: /api/company

        $defaults = array(
            '_controller' => "{$bundleName}Bundle:{$bundleName}:getList",
            'offset' => null,
            'limit' => null,
        );

        $route = new Route($path, $defaults, $this->requirementsMain);
        $route->setMethods('GET');
        $this->routes->add($routeName, $route);

        //--------one---------//
        $routeName = "api_{$bundle}_one";//ex: api_company_one
        $path      = "/api/{$bundle}/{id}"; //ex: /api/company/{id}

        $defaults = array(
            '_controller' => "{$bundleName}Bundle:{$bundleName}:getOne",
        );
        $route = new Route($path, $defaults, $this->requirementsMain);
        $route->setMethods('GET');
        $this->routes->add($routeName, $route);

        //--------add---------//
        $routeName = "api_{$bundle}_add"; //ex: api_company_add
        $path      = "/api/{$bundle}"; //ex: /api/company

        $defaults = array(
            '_controller' => "{$bundleName}Bundle:{$bundleName}:save",
            'id' => null,
        );
        $route = new Route($path, $defaults, $this->requirementsMain);
        $route->setMethods('POST');
        $this->routes->add($routeName, $route);

        //--------save---------//
        $routeName = "api_{$bundle}_save"; //ex: api_company_save
        $path      = "/api/{$bundle}/{id}"; //ex: /api/company/{id}

        $defaults = array(
            '_controller' => "{$bundleName}Bundle:{$bundleName}:save",
        );
        $route = new Route($path, $defaults, $this->requirementsMain);
        $route->setMethods('PUT');
        $this->routes->add($routeName, $route);

        //--------delete---------//
        $routeName = "api_{$bundle}_delete"; //ex: api_company_delete
        $path      = "/api/{$bundle}/{id}"; //ex: /api/company/{id}

        $defaults = array(
            '_controller' => "{$bundleName}Bundle:{$bundleName}:delete",
        );
        $route = new Route($path, $defaults, $this->requirementsMain);
        $route->setMethods('DELETE');
        $this->routes->add($routeName, $route);
    }

    public function setLinksAPIRoutes($bundleObj, $linkObj)
    {
        $bundle     = $bundleObj->name;
        $bundleName = ucfirst($bundle);
        $link       = $linkObj->name;
        $linkName   = ucfirst($link);

        //Links api routes
        //--------list---------//
        $routeName = "api_{$bundle}_link_{$link}"; //ex: api_company_link_contact
        $path      = "/api/{$bundle}/{id}/{$link}"; //ex: /api/company/{id}/contact

        $defaults = array(
            '_controller' => "{$bundleName}Bundle:{$bundleName}:getListLink",
            'offset' => null,
            'limit' => null,
        );
        $route = new Route($path, $defaults, $this->requirementsMain);
        $route->setMethods('GET');
        $this->routes->add($routeName, $route);

        //--------one---------//
        $routeName = "api_{$bundle}_link_{$link}_one"; //ex: api_company_link_contact_one
        $path      = "/api/{$bundle}/{id}/{$link}/{idl}"; //ex: /api/company/{id}/contact/{idl}

        $defaults = array(
            '_controller' => "{$bundleName}Bundle:{$bundleName}:getOneLink",
        );
        $route = new Route($path, $defaults, $this->requirementsLink);
        $route->setMethods('GET');
        $this->routes->add($routeName, $route);

        //--------add---------//
        $routeName = "api_{$bundle}_link_{$link}_add"; //ex: api_company_link_attachment_add
        $path      = "/api/{$bundle}/{id}/{$link}"; //ex: /api/company/{id}/contact

        $controller = "{$bundleName}Bundle:{$bundleName}:saveLink";
        if ($linkObj->linkType == 'media') {
            $controller = "{$bundleName}Bundle:{$bundleName}:uploadMedia";
        }
        $defaults = array(
            '_controller' => $controller,
            'relEntity' => $linkObj->relEntity,
            'link' => $link,
        );
        $route = new Route($path, $defaults, $this->requirementsMain);
        $route->setMethods('POST');
        $this->routes->add($routeName, $route);

        //--------save---------//
        $routeName = "api_{$bundle}_link_{$link}_save"; //ex: api_company_link_attachment_save
        $path      = "/api/{$bundle}/{id}/{$link}/{idl}"; //ex: /api/company/{id}/contact/{idl}

        $defaults = array(
            '_controller' => "{$bundleName}Bundle:{$bundleName}:saveLink",
            'relEntity' => $linkObj->relEntity,
            'link' => $link,
        );
        $route = new Route($path, $defaults, $this->requirementsLink);
        $route->setMethods('PUT');
        $this->routes->add($routeName, $route);

        //--------delete---------//
        $routeName = "api_{$bundle}_link_{$link}_delete"; //ex: api_company_link_attachment_delete
        $path      = "/api/{$bundle}/{id}/{$link}/{idl}"; //ex: /api/company/{id}/contact/{idl}

        $defaults = array(
            '_controller' => "{$bundleName}Bundle:{$bundleName}:deleteLink",
            'relEntity' => $linkObj->relEntity,
            'deleteTarget' => $linkObj->deleteTarget,
            'targetEntity' => $linkObj->targetEntity,
            'link' => $link,
        );
        $route = new Route($path, $defaults, $this->requirementsLink);
        $route->setMethods('DELETE');
        $this->routes->add($routeName, $route);
    }

    public function importManualRoutes($bundleObj)
    {
        $bundle = $bundleObj->name;
        $bundleName = ucfirst($bundle);

        $resource = "@{$bundleName}Bundle/Resources/config/routing.yml";
        $type = 'yaml';
        $importedRoutes = $this->import($resource, $type);
        $this->routes->addCollection($importedRoutes);
    }


    public function supports($resource, $type = null)
    {
        return 'cloud_pilot' === $type;
    }
}