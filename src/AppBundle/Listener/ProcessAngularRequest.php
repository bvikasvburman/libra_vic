<?php
namespace AppBundle\Listener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Http\AccessMap;
use Symfony\Component\HttpFoundation\RequestMatcher;
use Symfony\Component\Security\Http\Firewall\AccessListener;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use \Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManager;
use RoleBundle\Entity\RoomRole;

/**
 * Listener class for Angular requests
 *
 */
class ProcessAngularRequest
{
	var $em                    = '';
	var $container             = '';
	var $securityContext       = '';
	var $accessMap             = '';
	var $accessDecisionManager = '';
	var $authenticationManager = '';

	public function __construct(EntityManager $entityManager, $container, $securityContext,
        $accessDecisionManager, $accessMap, $authenticationManager){
		$this->em = $entityManager;
		$this->container = $container;
		$this->securityContext = $securityContext;
		$this->accessDecisionManager = $accessDecisionManager;
		$this->accessMap = $accessMap;
		$this->authenticationManager = $authenticationManager;
	}

    /**
     * Add params to POST
     *
     * @param Symfony\Component\HttpKernel\Event\GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        // die("ssssssssss");
        // die($event->getRequest()->get('_route'));

        // print "aaaaaaa=====<br>\n";
        // print $this->securityContext . "ffffffff";
        // if ( null === $this->securityContext) {
        //     die("null--------");
        // }

        // if ( false === $this->securityContext->isGranted('IS_AUTHENTICATED_FULLY') ) {
        //     // print "ddddddddddddddddd";
        //     die("fffffffffff");
        // }

        // print_r(count($this->securityContext->getToken()->getRoles()));

        if ($event->getRequest()->get('_route') != '_wdt' &&
            count($this->securityContext->getToken()->getRoles()) > 0) {
            $arrRoomRole = array();

            $query = $this->em->createQueryBuilder()
                ->select('rr')
                ->from('RoleBundle:RoomRole', 'rr')
                ->where('rr.roleId = :roleId')
                ->setParameter('roleId', $this->securityContext->getToken()->getRoles()[0]->getId());
            $results = $query->getQuery()->getArrayResult();

            // die("===============");
            $arrRole['columns'] = $this->container->getParameter('RoomColumnName');

    		$accessMap = $this->accessMap;
			foreach ($results as $result) {
				foreach ($arrRole['columns'] as $c) {
     				$arrRoomRole[$result['roomId']] = $result;
     				$requestMatcher = new RequestMatcher('^/get-template/main/'.strtolower($result['roomTitle']).'/'.strtolower($result['roomTitle']).'/'.$c);
     				if($result['ac'.ucwords($c)] == 1) {
	     				$accessMap->add($requestMatcher, array($this->securityContext->getToken()->getRoles()[0]->getRoleType()));
     				} else {
     					$accessMap->add($requestMatcher, array());
     					if($c == 'list'){
     						$requestMatcher = new RequestMatcher('^/api/'.strtolower($result['roomTitle']));
     						$accessMap->add($requestMatcher, array());
     					}
     				}
				}

     		}

// 	    	$requestMatcher = new RequestMatcher('^/api/contact');
// 	    	$accessMap->add($requestMatcher, array('ROLE_ADMIN'));

//	    	print_r($accessMap->getPatterns($event->getRequest())); exit;

	    	if(is_array($accessMap->getPatterns($event->getRequest())[0]) && !in_array($this->securityContext->getToken()->getRoles()[0]->getRoleType(), $accessMap->getPatterns($event->getRequest())[0])) {
	    		$accessDeniedException = new AccessDeniedException();
	    		$event->setResponse(new Response($accessDeniedException->getMessage(), $accessDeniedException->getCode() ));
	    	}

	    	/*$accessListener = new AccessListener(
	    			$this->securityContext ,
	    			$this->accessDecisionManager,
	    			$accessMap,
	    			$this->authenticationManager
	    	);

	    	$authorizationChecker = new AuthorizationChecker(
	    			$this->securityContext,
	    			$this->authenticationManager,
	    			$this->accessDecisionManager
	    	);

	    	var_dump($authorizationChecker->isGranted('ROLE_ADMIN')); exit;

	    	if (!$authorizationChecker->isGranted('ROLE_USER')) {
	    		///throw new AccessDeniedException();
	    	}*/
    	}

        //  read phpinput
        $data = file_get_contents("php://input");

        if (!empty($data)){
            // json decode request params
            $params = json_decode($data, true);
            if (!empty($params)) {
                // set params in POST
                $event->getRequest()->request->add($params);
            }
        }
    }
}