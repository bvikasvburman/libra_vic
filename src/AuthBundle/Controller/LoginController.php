<?php

namespace AuthBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;

class LoginController extends Controller
{
    /**
     * Display the login form
     *
     * @return Response
     */
    public function indexAction(Request $request)
    {
        $viewParams = array(
            'last_username' => $this->getLastUsername($request),
            'error'         => $this->getLoginOperation()->getError($request),
            'csrf_token'    => $this->getCsrfToken(),
        );

        return $this->render('AuthBundle:Login:index.html.twig', $viewParams);
    }

    /**
     * Get last username entered by user
     *
     * @param Symfony\Component\HttpFoundation\Request $request
     *
     * @return string
     */
    protected function getLastUsername($request)
    {
        $session = $request->getSession();

        return (null === $session) ? '' : $session->get(SecurityContext::LAST_USERNAME);
    }

    /**
     * Get token for CSRF protection
     *
     * @return string
     */
    protected function getCsrfToken()
    {
        return $this->get('form.csrf_provider')->generateCsrfToken('authenticate');
    }

    /**
     * Get user operation.
     *
     * @return Auth\Operation\LoginOperation
     */
    protected function getLoginOperation()
    {
        return $this->container->get('loginop');
    }
}
