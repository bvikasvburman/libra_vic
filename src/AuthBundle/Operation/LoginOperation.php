<?php

namespace AuthBundle\Operation;

use Symfony\Component\Security\Core\SecurityContext;
use AppBundle\Component\AbstractOperation;

/**
 * Login operation class
 *
 * @author Nicolae Tusinean
 */
class LoginOperation extends AbstractOperation
{
    /**
     * Get error from login attempt
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return string
     */
    public function getError(\Symfony\Component\HttpFoundation\Request $request)
    {
        $session = $request->getSession();

        $error = '';
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } elseif (null !== $session && $session->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }

        if ($error) {
            $error = $error->getMessage();
        }

        return $error;
    }

}
