<?php
namespace ApprovedMaterialBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ApprovedMaterialRepository extends AbstractRepository
{
    public function getAlias() {
        return 'am';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('am')
              ->select('am, c, pl, uc, um')
              ->leftjoin('am.company', 'c')
              ->leftjoin('am.partsLocation', 'pl')
              ->leftjoin('am.userCreated', 'uc')
              ->leftjoin('am.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'am.id', $id);
        }

        if ($sVars) {
            $qb = $dbUtil->setWhere($qb, 'am.company_id', $sVars->criteria);
        }
        return $qb;
    }
}
