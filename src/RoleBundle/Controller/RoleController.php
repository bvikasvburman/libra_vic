<?php
namespace RoleBundle\Controller;

use AppBundle\Component\AbstractController;
use AppBundle\Component\Util;
use AppBundle\Component\JsonResponse;

class RoleController extends AbstractController
{
	/*public function getListAction()
    {
		$req = $this->getRequest();
        $criteria = $req->query->get('criteria') ? $req->query->get('criteria') : [];
        $sort     = $req->query->get('sort') ? $req->query->get('sort') : [];
        $limit    = $req->query->get('limit');
        $offset   = $req->query->get('offset');

		$rows = $this->getOperation('room')->getList($criteria, $sort, $limit, $offset);
		//print_r($rows);exit;
        return new JsonResponse($this->container, $rows);
    }*/

	public function getOneAction($id)
    {
       // $bundleName = $this->util->getCurrentBundleName($this->container);
        $row = $this->getOperation('role')->getOne($id);
		//print_r($row); exit;
        return new JsonResponse($this->container, $row);
    }

	public function roomTemplateAction()
    {
		$row =  $this->getDoctrine()->getRepository('RoleBundle:Role')->roomTemplateFileds();
		return $this->render('RoleBundle:Role:room_list.html.twig',
            array('rooms' => $row)
        );
	}
}
