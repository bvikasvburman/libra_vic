<?php
namespace RoleBundle\Operation;

use \Symfony\Component\DependencyInjection\ContainerAware;
use \Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Component\AbstractOperation;
use RoleBundle\Entity\Role;

class RoleOperation extends AbstractOperation
{
    /**
     * Get the service container
     *
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    public function getContainer()
    {
        return $this->container;
    }

    /**
     * Get the entity manager
     *
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        // get the document manager
        return $this->getContainer()->get('doctrine')->getManager();
    }

    /**
     * Save a main bundle entity ex: Company
     *
     */
    public function save($postData)
    {
        $bundleName = $this->util->getCurrentBundleName();
        $entityPath = "{$bundleName}Bundle\Entity\\{$bundleName}";
        $serializer = $this->getJmsSerializer();


		//print_r($room);
		//exit;
        //print_r($postData);
        //die();
        $user = $this->container->get('security.context')->getToken()->getUser();

        $entityDeSerialized = $serializer->deserialize(json_encode($postData), $entityPath, 'json');

        if (method_exists($entityDeSerialized,'setUserCreated')) {
            $entityDeSerialized->setUserModified($user);
            if (!$entityDeSerialized->getId()) { //new record
                $entityDeSerialized->setUserCreated($user);
            }
        }
		$return = $this->getRepository()->save($entityDeSerialized);

		$query = $this->getEntityManager()->createQueryBuilder();
		$query
			->select('r.roomId,r.roomName')
			->from('RoleBundle:Room', 'r');
		$arrRooms = $query->getQuery()->getArrayResult();

		$room = array();
		foreach($arrRooms as $v) {
            $room[$v['roomId']]['roomId']    = $v['roomId'];
            $room[$v['roomId']]['roomTitle'] = $v['roomName'];
            $room[$v['roomId']]['roleId']    = $return->getId();
			if(isset($postData['roomRoleId_'.$v['roomId']])) {
				$room[$v['roomId']]['roomRoleId'] = $postData['roomRoleId_' . $v['roomId']];
			}
			//$room[$v]['roomTitle'] = $v.'-'.$postData['id'];
			foreach($this->container->getParameter('RoomColumnName') as $c) {
				$room[$v['roomId']]['ac'.ucwords($c)] = ($postData[$c . '_' . $v['roomId']])? 1 : 0;
			}
		}

		foreach($room as $value) {
			$entityDeSerializedRoomRole = $serializer->deserialize(
                json_encode($value),
                'RoleBundle\Entity\RoomRole', 'json');
			if(!isset($value['roomRoleId'])) {
				$entityDeSerializedRoomRole->setCreationDate(new \DateTime(date('Y-m-d H:i:s')));
			}
			$entityDeSerializedRoomRole->setModificationDate(new \DateTime(date('Y-m-d H:i:s')));
			$this->getRepository()->save($entityDeSerializedRoomRole);
		}
        // \Doctrine\Common\Util\Debug::dump($usr);
        // die();

        return $return;
    }
}