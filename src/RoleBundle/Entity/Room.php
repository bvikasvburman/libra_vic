<?php

namespace RoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Room
 */
class Room
{
    /**
     * @var integer
     */
    private $roomId;

    /**
     * @var boolean
     */
    private $list;

    /**
     * @var boolean
     */
    private $detail;

    /**
     * @var boolean
     */
    private $new;

    /**
     * @var boolean
     */
    private $edit;

    /**
     * @var boolean
     */
    private $delete;

    /**
     * @var boolean
     */
    private $duplicate;

    /**
     * @var boolean
     */
    private $search;

    /**
     * @var boolean
     */
    private $print;

    /**
     * @var boolean
     */
    private $publish;

    /**
     * @var boolean
     */
    private $unpublish;

    /**
     * @var boolean
     */
    private $import;

    /**
     * @var boolean
     */
    private $export;

    /**
     * @var string
     */
    private $roomName;

    /**
     * @var string
     */
    private $title;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;


    /**
     * Get roomId
     *
     * @return integer 
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Set list
     *
     * @param boolean $list
     * @return Room
     */
    public function setList($list)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return boolean 
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Set detail
     *
     * @param boolean $detail
     * @return Room
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return boolean 
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set new
     *
     * @param boolean $new
     * @return Room
     */
    public function setNew($new)
    {
        $this->new = $new;

        return $this;
    }

    /**
     * Get new
     *
     * @return boolean 
     */
    public function getNew()
    {
        return $this->new;
    }

    /**
     * Set edit
     *
     * @param boolean $edit
     * @return Room
     */
    public function setEdit($edit)
    {
        $this->edit = $edit;

        return $this;
    }

    /**
     * Get edit
     *
     * @return boolean 
     */
    public function getEdit()
    {
        return $this->edit;
    }

    /**
     * Set delete
     *
     * @param boolean $delete
     * @return Room
     */
    public function setDelete($delete)
    {
        $this->delete = $delete;

        return $this;
    }

    /**
     * Get delete
     *
     * @return boolean 
     */
    public function getDelete()
    {
        return $this->delete;
    }

    /**
     * Set duplicate
     *
     * @param boolean $duplicate
     * @return Room
     */
    public function setDuplicate($duplicate)
    {
        $this->duplicate = $duplicate;

        return $this;
    }

    /**
     * Get duplicate
     *
     * @return boolean 
     */
    public function getDuplicate()
    {
        return $this->duplicate;
    }

    /**
     * Set search
     *
     * @param boolean $search
     * @return Room
     */
    public function setSearch($search)
    {
        $this->search = $search;

        return $this;
    }

    /**
     * Get search
     *
     * @return boolean 
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * Set print
     *
     * @param boolean $print
     * @return Room
     */
    public function setPrint($print)
    {
        $this->print = $print;

        return $this;
    }

    /**
     * Get print
     *
     * @return boolean 
     */
    public function getPrint()
    {
        return $this->print;
    }

    /**
     * Set publish
     *
     * @param boolean $publish
     * @return Room
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return boolean 
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set unpublish
     *
     * @param boolean $unpublish
     * @return Room
     */
    public function setUnpublish($unpublish)
    {
        $this->unpublish = $unpublish;

        return $this;
    }

    /**
     * Get unpublish
     *
     * @return boolean 
     */
    public function getUnpublish()
    {
        return $this->unpublish;
    }

    /**
     * Set import
     *
     * @param boolean $import
     * @return Room
     */
    public function setImport($import)
    {
        $this->import = $import;

        return $this;
    }

    /**
     * Get import
     *
     * @return boolean 
     */
    public function getImport()
    {
        return $this->import;
    }

    /**
     * Set export
     *
     * @param boolean $export
     * @return Room
     */
    public function setExport($export)
    {
        $this->export = $export;

        return $this;
    }

    /**
     * Get export
     *
     * @return boolean 
     */
    public function getExport()
    {
        return $this->export;
    }

    /**
     * Set roomName
     *
     * @param string $roomName
     * @return Room
     */
    public function setRoomName($roomName)
    {
        $this->roomName = $roomName;

        return $this;
    }

    /**
     * Get roomName
     *
     * @return string 
     */
    public function getRoomName()
    {
        return $this->roomName;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Room
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Room
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return Room
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
}
