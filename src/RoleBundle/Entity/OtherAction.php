<?php

namespace RoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OtherAction
 */
class OtherAction
{
    /**
     * @var integer
     */
    private $otherActionId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $title;

    /**
     * @var boolean
     */
    private $hasList;

    /**
     * @var boolean
     */
    private $hasDetail;

    /**
     * @var boolean
     */
    private $hasNew;

    /**
     * @var boolean
     */
    private $hasEdit;

    /**
     * @var boolean
     */
    private $hasFind;

    /**
     * @var boolean
     */
    private $hasModify;

    /**
     * @var boolean
     */
    private $isGlobal;

    /**
     * @var integer
     */
    private $roomId;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;


    /**
     * Get otherActionId
     *
     * @return integer 
     */
    public function getOtherActionId()
    {
        return $this->otherActionId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return OtherAction
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return OtherAction
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set hasList
     *
     * @param boolean $hasList
     * @return OtherAction
     */
    public function setHasList($hasList)
    {
        $this->hasList = $hasList;

        return $this;
    }

    /**
     * Get hasList
     *
     * @return boolean 
     */
    public function getHasList()
    {
        return $this->hasList;
    }

    /**
     * Set hasDetail
     *
     * @param boolean $hasDetail
     * @return OtherAction
     */
    public function setHasDetail($hasDetail)
    {
        $this->hasDetail = $hasDetail;

        return $this;
    }

    /**
     * Get hasDetail
     *
     * @return boolean 
     */
    public function getHasDetail()
    {
        return $this->hasDetail;
    }

    /**
     * Set hasNew
     *
     * @param boolean $hasNew
     * @return OtherAction
     */
    public function setHasNew($hasNew)
    {
        $this->hasNew = $hasNew;

        return $this;
    }

    /**
     * Get hasNew
     *
     * @return boolean 
     */
    public function getHasNew()
    {
        return $this->hasNew;
    }

    /**
     * Set hasEdit
     *
     * @param boolean $hasEdit
     * @return OtherAction
     */
    public function setHasEdit($hasEdit)
    {
        $this->hasEdit = $hasEdit;

        return $this;
    }

    /**
     * Get hasEdit
     *
     * @return boolean 
     */
    public function getHasEdit()
    {
        return $this->hasEdit;
    }

    /**
     * Set hasFind
     *
     * @param boolean $hasFind
     * @return OtherAction
     */
    public function setHasFind($hasFind)
    {
        $this->hasFind = $hasFind;

        return $this;
    }

    /**
     * Get hasFind
     *
     * @return boolean 
     */
    public function getHasFind()
    {
        return $this->hasFind;
    }

    /**
     * Set hasModify
     *
     * @param boolean $hasModify
     * @return OtherAction
     */
    public function setHasModify($hasModify)
    {
        $this->hasModify = $hasModify;

        return $this;
    }

    /**
     * Get hasModify
     *
     * @return boolean 
     */
    public function getHasModify()
    {
        return $this->hasModify;
    }

    /**
     * Set isGlobal
     *
     * @param boolean $isGlobal
     * @return OtherAction
     */
    public function setIsGlobal($isGlobal)
    {
        $this->isGlobal = $isGlobal;

        return $this;
    }

    /**
     * Get isGlobal
     *
     * @return boolean 
     */
    public function getIsGlobal()
    {
        return $this->isGlobal;
    }

    /**
     * Set roomId
     *
     * @param integer $roomId
     * @return OtherAction
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;

        return $this;
    }

    /**
     * Get roomId
     *
     * @return integer 
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return OtherAction
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return OtherAction
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
}
