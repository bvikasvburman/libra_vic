<?php

namespace RoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoomRole
 */
class RoomRole
{
    /**
     * @var integer
     */
    private $roomRoleId;

    /**
     * @var integer
     */
    private $roomId;

    /**
     * @var boolean
     */
    private $list;

    /**
     * @var boolean
     */
    private $detail;

    /**
     * @var boolean
     */
    private $new;

    /**
     * @var boolean
     */
    private $edit;

    /**
     * @var boolean
     */
    private $delete;

    /**
     * @var boolean
     */
    private $duplicate;

    /**
     * @var boolean
     */
    private $print;

    /**
     * @var boolean
     */
    private $publish;

    /**
     * @var boolean
     */
    private $unpublish;

    /**
     * @var boolean
     */
    private $import;

    /**
     * @var boolean
     */
    private $export;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var integer
     */
    private $roleId;

    /**
     * @var string
     */
    private $roomTitle;

    /**
     * @var \DateTime
     */
    private $modificationDate;


    /**
     * Get roomRoleId
     *
     * @return integer 
     */
    public function getRoomRoleId()
    {
        return $this->roomRoleId;
    }

    /**
     * Set roomId
     *
     * @param integer $roomId
     * @return RoomRole
     */
    public function setRoomId($roomId)
    {
        $this->roomId = $roomId;

        return $this;
    }

    /**
     * Get roomId
     *
     * @return integer 
     */
    public function getRoomId()
    {
        return $this->roomId;
    }

    /**
     * Set list
     *
     * @param boolean $list
     * @return RoomRole
     */
    public function setList($list)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return boolean 
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Set detail
     *
     * @param boolean $detail
     * @return RoomRole
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * Get detail
     *
     * @return boolean 
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * Set new
     *
     * @param boolean $new
     * @return RoomRole
     */
    public function setNew($new)
    {
        $this->new = $new;

        return $this;
    }

    /**
     * Get new
     *
     * @return boolean 
     */
    public function getNew()
    {
        return $this->new;
    }

    /**
     * Set edit
     *
     * @param boolean $edit
     * @return RoomRole
     */
    public function setEdit($edit)
    {
        $this->edit = $edit;

        return $this;
    }

    /**
     * Get edit
     *
     * @return boolean 
     */
    public function getEdit()
    {
        return $this->edit;
    }

    /**
     * Set delete
     *
     * @param boolean $delete
     * @return RoomRole
     */
    public function setDelete($delete)
    {
        $this->delete = $delete;

        return $this;
    }

    /**
     * Get delete
     *
     * @return boolean 
     */
    public function getDelete()
    {
        return $this->delete;
    }

    /**
     * Set duplicate
     *
     * @param boolean $duplicate
     * @return RoomRole
     */
    public function setDuplicate($duplicate)
    {
        $this->duplicate = $duplicate;

        return $this;
    }

    /**
     * Get duplicate
     *
     * @return boolean 
     */
    public function getDuplicate()
    {
        return $this->duplicate;
    }

    /**
     * Set print
     *
     * @param boolean $print
     * @return RoomRole
     */
    public function setPrint($print)
    {
        $this->print = $print;

        return $this;
    }

    /**
     * Get print
     *
     * @return boolean 
     */
    public function getPrint()
    {
        return $this->print;
    }

    /**
     * Set publish
     *
     * @param boolean $publish
     * @return RoomRole
     */
    public function setPublish($publish)
    {
        $this->publish = $publish;

        return $this;
    }

    /**
     * Get publish
     *
     * @return boolean 
     */
    public function getPublish()
    {
        return $this->publish;
    }

    /**
     * Set unpublish
     *
     * @param boolean $unpublish
     * @return RoomRole
     */
    public function setUnpublish($unpublish)
    {
        $this->unpublish = $unpublish;

        return $this;
    }

    /**
     * Get unpublish
     *
     * @return boolean 
     */
    public function getUnpublish()
    {
        return $this->unpublish;
    }

    /**
     * Set import
     *
     * @param boolean $import
     * @return RoomRole
     */
    public function setImport($import)
    {
        $this->import = $import;

        return $this;
    }

    /**
     * Get import
     *
     * @return boolean 
     */
    public function getImport()
    {
        return $this->import;
    }

    /**
     * Set export
     *
     * @param boolean $export
     * @return RoomRole
     */
    public function setExport($export)
    {
        $this->export = $export;

        return $this;
    }

    /**
     * Get export
     *
     * @return boolean 
     */
    public function getExport()
    {
        return $this->export;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return RoomRole
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set roleId
     *
     * @param integer $roleId
     * @return RoomRole
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * Get roleId
     *
     * @return integer 
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * Set roomTitle
     *
     * @param string $roomTitle
     * @return RoomRole
     */
    public function setRoomTitle($roomTitle)
    {
        $this->roomTitle = $roomTitle;

        return $this;
    }

    /**
     * Get roomTitle
     *
     * @return string 
     */
    public function getRoomTitle()
    {
        return $this->roomTitle;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return RoomRole
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    /**
     * @var boolean
     */
    private $search;


    /**
     * Set search
     *
     * @param boolean $search
     * @return RoomRole
     */
    public function setSearch($search)
    {
        $this->search = $search;

        return $this;
    }

    /**
     * Get search
     *
     * @return boolean 
     */
    public function getSearch()
    {
        return $this->search;
    }
    /**
     * @var boolean
     */
    private $acList;

    /**
     * @var boolean
     */
    private $acDetail;

    /**
     * @var boolean
     */
    private $acNew;

    /**
     * @var boolean
     */
    private $acEdit;

    /**
     * @var boolean
     */
    private $acDelete;

    /**
     * @var boolean
     */
    private $acDuplicate;

    /**
     * @var boolean
     */
    private $acSearch;

    /**
     * @var boolean
     */
    private $acPrint;

    /**
     * @var boolean
     */
    private $acPublish;

    /**
     * @var boolean
     */
    private $acUnpublish;

    /**
     * @var boolean
     */
    private $acImport;

    /**
     * @var boolean
     */
    private $acExport;


    /**
     * Set acList
     *
     * @param boolean $acList
     * @return RoomRole
     */
    public function setAcList($acList)
    {
        $this->acList = $acList;

        return $this;
    }

    /**
     * Get acList
     *
     * @return boolean 
     */
    public function getAcList()
    {
        return $this->acList;
    }

    /**
     * Set acDetail
     *
     * @param boolean $acDetail
     * @return RoomRole
     */
    public function setAcDetail($acDetail)
    {
        $this->acDetail = $acDetail;

        return $this;
    }

    /**
     * Get acDetail
     *
     * @return boolean 
     */
    public function getAcDetail()
    {
        return $this->acDetail;
    }

    /**
     * Set acNew
     *
     * @param boolean $acNew
     * @return RoomRole
     */
    public function setAcNew($acNew)
    {
        $this->acNew = $acNew;

        return $this;
    }

    /**
     * Get acNew
     *
     * @return boolean 
     */
    public function getAcNew()
    {
        return $this->acNew;
    }

    /**
     * Set acEdit
     *
     * @param boolean $acEdit
     * @return RoomRole
     */
    public function setAcEdit($acEdit)
    {
        $this->acEdit = $acEdit;

        return $this;
    }

    /**
     * Get acEdit
     *
     * @return boolean 
     */
    public function getAcEdit()
    {
        return $this->acEdit;
    }

    /**
     * Set acDelete
     *
     * @param boolean $acDelete
     * @return RoomRole
     */
    public function setAcDelete($acDelete)
    {
        $this->acDelete = $acDelete;

        return $this;
    }

    /**
     * Get acDelete
     *
     * @return boolean 
     */
    public function getAcDelete()
    {
        return $this->acDelete;
    }

    /**
     * Set acDuplicate
     *
     * @param boolean $acDuplicate
     * @return RoomRole
     */
    public function setAcDuplicate($acDuplicate)
    {
        $this->acDuplicate = $acDuplicate;

        return $this;
    }

    /**
     * Get acDuplicate
     *
     * @return boolean 
     */
    public function getAcDuplicate()
    {
        return $this->acDuplicate;
    }

    /**
     * Set acSearch
     *
     * @param boolean $acSearch
     * @return RoomRole
     */
    public function setAcSearch($acSearch)
    {
        $this->acSearch = $acSearch;

        return $this;
    }

    /**
     * Get acSearch
     *
     * @return boolean 
     */
    public function getAcSearch()
    {
        return $this->acSearch;
    }

    /**
     * Set acPrint
     *
     * @param boolean $acPrint
     * @return RoomRole
     */
    public function setAcPrint($acPrint)
    {
        $this->acPrint = $acPrint;

        return $this;
    }

    /**
     * Get acPrint
     *
     * @return boolean 
     */
    public function getAcPrint()
    {
        return $this->acPrint;
    }

    /**
     * Set acPublish
     *
     * @param boolean $acPublish
     * @return RoomRole
     */
    public function setAcPublish($acPublish)
    {
        $this->acPublish = $acPublish;

        return $this;
    }

    /**
     * Get acPublish
     *
     * @return boolean 
     */
    public function getAcPublish()
    {
        return $this->acPublish;
    }

    /**
     * Set acUnpublish
     *
     * @param boolean $acUnpublish
     * @return RoomRole
     */
    public function setAcUnpublish($acUnpublish)
    {
        $this->acUnpublish = $acUnpublish;

        return $this;
    }

    /**
     * Get acUnpublish
     *
     * @return boolean 
     */
    public function getAcUnpublish()
    {
        return $this->acUnpublish;
    }

    /**
     * Set acImport
     *
     * @param boolean $acImport
     * @return RoomRole
     */
    public function setAcImport($acImport)
    {
        $this->acImport = $acImport;

        return $this;
    }

    /**
     * Get acImport
     *
     * @return boolean 
     */
    public function getAcImport()
    {
        return $this->acImport;
    }

    /**
     * Set acExport
     *
     * @param boolean $acExport
     * @return RoomRole
     */
    public function setAcExport($acExport)
    {
        $this->acExport = $acExport;

        return $this;
    }

    /**
     * Get acExport
     *
     * @return boolean 
     */
    public function getAcExport()
    {
        return $this->acExport;
    }
}
