<?php

namespace RoleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RoleOtherAction
 */
class RoleOtherAction
{
    /**
     * @var integer
     */
    private $roleOtherActionId;

    /**
     * @var integer
     */
    private $otherActionId;

    /**
     * @var integer
     */
    private $roleId;

    /**
     * @var boolean
     */
    private $hasAccess;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;


    /**
     * Get roleOtherActionId
     *
     * @return integer 
     */
    public function getRoleOtherActionId()
    {
        return $this->roleOtherActionId;
    }

    /**
     * Set otherActionId
     *
     * @param integer $otherActionId
     * @return RoleOtherAction
     */
    public function setOtherActionId($otherActionId)
    {
        $this->otherActionId = $otherActionId;

        return $this;
    }

    /**
     * Get otherActionId
     *
     * @return integer 
     */
    public function getOtherActionId()
    {
        return $this->otherActionId;
    }

    /**
     * Set roleId
     *
     * @param integer $roleId
     * @return RoleOtherAction
     */
    public function setRoleId($roleId)
    {
        $this->roleId = $roleId;

        return $this;
    }

    /**
     * Get roleId
     *
     * @return integer 
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * Set hasAccess
     *
     * @param boolean $hasAccess
     * @return RoleOtherAction
     */
    public function setHasAccess($hasAccess)
    {
        $this->hasAccess = $hasAccess;

        return $this;
    }

    /**
     * Get hasAccess
     *
     * @return boolean 
     */
    public function getHasAccess()
    {
        return $this->hasAccess;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return RoleOtherAction
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return RoleOtherAction
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
}
