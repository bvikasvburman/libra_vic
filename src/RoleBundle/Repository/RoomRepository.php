<?php

namespace RoleBundle\Repository;

use AppBundle\Component\AbstractRepository;
use Doctrine\ORM\Query;

class RoomRepository extends AbstractRepository
{
	public function getAlias() {
        return 'ro';
    }

    public function getQB($id = NULL)
    {
        $qb = $this->createQueryBuilder('ro')
              ->select('ro')
			  ->orderBy('ro.roomName', 'DESC');
        return $qb;
    }
	
	public function getList($criteria = [], $sort = null, $limit = null, $offset = null)
    {
		$connection = $this->getEntityManager()->getConnection();
		
        $qb = $this->getQB(null, $criteria);

        $query = $qb->getQuery()
            ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, 1);
        $arrRoom = $query->getArrayResult();
		
		$query = $this->getEntityManager()->createQueryBuilder();
        $query
            ->select('rr')
            ->from('RoleBundle:RoomRole', 'rr');
        
        $result = $query->getQuery()->getArrayResult();
		$arrRoomRole = array();
		foreach($result as $k => $v) {
			$arrRoomRole[$v['roomId']] = $v;
		}
		//print_r($arrRoomRole[1]); exit;
		/*SELECT r.room_id as rid, rr.room_id as rrid FROM `ma_room` r LEFT JOIN ma_room_role rr ON (r.room_id = rr.room_id)
		$statement = $connection->prepare("SELECT  rr.*  FROM `ma_room` r LEFT JOIN ma_room_role rr ON (r.room_id = rr.room_id)");
		$statement->execute();
		$result = $statement->fetchAll();
		print_r($result); exit;*/
		
		$statement = $connection->prepare("SHOW COLUMNS FROM ma_room WHERE type = 'tinyint(4)'");
		$statement->execute();
		$columns = $statement->fetchAll();

		foreach($columns as $k => $v) {
			for($i=0; $i < sizeof($arrRoom); $i++) {
				if($arrRoom[$i][$v['Field']]) {		
					$isChecked = '';
					if(isset($arrRoomRole[$arrRoom[$i]['roomId']])) {
						$isChecked = (($arrRoomRole[$arrRoom[$i]['roomId']][$v['Field']])? 'checked=checked' : '');
					}
					$arrRoom[$i][$v['Field']] = "<input type='checkbox' name='".$v['Field']."_".$i."' id='".$v['Field']."_".$i."' ng-model='".$v['Field']."_".$i."' ".$isChecked."/>";
				} else {
					$arrRoom[$i][$v['Field']] = "-";
				}
			}	
		}

        if ($arrRoom) {
            return $arrRoom;
        }

        return null;

    }
}
