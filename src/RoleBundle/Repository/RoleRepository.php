<?php
namespace RoleBundle\Repository;

use AppBundle\Component\AbstractRepository;
use Doctrine\ORM\Query;

class RoleRepository extends AbstractRepository
{
    public function getAlias() {
        return 'r';
    }

    public function getQB($id = null, $sVars = null)
    {
        $qb = $this->createQueryBuilder('r')
              ->select('r, uc, um')
              ->leftjoin('r.userCreated', 'uc')
              ->leftjoin('r.userModified', 'um');
        
        if ($id) {
        	$qb = $qb->where("r.id = :id")
        	->setParameter(':id', $id);
        }
        
        return $qb;
    }
	
	/**
     * Get a record by its id.
     *
     * @param integer $id
     *
     */
    public function getOne($id)
    {
        if ($id && is_numeric($id)) {
            $query = $this->getQB($id)->getQuery()
                ->setHint(Query::HINT_FORCE_PARTIAL_LOAD, 1);
			$arrRole = $query->getArrayResult()[0];

			$query = $this->getEntityManager()->createQueryBuilder();
			$query
				->select('rr')
				->from('RoleBundle:RoomRole', 'rr')
				->where('rr.roleId = :roleId')
				->setParameter('roleId', $id);
			$result = $query->getQuery()->getArrayResult();
			
			/*$connection = $this->getEntityManager()->getConnection();
			$statement = $connection->prepare("SHOW COLUMNS FROM ma_room WHERE type = 'tinyint(4)'");
			$statement->execute();
			$arrRole['columns'] = $statement->fetchAll();*/
			$arrRole['columns'] = $this->container->getParameter('RoomColumnName');
			
			$arrRole['roomId'] = array();
			
			$query = $this->getEntityManager()->createQueryBuilder();
			$query
				->select('r.roomId,r.roomName')
				->from('RoleBundle:Room', 'r');
			$arrRooms = $query->getQuery()->getArrayResult();
			foreach($arrRooms as $v) {
				$arrRole['roomId'][] = $v['roomId'];
			}
			
			foreach($result as $k => $v) {
				//$arrRole['roomId'][] = $v['roomId'];
				if(isset($v['roomRoleId'])) {
					$arrRole['roomRoleId'][$v['roomId']] = $v['roomRoleId'];
				}
				foreach($arrRole['columns'] as $c) {
					$arrRole[$c.'_'.$v['roomId']] = $v['ac'.ucwords($c)];
				}	
			}
			
			return $arrRole;
        }

        return null;
    }
	
	public function roomTemplateFileds()
    {
		$connection = $this->getEntityManager()->getConnection();

		$query = $this->getEntityManager()->createQueryBuilder();
		$query
			->select('r')
			->from('RoleBundle:Room', 'r');
		$arrRoom['room'] = $query->getQuery()->getArrayResult();
		
		$statement = $connection->prepare("SHOW COLUMNS FROM ma_room WHERE type = 'tinyint(4)'");
		$statement->execute();
		$arrRoom['columns'] = $statement->fetchAll();
		
		return $arrRoom;
	}
}
