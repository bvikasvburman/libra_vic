<?php
namespace ProtoTypeBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ProtoTypeRepository extends AbstractRepository
{
    public function getAlias() {
        return 'pt';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('pt')
            ->addSelect('pt, uc, um, pc.productFamily, pc.productType')
            ->addSelect('partial e.{id, code}')
            ->distinct()
            ->leftJoin('ProductCodingBundle:ProductCoding', 'pc', 'WITH',
                       'pt.productFamilyCode = pc.productFamilyCode')
            ->leftjoin('pt.enquiry', 'e')
            ->leftjoin('pt.userCreated', 'uc')
            ->leftjoin('pt.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'pt.id', $id);
        }
        return $qb;
    }

    public function getQBLinkResearch($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('r, e, c')
            ->addSelect('partial pt.{id, code}')
            ->from('ResearchBundle:Research', 'r')
            ->leftjoin('r.enquiry', 'e')
            ->leftjoin('r.company', 'c')
            ->leftjoin('r.protoType', 'pt');
        $qb = $this->dbUtil->setWhere($qb, 'r.protoTypeId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'r.id', $idl);
        return $qb;
    }

    public function getQBLinkColorMaster($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('ptcm')
            ->addSelect('cm')
            ->from('ProtoTypeBundle:ProtoTypeColorMaster', 'ptcm')
            ->leftjoin('ptcm.colorMaster', 'cm');
        $qb = $this->dbUtil->setWhere($qb, 'ptcm.protoTypeId', $id);
        // $qb = $this->dbUtil->setWhere($qb, 'ptcm.id', $idl);
        return $qb;
    }

}
