<?php

namespace ProtoTypeBundle\Entity;

class ProtoType
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $enquiryId;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $artNo;

    /**
     * @var string
     */
    private $productFamilyCode;

    /**
     * @var string
     */
    private $productTypeCode;

    /**
     * @var string
     */
    private $colorCode;

    /**
     * @var string
     */
    private $productName;

    /**
     * @var string
     */
    private $creationSequence;

    /**
     * @var \DateTime
     */
    private $sampleReadyDate;

    /**
     * @var integer
     */
    private $requestedQty;

    /**
     * @var string
     */
    private $weight;

    /**
     * @var string
     */
    private $capacity;

    /**
     * @var string
     */
    private $dimLength;

    /**
     * @var string
     */
    private $dimWidth;

    /**
     * @var string
     */
    private $dimHeight;

    /**
     * @var integer
     */
    private $companyIdSupplier;

    /**
     * @var integer
     */
    private $companyIdClient;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mediaHistory;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \EnquiryBundle\Entity\Enquiry
     */
    private $enquiry;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mediaHistory = new \Doctrine\Common\Collections\ArrayCollection();
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set enquiryId
     *
     * @param integer $enquiryId
     * @return ProtoType
     */
    public function setEnquiryId($enquiryId)
    {
        $this->enquiryId = $enquiryId;

        return $this;
    }

    /**
     * Get enquiryId
     *
     * @return integer 
     */
    public function getEnquiryId()
    {
        return $this->enquiryId;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return ProtoType
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set artNo
     *
     * @param string $artNo
     * @return ProtoType
     */
    public function setArtNo($artNo)
    {
        $this->artNo = $artNo;

        return $this;
    }

    /**
     * Get artNo
     *
     * @return string 
     */
    public function getArtNo()
    {
        return $this->artNo;
    }

    /**
     * Set productFamilyCode
     *
     * @param string $productFamilyCode
     * @return ProtoType
     */
    public function setProductFamilyCode($productFamilyCode)
    {
        $this->productFamilyCode = $productFamilyCode;

        return $this;
    }

    /**
     * Get productFamilyCode
     *
     * @return string 
     */
    public function getProductFamilyCode()
    {
        return $this->productFamilyCode;
    }

    /**
     * Set productTypeCode
     *
     * @param string $productTypeCode
     * @return ProtoType
     */
    public function setProductTypeCode($productTypeCode)
    {
        $this->productTypeCode = $productTypeCode;

        return $this;
    }

    /**
     * Get productTypeCode
     *
     * @return string 
     */
    public function getProductTypeCode()
    {
        return $this->productTypeCode;
    }

    /**
     * Set colorCode
     *
     * @param string $colorCode
     * @return ProtoType
     */
    public function setColorCode($colorCode)
    {
        $this->colorCode = $colorCode;

        return $this;
    }

    /**
     * Get colorCode
     *
     * @return string 
     */
    public function getColorCode()
    {
        return $this->colorCode;
    }

    /**
     * Set productName
     *
     * @param string $productName
     * @return ProtoType
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productName
     *
     * @return string 
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set creationSequence
     *
     * @param string $creationSequence
     * @return ProtoType
     */
    public function setCreationSequence($creationSequence)
    {
        $this->creationSequence = $creationSequence;

        return $this;
    }

    /**
     * Get creationSequence
     *
     * @return string 
     */
    public function getCreationSequence()
    {
        return $this->creationSequence;
    }

    /**
     * Set sampleReadyDate
     *
     * @param \DateTime $sampleReadyDate
     * @return ProtoType
     */
    public function setSampleReadyDate($sampleReadyDate)
    {
        $this->sampleReadyDate = $sampleReadyDate;

        return $this;
    }

    /**
     * Get sampleReadyDate
     *
     * @return \DateTime 
     */
    public function getSampleReadyDate()
    {
        return $this->sampleReadyDate;
    }

    /**
     * Set requestedQty
     *
     * @param integer $requestedQty
     * @return ProtoType
     */
    public function setRequestedQty($requestedQty)
    {
        $this->requestedQty = $requestedQty;

        return $this;
    }

    /**
     * Get requestedQty
     *
     * @return integer 
     */
    public function getRequestedQty()
    {
        return $this->requestedQty;
    }

    /**
     * Set weight
     *
     * @param string $weight
     * @return ProtoType
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set capacity
     *
     * @param string $capacity
     * @return ProtoType
     */
    public function setCapacity($capacity)
    {
        $this->capacity = $capacity;

        return $this;
    }

    /**
     * Get capacity
     *
     * @return string 
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * Set dimLength
     *
     * @param string $dimLength
     * @return ProtoType
     */
    public function setDimLength($dimLength)
    {
        $this->dimLength = $dimLength;

        return $this;
    }

    /**
     * Get dimLength
     *
     * @return string 
     */
    public function getDimLength()
    {
        return $this->dimLength;
    }

    /**
     * Set dimWidth
     *
     * @param string $dimWidth
     * @return ProtoType
     */
    public function setDimWidth($dimWidth)
    {
        $this->dimWidth = $dimWidth;

        return $this;
    }

    /**
     * Get dimWidth
     *
     * @return string 
     */
    public function getDimWidth()
    {
        return $this->dimWidth;
    }

    /**
     * Set dimHeight
     *
     * @param string $dimHeight
     * @return ProtoType
     */
    public function setDimHeight($dimHeight)
    {
        $this->dimHeight = $dimHeight;

        return $this;
    }

    /**
     * Get dimHeight
     *
     * @return string 
     */
    public function getDimHeight()
    {
        return $this->dimHeight;
    }

    /**
     * Set companyIdSupplier
     *
     * @param integer $companyIdSupplier
     * @return ProtoType
     */
    public function setCompanyIdSupplier($companyIdSupplier)
    {
        $this->companyIdSupplier = $companyIdSupplier;

        return $this;
    }

    /**
     * Get companyIdSupplier
     *
     * @return integer 
     */
    public function getCompanyIdSupplier()
    {
        return $this->companyIdSupplier;
    }

    /**
     * Set companyIdClient
     *
     * @param integer $companyIdClient
     * @return ProtoType
     */
    public function setCompanyIdClient($companyIdClient)
    {
        $this->companyIdClient = $companyIdClient;

        return $this;
    }

    /**
     * Get companyIdClient
     *
     * @return integer 
     */
    public function getCompanyIdClient()
    {
        return $this->companyIdClient;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ProtoType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return ProtoType
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer 
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return ProtoType
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer 
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return ProtoType
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer 
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return ProtoType
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return ProtoType
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add mediaHistory
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaHistory
     * @return ProtoType
     */
    public function addMediaHistory(\MediaBundle\Entity\MediaHistory $mediaHistory)
    {
        $this->mediaHistory[] = $mediaHistory;

        return $this;
    }

    /**
     * Remove mediaHistory
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaHistory
     */
    public function removeMediaHistory(\MediaBundle\Entity\MediaHistory $mediaHistory)
    {
        $this->mediaHistory->removeElement($mediaHistory);
    }

    /**
     * Get mediaHistory
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMediaHistory()
    {
        return $this->mediaHistory;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return ProtoType
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return ProtoType
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set enquiry
     *
     * @param \EnquiryBundle\Entity\Enquiry $enquiry
     * @return ProtoType
     */
    public function setEnquiry(\EnquiryBundle\Entity\Enquiry $enquiry = null)
    {
        $this->enquiry = $enquiry;

        return $this;
    }

    /**
     * Get enquiry
     *
     * @return \EnquiryBundle\Entity\Enquiry 
     */
    public function getEnquiry()
    {
        return $this->enquiry;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return ProtoType
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return ProtoType
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
