<?php

namespace ProtoTypeBundle\Entity;

class ProtoTypeColorMaster
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $protoTypeId;

    /**
     * @var integer
     */
    private $colorMasterId;

    /**
     * @var string
     */
    private $colorType;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \ColorMasterBundle\Entity\ColorMaster
     */
    private $colorMaster;

    /**
     * @var \ProtoTypeBundle\Entity\ProtoType
     */
    private $protoType;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set protoTypeId
     *
     * @param integer $protoTypeId
     * @return ProtoTypeColorMaster
     */
    public function setProtoTypeId($protoTypeId)
    {
        $this->protoTypeId = $protoTypeId;

        return $this;
    }

    /**
     * Get protoTypeId
     *
     * @return integer 
     */
    public function getProtoTypeId()
    {
        return $this->protoTypeId;
    }

    /**
     * Set colorMasterId
     *
     * @param integer $colorMasterId
     * @return ProtoTypeColorMaster
     */
    public function setColorMasterId($colorMasterId)
    {
        $this->colorMasterId = $colorMasterId;

        return $this;
    }

    /**
     * Get colorMasterId
     *
     * @return integer 
     */
    public function getColorMasterId()
    {
        return $this->colorMasterId;
    }

    /**
     * Set colorType
     *
     * @param string $colorType
     * @return ProtoTypeColorMaster
     */
    public function setColorType($colorType)
    {
        $this->colorType = $colorType;

        return $this;
    }

    /**
     * Get colorType
     *
     * @return string 
     */
    public function getColorType()
    {
        return $this->colorType;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return ProtoTypeColorMaster
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer 
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return ProtoTypeColorMaster
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer 
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return ProtoTypeColorMaster
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return ProtoTypeColorMaster
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Set colorMaster
     *
     * @param \ColorMasterBundle\Entity\ColorMaster $colorMaster
     * @return ProtoTypeColorMaster
     */
    public function setColorMaster(\ColorMasterBundle\Entity\ColorMaster $colorMaster = null)
    {
        $this->colorMaster = $colorMaster;

        return $this;
    }

    /**
     * Get colorMaster
     *
     * @return \ColorMasterBundle\Entity\ColorMaster 
     */
    public function getColorMaster()
    {
        return $this->colorMaster;
    }

    /**
     * Set protoType
     *
     * @param \ProtoTypeBundle\Entity\ProtoType $protoType
     * @return ProtoTypeColorMaster
     */
    public function setProtoType(\ProtoTypeBundle\Entity\ProtoType $protoType = null)
    {
        $this->protoType = $protoType;

        return $this;
    }

    /**
     * Get protoType
     *
     * @return \ProtoTypeBundle\Entity\ProtoType 
     */
    public function getProtoType()
    {
        return $this->protoType;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return ProtoTypeColorMaster
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return ProtoTypeColorMaster
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User 
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
