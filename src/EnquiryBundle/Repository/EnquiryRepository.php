<?php
namespace EnquiryBundle\Repository;

use AppBundle\Component\AbstractRepository;

class EnquiryRepository extends AbstractRepository
{
    public function getAlias() {
        return 'e';
    }

    public function getQB($id = null)
    {
        $qb = $this->createQueryBuilder('e')
              ->select('e, com, c, uc, um')
              ->leftjoin('e.company', 'com')
              ->leftjoin('e.contact', 'c')
              ->leftjoin('e.userCreated', 'uc')
              ->leftjoin('e.userModified', 'um');

        if ($id != null) {
            $qb = $this->dbUtil->setWhere($qb, 'e.id', $id);
        }

        return $qb;
    }

    public function getQBLinkProtoType($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('pt, pc.productFamily, pc.productType')
            ->from('ProtoTypeBundle:ProtoType', 'pt')
            ->distinct()
            ->leftJoin('ProductCodingBundle:ProductCoding', 'pc', 'WITH',
                       'pt.productFamilyCode = pc.productFamilyCode');
        $qb = $this->dbUtil->setWhere($qb, 'pt.enquiryId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'pt.id', $idl);
        return $qb;
    }

    public function getQBLinkQuotation($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('q')
            ->from('QuotationBundle:Quotation', 'q');
        $qb = $this->dbUtil->setWhere($qb, 'q.enquiryId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'q.id', $idl);
        return $qb;
    }

    public function getQBLinkTradingOrder($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('to')
            ->from('QuotationBundle:Quotation', 'to');
        $qb = $this->dbUtil->setWhere($qb, 'to.enquiryId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'to.id', $idl);
        return $qb;
    }


}
