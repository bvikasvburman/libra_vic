<?php

namespace EnquiryBundle\Entity;

class Enquiry
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'E';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $contactId;

    /**
     * @var string
     */
    private $description;

    /**
     * @var \DateTime
     */
    private $enquiryDate;

    /**
     * @var \DateTime
     */
    private $projectStartDate;

    /**
     * @var \DateTime
     */
    private $projectEndDate;

    /**
     * @var string
     */
    private $status;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $quotationM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tradingOrderM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \ContactBundle\Entity\Contact
     */
    private $contact;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->quotationM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tradingOrderM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Enquiry
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return Enquiry
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set contactId
     *
     * @param integer $contactId
     * @return Enquiry
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;

        return $this;
    }

    /**
     * Get contactId
     *
     * @return integer
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Enquiry
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set enquiryDate
     *
     * @param \DateTime $enquiryDate
     * @return Enquiry
     */
    public function setEnquiryDate($enquiryDate)
    {
        $this->enquiryDate = $enquiryDate;

        return $this;
    }

    /**
     * Get enquiryDate
     *
     * @return \DateTime
     */
    public function getEnquiryDate()
    {
        return $this->enquiryDate;
    }

    /**
     * Set projectStartDate
     *
     * @param \DateTime $projectStartDate
     * @return Enquiry
     */
    public function setProjectStartDate($projectStartDate)
    {
        $this->projectStartDate = $projectStartDate;

        return $this;
    }

    /**
     * Get projectStartDate
     *
     * @return \DateTime
     */
    public function getProjectStartDate()
    {
        return $this->projectStartDate;
    }

    /**
     * Set projectEndDate
     *
     * @param \DateTime $projectEndDate
     * @return Enquiry
     */
    public function setProjectEndDate($projectEndDate)
    {
        $this->projectEndDate = $projectEndDate;

        return $this;
    }

    /**
     * Get projectEndDate
     *
     * @return \DateTime
     */
    public function getProjectEndDate()
    {
        return $this->projectEndDate;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Enquiry
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return Enquiry
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return Enquiry
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return Enquiry
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Enquiry
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return Enquiry
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add quotationM
     *
     * @param \QuotationBundle\Entity\Quotation $quotationM
     * @return Enquiry
     */
    public function addQuotationM(\QuotationBundle\Entity\Quotation $quotationM)
    {
        $this->quotationM[] = $quotationM;

        return $this;
    }

    /**
     * Remove quotationM
     *
     * @param \QuotationBundle\Entity\Quotation $quotationM
     */
    public function removeQuotationM(\QuotationBundle\Entity\Quotation $quotationM)
    {
        $this->quotationM->removeElement($quotationM);
    }

    /**
     * Get quotationM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuotationM()
    {
        return $this->quotationM;
    }

    /**
     * Add tradingOrderM
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrderM
     * @return Enquiry
     */
    public function addTradingOrderM(\TradingOrderBundle\Entity\TradingOrder $tradingOrderM)
    {
        $this->tradingOrderM[] = $tradingOrderM;

        return $this;
    }

    /**
     * Remove tradingOrderM
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrderM
     */
    public function removeTradingOrderM(\TradingOrderBundle\Entity\TradingOrder $tradingOrderM)
    {
        $this->tradingOrderM->removeElement($tradingOrderM);
    }

    /**
     * Get tradingOrderM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTradingOrderM()
    {
        return $this->tradingOrderM;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return Enquiry
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return Enquiry
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return Enquiry
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contact
     *
     * @param \ContactBundle\Entity\Contact $contact
     * @return Enquiry
     */
    public function setContact(\ContactBundle\Entity\Contact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \ContactBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return Enquiry
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return Enquiry
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $researchM;


    /**
     * Add researchM
     *
     * @param \ResearchBundle\Entity\Research $researchM
     * @return Enquiry
     */
    public function addResearchM(\ResearchBundle\Entity\Research $researchM)
    {
        $this->researchM[] = $researchM;

        return $this;
    }

    /**
     * Remove researchM
     *
     * @param \ResearchBundle\Entity\Research $researchM
     */
    public function removeResearchM(\ResearchBundle\Entity\Research $researchM)
    {
        $this->researchM->removeElement($researchM);
    }

    /**
     * Get researchM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResearchM()
    {
        return $this->researchM;
    }
}
