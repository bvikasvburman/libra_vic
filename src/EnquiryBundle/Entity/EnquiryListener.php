<?php

namespace EnquiryBundle\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;

class EnquiryListener
{
    public function postPersist(Enquiry $enquiry, LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $code = 'E' . str_pad($enquiry->getId(), 6, "0", STR_PAD_LEFT);
        $enquiry->setCode($code);
        $em->persist($enquiry);
        $em->flush();
    }
}

