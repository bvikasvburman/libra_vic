<?php

namespace ResearchBundle\Entity;

class Research
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'R';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var integer
     */
    private $protoTypeId;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var string
     */
    private $researchArea;

    /**
     * @var integer
     */
    private $enquiryId;

    /**
     * @var string
     */
    private $materials;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Research
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set protoTypeId
     *
     * @param integer $protoTypeId
     * @return Research
     */
    public function setProtoTypeId($protoTypeId)
    {
        $this->protoTypeId = $protoTypeId;

        return $this;
    }

    /**
     * Get protoTypeId
     *
     * @return integer
     */
    public function getProtoTypeId()
    {
        return $this->protoTypeId;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     * @return Research
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set researchArea
     *
     * @param string $researchArea
     * @return Research
     */
    public function setResearchArea($researchArea)
    {
        $this->researchArea = $researchArea;

        return $this;
    }

    /**
     * Get researchArea
     *
     * @return string
     */
    public function getResearchArea()
    {
        return $this->researchArea;
    }

    /**
     * Set enquiryId
     *
     * @param integer $enquiryId
     * @return Research
     */
    public function setEnquiryId($enquiryId)
    {
        $this->enquiryId = $enquiryId;

        return $this;
    }

    /**
     * Get enquiryId
     *
     * @return integer
     */
    public function getEnquiryId()
    {
        return $this->enquiryId;
    }

    /**
     * Set materials
     *
     * @param string $materials
     * @return Research
     */
    public function setMaterials($materials)
    {
        $this->materials = $materials;

        return $this;
    }

    /**
     * Get materials
     *
     * @return string
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return Research
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return Research
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return Research
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Research
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return Research
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return Research
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return Research
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
    /**
     * @var string
     */
    private $description;

    /**
     * @var \CompanyBundle\Entity\Company
     */
    private $company;

    /**
     * @var \ProtoTypeBundle\Entity\ProtoType
     */
    private $protoType;

    /**
     * @var \EnquiryBundle\Entity\Enquiry
     */
    private $enquiry;


    /**
     * Set description
     *
     * @param string $description
     * @return Research
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set company
     *
     * @param \CompanyBundle\Entity\Company $company
     * @return Research
     */
    public function setCompany(\CompanyBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \CompanyBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set protoType
     *
     * @param \ProtoTypeBundle\Entity\ProtoType $protoType
     * @return Research
     */
    public function setProtoType(\ProtoTypeBundle\Entity\ProtoType $protoType = null)
    {
        $this->protoType = $protoType;

        return $this;
    }

    /**
     * Get protoType
     *
     * @return \ProtoTypeBundle\Entity\ProtoType
     */
    public function getProtoType()
    {
        return $this->protoType;
    }

    /**
     * Set enquiry
     *
     * @param \EnquiryBundle\Entity\Enquiry $enquiry
     * @return Research
     */
    public function setEnquiry(\EnquiryBundle\Entity\Enquiry $enquiry = null)
    {
        $this->enquiry = $enquiry;

        return $this;
    }

    /**
     * Get enquiry
     *
     * @return \EnquiryBundle\Entity\Enquiry
     */
    public function getEnquiry()
    {
        return $this->enquiry;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mediaHistory;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->mediaHistory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add mediaHistory
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaHistory
     * @return Research
     */
    public function addMediaHistory(\MediaBundle\Entity\MediaHistory $mediaHistory)
    {
        $this->mediaHistory[] = $mediaHistory;

        return $this;
    }

    /**
     * Remove mediaHistory
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaHistory
     */
    public function removeMediaHistory(\MediaBundle\Entity\MediaHistory $mediaHistory)
    {
        $this->mediaHistory->removeElement($mediaHistory);
    }

    /**
     * Get mediaHistory
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMediaHistory()
    {
        return $this->mediaHistory;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;


    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return Research
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return Research
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessageM()
    {
        return $this->messageM;
    }
}
