<?php
namespace ResearchBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ResearchRepository extends AbstractRepository
{
    public function getAlias() {
        return 'r';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('r')
            ->addSelect('r')
            ->addSelect('partial e.{id, code}')
            ->addSelect('partial c.{id, name}')
            ->addSelect('partial pt.{id, code}')
            ->addSelect('partial uc.{id, firstName, lastName}')
            ->addSelect('partial um.{id, firstName, lastName}')
            ->leftjoin('r.enquiry', 'e')
            ->leftjoin('r.company', 'c')
            ->leftjoin('r.protoType', 'pt')
            ->leftjoin('r.userCreated', 'uc')
            ->leftjoin('r.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'r.id', $id);
        }
        return $qb;
    }
}
