<?php
namespace ApprovedComponentBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ApprovedComponentRepository extends AbstractRepository
{
    public function getAlias() {
        return 'ac';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('ac')
              ->select('ac, c, pl, uc, um')
              ->leftjoin('ac.company', 'c')
              ->leftjoin('ac.partsLocation', 'pl')
              ->leftjoin('ac.userCreated', 'uc')
              ->leftjoin('ac.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'ac.id', $id);
        }
        return $qb;
    }
}
