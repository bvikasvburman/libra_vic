<?php
namespace CompanyBundle\Repository;

use AppBundle\Component\AbstractRepository;

class CompanyRepository extends AbstractRepository
{
    public function getQB($id = null, $sVars = null)
    {
        $qb = $this->createQueryBuilder('c')
              ->select('c, uc, um')
              ->leftjoin('c.userCreated', 'uc')
              ->leftjoin('c.userModified', 'um');

        if ($id) {
            $qb = $this->dbUtil->setWhere($qb, 'c.id', $id);
        }

        return $qb;
    }

    public function getQBLinkContact($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('c')
            ->from('ContactBundle:Contact', 'c');
        $qb = $this->dbUtil->setWhere($qb, 'c.companyId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'c.id', $idl);
        return $qb;
    }

    public function getQBLinkBranch($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('b')
            ->from('BranchBundle:Branch', 'b');
        $qb = $this->dbUtil->setWhere($qb, 'b.companyId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'b.id', $idl);
        return $qb;
    }

    public function getQBLinkAttachment($id, $idl, $sVars, $linkInfo, $qb) {
        $expr = $qb->expr()->eq("mh.mediaType", $qb->expr()->literal('attachment'));
        $qb->select("mh, m")
            ->from('MediaBundle:MediaHistory', 'mh')
            ->join("mh.media", 'm', 'WITH', $expr);

        $qb = $this->dbUtil->setWhere($qb, "mh.id", $idl);
        $qb = $this->dbUtil->setWhere($qb, "mh.companyId", $id);

        return $qb;
    }

    public function getQBLinkEnquiry($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('e')
            ->from('EnquiryBundle:Enquiry', 'e');
        $qb = $this->dbUtil->setWhere($qb, 'e.companyId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'e.id', $idl);
        return $qb;
    }

    public function getQBLinkQuotation($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('q, e')
            ->from('QuotationBundle:Quotation', 'q')
            ->leftjoin('q.enquiry', 'e');
        $qb = $this->dbUtil->setWhere($qb, 'q.companyId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'q.id', $idl);
        return $qb;
    }

    public function getQBLinkTradingOrder($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('to, e, q')
            ->from('TradingOrderBundle:TradingOrder', 'to')
            ->leftjoin('to.enquiry', 'e')
            ->leftjoin('to.quotation', 'q');
        $qb = $this->dbUtil->setWhere($qb, 'to.companyId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'to.id', $idl);
        return $qb;
    }

    public function getQBLinkSalesConfirmation($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('sc, to')
            ->from('SalesConfirmationBundle:SalesConfirmation', 'sc')
            ->leftjoin('sc.tradingOrder', 'to');
        $qb = $this->dbUtil->setWhere($qb, 'sc.companyId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'sc.id', $idl);
        return $qb;
    }

    public function getQBLinkPurchaseOrder($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('po, to')
            ->from('PurchaseOrderBundle:PurchaseOrder', 'po')
            ->leftjoin('po.tradingOrder', 'to');
        $qb = $this->dbUtil->setWhere($qb, 'po.companyIdSupplier', $id);
        $qb = $this->dbUtil->setWhere($qb, 'po.id', $idl);
        return $qb;
    }

    public function getQBLinkManufacturingOrder($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('mo, to')
            ->from('ManufacturingOrderBundle:ManufacturingOrder', 'mo')
            ->leftjoin('mo.tradingOrder', 'to');
        $qb = $this->dbUtil->setWhere($qb, 'mo.companyIdSupplier', $id);
        $qb = $this->dbUtil->setWhere($qb, 'mo.id', $idl);
        return $qb;
    }

    public function getQBLinkInvoice($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('i, to')
            ->from('InvoiceBundle:Invoice', 'i')
            ->leftjoin('i.tradingOrder', 'to');
        $qb = $this->dbUtil->setWhere($qb, 'i.companyId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'i.id', $idl);
        return $qb;
    }

    public function getQBLinkClaim($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('c, to')
            ->from('ClaimBundle:Claim', 'c')
            ->leftjoin('c.tradingOrder', 'to');
        $qb = $this->dbUtil->setWhere($qb, 'c.companyId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'c.id', $idl);
        return $qb;
    }

    public function getQBLinkCreditDebitNote($id, $idl, $sVars, $linkInfo, $qb) {
        $qb->select('cdn, to')
            ->from('CreditDebitNoteBundle:CreditDebitNote', 'cdn')
            ->leftjoin('cdn.tradingOrder', 'to');
        $qb = $this->dbUtil->setWhere($qb, 'cdn.companyId', $id);
        $qb = $this->dbUtil->setWhere($qb, 'cdn.id', $idl);
        return $qb;
    }

    // public function getQBLinkNote($id, $idl, $sVars, $linkInfo, $qb) {
    //     $expr = $qb->expr()->eq('n.module', $qb->expr()->literal('company'));

    //     $qb->select('n')
    //         ->from('NoteBundle:Note', 'n')
    //         ->join("n.company", 'c');
    //     $qb = $this->dbUtil->setWhere($qb, 'n.companyId', $id);
    //     $qb = $this->dbUtil->setWhere($qb, 'n.id', $idl);
    //     return $qb;
    // }

    // public function getQBLinkMessage($id, $idl, $sVars, $linkInfo, $qb) {
    //     $qb->select('m, com')
    //         ->from('MessageBundle:Message', 'm')
    //         ->leftjoin('m.company', 'com');
    //     $qb = $this->dbUtil->setWhere($qb, 'm.companyId', $id);
    //     $qb = $this->dbUtil->setWhere($qb, 'm.id', $idl);
    //     return $qb;
    // }
}
