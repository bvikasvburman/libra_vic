<?php

namespace CompanyBundle\Entity;

class Company
{
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getCodePrefix()
    {
        return 'C';
    }

    // ---AUTO GENERATION STARTS--- //
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $code;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $category;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $address;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $fax;

    /**
     * @var string
     */
    private $website;

    /**
     * @var string
     */
    private $addressDelivery;

    /**
     * @var string
     */
    private $paymentTerms;

    /**
     * @var string
     */
    private $deliveryTerms;

    /**
     * @var string
     */
    private $portOfDelivery;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $contactM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $branchM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $enquiryM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $quotationM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tradingOrderM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $salesConfirmationM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $purchaseOrderM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $manufacturingOrderM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $invoiceM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $claimM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $creditDebitNoteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $noteM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $messageM;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $mediaHistory;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userCreated;

    /**
     * @var \UserBundle\Entity\User
     */
    private $userModified;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contactM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->branchM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->enquiryM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->quotationM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->tradingOrderM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->salesConfirmationM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->purchaseOrderM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->manufacturingOrderM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->invoiceM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->claimM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->creditDebitNoteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->noteM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->messageM = new \Doctrine\Common\Collections\ArrayCollection();
        $this->mediaHistory = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Company
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Company
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Company
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Company
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return Company
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Company
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     * @return Company
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Company
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Company
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set fax
     *
     * @param string $fax
     * @return Company
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Company
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set addressDelivery
     *
     * @param string $addressDelivery
     * @return Company
     */
    public function setAddressDelivery($addressDelivery)
    {
        $this->addressDelivery = $addressDelivery;

        return $this;
    }

    /**
     * Get addressDelivery
     *
     * @return string
     */
    public function getAddressDelivery()
    {
        return $this->addressDelivery;
    }

    /**
     * Set paymentTerms
     *
     * @param string $paymentTerms
     * @return Company
     */
    public function setPaymentTerms($paymentTerms)
    {
        $this->paymentTerms = $paymentTerms;

        return $this;
    }

    /**
     * Get paymentTerms
     *
     * @return string
     */
    public function getPaymentTerms()
    {
        return $this->paymentTerms;
    }

    /**
     * Set deliveryTerms
     *
     * @param string $deliveryTerms
     * @return Company
     */
    public function setDeliveryTerms($deliveryTerms)
    {
        $this->deliveryTerms = $deliveryTerms;

        return $this;
    }

    /**
     * Get deliveryTerms
     *
     * @return string
     */
    public function getDeliveryTerms()
    {
        return $this->deliveryTerms;
    }

    /**
     * Set portOfDelivery
     *
     * @param string $portOfDelivery
     * @return Company
     */
    public function setPortOfDelivery($portOfDelivery)
    {
        $this->portOfDelivery = $portOfDelivery;

        return $this;
    }

    /**
     * Get portOfDelivery
     *
     * @return string
     */
    public function getPortOfDelivery()
    {
        return $this->portOfDelivery;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return Company
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return Company
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return Company
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return Company
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return Company
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * Add contactM
     *
     * @param \ContactBundle\Entity\Contact $contactM
     * @return Company
     */
    public function addContactM(\ContactBundle\Entity\Contact $contactM)
    {
        $this->contactM[] = $contactM;

        return $this;
    }

    /**
     * Remove contactM
     *
     * @param \ContactBundle\Entity\Contact $contactM
     */
    public function removeContactM(\ContactBundle\Entity\Contact $contactM)
    {
        $this->contactM->removeElement($contactM);
    }

    /**
     * Get contactM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactM()
    {
        return $this->contactM;
    }

    /**
     * Add branchM
     *
     * @param \BranchBundle\Entity\Branch $branchM
     * @return Company
     */
    public function addBranchM(\BranchBundle\Entity\Branch $branchM)
    {
        $this->branchM[] = $branchM;

        return $this;
    }

    /**
     * Remove branchM
     *
     * @param \BranchBundle\Entity\Branch $branchM
     */
    public function removeBranchM(\BranchBundle\Entity\Branch $branchM)
    {
        $this->branchM->removeElement($branchM);
    }

    /**
     * Get branchM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBranchM()
    {
        return $this->branchM;
    }

    /**
     * Add enquiryM
     *
     * @param \EnquiryBundle\Entity\Enquiry $enquiryM
     * @return Company
     */
    public function addEnquiryM(\EnquiryBundle\Entity\Enquiry $enquiryM)
    {
        $this->enquiryM[] = $enquiryM;

        return $this;
    }

    /**
     * Remove enquiryM
     *
     * @param \EnquiryBundle\Entity\Enquiry $enquiryM
     */
    public function removeEnquiryM(\EnquiryBundle\Entity\Enquiry $enquiryM)
    {
        $this->enquiryM->removeElement($enquiryM);
    }

    /**
     * Get enquiryM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEnquiryM()
    {
        return $this->enquiryM;
    }

    /**
     * Add quotationM
     *
     * @param \QuotationBundle\Entity\Quotation $quotationM
     * @return Company
     */
    public function addQuotationM(\QuotationBundle\Entity\Quotation $quotationM)
    {
        $this->quotationM[] = $quotationM;

        return $this;
    }

    /**
     * Remove quotationM
     *
     * @param \QuotationBundle\Entity\Quotation $quotationM
     */
    public function removeQuotationM(\QuotationBundle\Entity\Quotation $quotationM)
    {
        $this->quotationM->removeElement($quotationM);
    }

    /**
     * Get quotationM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuotationM()
    {
        return $this->quotationM;
    }

    /**
     * Add tradingOrderM
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrderM
     * @return Company
     */
    public function addTradingOrderM(\TradingOrderBundle\Entity\TradingOrder $tradingOrderM)
    {
        $this->tradingOrderM[] = $tradingOrderM;

        return $this;
    }

    /**
     * Remove tradingOrderM
     *
     * @param \TradingOrderBundle\Entity\TradingOrder $tradingOrderM
     */
    public function removeTradingOrderM(\TradingOrderBundle\Entity\TradingOrder $tradingOrderM)
    {
        $this->tradingOrderM->removeElement($tradingOrderM);
    }

    /**
     * Get tradingOrderM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTradingOrderM()
    {
        return $this->tradingOrderM;
    }

    /**
     * Add salesConfirmationM
     *
     * @param \SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmationM
     * @return Company
     */
    public function addSalesConfirmationM(\SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmationM)
    {
        $this->salesConfirmationM[] = $salesConfirmationM;

        return $this;
    }

    /**
     * Remove salesConfirmationM
     *
     * @param \SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmationM
     */
    public function removeSalesConfirmationM(\SalesConfirmationBundle\Entity\SalesConfirmation $salesConfirmationM)
    {
        $this->salesConfirmationM->removeElement($salesConfirmationM);
    }

    /**
     * Get salesConfirmationM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSalesConfirmationM()
    {
        return $this->salesConfirmationM;
    }

    /**
     * Add purchaseOrderM
     *
     * @param \PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrderM
     * @return Company
     */
    public function addPurchaseOrderM(\PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrderM)
    {
        $this->purchaseOrderM[] = $purchaseOrderM;

        return $this;
    }

    /**
     * Remove purchaseOrderM
     *
     * @param \PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrderM
     */
    public function removePurchaseOrderM(\PurchaseOrderBundle\Entity\PurchaseOrder $purchaseOrderM)
    {
        $this->purchaseOrderM->removeElement($purchaseOrderM);
    }

    /**
     * Get purchaseOrderM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPurchaseOrderM()
    {
        return $this->purchaseOrderM;
    }

    /**
     * Add manufacturingOrderM
     *
     * @param \ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrderM
     * @return Company
     */
    public function addManufacturingOrderM(\ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrderM)
    {
        $this->manufacturingOrderM[] = $manufacturingOrderM;

        return $this;
    }

    /**
     * Remove manufacturingOrderM
     *
     * @param \ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrderM
     */
    public function removeManufacturingOrderM(\ManufacturingOrderBundle\Entity\ManufacturingOrder $manufacturingOrderM)
    {
        $this->manufacturingOrderM->removeElement($manufacturingOrderM);
    }

    /**
     * Get manufacturingOrderM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getManufacturingOrderM()
    {
        return $this->manufacturingOrderM;
    }

    /**
     * Add invoiceM
     *
     * @param \InvoiceBundle\Entity\Invoice $invoiceM
     * @return Company
     */
    public function addInvoiceM(\InvoiceBundle\Entity\Invoice $invoiceM)
    {
        $this->invoiceM[] = $invoiceM;

        return $this;
    }

    /**
     * Remove invoiceM
     *
     * @param \InvoiceBundle\Entity\Invoice $invoiceM
     */
    public function removeInvoiceM(\InvoiceBundle\Entity\Invoice $invoiceM)
    {
        $this->invoiceM->removeElement($invoiceM);
    }

    /**
     * Get invoiceM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInvoiceM()
    {
        return $this->invoiceM;
    }

    /**
     * Add claimM
     *
     * @param \ClaimBundle\Entity\Claim $claimM
     * @return Company
     */
    public function addClaimM(\ClaimBundle\Entity\Claim $claimM)
    {
        $this->claimM[] = $claimM;

        return $this;
    }

    /**
     * Remove claimM
     *
     * @param \ClaimBundle\Entity\Claim $claimM
     */
    public function removeClaimM(\ClaimBundle\Entity\Claim $claimM)
    {
        $this->claimM->removeElement($claimM);
    }

    /**
     * Get claimM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClaimM()
    {
        return $this->claimM;
    }

    /**
     * Add creditDebitNoteM
     *
     * @param \CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNoteM
     * @return Company
     */
    public function addCreditDebitNoteM(\CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNoteM)
    {
        $this->creditDebitNoteM[] = $creditDebitNoteM;

        return $this;
    }

    /**
     * Remove creditDebitNoteM
     *
     * @param \CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNoteM
     */
    public function removeCreditDebitNoteM(\CreditDebitNoteBundle\Entity\CreditDebitNote $creditDebitNoteM)
    {
        $this->creditDebitNoteM->removeElement($creditDebitNoteM);
    }

    /**
     * Get creditDebitNoteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCreditDebitNoteM()
    {
        return $this->creditDebitNoteM;
    }

    /**
     * Add noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     * @return Company
     */
    public function addNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM[] = $noteM;

        return $this;
    }

    /**
     * Remove noteM
     *
     * @param \NoteBundle\Entity\Note $noteM
     */
    public function removeNoteM(\NoteBundle\Entity\Note $noteM)
    {
        $this->noteM->removeElement($noteM);
    }

    /**
     * Get noteM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNoteM()
    {
        return $this->noteM;
    }

    /**
     * Add messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     * @return Company
     */
    public function addMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM[] = $messageM;

        return $this;
    }

    /**
     * Remove messageM
     *
     * @param \MessageBundle\Entity\Message $messageM
     */
    public function removeMessageM(\MessageBundle\Entity\Message $messageM)
    {
        $this->messageM->removeElement($messageM);
    }

    /**
     * Get messageM
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageM()
    {
        return $this->messageM;
    }

    /**
     * Add mediaHistory
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaHistory
     * @return Company
     */
    public function addMediaHistory(\MediaBundle\Entity\MediaHistory $mediaHistory)
    {
        $this->mediaHistory[] = $mediaHistory;

        return $this;
    }

    /**
     * Remove mediaHistory
     *
     * @param \MediaBundle\Entity\MediaHistory $mediaHistory
     */
    public function removeMediaHistory(\MediaBundle\Entity\MediaHistory $mediaHistory)
    {
        $this->mediaHistory->removeElement($mediaHistory);
    }

    /**
     * Get mediaHistory
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMediaHistory()
    {
        return $this->mediaHistory;
    }

    /**
     * Set userCreated
     *
     * @param \UserBundle\Entity\User $userCreated
     * @return Company
     */
    public function setUserCreated(\UserBundle\Entity\User $userCreated = null)
    {
        $this->userCreated = $userCreated;

        return $this;
    }

    /**
     * Get userCreated
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserCreated()
    {
        return $this->userCreated;
    }

    /**
     * Set userModified
     *
     * @param \UserBundle\Entity\User $userModified
     * @return Company
     */
    public function setUserModified(\UserBundle\Entity\User $userModified = null)
    {
        $this->userModified = $userModified;

        return $this;
    }

    /**
     * Get userModified
     *
     * @return \UserBundle\Entity\User
     */
    public function getUserModified()
    {
        return $this->userModified;
    }
}
