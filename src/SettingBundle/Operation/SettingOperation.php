<?php
namespace SettingBundle\Operation;

use AppBundle\Component\AbstractOperation;
use SettingBundle\Entity\Setting;

/**
 * Abstract Operation
 *
 */
class SettingOperation extends AbstractOperation
{
}
