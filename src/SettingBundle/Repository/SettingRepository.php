<?php
namespace SettingBundle\Repository;

use AppBundle\Component\AbstractRepository;

class SettingRepository extends AbstractRepository
{
    public function getAlias() {
        return 's';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('s')
              ->select('s, uc, um')
              ->leftjoin('s.userCreated', 'uc')
              ->leftjoin('s.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 's.id', $id);
        }
        return $qb;
    }
}
