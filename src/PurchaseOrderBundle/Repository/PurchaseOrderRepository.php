<?php
namespace PurchaseOrderBundle\Repository;

use AppBundle\Component\AbstractRepository;

class PurchaseOrderRepository extends AbstractRepository
{
    public function getAlias() {
        return 'po';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('po')
              ->select('po, uc, um')
              ->leftjoin('po.userCreated', 'uc')
              ->leftjoin('po.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'po.id', $id);
        }
        return $qb;
    }
}
