<?php
namespace InterestGroupBundle\Repository;

use AppBundle\Component\AbstractRepository;

class InterestGroupRepository extends AbstractRepository
{
    public function getAlias() {
        return 'ig';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('ig')
              ->select('ig, uc, um')
              ->leftjoin('ig.userCreated', 'uc')
              ->leftjoin('ig.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'ig.id', $id);
        }
        return $qb;
    }
}
