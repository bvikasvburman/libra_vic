<?php
namespace ProductCodingBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ProductCodingRepository extends AbstractRepository
{
    public function getAlias() {
        return 'pc';
    }

    public function getQB($id = null, $sVars = null, $sOpts = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('pc');


        if ($sOpts->retType == 'prodFamily') {
            $qb->select('pc.productFamilyCode, pc.productFamily')
            ->distinct();
        } else if ($sOpts->retType == 'prodType') {
            $qb->select('pc.productTypeCode, pc.productType')
            ->distinct();
            $qb = $dbUtil->setWhere($qb, 'pc.productFamilyCode', $sVars->criteria);
        } else {
            $qb->select('pc');
        }

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'pc.id', $id);
        }
        return $qb;
    }
}
