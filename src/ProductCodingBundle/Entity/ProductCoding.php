<?php

namespace ProductCodingBundle\Entity;

class ProductCoding
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $productFamily;

    /**
     * @var string
     */
    private $productFamilyCode;

    /**
     * @var string
     */
    private $productType;

    /**
     * @var string
     */
    private $productTypeCode;

    /**
     * @var integer
     */
    private $flag;

    /**
     * @var integer
     */
    private $userIdCreated;

    /**
     * @var integer
     */
    private $userIdModified;

    /**
     * @var \DateTime
     */
    private $creationDate;

    /**
     * @var \DateTime
     */
    private $modificationDate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productFamily
     *
     * @param string $productFamily
     * @return ProductCoding
     */
    public function setProductFamily($productFamily)
    {
        $this->productFamily = $productFamily;

        return $this;
    }

    /**
     * Get productFamily
     *
     * @return string 
     */
    public function getProductFamily()
    {
        return $this->productFamily;
    }

    /**
     * Set productFamilyCode
     *
     * @param string $productFamilyCode
     * @return ProductCoding
     */
    public function setProductFamilyCode($productFamilyCode)
    {
        $this->productFamilyCode = $productFamilyCode;

        return $this;
    }

    /**
     * Get productFamilyCode
     *
     * @return string 
     */
    public function getProductFamilyCode()
    {
        return $this->productFamilyCode;
    }

    /**
     * Set productType
     *
     * @param string $productType
     * @return ProductCoding
     */
    public function setProductType($productType)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return string 
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * Set productTypeCode
     *
     * @param string $productTypeCode
     * @return ProductCoding
     */
    public function setProductTypeCode($productTypeCode)
    {
        $this->productTypeCode = $productTypeCode;

        return $this;
    }

    /**
     * Get productTypeCode
     *
     * @return string 
     */
    public function getProductTypeCode()
    {
        return $this->productTypeCode;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return ProductCoding
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer 
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set userIdCreated
     *
     * @param integer $userIdCreated
     * @return ProductCoding
     */
    public function setUserIdCreated($userIdCreated)
    {
        $this->userIdCreated = $userIdCreated;

        return $this;
    }

    /**
     * Get userIdCreated
     *
     * @return integer 
     */
    public function getUserIdCreated()
    {
        return $this->userIdCreated;
    }

    /**
     * Set userIdModified
     *
     * @param integer $userIdModified
     * @return ProductCoding
     */
    public function setUserIdModified($userIdModified)
    {
        $this->userIdModified = $userIdModified;

        return $this;
    }

    /**
     * Get userIdModified
     *
     * @return integer 
     */
    public function getUserIdModified()
    {
        return $this->userIdModified;
    }

    /**
     * Set creationDate
     *
     * @param \DateTime $creationDate
     * @return ProductCoding
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * Get creationDate
     *
     * @return \DateTime 
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Set modificationDate
     *
     * @param \DateTime $modificationDate
     * @return ProductCoding
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;

        return $this;
    }

    /**
     * Get modificationDate
     *
     * @return \DateTime 
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
}
