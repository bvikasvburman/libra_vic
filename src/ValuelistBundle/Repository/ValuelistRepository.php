<?php
namespace ValuelistBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ValuelistRepository extends AbstractRepository
{
    public function getAlias() {
        return 'vl';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('vl')
              ->select('vl, uc, um')
              ->leftjoin('vl.userCreated', 'uc')
              ->leftjoin('vl.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'vl.id', $id);
        }
        if ($sVars) {
            foreach ($sVars->criteria as $colName => $value) {
                $qb = $dbUtil->setWhere($qb, "vl.{$colName}", $value);
            }

            if (count($sVars->sort) > 0) {
                $qb = $dbUtil->setOrderBy($qb, $sVars->sort);
            } else {
                $qb->orderBy('vl.value', 'ASC');
            }
        }

        return $qb;
    }
}
