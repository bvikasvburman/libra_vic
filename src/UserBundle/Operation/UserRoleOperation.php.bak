<?php
namespace UserBundle\Operation;

use AppBundle\Component\AbstractOperation;
use UserBundle\Entity\UserRole;

/**
 * User Role Operation
 *
 * @author Nicolae Tusinean
 */
class UserRoleOperation extends AbstractOperation
{
    /**
     * Get all user roles
     *
     * @return UserRole[]
     */
    public function getAll()
    {
        return $this->getUserRoleRepository()->getAll();
    }

    /**
     * Get user role by id.
     *
     * @param integer $id
     *
     * @return UserRole
     */
    public function getOne($id)
    {
        return $this->getUserRoleRepository()->getOne($id);
    }

    /**
     * Save an user role
     *
     * @param UserRole|array $userRole
     * @return UserRole
     */
    public function save($userRole)
    {
        if (!$userRole instanceof UserRole) {
            // create user role from json
            $serializer = $this->getJmsSerializer();
            $userRole = $serializer->deserialize(json_encode($userRole), 'UserBundle\Entity\UserRole', 'json');
        }

        return $this->getUserRoleRepository()->save($userRole);
    }

    /**
     * Get user role repository's instance.
     *
     * @return \UserBundle\Repository\UserRoleRepository
     */
    public function getUserRoleRepository()
    {
        return $this->getEntityManager()->getRepository('UserBundle:UserRole');
    }
}