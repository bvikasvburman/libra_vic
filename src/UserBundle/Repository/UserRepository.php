<?php
namespace UserBundle\Repository;

use AppBundle\Component\AbstractRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class UserRepository extends AbstractRepository implements UserProviderInterface
{
    public function getQB($id = null, $sVars = null)
    {
        // $qb = $this->createQueryBuilder('u');
        // if ($id) {
        //     $qb->where('u.id = :id')
        //        ->setParameter(':id', $id);
        // }

        // return $qb;

        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('u')
              ->select('u, r')
              ->leftjoin('u.role', 'r');
        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'u.id', $id);
        }

        return $qb;
    }





    /**
     * Get a user from database by its username or email.
     *
     * @param string $username
     * @return UserInterface
     * @throws UsernameNotFoundException
     */
    public function loadUserByUsername($username)
    {

        $user = $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();

        // die($user->getFirstName());

        if (null === $user) {
            $message = sprintf(
                'Unable to find an active admin UserBundle:User object identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message);
        }

        return $user;
    }

    /**
     * Reload from database the provided user. This methos is especially used
     *
     *
     * @param UserInterface $user
     * @return UserInterface
     * @throws UnsupportedUserException
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    /**
     * @param string $class
     * @return boolean
     */
    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
            || is_subclass_of($class, $this->getEntityName());
    }

}
