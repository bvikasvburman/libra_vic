<?php
namespace ColorMasterBundle\Repository;

use AppBundle\Component\AbstractRepository;

class ColorMasterRepository extends AbstractRepository
{
    public function getAlias() {
        return 'cm';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('cm')
              ->select('cm, uc, um')
              ->leftjoin('cm.userCreated', 'uc')
              ->leftjoin('cm.userModified', 'um');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, 'cm.id', $id);
        }

        return $qb;
    }

}