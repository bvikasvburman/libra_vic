'use strict';
var fs         = require('fs');
var gulp       = require('gulp');
var livereload = require('gulp-livereload');

fs.readdirSync(__dirname + '/gulp').forEach(function (module) {
    require(__dirname + '/gulp/' + module);
});

//gulp.task('build', ['rev']);
gulp.task('default', ['js-vendors', 'js', 'sass', 'css-vendors']);


// Watch Files For Changes
gulp.task('watch', function() {
    livereload.listen();
    var jsWatcher = gulp.watch(['./web/js/**/*.js', '!./web/js/app.js'], ['js']);
    gulp.watch(['./web/js/app.js'], livereload.changed);

    // jsWatcher.on('change', function (event) {
    //    console.log('Event path: ' + event.path); // The path of the modified file
    //    // livereload.changed(event.path);
    // });

    // gulp.watch(['./web/js/app.js'], function (event) {
    //    console.log('Event type: ' + event.type); // added, changed, or deleted
    //    console.log('Event path: ' + event.path); // The path of the modified file
    // })

    gulp.watch(['./web/css/bundles/**/*.scss'], ['sass']);
    gulp.watch(['./src/**/*.twig', './app/**/*.twig'], livereload.reload);
    gulp.watch(['./src/**/*.php', './app/*.php', './app/Resources/*.php'], livereload.reload);
});
