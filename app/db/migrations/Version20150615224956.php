<?php

namespace Db\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Table: ma_staff
 * Drop columns: protected, team, developer.
 */
class Version20150615224956 extends AbstractMigration
{
    /**
     * Remove columns: protected, team, developer
     * 
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable('ma_staff');
        $table->dropColumn('protected');
        $table->dropColumn('team');
        $table->dropColumn('developer');
    }

    /**
     * Add columns: protected, team, developer
     * 
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $table = $schema->getTable('ma_staff');
        $table->addColumn('protected', 'integer', array('length' => 11));
        $table->addColumn('team', 'string', array('length' => 50));
        $table->addColumn('developer', 'smallint', array('length' => 4));
    }
}
