<?php

namespace Db\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Table: ma_staff
 * Column: pass_word
 * Change pass_word length from 20 to 60 to store bcrypt hash. 
 */
class Version20150615232137 extends AbstractMigration
{
    /**
     * Set pass_word column length to 60.
     * 
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable('ma_staff');
        $table->getColumn('pass_word')->setLength('60');
    }

    /**
     * Set pass_word column length to 20.
     * 
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $table = $schema->getTable('ma_staff');
        $table->getColumn('pass_word')->setLength('20');
    }
}
