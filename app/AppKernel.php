<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function init() {
        date_default_timezone_set('Asia/Hong_Kong');
        parent::init();
    }

    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            // new SGK\BarcodeBundle\SGKBarcodeBundle(),
            new Avalanche\Bundle\ImagineBundle\AvalancheImagineBundle(),
            new Mopa\Bundle\BarcodeBundle\MopaBarcodeBundle(),

            new AppBundle\AppBundle(),
            new AuthBundle\AuthBundle(),

            //----- Masters -----//
            new ColorMasterBundle\ColorMasterBundle(),
            new PartsLocationBundle\PartsLocationBundle(),
            new ApprovedMaterialBundle\ApprovedMaterialBundle(),
            new ApprovedComponentBundle\ApprovedComponentBundle(),
            new ApprovedPackagingBundle\ApprovedPackagingBundle(),
            new MasterPerformanceBundle\MasterPerformanceBundle(),
            new MasterComplianceBundle\MasterComplianceBundle(),
            new MasterChecklistBundle\MasterChecklistBundle(),
            new ProductCodingBundle\ProductCodingBundle(),

            //----- ERM -----//
            new EnquiryBundle\EnquiryBundle(),
            new QuotationBundle\QuotationBundle(),
            new TradingOrderBundle\TradingOrderBundle(),
            new SalesConfirmationBundle\SalesConfirmationBundle(),
            new PurchaseOrderBundle\PurchaseOrderBundle(),
            new ManufacturingOrderBundle\ManufacturingOrderBundle(),
            new InvoiceBundle\InvoiceBundle(),
            new VendorInvoiceBundle\VendorInvoiceBundle(),
            new AccountsReceivableBundle\AccountsReceivableBundle(),
            new AccountsPayableBundle\AccountsPayableBundle(),
            new ClaimBundle\ClaimBundle(),
            new CreditDebitNoteBundle\CreditDebitNoteBundle(),
            new MessageBundle\MessageBundle(),

            //----- PLM -----//
            new ProtoTypeBundle\ProtoTypeBundle(),

            //----- CRM -----//
            new CompanyBundle\CompanyBundle(),
            new BranchBundle\BranchBundle(),
            new ContactBundle\ContactBundle(),

            //----- Admin -----//
            new SettingBundle\SettingBundle(),
            new TranslationBundle\TranslationBundle(),
            new ValuelistBundle\ValuelistBundle(),
            new RoleBundle\RoleBundle(),
            new UserBundle\UserBundle(),

            //----- Others -----//
            new MediaBundle\MediaBundle(),

            new ResearchBundle\ResearchBundle(),
            new InterestGroupBundle\InterestGroupBundle(),
            new ActivityBundle\ActivityBundle(),
            new NoteBundle\NoteBundle(),
            /* [NewBundleDefPlaceHolder] */
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            // $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            // $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            // $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            // $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
