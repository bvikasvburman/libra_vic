<?php

namespace <BundleName>Bundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class <BundleName>Bundle extends Bundle
{
}
