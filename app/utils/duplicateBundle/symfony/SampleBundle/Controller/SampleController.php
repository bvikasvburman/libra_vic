<?php
namespace <BundleName>Bundle\Controller;

use AppBundle\Component\AbstractController;

class <BundleName>Controller extends AbstractController
{
}
