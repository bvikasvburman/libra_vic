<?php
namespace <BundleName>Bundle\Repository;

use AppBundle\Component\AbstractRepository;

class <BundleName>Repository extends AbstractRepository
{
    public function getAlias() {
        return '<EntityAlias>';
    }

    public function getQB($id = null, $sVars = null)
    {
        $dbUtil = $this->container->get('app.dbutil');

        $qb = $this->createQueryBuilder('<EntityAlias>')
              ->select('<EntityAlias>');

        if ($id) {
            $qb = $dbUtil->setWhere($qb, '<EntityAlias>.id', $id);
        }
        return $qb;
    }
}
