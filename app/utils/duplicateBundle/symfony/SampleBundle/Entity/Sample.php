<?php

namespace <BundleName>Bundle\Entity;

class <BundleName>
{
    public function setId($id) {
        $this->id = $id;
        return $this;
    }
}
