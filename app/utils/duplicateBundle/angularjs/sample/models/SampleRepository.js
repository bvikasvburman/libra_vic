angular.module('<BundleNameCC>Bundle').factory('<BundleName>Repository', ['BaseRepository', '<BundleName>',
    function (BaseRepository, <BundleName>) {

    function <BundleName>Repository(data) {
        // inherit BaseRepository constructor
        BaseRepository.constructor.call(this, <BundleName>, data);
        // set api url
        this.setApiURL(Router.getRoutePath('api_<BundleNameCC>', true));
    };

    BaseRepository.apply(<BundleName>Repository);

    return new <BundleName>Repository();
}]);