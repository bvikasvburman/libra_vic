'use strict';

angular.module('<BundleNameCC>Bundle').factory('<BundleName>', ['BaseEntity',
    function (BaseEntity) {
    function <BundleName>(data) {
        // inherit BaseEntity constructor
        BaseEntity.constructor.call(this, data);

        this.add('id', 'number');
<entityFields>
    };

    BaseEntity.apply(<BundleName>);

    return <BundleName>;
}]);

