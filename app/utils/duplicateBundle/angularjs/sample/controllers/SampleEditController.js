'use strict';

angular.module('<BundleNameCC>Bundle').controller('<BundleName>EditController', [
    '$scope',
    '$rootScope',
    'row',
<linksDataArrDepInj1InController>
    '<BundleName>Repository',
    '<BundleNameCC>Config',
    'UtilService',
    function <BundleName>EditController(
        $scope,
        $rootScope,
        row,
<linksDataArrDepInj2InController>
        <BundleName>Repository,
        <BundleNameCC>Config,
        UtilService
    ) {
        UtilService.initializeCommonControllerVars($scope, row);

        //set the links array here
<linksDataArrInRowArray>
    }
]);