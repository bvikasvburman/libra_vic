'use strict';

angular.module('<BundleNameCC>Bundle').controller('<BundleName>ListController', [
    '$scope',
    'rows',
    function <BundleName>ListController(
        $scope,
        rows
    ) {
        $scope.rows = rows;
    }
]);