angular.module('<BundleNameCC>Bundle').factory('<BundleNameCC>Config', [
  'BundleDefConfig',
  'ListService',
  'ListFldService',
  'UtilService',
  'LinkService',
  'LinkFldService',
  'TabService',
    function(
        BundleDefConfig,
        ListService,
        ListFldService,
        UtilService,
        LinkService,
        LinkFldService,
        TabService
    ) {
        var lf = ListFldService;
        var lnkFld = LinkFldService;

        var listFlds = [
<listFldsArray>
        ];

        var links = [
<linksArray>
        ];

        var tabs = [
            new TabService('main'),
        ];
        tabs = new TabService().addLinksToTab(tabs, links);

        return new BundleDefConfig('<BundleNameCC>', {
           listObj: new ListService(UtilService.getCurrBundle(), listFlds),
           links: links,
           tabs: tabs,
        })
   }]
);