var getLOFR = Router.getLinkObjForRoute;
var searchLOFR = Router.searchLinkObjForRoute;
var linksForAutoRouteGen = [
<linksArrayInRouting>
];

Router.addRoutes([
    /* api */
    {name: 'api_<BundleNameCC>', url: 'api/<BundleNameCC>'},

    Router.getRouteFromBundle('<BundleNameCC>'),
    Router.getRouteFromBundle('<BundleNameCC>.list'),
    Router.getRouteFromBundle('<BundleNameCC>.detail', {links: linksArrayInRouting}),
    Router.getRouteFromBundle('<BundleNameCC>.new', {links: linksArrayInRouting}),
    Router.getRouteFromBundle('<BundleNameCC>.edit', {links: linksArrayInRouting}),

    Router.getRouteFromBundle('<BundleNameCC>.detail.main'),
    Router.getRouteFromBundle('<BundleNameCC>.new.main'),
    Router.getRouteFromBundle('<BundleNameCC>.edit.main'),
]);

Router.createRoutesForLinks('<BundleNameCC>', linksArrayInRouting);