app/console duplicateBundle SalesConfirmation "{
    'entities': [
        {'name': 'code', 'type': 'string', 'length': 30},
        {'name': 'companyId', 'type': 'integer'},
        {'name': 'contactId', 'type': 'integer'},
        {'name': 'enquiryId', 'type': 'integer'},
        {'name': 'tradingOrderId', 'type': 'integer'},
        {'name': 'specification', 'type': 'string', 'length': 600},
        {'name': 'clientOrderRef', 'type': 'string', 'length': 100},
        {'name': 'deliveryTerms', 'type': 'string', 'length': 150},
        {'name': 'paymentTerms', 'type': 'string', 'length': 150},
        {'name': 'expectedDateDeparture', 'type': 'date'},
        {'name': 'expectedDateArrival', 'type': 'date'},
        {'name': 'shippingMedium', 'type': 'string', 'length': 50},
        {'name': 'portOfDelivery', 'type': 'string', 'length': 100},
        {'name': 'finalDestination', 'type': 'string', 'length': 100},
        {'name': 'warrantyAndClaims', 'type': 'string', 'length': 200},
        {'name': 'notes', 'type': 'string', 'length': 300},
        {'name': 'status', 'type': 'string', 'length': 40},
        {'name': 'flag', 'type': 'integer'},
        {'name': 'userIdCreated', 'type': 'integer'},
        {'name': 'userIdModified', 'type': 'integer'},
        {'name': 'creationDate', 'type': 'datetime'},
        {'name': 'modificationDate', 'type': 'datetime'}
    ],

    'listFlds': [
        {'name': 'code', 'title': 'S/C Code'},
        {'name': 'tradingOrder.code'},
        {'name': 'company.code'},
        {'name': 'company.name'},
        {'name': 'status'},
        {'name': 'expectedDateDeparture'},
        {'name': 'creationDate'}
    ],

    'detailFlds': {
        'keyDetail': [
            {'name': 'code', 'label': 'S/C Code', 'flex': 10},
            {'name': 'tradingOrder.code', 'label': 'Order Code', 'flex': 10},
            {'name': 'company.code', 'label': 'Client Name', 'flex': 30},
            {'name': 'contact.name', 'label': 'Contact Name', 'flex': 30}
        ],

        'main': [
            {'name': 'specification'},
            {'name': 'enquiry.code', 'label': 'Enquiry Code'},
            {'name': 'clientOrderRef'},
            {'name': 'deliveryTerms'},
            {'name': 'paymentTerms'},
            {'name': 'expectedDateDeparture'},
            {'name': 'expectedDateArrival'},
            {'name': 'shippingMedium'},
            {'name': 'portOfDelivery'},
            {'name': 'finalDestination'},
            {'name': 'warrantyAndClaims'},
            {'name': 'notes'},
            {'name': 'status'}
        ]
    },

    'links': [
        {'name': 'attachment'},
        {'name': 'note'},
        {'name': 'messaging'},
        {'name': 'task'}
    ]
}"

