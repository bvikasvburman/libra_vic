/.git
/app/cache
/app/config/parameters_local.yml
/app/logs
/app/reports
/app/db
/app/utils
/bin
/gulp
/node_modules
/composer.json
/composer.lock
/gulpfile.js
/npm-debug.log
/opts-
/README.md
/.gitignore
/web/css/bundles
/web/css/vendors
/web/js/tests
/web/js/vendors
/web/js/app/bundles
/web/js/app/components
/web/js/app/configs
/web/js/bower.json
/web/js/karma.conf.js
/web/js/.bowerrc




